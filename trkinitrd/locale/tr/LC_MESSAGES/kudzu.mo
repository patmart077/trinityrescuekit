��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  e  �  @   �     +     7     H     Z     o     �     �     �  #   �  $   �  %   �     #	  
   4	     ?	     S	     Z	     s	     y	     �	  
   �	     �	  0   �	  ,   �	  8   �	     2
  !   K
  #   m
  
   �
  2   �
  *   �
  
   �
                 5     V  $   h     �     �               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: tr
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-03-15 23:26+0200
Last-Translator: Bahadir Yagan <bahadir.yagan@mentorsystem.com>
Language-Team: Turkish <fedora-trans-tr@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
 
HATA - kudzu'yu çalıştırabilmek için root olmalısınız.
 %s aygıtı CD-ROM sürücü IDE denetleyicisi IEEE1394 denetleyici PCMCIA/Cardbus sürücü RAID sürücü SCSI sürücü USB sürücü %s için %s takma adı oluşturuldu sadece güvenli donanım tanıma yap Donanım bilgisinin okunacağı dosya disket sürücü sabit disk çekirdek sürümü klavye %s'ten %s'e bağ kuruldu modem monitör fare ağ kartı yazıcı sadece belirtilen `sınıf' için tanıma uygula sadece belirtilen `bus' için tanıma uygula sadece tanıma uygula, bilgileri standart çıktıya yaz loadkeys %s çalıştır %s için mouseconfig çalıştır Tanınan donanımları dosyadan oku tarayıcı belli bir çekirdek sürümünün modüllerini ara Zamanaşımını saniye cinsinden tanımla ses kartı Teyp sürücü aep1000 hizmeti etkinleştirildi bcm5820 hizmeti etkinleştirildi %s bağı silindi %s bağı silindi (%s'e bağlıydı) ekran kartı görüntü yakalama kartı 