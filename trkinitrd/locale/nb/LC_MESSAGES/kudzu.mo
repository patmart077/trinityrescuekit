��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  L  �  /   �          
          '     ;     U     e     u     �  7   �  ,   �     �     	     	     '	     0	     A	     G	     N	     R	     `	  #   h	     �	  )   �	     �	     �	      
     &
  4   .
     c
     
     �
     �
     �
     �
  $   �
  
   �
     	               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu 1.48
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-03-07 09:19+0100
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team: Norwegian <i18n-nb@lister.ping.uio.no>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8-bit
 
FEIL - Du må være root for å kjøre kudzu.
 %s enhet CDROM-spiller IDE-kontroller IEEE1394-kontroller PCMCIA/Cardbus-kontroller RAID-kontroller SCSI-kontroller USB-kontroller laget alias %s til %s kjør kun 'trygge' søk som ikke forstyrrer maskinvaren fil som maskinvareinformasjon skal leses fra diskettstasjon harddisk kjerneversjoner tastatur lenket %s til %s modem skjerm mus nettverkskort skriver søk kun etter spesifisert 'klasse' søk kun på spesifisert 'buss' kun søk, skriv ut informasjon til stdout kjørte loadkeys %s kjørte mouseconfig for %s les testet maskinvare fra en fil scanner søk etter moduler for en bestemt versjon av kjernen sett tidsavbrudd i sekunder lydkort båndstasjon slo på aep1000-tjenesten Slo på bcm5820-tjenesten fjernet lenke %s fjernet lenke %s (var lenket til %s) skjermkort videoredigeringskort 