��    *      l  ;   �      �  (   �  	   �     �     �     �          &     6     F     U  0   f  1   �     �     �  	   �                     (     .     6     <     I  $   Q     v  '   �     �     �      �  "        (  2   0     c  
   z  
   �     �     �     �     �     �     �  �    �   �     J	     `	     z	  %   �	  ;   �	      �	      
     >
  I   ^
  �   �
  �   0  v   �  "   i     �  +   �     �  J   �     9     I     Y  +   f     �  y   �  g   (  z   �  .     E   :  a   �  j   �     M  �   i  Q     "   _     �  P   �  P   �  O   A  �   �  1     8   L        "          
          $                    	       *   &              '         %                #                               )   (                             !                                      
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do configuration that doesn't require user input do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
POT-Creation-Date: 2005-03-03 18:15-0500
PO-Revision-Date: 2005-04-19 21:08+0600
Last-Translator: Samia Niamatullah <mailsamia2001@yahoo.com>
Language-Team: Bengali <redhat-translation@bengalinux.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.3.1
Plural-Forms: Plural-Forms: nplurals=2; plural=(n != 1);
 
সমস্যা - Kudzu চালাতে হলে আপনাকে root পরিচয় ব্যবহার করতে হবে।
 %s ডিভাইস CD-ROM ড্রাইভ IDE নিয়ন্ত্রক IEEE 1394 নিয়ন্ত্রক PCMCIA/কার্ডবাস নিয়ন্ত্রক RAID নিয়ন্ত্রক SCSI নিয়ন্ত্রক USB নিয়ন্ত্রক %s কে %s উপনাম প্রদান করা হয়েছে ব্যবহারকারীর ইনপুট এর সাহায্য ছাড়া কনফিগারেশন করো হার্ডওয়্যারের কার্যক্রম বিঘ্নিত না করে শুধুমাত্র 'নিরাপদ' অনুসন্ধান করো যে ফাইল থেকে হার্ডওয়ের সম্বন্ধে তথ্য পড়া হবে ফ্লপি ড্রাইভ হার্ড ডিস্ক কার্ণেল সংস্করণ কীবোর্ড %s কে %s এর সাথে সংযোগ করা হয়েছে মোডেম মনিটর মাউস নেটওয়ার্ক কার্ড প্রিন্টার কেবলমাত্র নির্ধারিত 'class'-এর জন্য অনুসন্ধান করো কেবলমাত্র নির্ধারিত 'bus'-এ অনুসন্ধান করো কেবলমাত্র অনুসন্ধান করো, stdout-এ তথ্য প্রিন্ট করো %s loadkeys চালানো হয়েছে %s এর জন্য mouseconfig চালানো হয়েছে ফাইল থেকে সনাক্তকৃত হার্ডওয়্যার পড়ো সকেট থেকে অনুসন্ধানকৃত হার্ডওয়্যার পড়ো স্ক্যানার একটি নির্ধারিত সংস্করণের কার্ণেলের জন্য মডিউল অনুসন্ধান করো সময়সীমা সেকেন্ডে নির্ধারণ করো সাউন্ড কার্ড টেপ ড্রাইভ aep1000 পরিসেবাটি সক্রিয় করা হয়েছে bcm5820 পরিসেবাটি সক্রিয় করা হয়েছে %s এর সংযোগ বিচ্ছিন্ন করা হয়েছে %s এর সংযোগ বিচ্ছিন্ন করা হয়েছে (%s এর সাথে সংযুক্ত ছিল) ভিডিও অ্যাডাপ্টার ভিডিও ক্যাপচার কার্ড 