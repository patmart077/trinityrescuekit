��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  ;  �  >        J     X  
   h     s     �     �     �  
   �     �  ;   �  5   	     B	     V	     b	     o	     {	     �	     �	     �	     �	     �	      �	  "   �	  >   �	     >
     T
  +   q
  ,   �
     �
  /   �
  %        '     6     I     b     {  !   �     �     �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-01-04 23:02+0100
Last-Translator: Marcel Telka <marcel@telka.sk>
Language-Team: Slovak <sk-i18n@lists.linux.sk>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
CHYBA - Musíte byť správca, aby ste mohli spustiť kudzu.
 zariadenie %s jednotka CD-ROM IDE radič IEEE1394 radič PCMCIA/Cardbus radič RAID radič SCSI radič USB radič prezývka %s ako %s urobiť len 'bezpečné' testy, ktoré neovplyvnia hardvér súbor, z ktorého načítať informácie o hardvéri disketová jednotka pevný disk verzia jadra klávesnica spojené %s s %s modem monitor myš sieťová karta tlačiareň vyskúšať len zadanú 'triedu' vyskúšať len zadanú 'zbernicu' len vyskúšať, vypísať informácie na štandardný výstup spustený loadkeys %s spustený mouseconfig pre %s načítať vyskúšaný hardvér zo súboru načítať vyskúšaný hardvér zo zásuvky skener vyhľadať moduly pre príslušnú verziu jadra nastaviť časový limit v sekundách zvuková karta pásková jednotka zapnutá služba aep1000 zapnutá služba bcm5820 odpojené %s odpojené %s (bolo spojené s %s) grafický adaptér karta pre zachytávanie videa 