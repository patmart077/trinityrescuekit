��    K      t  e   �      `     a     x     �     �  
   �     �     �     �     �     �     �     �  
   �     �     �               (     =     Q     `     t     �     �     �     �     �     �     �            
        !     -     9     P     ]     i     w     �     �     �     �     �     �     �     �     �     �     �     �     �     �      	     	     	     (	     >	     R	  
   _	     j	     z	     �	     �	     �	  
   �	     �	  '   �	     �	  4   
     F
     M
     Q
     _
  0  k
     �     �  
   �     �     �     �     �          
               $  
   ,     7     @     L     _     v     �     �     �     �     �     �     	          %     8     I     g     t     �     �     �     �     �     �     �     �               $     +     1     7     J     Q     W     _     f     r     �     �     �     �     �     �                )     5     C     U     g     w     {     �  2   �     �  9   �     !     &     *  
   9        ;   <               &          :             5   (   4           1       3      0                   8   -              +               G          *      	   )       !   6       /   "   ?       @   B   %               D   I   A         C          F   J      2       '   H   K   #   7   .           >   
      9      =                           E              $      ,    %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID Max 	Power Drain OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status USB USB Bandwidth USB Version Project-Id-Version: hal TBD
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-07-20 10:21+0200
PO-Revision-Date: 2005-08-22 18:50+0200
Last-Translator: Piotr Bolek <pb@7bulls.com>
Language-Team: Polish <i18n@suse.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Zewnętrzny dysk twardy %s Dysk twardy %s Nośnik %s Nośnik wymienialny %s Napęd %s%s /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Szybkość:</b> <b>Typ magistrali:</b> <b>Możliwości:</b> <b>Typ urządzenia:</b> <b>Urządzenie:</b> <b>Produkt OEM:</b> <b>Dostawca OEM:</b> <b>Zużycie energii:</b> <b>Produkt:</b> <b>Wersja:</b> <b>Stan:</b> <b>Wersja USB:</b> <b>Dostawca:</b> Dodaj pozycję do pliku fstab Zaawansowane CD muzyczne Wyczyść CD-R Wyczyść CD-RW Wyczyść DVD+R Wyczyść DVD+R dwuwarstwowe Wyczyść DVD+RW Wyczyść DVD-R Wyczyść DVD-RAM Wyczyść DVD-RW Typ magistrali CD-R CD-ROM CD-RW DVD+R DVD+R dwuwarstwowe DVD+RW DVD-R DVD-RAM DVD-RW Urządzenie Zarządzanie urządzeniami Nazwa urządzenia Typ urządzenia Dostawca urządzenia Napęd Napęd zewnętrzny %s%s Zewnętrzny napęd dyskietek Zewnętrzny dysk twardy Napęd dyskietek Dysk twardy ID producenta Maks 	ubytek mocy ID producenta OEM ID produktu OEM PCI ID produktu Wersja produktu Usuń wszystkie wygenerowane pozycje z pliku fstab Usuń pozycję z pliku fstab Podawaj szczegółową informację o postępie działania Stan USB Szynkość USB Wersja USB 