��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  G  �  Q   �          ,     >     N     c     ~     �     �     �  5   �  !   �     	     -	     ;	     J	     V	     g	  
   o	     z	     �	     �	  "   �	      �	  )   �	     
     !
  *   8
     c
  ,   l
  '   �
     �
     �
     �
     �
           "     C     S               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: lt
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-05-17 10:48+0300
Last-Translator: Robertas Dereškevičius <roberto@mikrovisata.net>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3.1
 
KLAIDA: Tu turi turėti supervartotojo(root) teises, norint paleisti programą.
 %s įrenginys CD-ROM įrenginys IDE kontroleris IEEE1394 kontroleris PCMCIA/Cardbus kontroleris RAID kontroleris SCSI kontroleris USB kontroleris susiejo %s kaip %s atlikti tik 'saugius' bandymus, nesugadinant įrangos byla nuskaityti įrangos info iš floppy įrenginys kietas diskas kernel versija klaviatūra susieta %s su %s modemas monitorius pelė tinklo plokštė spausdintuvas bandyti tiktai pasirinktą 'class' bandyti tiktai pasirinktą 'bus' bandyti tiktai, spausdinti info į stdout paleido loadkeys %s paleido mouseconfig %s nuskaitytį išbandytą įrangą iš bylos skaneris ieškoti modulių atskiroms kernel versijoms įveskite laiko išsekimą sekundėmis. garso korta juostinis įrenginys įjungė aep1000 servisą įjungė bcm5820 servisą išsieta %s išsieta %s (buvo susieta su %s) video adapteris video įrašymo korta 