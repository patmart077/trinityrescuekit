��    *      l  ;   �      �  (   �  	   �     �     �     �          &     6     F     U  0   f  1   �     �     �  	   �                     (     .     6     <     I  $   Q     v  '   �     �     �      �  "        (  2   0     c  
   z  
   �     �     �     �     �     �     �  �    �   �     .	  '   >	     f	     �	  3   �	     �	     �	     	
  *   #
  p   N
  �   �
  @   _     �     �     �     �  )        1     >     Q  "   ^     �  U   �  L   �  m   :      �  "   �  ]   �  `   J     �  W   �  \        p     �  +   �  +   �  3   �  Y   3     �  ,   �        "          
          $                    	       *   &              '         %                #                               )   (                             !                                      
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do configuration that doesn't require user input do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: pa
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-04-22 14:40-0400
PO-Revision-Date: 2005-04-25 12:22+0530
Last-Translator: Amanpreet Singh Alam <amanpreetalam@yahoo.com>
Language-Team: Punjabi <punlinux-i18n@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
Plural-Forms: nplurals=2; plural=(n != 1);

 
ਗਲਤੀ - ਤੁਸੀਂ kudzu ਚਲਾਉਣ ਲਈ ਪ੍ਰਬੰਧਕ(root) ਹੋਣੇ ਜਰੂਰੀ ਹੋ।
 %s ਜੰਤਰ ਸੀ-ਡੀ ਰੋਮ ਡਰਾਈਵ IDE ਕੰਟਰੋਲਰ IEEE1394 ਪ੍ਰਬੰਧਕ PCMCIA/ਕਾਡਰ-ਬੱਸ ਕੰਟਰੋਲਰ RAID ਕੰਟਰੋਲਰ SCSI ਪ੍ਰਬੰਧਕ USB ਪ੍ਰਬੰਧਕ %s ਨੂੰ %s ਉਪ-ਨਾਂ ਦਿਓ ਸੰਰਚਨਾ ਕਰੋ, ਜਿਸ ਲਈ ਉਪਭੋਗੀ ਦਖਲ ਦੀ ਲੋੜ ਨਾ ਹੋਵੇ ਸਿਰਫ 'ਸੁਰੱਖਿਅਤ' ਛਾਣਬੀਣ ਕਰੋ ਜਿਸ ਦਾ ਜੰਤਰ ਉੱਪਰ ਪ੍ਰਭਾਵ ਨਹੀਂ ਪਵੇਗਾ ਜੰਤਰ ਜਾਣਕਾਰੀ ਪੜਨ ਲਈ ਫਾਇਲ ਫਲਾਪੀ ਡਰਾਈਵ ਹਾਰਡ ਡਿਸਕ ਕਰਨਲ ਵਰਜ਼ਨ ਕੀਬੋਰਡ %s ਨੂੰ %s ਨਾਲ ਜੋੜਿਆ ਮਾਡਮ ਮਾਨੀਟਰ ਮਾਊਸ ਨੈੱਟਵਰਕ ਕਾਰਡ ਪ੍ਰਿੰਟਰ ਸਿਰਫ ਦਰਸਾਈ 'ਸ਼੍ਰੇਣੀ' ਦੀ ਛਾਣਬੀਣ ਕਰੋ ਸਿਰਫ ਦਰਸਾਈ 'ਬੱਸ' ਦੀ ਛਾਣਬੀਣ ਕਰੋ ਸਿਰਫ ਪ੍ਰਿੰਟ ਜਾਣਕਾਰੀ ਦੀ ਰੁਕਣ ਲਈ ਛਾਣਬੀਣ ਕਰੋ ਲੋਡ-ਕੀ %s ਚਲਾਉ %s ਲਈ mouseconfig ਚਲਾਉ ਫਾਇਲ ਵਿੱਚੋ ਛਾਣਬੀਣ ਕੀਤੇ ਜੰਤਰ ਨੂੰ ਪੜੋ ਸਾਕਟ ਵਿੱਚੋਂ ਛਾਣਬੀਣ ਕੀਤੇ ਜੰਤਰ ਨੂੰ ਪੜੋ ਸਕੈਨਰ ਵੱਖਰੇ ਕਰਨਲ ਵਰਜ਼ਨ ਲਈ ਮੈਡਊਲਾਂ ਦੀ ਭਾਲ ਸਮਾਂ-ਹੱਦ ਸਕਿੰਟਾਂ ਵਿੱਚ ਨਿਰਧਾਰਿਤ ਕਰੋ ਸਾਊਂਡ ਕਾਰਡ ਟੇਪ ਡਰਾਈਵ aep1000 ਸੇਵਾ ਚਾਲੂ ਕਰੋ bcm5820 ਸੇਵਾ ਚਾਲੂ ਕਰੋ %s ਨਾਲ ਸੰਬੰਧ ਖਤਮ ਹੋਇਆ %s ਨਾਲ ਸੰਬੰਧ ਖਤਮ ਹੋਇਆ (%s ਨਾਲ ਜੁੜਿਆ ਸੀ) ਵੀਡਿਓ ਐਡਪਟਰ ਵੀਡੀਓ ਕੈਪਚਰ ਕਾਰਡ 