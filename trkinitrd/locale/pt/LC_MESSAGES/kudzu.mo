��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  h  �  -   8	     f	     u	     �	     �	     �	     �	     �	     �	  *   �	  =   $
  3   b
     �
     �
     �
     �
     �
     �
     �
     �
     �
  
     &     !   5  5   W     �     �  %   �  %   �       :        U     q     ~     �     �     �  !   �     �             !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-17 09:30+0000
Last-Translator: Pedro Morais <morais@kde.org>
Language-Team: pt <morais@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-POFile-SpellExtra: stdout kernel RAID USB IEEE SCSI PCMCIA kudzu Kudzu 
X-POFile-SpellExtra: root Cardbus securetty inittab bcm aep
X-POFile-SpellExtra: Red Hat Linux Inc system config keyboard Tab Alt
X-POFile-SpellExtra: mouseconfig netconfig printconf loadkeys
X-POFile-SpellExtra: digitalizador ecrã ecrãs
 
ERRO - Deve ser root para executar o kudzu.
 dispositivo %s unidade de CD-ROM controlador IDE controlador IEEE1394 controlador PCMCIA/Cardbus controlador RAID controlador SCSI controlador USB %s passa a também poder ser chamado de %s fazer apenas procurar 'seguras' que não perturbam o hardware ficheiro de onde ler informações sobre o hardware unidade de disquetes disco rígido versão do kernel teclado liguei %s a %s modem monitor rato placa de rede impressora procurar apenas pela 'classe' indicado procurar apenas no 'bus' indicado procurar apenas, escrever informações para o stdout executei loadkeys %s executado o mouseconfig em %s ler hardware detectado de um ficheiro ler hardware detectado de um 'socket' digitalizador procurar por módulos de uma versão específica do kernel tempo de espera em segundos placa de som unidade de 'tapes' ligado o serviço aep1000 ligado o serviço bcm5820 desliguei %s desliguei %s (estava ligado a %s) placa de vídeo placa de captura de vídeo 