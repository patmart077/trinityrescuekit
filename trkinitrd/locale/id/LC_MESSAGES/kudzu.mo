��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  j  �  :   �  
   *     5     B     Q     e          �     �     �  B   �  ,   
	     7	  	   D	     N	     [	     d	     w	     }	     �	     �	     �	  .   �	  &   �	  *   �	     $
     9
  *   W
     �
  2   �
     �
  
   �
  
   �
     �
          (  +   ;     g     u               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: id
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-07-13 03:13+0700
Last-Translator: Alexander Agung <redtux@sby.dnet.net.id>
Language-Team: Bahasa Indonesia <viribus@traderweb.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.2
 
SALAH - Anda harus menjadi root untuk menjalankan kudzu.
 piranti %s drive CD-ROM pengontrol IDE pengontrol IEEE1394 pengontrol PCMCIA/Cardbus pengontrol RAID Pengontrol SCSI pengontrol USB %s dialiaskan sebagai %s Hanya lakukan pengujian 'safe' yang tidak mengganggu piranti keras berkas untuk membaca info piranti keras dari floppy drive hard disk versi kernel keyboard terhubung %s ke %s modem monitor mouse kartu jaringan pencetak hanya pengujian untuk 'class' (jenis) tertentu hanya pengujian 'bus' (jenis) tertentu hanya pengujian, cetak informasi ke stdout jalankan loadkeys %s jalankan mouseconfig untuk %s baca piranti keras yang dicoba dari berkas scanner / pemindai mencari modul-modul untuk versi kernel yang khusus set timeout dalam detik sound card tape drive hidupkan layanan aep1000 hidupkan layanan bcm5820 %s tidak terhubung %s tidak terhubung (pernah terhubung ke %s) adapter video video capture card 