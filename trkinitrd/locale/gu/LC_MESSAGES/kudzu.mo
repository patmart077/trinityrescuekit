��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  �  �  f   ^     �     �     �  !   	  5   1	     g	     �	     �	  C   �	  �   
  x   �
  %        B  %   b     �  *   �     �     �     �  %   �       e   8  O   �  Q   �  9   @  A   z  ]   �  `        {  u   �  ]        b     �  >   �  >   �        Q   >  (   �  5   �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: gu
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-17 09:42+0530
Last-Translator: Ankit Patel <ankit@redhat.com>
Language-Team: Gujarati <gu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.9.1
Plural-Forms: Plural-Forms: Plural-Forms: nplurals=2; plural=(n!=1);


 
ભૂલ - kudzu ચલાવવા માટે તમે રુટ હોવા જ જોઈએ.
 %s ઉપકરણ CD-ROM ડ્રાઈવ IDE નિયંત્રક IEEE1394 નિયંત્રક PCMCIA/કાર્ડબસ નિયંત્રક RAID નિયંત્રક SCSI નિયંત્રક USB નિયંત્રક %s એ %s તરીકે ઉપનામ અપાયેલ છે માત્ર 'સુરક્ષિત' ચકાસણીઓ કરો કે જે હાર્ડવેરને હેરાન નહિં કરે ફાઈલ કે જેમાંથી હાર્ડવેર જાણકારી વાંચવાની છે ફ્લોપી ડ્રાઈવ હાર્ડ ડિસ્ક કર્નલ આવૃત્તિ કીબોર્ડ %s ને %s કડી થયેલ છે મોડેમ મોનિટર માઉસ નેટવર્ક કાર્ડ પ્રિન્ટર માત્ર સ્પષ્ટ કરેલ 'વર્ગ' માટે પ્રોબ કરો માત્ર સ્પષ્ટ કરેલા 'bus' ને ચકાસો માત્ર ચકાસો, stdout ને છાપન જાણકારી કી લાવો %s ચલાવાયેલ હતી %s માટે mouseconfig ચલાવાયેલ હતું ફાઈલમાંથી ચકાસાયેલ હાર્ડવેર વાંચો સોકેટમાંથી ચકાસાયેલ હાર્ડવેર વાંચો સ્કેનર કોઈ ચોક્કસ કર્નલ આવૃત્તિ માટે મોડ્યુલો શોધો સમયસમાપ્તિ સેકન્ડોમાં સુયોજિત કરો ધ્વનિ કાર્ડ ટેપ ડ્રાઈવ aep1000 સેવા ચાલુ કરાયેલ હતી bcm5820 સેવા ચાલુ કરાયેલ હતી %s કડી થયેલ છે %s કડી દૂર થયેલ (કે જે %s ની કડી હતી) વિડીયો એડેપ્ટર વિડીયો કેપ્ચર કાર્ડ 