��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  t  �  T   D     �     �     �     �  !    	     "	     :	     R	  *   i	  �   �	  `   
  %   ~
     �
     �
     �
     �
  
          
   "     -     G  /   V  -   �  L   �       $     9   C  ?   }     �  Y   �  A   $     f  #   �  ,   �  ,   �     �  8        K  4   e        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: bg
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-03-05 00:03+0200
Last-Translator: Doncho N. Gunchev <mr700@globalnet.bg>
Language-Team: Bulgarian <Bulgarian translators' team <dict@fsa-bg.org>>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.9.1
 
ГРЕШКА - Трябва да сте root за да стартирате kudzu.
 %s устройство CD-ROM устройство IDE контролер IEEE1394 контролер PCMCIA/Cardbus контролер RAID контролер SCSI контролер USB контролер представяне на %s като %s извършва само 'безопасни' проби които няма да нарушат работата на хардуера файл от който да се прочете информацията за хардуера дискетно устройство твърд диск версия на ядрото клавиатура свързан %s към %s модем монитор мишка мрежова карта принтер проба само на посочен 'class' проба само на посочен 'bus' само проба, извеждане информацията на stdout изпълнен loadkeys %s изпълнен mouseconfig за %s чете за пробван хардуер от файл четене за пробван хардуер от сокет скенер търсене на модули за определена версия на ядрото задава време за изчакване в секунди звукова карта лентово устройство включена е услугата aep1000 включена е услугата bcm5820 разкачен %s разкачен %s (бил е свързан към %s) видео адаптор карта за прехващане на видео 