��    *      l  ;   �      �  (   �  	   �     �     �     �          &     6     F     U  0   f  1   �     �     �  	   �                     (     .     6     <     I  $   Q     v  '   �     �     �      �  "        (  2   0     c  
   z  
   �     �     �     �     �     �     �  a    2   r  	   �  	   �     �     �     �     �     	     	     	  -   6	  .   d	  !   �	  	   �	     �	     �	     �	     �	  	   �	     �	     �	  	   
  	   
     
     3
  +   R
     ~
     �
  !   �
  )   �
  	   �
  '        )  	   B  	   L     V     l     �     �  	   �     �        "          
          $                    	       *   &              '         %                #                               )   (                             !                                      
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do configuration that doesn't require user input do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: zh_TW
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-04-22 14:40-0400
PO-Revision-Date: 2005-03-03 09:38-0800
Last-Translator: Ben Wu <hpwu@redhat.com>
Language-Team: Traditional Chinese <fedora-trans-zh_tw@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
 
錯誤 - 您必須是 root 才能執行 kudzu。
 %s 裝置 光碟機 IDE 控制卡 IEEE1394 控制卡 PCMCIA/Cardbus 控制器 RAID 控制器 SCSI 控制卡 USB 控制器 設 %s 的別名為 %s 執行不需要使用者輸入的組態設定 只做 '安全的' 偵測，不會擾亂硬體 用來讀取硬體資料的檔案 軟碟機 硬碟 核心版本 鍵盤 連結 %s 至 %s 數據機 螢幕 滑鼠 網路卡 印表機 只偵測指定的 '類別' 只偵測指定的 '匯流排' 只做偵測，印出相關訊息到 stdout 執行載入鍵 %s 為 %s 執行 mouseconfig 從檔案讀取偵測到的硬體 從某個 socket 讀取偵測到的硬體 掃描器 搜尋用於特定核心版本的模組 以秒設定逾時時間 音效卡 磁帶機 啟動 aep1000 服務 啟動 bcm5820 服務 解除連結 %s 解除連結 %s (連結到 %s) 顯示卡 影像擷取卡 