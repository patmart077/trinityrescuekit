��    c      4  �   L      p     q     �     �  F   �  D   	  #   T	  !   x	  H   �	     �	  *    
  +   +
     W
  *   v
  &   �
     �
     �
            "   )  ;   L  +   �  +   �  v   �  &   W  #   ~  (   �     �  R   �     =     T     b     k  
   ~     �     �     �     �     �     �     �               /     @     O     c     r     �     �  
   �     �     �     �     �     �     �     �                         $     *     ;     B     H     P     X     _     f     u     �     �     �     �     �     �     �  
   �     �               ,     ;  
   ?     J  '   [     �  4   �     �     �     �     �     �     �       "   )     L  �  j            #   :  b   ^  S   �  ,     !   B  r   d  $   �  0   �  5   -  !   c  ;   �  ,   �  "   �  &        8     O  $   d  J   �  C   �  +     w   D  (   �  #   �  <   	  #   F  S   j     �     �  
   �     �               &     :     L     h     |     �     �     �     �     �     �               :     C     O     [     h     u     �     �     �     �     �     �     �     �     �     �                               $     0     N     e     y     �     �     �     �     �  
   �     �          ,     B     V     Z     j  .   �     �  H   �               "     &     9     F  '   ^  .   �  '   �     ]                 1              .   6      c       ?   b                  !         7   8   
      _   \       ;      &   ^           T      4   5   +   Z          J              O      %   X   F      =       U       '                   0          R   `   E   Y                        	       [          V   S   D   a          G   $   L   "          I   #             A   3   Q   2          M   B   @   <   ,      9   :           C   (   N   *   -           K   W       H               /   >   P      )              %d: %s entering; %s udi=%s
 %d: %s exiting; %s udi=%s
 %d: Acquiring advisory lock on  %d: Could not add entry to fstab file: block device %s already listed
 %d: Could not open temporary file for writing in directory '%s': %s
 %d: Could not parse data from '%s'
 %d: Could not read from '%s': %s
 %d: Could not remove device '%s' with UDI '%s' from fs table: not found
 %d: Could not stat '%s': %s
 %d: Could not write to temporary file: %s
 %d: Error acquiring lock '%s'; bailing out
 %d: Error releasing lock '%s'
 %d: Failed to remove mount point '%s': %s
 %d: Failed to rename '%s' to '%s': %s
 %d: Found %s pointing to %s in %d: Line ended prematurely
 %d: Lock acquired
 %d: Lock released
 %d: Releasing advisory lock on %s
 %d: Seeing if line for dev='%s',mnt='%s' should be removed
 %d: added mount point '%s' for device '%s'
 %d: couldn't open %s O_RDONLY; bailing out
 %d: drive: desired_mount_point='%s', fstype='%s', options='%s', mount_point='%s', normalized_desired_mount_point='%s'
 %d: entering add_hal_device, udi='%s'
 %d: is_generated=%d, is_mounted=%d
 %d: removed mount point for device '%s'
 %d: using temporary file '%s'
 %d: volume: desired_mount_point='%s', fstype='%s', options='%s', mount_point='%s'
 %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM  CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-ROM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID Max 	Power Drain OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status UDI USB USB Bandwidth USB Version _Virtual Devices added mount point %s for %s removed all generated mount points removed mount point %s for %s Project-Id-Version: es
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-10-21 09:18+0200
PO-Revision-Date: 2005-10-21 10:18+0200
Last-Translator: Francisco Javier Fernandez <serrador@hispafuentes.com>
Language-Team: Spanish <l10n@hispafuentes.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms:  nplurals=2; plural=(n != 1);
 %d: %s entrando; %s udi=%s
 %d: %s saliendo; %s udi=%s
 %d: Adquiriendo aviso de bloqueo en %d: No se pudo añadir la entrada al archivo fstab: el dispositivo de bloques %s ya está listado
 %d: No se pudo abrir el archivo temporal para escribir en el directorio «%s»: %s
 %d: No se pudo analizar los datos de «%s»
 %d: No se pudo leer de«%s»: %s
 %d: No se pudo eliminar el dispositivo «%s» con UDI «%s» de la tabla de sistemas de archivos: no se encontró
 %d: No se pudo acceder a «%s»: %s
 %d: No se pudo escribir al archivo temporal: %s
 %d: Error adquiriendo el bloqueo de «%s»; saliendo
 %d: Error soltando bloqueo«%s»
 %d: Falló al eliminar el punto de montaje para «%s»: %s
 %d: Falló al renombrar «%s» a «%s»: %s
 %d: Se encontró %s apuntando a %s %d: La línea terminó prematuramente
 %d: Bloqueo adquirido
 %d: Bloqueo soltado
 %d: Soltando aviso de bloqueo en %s
 %d: Viendo si la línea para dev=«%s»,mnt=«%s» debería ser eliminada
 %d: añadió el punto de montaje «%s» para el dispositivo «%s»
 %d: no se pudo abrir %s O_RDONLY; saliendo
 %d: Unidad: desired_mount_point='%s', fstype='%s', options='%s', mount_point='%s', normalized_desired_mount_point='%s'
 %d: entrando add_hal_device, udi=«%s»
 %d: is_generated=%d, is_mounted=%d
 %d: eliminó el punto de montaje para el dispositivo «%s»
 %d: usando archivo temporal «%s»
 %d: volumen: desired_mount_point='%s', fstype='%s', options='%s', mount_point='%s'
 Disco duro externo %s Disco duro %s Soporte %s Soporte extraíble %s Unidad %s%s <b>Ancho de banda:</b> <b>Tipo de bus:</b> <b>Capacidad:</b> <b>Tipo de dispositivo:</b> <b>Dispositivo:</b> <b>Producto OEM:</b> <b>Fabricante OEM:</b> <b>Consumo energético:</b> <b>Producto:</b> <b>Revisión:</b> <b>Estado:</b> <b>Versión USB:</b> <b>Fabricante:</b> Añadir una entrada a fstab Avanzado CD de audio CD-R virgen CD-RW virgen DVD+R virgen DVD+R de doble capa virgen DVD+RW virgen DVD-R virgen DVD-RAM virgen DVD-RW virgen Tipo de bus CD-R CD-ROM CD-RW DVD+R DVD+R de doble capa DVD+RW DVD-R DVD-RAM DVD-ROM DVD-RW Dispositivo Administrador de dispositivos Nombre del dispositivo Tipo de dispositivo Fabricante del dispositivo Unidad Unidad %s%s externa Unidad de disquetes externa Disco duro externo Unidad de disquetes Disco duro ID del fabricante Máximo 	consumo energético ID del fabricante OEM ID del producto OEM PCI ID del producto Revisión del producto Eliminar todas las entradas generadas de fstab Eliminar una entrada de fstab Proporcionar información detallada sobre el progreso de las operaciones Estado UDI USB Ancho de banda USB Versión USB Dispositivos _virtuales añadió un punto de montaje %s para %s eliminó todos los puntos de montaje generados eliminó el punto de montaje %s para %s 