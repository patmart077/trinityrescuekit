��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  J  �  3        N     ]     n     ~     �     �     �     �     �  C   �  -   A	     o	  
   �	     �	     �	     �	     �	     �	     �	     �	  	   �	  &   �	  $   
  L   B
     �
     �
  +   �
  5   �
     #  6   ,  )   c     �     �     �     �     �  &   �          2        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: es
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-03-22 13:22+1000
Last-Translator: Manuel Ospina <mospina@redhat.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.0.2
 
ERROR -  Debe ser root para poder ejecutar kudzu.
 dispositivo %s lector de CD-ROM controlador IDE controlador IEEE1394 controlador PCMCIA/Cardbus controlador RAID controlador SCSI controlador USB alias creado para %s como %s realizar solamente sondeos 'seguros' que no perjudiquen el hardware fichero para leer información sobre hardware unidad de disco flexible disco duro versión de kernel teclado enlace creado de %s a %s módem monitor ratón tarjeta de red impresora comprobar sólo la 'clase' específica comprobar sólo el 'bus' específico sondear únicamente; imprimir la información a la salida estándar (stdout) ejecutó loadkeys %s ejecutó mouseconfig para %s leer hardware monitorizado desde un fichero leer hardware monitorizado desde un socket (conector) escáner buscar módulos para una versión particular de kernel fijar el tiempo de operación en segundos tarjeta de sonido unidad de cinta servicio aep1000 activado servicio bcm5820 activado eliminado enlace %s eliminado el enlace %s (apuntaba a %s) adaptador de vídeo tarjeta de captura de vídeo 