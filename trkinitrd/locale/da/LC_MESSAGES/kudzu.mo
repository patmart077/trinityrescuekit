��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  J  �  1        L     U     a     p     �     �     �     �     �  =   �     $	     B	     O	     X	     e	     n	     �	     �	     �	     �	     �	  #   �	     �	  *   �	     
     %
     A
  "   a
     �
  .   �
     �
     �
     �
     �
          #  (   7  
   `     k        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: da
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-03-14 02:00+0100
Last-Translator: Keld Simonsen <keld@dkuug.dk>
Language-Team: Danish <<dansk@klid.dk>>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.0.2
 
FEJL - Du skal være 'root' for at køre kudzu.
 %s enhed Cd-rom-drev IDE-kontroller IEEE1394-controller PCMCIA/Cardbus-controller RAID-controller SCSI-controller USB-controller oprettet alias %s som %s Udfør kun 'sikre' søgninger som ikke vil forstyrre udstyret fil at læse hardwareinfo fra diskettedrev harddisk kerneversion tastatur lænkede %s til %s modem skærm mus netkort printer søg kun efter den angivne 'klasse' søg kun på den angivne 'bus' søg kun, send information til standard-ud kørte 'loadkeys' %s kørte 'mouseconfig' for %s Læs søgte hardware fra en fil Læs søgte hardware fra en sokkel skanner søg efter moduler for en bestemt kerneversion sæt tidsudløb i sekunder lydkort båndstation aktiverede aep1000-tjeneste aktiverede bcm5820-tjeneste fjernede lænken %s fjernede lænken %s (var lænket til %s) grafikkort video-indlæsningskort 