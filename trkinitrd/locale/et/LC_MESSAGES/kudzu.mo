��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  >  �  ;   �     �               $     8     U     e     u     �  ,   �     �  
   �  
   �     �  
   �     
	     	     #	     +	     0	     <	  "   D	  !   g	  4   �	     �	  "   �	  '   �	     #
  1   +
     ]
  	   t
  	   ~
     �
     �
     �
  $   �
     �
     �
               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: et
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2005-02-05 17:07+0200
Last-Translator: Allan Sims <allsi@eau.ee>
Language-Team: Estonian <Eesti>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.2
 
Vuga - kudzu käivitamiseks on vaja juurkasutaja õigusi.
 %s seade CD-ROM seade IDE regulaator IEEE1394 regulaator PCMCIA/kaardisiin regulaator RAID regulaator SCSI regulaator USB regulaator %s samastatud kui %s proovi ainult ohutult, mis ei sega riistvara riistvara info fail flopiseade kõvaketas tuuma versioon klaviatuur %s ühendatud %s-i modem monitor hiir võrgukaart printer proovi ainult täpsustatud 'klass' proovi ainult täpsustatud 'siin' proovi ainult, väljasta teave standard väljundisse käivitus laadevõtmed %s mouseconfig käivitatakse %s jaoks loe järele proovitud riistvara failist skanner otsi mooduleid konkreetse kerneli versiooni jaoks sea ooteaeg sekundites helikaart lintseade teenus aep1000 käivitatud teenus bcm5820 käivitatud ühendamata %s %s ühendamata (oli ühendatud %s-i) videoadapter videohõivekaart 