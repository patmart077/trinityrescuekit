��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  i  �  ?   9     y     �     �     �     �     �     �     �     	  :   	  1   O	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	  %   �	  "   
  D   B
     �
     �
  &   �
  (   �
       8        H     h     u     �     �     �  "   �     �     �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: it
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-20 22:26+0100
Last-Translator: Lorenzo Stobbione <lorenzo.stobbione@clsengineering.it>
Language-Team:  <fedora-trans-it@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.3.1
 
ERRORE - Sono necessari i diritti di root per eseguire kudzu.
 dispositivo %s lettore CD-ROM controller IDE Controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB alias %s come %s Eseguire solo rilevamenti che non compromettono l'hardware file in cui trovare le informazioni sull'hardware unità floppy disco fisso versione della kernel tastiera collegato %s a %s modem monitor mouse scheda di rete stampante rilevare solo la 'classe' specificata rilevare solo il 'bus' specificato solo rilevamento, visualizzare le informazioni sullo standard output eseguito loadkey %s eseguito mouseconfig per %s leggere l'hardware rilevato da un file leggere l'hardware rilevato da un socket scanner cerca i moduli per una particolare versione della kernel impostare il timeout in secondi scheda audio unità a nastro servizio aep1000 attivo servizio bcm5820 attivato scollegato %s scollegato %s (era collegato a %s) scheda video scheda di acquisizione video 