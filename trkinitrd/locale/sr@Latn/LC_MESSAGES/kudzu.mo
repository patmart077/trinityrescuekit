��          �            h  (   i     �  0   �  1   �          &  $   5     Z  '   y     �      �  "   �  2   �     (  D  ?  3   �     �  :   �  E   
  5   P     �  "   �  '   �  -   �       $   #  '   H  +   p      �                      	         
                                                 
ERROR - You must be root to run kudzu.
 aliased %s as %s do configuration that doesn't require user input do only 'safe' probes that won't disturb hardware file to read hardware info from kernel version probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s read probed hardware from a file read probed hardware from a socket search for modules for a particular kernel version set timeout in seconds Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-01-22 18:34-0500
PO-Revision-Date: 2006-01-09 20:37-0000
Last-Translator: Miloš Komarčević <kmilos@gmail.com>
Language-Team: Serbian (sr) <fedora@prevod.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
GREŠKA - Morate biti root da bi pokrenuli kudzu.
 %s predstavljen kao %s izvršavaj podešavanja koja ne zahtevaju korisnički unos izvršavaj samo „sigurne“ testove koji neće uznemiravati hardver datoteka iz koje se učitavaju informacije o hardveru verzija jezgra testiraj samo navedenu „klasu“ testiraj samo navedenu „magistralu“ samo testiraj, štampaj informacije na stdout pokrenut loadkeys %s učitaj testiran hardver iz datoteke učitaj testiran hardver sa priključka potraži module za određenu verziju jezgra postavi vreme isteka u sekundama 