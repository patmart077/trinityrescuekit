��    K      t  e   �      `     a     x     �     �  
   �     �     �     �     �     �     �     �  
   �     �     �               (     =     Q     `     t     �     �     �     �     �     �     �            
        !     -     9     P     ]     i     w     �     �     �     �     �     �     �     �     �     �     �     �     �     �      	     	     	     (	     >	     R	  
   _	     j	     z	     �	     �	     �	  
   �	     �	  '   �	     �	  4   
     F
     M
     Q
     _
  A  k
     �     �     �     �  
   �                               (     0  
   8     C     L     X     n     �     �     �     �     �     �     �               (     ;     N     i     x     �     �     �     �     �     �     �     �     �                              0     7     =     E     L     R     `     n     ~     �     �     �     �     �  
   �     �     �               ,     0     9  -   J     x  :   �     �     �     �  
   �        ;   <               &          :             5   (   4           1       3      0                   8   -              +               G          *      	   )       !   6       /   "   ?       @   B   %               D   I   A         C          F   J      2       '   H   K   #   7   .           >   
      9      =                           E              $      ,    %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID Max 	Power Drain OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status USB USB Bandwidth USB Version Project-Id-Version: hal
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-07-20 10:21+0200
PO-Revision-Date: 2005-08-11 22:53+0300
Last-Translator: Jyri Palokangas <jmp@netti.fi>
Language-Team:  <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
 %s ulkoinen kiintolevyasema %s kiintolevy %s levy %s siirrettävä levy %s%s asema /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Kaistanleveys:</b> <b>Väylän tyyppi:</b> <b>Ominaisuudet:</b> <b>Laitteen tyyppi:</b> <b>Laite:</b> <b>OEM tuote:</b> <b>OEM myyjä:</b> <b>Virrankäyttö:</b> <b>Tuote:</b> <b>Revisio:</b> <b>Tila:</b> <b>USB versio:</b> <b>Valmistaja:</b> Lisää merkintä fstabiin Lisäasetukset Audio CD Tyhjä CD-R Tyhjä CD-RW Tyhjä DVD+R Tyhjä DVD+R Dual-Layer Tyhjä DVD+RW Tyhjä DVD-R Tyhjä DVD-RAM Tyhjä DVD-RW Väylän tyyppi CD-R CD-ROM CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-RW Laite Laitehallinta Laitteen nimi Laitteen tyyppi Laitteen valmistaja Asema Ulkoinen %s%s asema Ulkoinen levykeasema Ulkoinen kiintolevy Levykeasema Kiintolevy Valmistajan ID Max 	Power Drain OEM valmistajan ID OEM tuote ID PCI Tuote ID Tuotteen revisio Poista kaikki generoidut merkinnät fstabista Poista merkintä fstabista Raportoi yksityiskohtaiset tiedot toiminnon edistymisestä Tila USB USB kaistanleveys USB versio 