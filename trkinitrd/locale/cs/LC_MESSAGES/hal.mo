��    K      t  e   �      `     a     x     �     �  
   �     �     �     �     �     �     �     �  
   �     �     �               (     =     Q     `     t     �     �     �     �     �     �     �            
        !     -     9     P     ]     i     w     �     �     �     �     �     �     �     �     �     �     �     �     �     �      	     	     	     (	     >	     R	  
   _	     j	     z	     �	     �	     �	  
   �	     �	  '   �	     �	  4   
     F
     M
     Q
     _
  �  k
     /     @  
   H     S     m     |     �     �     �     �     �     �  
   �     �     �     �     �               )     =     Q     g     y     �     �     �     �  !   �     �     �                     0     C     T     d     v     �     �     �     �     �     �     �     �     �     �     �     �     �            	   +     5     M     k     y     �     �     �     �     �     �     �     �  7   �  #   #     G     b     i     m  	   �        ;   <               &          :             5   (   4           1       3      0                   8   -              +               G          *      	   )       !   6       /   "   ?       @   B   %               D   I   A         C          F   J      2       '   H   K   #   7   .           >   
      9      =                           E              $      ,    %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID Max 	Power Drain OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status USB USB Bandwidth USB Version Project-Id-Version: messages
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-07-20 10:21+0200
PO-Revision-Date: 2005-08-01 15:59+0200
Last-Translator: Klara Cihlarova <koty@seznam.cz>
Language-Team: cs_CZ <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.1
Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Externí disk %s %s disk Médium %s Odstranitelné médium %s Mechanika %s%s /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Šířka pásma:</b> <b>Typ sběrnice:</b> <b>Funkce:</b> <b>Typ zařízení:</b> <b>Zařízení:</b> <b>OEM produkt:</b> <b>OEM dodavatel:</b> <b>Spotřeba:</b> <b>Produkt:</b> <b>Revize:</b> <b>Status:</b> <b>Verze USB:</b> <b>Výrobce:</b> Přidat položku do souboru fstab Pokročilé Audio CD Prázdné CD-R Prázdné CD-RW Prázdné DVD+R Prázdné DVD+R DL Prázdné DVD+RW Prázdné DVD-R Prázdné DVD-RAM Prázdné DVD-RW Typ sběrnice CD-R CD-ROM CD-RW DVD+R DVD+R DL DVD+RW DVD-R DVD-RAM DVD-RW Zařízení Správce zařízení Název zařízení Typ zařízení Výrobce zařízení Mechanika Externí mechanika %s%s Externí disketová mechanika Externí disk Disketová mechanika Disk ID výrobce Max 	odběr ID OEM výrobce ID OEM produktu PCI ID produktu Revize produktu Odstranit všechny doplněné položky ze souboru fstab Odstranit položku ze souboru fstab Podrobný výpis o postupu Status USB USB šířka pásma Verze USB 