#!/bin/bash
# Name: virusscan
# Author: Harakiri
# Script that gets you external antivirus scanners, updates them and scans your computer
#set -x
grep -q trkinmem /proc/cmdline && export TRKINMEM=0
export TRKMOUNTDIR=`cat /etc/trkmountdir`
. /etc/rc.d/init.d/functions
export SWGET="wget --ca-certificate=/usr/lib/ssl/certs/trinityhome.crt -q https://"
export EXTENSIONS=".exe .EXE .com .COM .bat .BAT .cmd .CMD .msi .MSI .vbs .VBS .scr .SCR .csh .CSH .pif .PIF .lnk .LNK .js .JS .jar .JAR .zip .ZIP .arj .ARJ .gz .GZ .z .Z .doc .DOC .xls .XLS .dll .DLL"
> /tmp/.vlogfiles
LOGFILEEXIST="! test -e $LOGFILE && echo \"No logfile found at $LOGFILE, something's wrong, exiting!\" && exit 1"

function checkconnection {

 if [ ! -s /etc/resolv.conf -a ! -s /etc/proxy.conf ]; then
 echo
 echow "Neither DNS nor proxy parameters for wget found so no internet connection, cannot fetch AV engine";
 echo
 exit 1; 
 fi;
 # wget -q http://trinityhome.org/messages/msgurl.txt -O /dev/null && MSGURL=yes
} # checkconnection

function sanitise_path {
    uprooted="${1#/}"
    deslashed="${uprooted//\//-}"
    unspaced="${deslashed//\ /_}"
    echo $unspaced
} # sanitise_path

function updateURL {
    SCANNER=$1 ; shift

    # Make sure we can adapt TRK should the URLs change
    if ! RETVAL=$($SWGET"trinityhome.org/messages/${SCANNER}url.txt" -O /tmp/${SCANNER}url.txt) ; then
	cat /etc/${SCANNER}url.txt
        return 1
    else
        # not sure the file really needs to be updated, but may be useful for debugging -- Phil Hands
	echo $RETVAL > /etc/fproturl.txt
    fi
    cat /tmp/${SCANNER}url.txt
    return 0
} # updateURL

function clam 	{
# First check for new virus definitions
 if [ ! "$NOUPDATE" ]
 then
 checkconnection
 echo
 echow "Checking for updated ClamAV program packages"
 echo
 OLD_URL=$(cat /etc/clamurl.txt)
  if NEW_URL=$(updateURL clam); then
   if [ "${NEW_URL}" != "${OLD_URL}" ] ; then
   cd /tmp
   for i in ${NEW_URL}; do wget  "$i" || exit 1; done
   nice -n 19 rpm --nodeps -Uvh clam*.rpm || exit 1 
    rm -rf clam*.rpm
   else echo; echow "No program update, leaving current version"; echo
   fi
   if ! [ -S /var/run/clamav/clamd.sock ]; then 
   echo; echow "Starting clamd"; clamd; echo
   fi
  echo; echow "Updating virus definitions over the internet"; echo
  test -e /bin/trkscript && /bin/trkscript
  freshclam;
  fi
 else echo; echow "Not updating Clamav"; echo
 fi;

# If getonly is specified, exit here
 if [ "$GETONLY" ]; then exit 0; fi;
	
# Is the -c parameter specified?
 if [ "$COMM" ]; then
  for s in $EXTENSIONS; do COMMONS="$COMMONS --include=$s"; done
 else
 COMMONS=" "
 fi;

# Next identify viruses
echo; echow "Started scanning with ClamAV, first identify viruses"; echo
 cat "$DESTINATION" | while read i;
 do

	echo; echow "Scanning \"$i\""
        hi=$(sanitise_path $i)
	# Set the logfile
	if [ "$LOGDEST" ]; then
	 export LOGFILE=$LOGDEST/clamscan-$hi.log;
        else
         if [ "$READONLY" ]; then 
	  export LOGFILE=/tmp/clamscan-$hi.log;
         else
          export LOGFILE=$i/clamscan-$hi.log;
         fi
	fi
	# Reset the logfile or false positives might appear from old scans
	> "$LOGFILE"
	clamscan --detect-pua --tempdir=/tmp --exclude-dir TRK-INFECTED --exclude=pagefile.sys --exclude=hiberfil.sys $COMMONS  -r -l "$LOGFILE" "$i"
	eval $LOGFILEEXIST
        FILES_FOUND=/var/log/clamav/infectedlist-$hi.log
	grep FOUND "$LOGFILE" | grep -v "Broken.Executable" | cut -d ":" -f 1 > ${FILES_FOUND}
	TIMES=$(wc -l < ${FILES_FOUND})
        [ $TIMES -gt 1 ] && PLURAL=1
	 if [ -s ${FILES_FOUND} ]; then
	 color ltred
	 echo
	 echo "$TIMES Virus${PLURAL:+es} found on $i!"
         VSTATE=infected
	  if ! [ "$READONLY" ]; then
          echo; 
          echow "Copying infected files to $i/TRK-INFECTED/infected-$hi.tar.gz, you will be prompted afterwards for deletion"; 
          echo
          tar --files-from=/var/log/clamav/infectedlist-$hi.log -czf /tmp/infected-$hi.tar.gz
          VIRUSFOUND=1
          echo $i >> /tmp/.virusfound
          else
          echow "Ran in read-only mode, see log file(s) in /tmp"
          fi;
         color off;
	 else 
	 color ltgreen
	 echo "No viruses found on $i";
         VSTATE=clean
	 color off
	 fi;
        echo "$VSTATE;$LOGFILE" >> /tmp/.vlogfiles



 done;
# Set $VIRUSFOUND again since it doesn't survive the while loop.
test -e /tmp/.virusfound && VIRUSFOUND=1
# If nothing is found or we ran in read-only mode, no cleanup needed
 if [ -z  "$VIRUSFOUND" -o "$READONLY" ]; then eval $DISABLESWAP; exit 0; fi;
		
 cat /tmp/.virusfound | while read i 
 do
 hi=$(sanitise_path $i)
  if [ ! -e "$i/TRK-INFECTED" ]; then
  mkdir "$i/TRK-INFECTED";
  fi;
 cp -f /var/log/clamav/infectedlist-$hi.log "$i/TRK-INFECTED";
 # cp -f /var/log/clamav/clamscan-$hi.log "$i/TRK-INFECTED/"
 mv -f /tmp/infected-$hi.tar.gz "$i/TRK-INFECTED/"
 done 
rm -f /tmp/.virusfound

echo
echow "Would you like to delete ALL infected files at once?"
echow "Warning! Some viruses infect system-critical files and may prevent your system from booting"
echow "if you move these files into quarantaine. Clamav can only detect viruses, but not clean"
echow "files internally"
echo
echow "You can restore your files afterwards from the quarantaine archives in folder TRK-INFECTED."
echow "This will put back every file, including viruses, to its original location"
while read -n1 -p "Delete ALL files? ([y]es, [n]o, [l]ist the files, or [q]uit) " KEY
do
    echo
    case ${KEY} in
        'y'|'Y')
	    for i in /var/log/clamav/infectedlist-*.log; do xargs rm -v -f  < $i; done
            break;
            ;;
         'n'|'N')
            break;
            ;;
        'q'|'Q')
            exit 2;
            ;;
        'l'|'L')
            more /var/log/clamav/infectedlist-*.log;
            ;;
    esac
done
} # End function clam
			
function fprot	{

 if ! [ -x /usr/share/f-prot/fpscan ]; then

 checkconnection
 # Make sure we can adapt TRK should the URL to fp-linux change
 if ! FPFILENAME=$($SWGET"trinityhome.org/messages/fproturl.txt" -O -) ; then
  echo; echow "Failed to get updated F-Prot url, trying original location";
  FPFILENAME=$(cat /etc/fproturl.txt)
 else
  # not sure the file really needs to be updated, but may be useful for debugging -- Phil Hands
  echo $FPFILENAME > /etc/fproturl.txt
 fi
 
 if [ "$FPFILENAME" = "deadfeature" ]; then echo "This feature no longer works, please check http://trinityhome.org/trk/ for more news on this" && exit 1; fi;

	
 # When all is ok, get fp-linux from the web
 echo
 echow "Downloading F-prot from $FPFILENAME"
 echo
 cd /tmp
 if ! wget $FPFILENAME; then exit 1; fi;
 FPFILENAME=$(basename $FPFILENAME)
 cd /usr/share/
 tar xzf /tmp/$FPFILENAME 
 rm -f /tmp/$FPFILENAME
 cd /usr/share/f-prot
 cp -f f-prot.conf.default /etc/f-prot.conf
 cd /usr/bin
 ln -s /usr/share/f-prot/fpscan 2> /dev/null
 ln -s /usr/share/f-prot/fpupdate 2> /dev/null
 fi;



. /bin/trkscript 2> /dev/null
cd /usr/share/f-prot	
 if [ -s /etc/checkupdatesargs ]; then
 CHECKUPDATEARGS=`cat /etc/checkupdatesargs`
 fi;
 if [ ! -e product.data ] || ! grep -q FPROT_LICENSE_KEY license.key 2> /dev/null ]; then
 echow "In order for F-Prot to work, you will need to provide a name and an e-mail address to get a trial license from the F-prot update server. No confirmation is needed afterwards."
 echo -n "Enter a name: "
 read FPNAME
 echo -n "Enter an e-mail address: "
 read FPEMAIL
 cp -f product.data.default product.data
 echo "name=$FPNAME" >> product.data
 echo "email=$FPEMAIL" >> product.data
 echo "where=n/a" >> product.data
 ./fpupdate --trial $CHECKUPDATEARGS > license.key
 fi;
 
 if [ "r$NOUPDATE" != "r1" ] ; then 
 ./fpupdate $CHECKUPDATEARGS
 else echo "Update skipped" 
 fi;
 if [ "r$GETONLY" = "r1" ]; then exit 0; fi;
 
 
# Start scanning
echo "Started scanning with F-Prot"
cat "$DESTINATION"| while read i; do
hi=$(sanitise_path $i)
 # Set the logfile
 if [ r$LOGDEST = r -a r$READONLY = r ]; 
 then export LOGFILE=$i/fpscan-$hi.log; 
 elif [ r$LOGDEST = r -a r$READONLY = r1 ];
 then export LOGFILE=/tmp/fpscan-$hi.log;	
 else export LOGFILE=$LOGDEST/fpscan-$hi.log;
 fi;
 echo $LOGFILE  >> /tmp/.vlogfiles
 if [ r$READONLY = r1 ]; then 
 echow "Running in read-only mode"; export FPDISINF=""
 else FPDISINF="--adware --disinfect" 
 fi;
./fpscan -e *TRK-INFECTED/ -b -m -s 4 $FPDISINF --exclude=pagefile.sys,hyberfil.sys,TRK-INFECTED "$i" | tee "$LOGFILE"
eval $LOGFILEEXIST
if [ "`grep "Infected objects:" "$LOGFILE"`" != "Infected objects: 0" ]; then 
echo
color ltred; echo "Viruses found! You can find a log in $LOGFILE"; color off
VSTATE=infected
else
color ltgreen; echo "All clean"; color off;
VSTATE=clean
fi;
done;
echo "$VSTATE;$LOGFILE"  >> /tmp/.vlogfiles
			} # End function fprot
function bde {
rpm -qa|grep -qi bitdefender 2>/dev/null
 if [ $? = 1 ]; then
 checkconnection
 cd /tmp
 # Make sure we can adapt TRK should the URL to BD change (or newer version)
 BDEURL=`cat /etc/bderpmurl.txt`
 $SWGET"trinityhome.org/messages/bderpmurl.txt" -O /etc/bderpmurl.txt.new && mv -f /etc/bderpmurl.txt.new /etc/bderpmurl.txt
  if [ $? = 0 ]; then
  export BDEURL=`cat /etc/bderpmurl.txt`
  fi;

  if [ "`cat /etc/bderpmurl.txt`" = "deadfeature" ]; then echo "This feature no longer works, please check http://trinityhome.org/trk/ for more news on this" && exit 1; 
  fi;
  echow "Downloading BitDefender Scanner from $BDEURL"
  wget -i /etc/bderpmurl.txt || exit 1
  BDEFILE=`basename $BDEURL`
  echow "The installation procedure for BitDefender will now run"
  echow "Press enter to continue"
  read ENTERKE
  # For some reason Bitdefender rpm.run hangs on install when launched normally. Strace or lowering 
  # priority with 'nice' works around the problem
  chmod +x $BDEFILE
  nice -n 19 ./$BDEFILE
   if [ $? != 0 ]; then exit 1; 
   else rm -f $BDEFILE
   fi;
   cd /usr/bin
   ln -s /opt/BitDefender-scanner/bin/bdscan 2>/dev/null

 fi; # End if on existence of bdescanner

. /bin/trkscript 2>/dev/null
 if [ "r$NOUPDATE" != "r1" ]; then
 bdscan --update
 else echo "Update skipped"
 fi;
 if [ "r$GETONLY" = "r1" ]; then exit 0; fi;

 if [ r$READONLY = r1 ]; then 
 echow "Running in read-only mode"; export BDDISINF="--action=ignore"
 else BDDISINF="--action=disinfect" 
 fi;

# Open a long 'while'
echow "Started scanning with BitDefender"
cat "$DESTINATION"| while read j; do
 echow "Scanning " "$j"	
# Remove any leading slashes
 if [ r`echo "$j" | cut --bytes=1` = "r/" ]; then export x=`echo "$j" | cut --bytes=2-1000`; fi
 # Move $j to hyphenated names
 hj=`echo "$x" | tr "/" "-"`
 hj=`echo "$hj" | tr "\ " "_"`
  # Set the logfile
  if [ r$LOGDEST = r -a r$READONLY = r ];
  then export LOGFILE="$j/bdscan-$hj.log";
  elif [ r$LOGDEST = r -a r$READONLY = r1 ];
  then export LOGFILE="/tmp/bdscan-$hj.log"
  else export LOGFILE="$LOGDEST/bdscan-$hj.log";
  fi;
 echo $LOGFILE  >> /tmp/.vlogfiles
 bdscan $BDDISINF "$j" | tee "$LOGFILE"
 eval $LOGFILEEXIST
  VIRNR=`grep "Infected files:" "$LOGFILE" | cut -d " " -f 3`
  if [ -n $VIRNR -a "r$VIRNR" != "r0" ]; 
  then export VFOUND=1; 
  fi
  if [ r$READONLY = r1 ]; then 
   if [ r$VFOUND = r1 ]; then 
   echo
   color ltred; echo "Viruses found! You can find a log in $LOGFILE"; color off
   else
   color ltgreen; echo "All clean"; color off;
   fi;
  echow "Ran in read-only mode, exiting here"; color off;
  return 0; 
  fi;
 # Filter out whatever failed to get disinfected and prompt for deletion
 grep -q "disinfection failed" "$LOGFILE"
  if [ $? = 0 ]; then
  grep "disinfection failed" "$LOGFILE" | sed -e s'/  disinfection failed//g' > "/tmp/bdscan-non-disinfected-$LOGNAME.log"
  unset k
  mkdir $j/TRK-INFECTED 2>/dev/null
  echow "Disinfection failed on some files, quarantaining them to $j/TRK-INFECTED"
   while read k;  do 
   mv -vf "$k" $j/TRK-INFECTED
   done < "/tmp/bdscan-non-disinfected-$LOGNAME.log"
   if [ r$VFOUND = r1 ]; then 
   echo
   color ltred; echo "Viruses found! You can find a log in $LOGFILE"; color off
   VSTATE=infected
   else
   color ltgreen; echo "All clean"; color off;
   VSTATE=clean
   fi;
   echo "$VSTATE;$LOGFILE" >> /tmp/.vlogfiles
 fi;
done; # Close long 'while'
 		} #End function bde
		
function va	{

 if [ ! -e /usr/share/vascan/vascan ]; then
 VAINSTALLED=no
 fi;

checkconnection

$SWGET"trinityhome.org/messages/vaurl.txt" -O /etc/vaurl.txt.new
 if [ $? = 0 ]; then
  if [ "`cat /etc/vaurl.txt`" = "deadfeature" ]; then 
  echo "This feature no longer works, please check http://trinityhome.org/trk/ for more news on this" && exit 1; 
  fi;
 diff -q /etc/vaurl.txt /etc/vaurl.txt.new >/dev/null
  if [ $? != 0 ]; then
  echow "A newer version of Vexira is available"
  test -e /usr/share/vascan/vascan && rm -rf /usr/share/vascan/
  mv -f /etc/vaurl.txt.new /etc/vaurl.txt
  else 
  rm -f /etc/vaurl.txt.new
  fi;
 fi;
 
 if [ r$VAINSTALLED = rno ]; then
 cd /tmp
 echow "Downloading Vexira Antivirus"
 wget -i /etc/vaurl.txt || exit 1
 VAFILENAME=`cat /etc/vaurl.txt | tr "/" "\n" | tail -n 1`
 cd /usr/share/
 tar xzf /tmp/$VAFILENAME && rm -f /tmp/$VAFILENAME
 mv vascan* vascan || exit 1
 cd /usr/bin
 rm -f vascan 2>/dev/null; ln -s /usr/share/vascan/vascan
 else echo; echow "Program already at latest version, leaving current version"; echo
 fi;
  
 if [ "r$NOUPDATE" != "r1" ] ; then 
 cd /usr/share/vascan/ || exit 1
 echow "Getting latest Vexira virus definitions"
 ./vdbupdate.sh && rm -rf vdb/backup/
 else echow "Update skipped" 
 fi;
 if [ "r$GETONLY" = "r1" ]; then exit 0; fi;
 
 # Set read-only mode yes or no
 if [ r$READONLY = r1 ]; then 
 echow "Running in read-only mode"; export VADISINF="s"
 else 
 export VADISINF="k,q" 
 fi;


# Start scanning
echow "Started scanning with Vexira"
cat "$DESTINATION"| while read i; do
hi=$(sanitise_path $i)
 # Set the logfile
 if [ r$LOGDEST = r -a r$READONLY = r ];
 then export LOGFILE="$i/vascan-$hi.log";
 elif [ r$LOGDEST = r -a r$READONLY = r1 ];
 then export LOGFILE="/tmp/vascan-$hi.log";
 else export LOGFILE="$LOGDEST/fpscan-$hi.log";
 fi;
 if [ r$READONLY = r ]; then
 mkdir $i/TRK-INFECTED/ 2>/dev/null
 vascan --log="$LOGFILE" --action=$VADISINF "$i/TRK-INFECTED/" -y "$i/TRK-INFECTED/" "$i"; 
 export VAEXIT=$?
 else
 vascan --log="$LOGFILE" --action=$VADISINF  "$i"; VAEXIT=$?
 fi;
 eval $LOGFILEEXIST
 if [ $VAEXIT = 2 ]; then 
 echo
 color ltred; echo "Viruses found! You can find a log in $LOGFILE"; color off
 elif [ $VAEXIT = 0 ]; then
 color ltgreen
 echo "No viruses found on $i";
 color off
 fi;
done;

			} # End function va
function avs 	{

# Check if we got any special local parameters from DHCP


checkconnection

$SWGET"trinityhome.org/messages/avsurl.txt" -O /etc/avsurl.txt
 if [ $? = 0 ]; then
 AVSURL=`cat /etc/avsurl.txt`
 else echo "Unable to fetch updated Avast URL, using default"
 AVSURL=`cat /etc/avsurl.txt`
 fi;

 
 if [ "`cat /etc/avsurl.txt`" = "deadfeature" ]; then echo "This feature no longer works, please check http://trinityhome.org/trk/ for more news on this" && exit 1; fi;
	
# Check whether avast is already installed
rpm -qa|grep avast
if [ $? = 1 ]; then
 # When all is ok, get Avast from the web
 echo
 echow "Downloading Avast Antivirus from $AVSURL"
 echo
 cd /tmp
 wget $AVSURL
 if [ $? != 0 ]; then exit 1; fi;
 AVSFILE=`basename $AVSURL`
 rpm -ivh --nodeps $AVSFILE || exit 1
 rm -f $AVSFILE
fi;
# Register Avast first
EXITAVS=0
while [ $EXITAVS != 42 ]; do
avast -V
EXITAVS=$?
 if [ $EXITAVS = 21 ]; then rm ~/.avast/avastrc; fi;
done;
 if [ "r$NOUPDATE" != "r1" ] ; then 
 echo
 echow "Getting latest Avast virus definitions"
 echo
avast-update
 else echo; echow "Update skipped" ; echo
 fi;
 if [ "r$GETONLY" = "r1" ]; then exit 0; fi;
 
 # Set read-only mode yes or no
 if [ r$READONLY = r1 ]; then 
 echow "Running in read-only mode"; export AVSDISINF=""
 else AVSDISINF="--continue=31" 
 fi;

# Increasing kernel shared memory so the program loads
sysctl -w kernel.shmmax=128000000
# Start scanning
echow "Scanning and disinfecting "
cat "$DESTINATION"| while read i; do
hi=$(sanitise_path $i)
 # Set the logfile
 if [ r$LOGDEST = r -a r$READONLY = r ]; 
 then export LOGFILE="$i/avast-scan-$hi.log"; 
 elif [ r$LOGDEST = r -a r$READONLY = r1 ];
 then export LOGFILE="/tmp/avastscan-$hi.log";	
 else export LOGFILE="$LOGDEST/avast-scan-$hi.log";
 fi;

avast $AVSDISINF -r "$LOGFILE" "$i"
if [ $? -ge 1 -a $? -le 5 ]; then 
echo
color ltred; echo "Viruses found! You can find a log in $LOGFILE"; color off
VSTATE=infected
else VSTATE=clean
fi;
done;
echo "$VSTATE;$LOGFILE" >> /tmp/.vlogfiles 

			} # End function avs

function md5 {

 
# Start scanning
 cat "$DESTINATION"| while read i; do
 hi=$(sanitise_path $i)
  # Set the logfile
  if [ r$LOGDEST = r -a r$READONLY = r ]; 
  then export LOGFILE="$i/md5sum-$hi.log"; 
  elif [ r$LOGDEST = r -a r$READONLY = r1 ];
  then export LOGFILE="/tmp/md5sum-$hi.log";	
  else export LOGFILE="$LOGDEST/md5sum-$hi.log";
  fi;
   
 echow "Writing modification date, filesize md5sum and path/filename to $LOGFILE"
 find "$i" -type f -printf '%C@ %6s ' -exec xargs md5sum {} \;| tee "$LOGFILE"
 
 done;

		} # md5
	function Help {

 echo "Usage: 'virusscan -a [clam,avs,fprot,bde,va] -c -g -d {DESTINATION}' -n -r -l {LOGFILE FOLDER} -h -s where"
 echo "-a: What antivirus you want to use. Takes 'clam' for ClamAv, 'fprot' for F-Prot, 'avs'"
 echo "    for Avast Antivirus (free license key required), 'bde' for BitDefender and 'va' for Vexira Antivirus. "
 echo "    Note that ClamAv is actually meant to plug into mailservers and block infections."
 echo "    Therefore it can only quarantain infected files not disinfect them."
 echo "    BitDefender proposes disinfection as a first action and deletion as a second"
 echo "    If the parameter '-a'is omitted, ClamAv will be the default"
 echo "    You can comma separate your antivirus engine arguments to have them scan consecutively"
 echo "    This gives you the opportuninty to use ALL virusscanners at once in a single commandline"
 echo "-c: Use Common extensions: $EXTENSIONS (only with ClamAv, speeds up scan a little)"
 echo "-g: Get only, just download the AV and updates, no scanning. For use with updatetrk"
 echo "-d: Destination folder to scan. If no destination is given, virusscan will scan all local"
 echo "    filesystems it can find. You can specify multiple destinations when you separate them"
 echo "    with commas (without spaces behind)"
 echo "-n: No update. Don 't check for new AV signatures"
 echo "-r: Read-only. This will not touch anything, except create a logfile on the destination"
 echo "-l: Set the logfile destination folder. Default, this is the root of the scanned location."
 echo "    Filename is set automatically, folder is created when not existing"
 echo "-s: Do not search for and enable swap."
 echo "-N: Do not show a summary report on viruses found but return to the prompt"
 echo "-h: Displays this helpmessage"
 echo "Example 1: 'virusscan -a avs -d /mnt0,/hda1/WINDOWS,/hda1/Program\ Files'"
 echo "    This downloads and updates Avast if not available and scans the locations /mnt0,"
 echo "    /hda1/WINDOWS and /hda1/Program\ Files"
 echo "Example 2: 'virusscan -a clam,fprot,avs,bde,va' will scan everything with all antivirus engines"
	} # End function Help

function MoreMem {
if [ `grep SwapTotal /proc/meminfo| awk '{print $2}'` = 0 ]; then
export SWAPWAS=0
DISABLESWAP="! test -z $SWAPWAS && getswap -d -f"
getswap	
fi;
} # MoreMem

while getopts a:d:gchnNl:rps name 
do
	
case $name in

	c) export COMM="1";;
	g) export GETONLY=1;;
	d) export DESTARG="$OPTARG";;
	a) export AVARG="$OPTARG";;
	n) export NOUPDATE=1;;
	N) export NOSUMMARY=1;;
	l) export LOGDEST="$OPTARG"; mkdir -p $LOGDEST 2&>1| grep -i "Permission denied" && exit 1;;
	r) export READONLY=1;;
	p) export SWGET="wget -q http://";;
	h) Help; exit 0;;
	s) export NOSWAP=1;;
	
	[?]) Help; exit 2;;
esac
done

 if [ "r$DESTARG$GETONLY" = "r" ]; then
  if [ ! -e /etc/mountallfstab ]; then mountallfs -q ${READONLY:+ -k -r}; fi;
  > /tmp/avdestinations
   for i in `cat /etc/mountallfstab`; do echo "/$i" >> /tmp/avdestinations; done
  export DESTINATION="/tmp/avdestinations"
  else 
  echo "$DESTARG" | tr "," "\n"| sed -e '/^$/d' > /tmp/avdestinations
  export DESTINATION="/tmp/avdestinations"
 fi;
 if [ "r$AVARG" = "r" ]; then AVARG=clam; fi;
# Execute all given AV scanners consecutively
AVARG=`echo $AVARG | tr "," " "`
# Prevent screen blanking
setterm -blank off
# Enable swap if it wasn't set yet, but not in read-only mode
[ "$NOSWAP" -o "$READONLY" ] || MoreMem
# The next 3 lines are where the shit happens!
for EXECAVARG in $AVARG; do
	$EXECAVARG
done
 if grep -q infected /tmp/.vlogfiles  && test -z $NOSUMMARY; 
 then 
  for i in $(grep infected /tmp/.vlogfiles); do j=$(echo $i| awk -F ';' '{print $2}'); color ltwhite; echo "==> Virus report of $j <=="; color off; echo; tail -n 30 $j; done | more -d -c
 fi;

# Disable swap if it was activated from this script
eval $DISABLESWAP
exit 0
