#!/bin/sh

# udev external PROGRAM script
# return devfs-names for ide-devices
# BUS="ide", KERNEL="hd*", PROGRAM="/etc/udev/ide-devfs.sh %k %b %n", NAME="%1c", SYMLINK="%k %c{2} %c{3} %c{4} %c{5}", GROUP="%c{3}"

HOST="${2%\.[0-9]}"
TARGET="${2#[0-9]\.}"

if [ -z "${HOST#[13579]}" ]; then
	HOST=$((${HOST} - 1))
	BUS="1"
else
	BUS="0"
fi

get_dev_number() {
	num=0
	DRIVE="${1%%[0-9]*}"

	for x in /proc/ide/*/media; do
		if [ -e "${x}" ]; then
			lMEDIA=`cat ${x}`
			if [ "${lMEDIA}" = "$2" ]; then
				num=$((${num} + 1))
			fi
			if [ "${x}" = "/proc/ide/${DRIVE}/media" ]; then
				break
			fi
		fi
	done
	
	echo $((${num} - 1))
}

if [ -z "$3" ] && [ -f /proc/ide/${1}/media ]; then
	MEDIA=`cat /proc/ide/${1}/media`
	if [ "${MEDIA}" = "cdrom" ]; then
		echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/cd cdroms/cdrom`get_dev_number $1 cdrom` ${MEDIA}
	    case `get_dev_number $1 cdrom` in
	      0)
		echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/cd ide/cd/c${HOST}b${BUS}t${TARGET}u0 cdroms/cdrom0 cdrom cdrom0 ${MEDIA}
		;;
	      *)
		echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/cd ide/cd/c${HOST}b${BUS}t${TARGET}u0 cdroms/cdrom`get_dev_number $1 cdrom` cdrom`get_dev_number $1 cdrom` ${MEDIA}
		;;
	    esac  
	elif [ "${MEDIA}" = "floppy" ]; then
		echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/disc floppies/floppy`get_dev_number $1 floppy`/disc ${MEDIA}
	elif [ "${MEDIA}" = "disk" ]; then
		echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/disc discs/disc`get_dev_number $1 disk`/disc ${MEDIA}
		echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/disc ide/hd/c${HOST}b${BUS}t${TARGET}u0 discs/disc`get_dev_number $1 disk`/disc ${MEDIA}
	fi
else
	echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/part$3 discs/disc`get_dev_number $1 disk`/part$3
	echo ide/host${HOST}/bus${BUS}/target${TARGET}/lun0/part$3 ide/hd/c${HOST}b${BUS}t${TARGET}u0p${3} discs/disc`get_dev_number $1 disk`/part$3
fi
