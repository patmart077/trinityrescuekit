#!/bin/bash
# Network Interface Configuration System
# Copyright (c) 1996-2002 Red Hat, Inc. all rights reserved.
#
# Based on PCMCIA wireless script by (David Hinds/Jean Tourrilhes)
#
# This software may be freely redistributed under the terms of the GNU
# public license.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# Configure wireless network device options.  See iwconfig(8) for more info.
# Valid variables:
#    MODE: Ad-Hoc, Managed, etc.
#    ESSID: Name of the wireless network
#    NWID: Name of this machine on the network.  Hostname is default
#    FREQ: Frequency to operate on.  See CHANNEL
#    CHANNEL: Numbered frequency to operate on.  See FREQ
#    SENS: Sensitivity threshold for packet rejection.
#    RATE: Transfer rate.  Usually one of Auto, 11, 5, 2, or 1.
#    KEY: Encryption key for WEP.
#    RTS: Explicit RTS handshake.  Usually not specified (auto)
#    FRAG: Fragmentation threshold to split packets.  Usually not specified.
#    SPYIPS: List of IP addresses to "spy" on for link performance stats.
#    IWCONFIG: Extra parameters to pass directly to IWCONFIG
#    IWPRIV: Extra parameters to pass directly to IWPRIV

# Only meant to be called from ifup.

# Mode need to be first : some settings apply only in a specific mode !
if [ -n "$MODE" ] ; then
    iwconfig $DEVICE mode $MODE
fi
# This is a bit hackish, but should do the job right...
if [ -n "$ESSID" -o -n "$MODE" ] ; then
    NICKNAME=`/bin/hostname`
    iwconfig $DEVICE nick "$NICKNAME" >/dev/null 2>&1
fi
# Regular stuff...
if [ -n "$NWID" ] ; then
    iwconfig $DEVICE nwid $NWID
fi
if [ -n "$FREQ" -a "$MODE" != "Managed" ] ; then
    iwconfig $DEVICE freq $FREQ
elif [ -n "$CHANNEL" -a "$MODE" != "Managed" ] ; then
    iwconfig $DEVICE channel $CHANNEL
fi
if [ -n "$SENS" ] ; then
    iwconfig $DEVICE sens $SENS
fi
if [ -n "$RATE" ] ; then
    iwconfig $DEVICE rate $RATE
fi
if [ -n "$POWER" ] ; then
    iwconfig $DEVICE power $POWER
fi
if [ -n "$KEY" ] ; then
    iwconfig $DEVICE key $KEY
else
    iwconfig $DEVICE key off
fi
if [ -n "$RTS" ] ; then
    iwconfig $DEVICE rts $RTS
fi
if [ -n "$FRAG" ] ; then
    iwconfig $DEVICE frag $FRAG
fi

# More specific parameters passed directly to IWCONFIG
if [ -n "$IWCONFIG" ] ; then
    iwconfig $DEVICE $IWCONFIG
fi

if [ -n "$SPYIPS" ] ; then
    for IP in $SPYIPS; do
	iwspy $DEVICE + $IP
    done
fi
if [ -n "$IWPRIV" ] ; then
    iwpriv $DEVICE $IWPRIV
fi

# ESSID need to be last : most device re-perform the scanning/discovery
# when this is set, and things like encryption keys are better be
# defined if we want to discover the right set of APs/nodes.
if [ -n "$ESSID" ] ; then
    iwconfig $DEVICE essid "$ESSID"
else
    # use any essid
    iwconfig $DEVICE essid any
fi

# Now, run the specific script for Wireless LAN interfaces
# Note : we need the wireless parameters to be set up before IP parameters,
# so that we can perform DHCP over the Wireless link if needed. Jean II

if [ -n "${WIRELESS_MODE}${WIRELESS_ESSID}${WIRELESS_NWID}${WIRELESS_FREQ}${WIRELESS_SENS}${WIRELESS_RATE}${WIRELESS_ENC_KEY}${WIRELESS_RTS}${WIRELESS_FRAG}${WIRELESS_IWCONFIG}${WIRELESS_IWSPY}${WIRELESS_IWPRIV}${WIRELESS_ESSID}${WIRELES_POWER}" -a -x /sbin/iwconfig ] ; then
    # Set all desired settings via iwconfig

    # Mode need to be first : some settings apply only in a specific mode !
    if [ "$WIRELESS_MODE" ] ; then
	/sbin/iwconfig $DEVICE mode $WIRELESS_MODE
    fi
    # This is a bit hackish, but should do the job right...
    if [ "$WIRELESS_ESSID" ] || [ "$WIRELESS_MODE" ] ; then
        NICKNAME=`/bin/hostname`
	/sbin/iwconfig $DEVICE nick $NICKNAME >/dev/null 2>&1
    fi
    # Regular stuff...
    if [ "$WIRELESS_NWID" ] ; then
	/sbin/iwconfig $DEVICE nwid $WIRELESS_NWID
    fi
    if [ "$WIRELESS_FREQ" ] ; then
	/sbin/iwconfig $DEVICE freq $WIRELESS_FREQ
    elif [ "$WIRELESS_CHANNEL" ] ; then
	/sbin/iwconfig $DEVICE channel $WIRELESS_CHANNEL
    fi
    if [ "$WIRELESS_SENS" ] ; then
	/sbin/iwconfig $DEVICE sens $SENS
    fi
    if [ "$WIRELESS_RATE" ] ; then
	/sbin/iwconfig $DEVICE rate $WIRELESS_RATE
    fi
    if [ "$WIRELESS_ENC_KEY" ] ; then
	/sbin/iwconfig $DEVICE key $WIRELESS_ENC_KEY
    fi
    if [ "$WIRELESS_POWER" ] ; then
	/sbin/iwconfig $DEVICE power $WIRELESS_POWER
    fi
    if [ "$WIRELESS_RTS" ] ; then
	/sbin/iwconfig $DEVICE rts $WIRELESS_RTS
    fi
    if [ "$WIRELESS_FRAG" ] ; then
	/sbin/iwconfig $DEVICE frag $WIRELESS_FRAG
    fi
    # More specific parameters
    if [ "$WIRELESS_IWCONFIG" ] ; then
	/sbin/iwconfig $DEVICE $WIRELESS_IWCONFIG
    fi
    if [ "$WIRELESS_IWSPY" ] ; then
	/sbin/iwspy $DEVICE $WIRELESS_IWSPY
    fi
    if [ "$WIRELESS_IWPRIV" ] ; then
	/sbin/iwpriv $DEVICE $WIRELESS_IWPRIV
    fi
    # ESSID need to be last : most device re-perform the scanning/discovery
    # when this is set, and things like encryption keys are better be
    # defined if we want to discover the right set of APs/nodes.
    if [ "$WIRELESS_ESSID" ] ; then
	/sbin/iwconfig $DEVICE essid "$WIRELESS_ESSID"
    fi
fi

if [ -n "$WIRELESS_WPA_DRIVER" -a -x /usr/sbin/wpa_supplicant -a -f /etc/wpa_supplicant.conf ]; then	
    /usr/sbin/wpa_supplicant -B -i ${DEVICE} \
	-c /etc/wpa_supplicant.conf  \
	-D ${WIRELESS_WPA_DRIVER}
fi

