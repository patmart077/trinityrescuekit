# $Id: tmpdir.csh,v 1.1.1.1 2000/07/30 05:07:38 chmouel Exp $
# Set TMPDIR to ~/tmp create it if he not exist.

if ( -d ${HOME}/tmp && -w ${HOME}/tmp ) then
    setenv TMPDIR ${HOME}/tmp
    setenv TMP ${HOME}/tmp
    exit
endif

if ( `mkdir -p ${HOME}/tmp >&! /dev/null` ) then 
    setenv TMPDIR ${HOME}/tmp
    setenv TMP ${HOME}/tmp
    exit
endif

setenv TMPDIR /tmp
setenv TMP /tmp