#!/bin/sh 
#set -x
# Examples: 
# ./mkvirtualrpm.sh glibc python db3 XFree86-libs zlib mysql postgresql 
# ./mkvirtualrpm.sh /bin/csh 
# ./mkvirtualrpm.sh libstdc++-2.96-98 
# ./mkvirtualrpm.sh /usr/bin/env 

if [ -z "$1" ]; then 
        echo "usage: $0 <name of lib or rpm>" 
        exit 1 
fi 

if test -z $SCRIPT_VERSION; then 
  SCRIPT_VERSION=3.4 
fi 
if test -z $RELEASE_VERSION; then 
  RELEASE_VERSION=0.1 
fi 

mySpecName=TRKVirtual.spec 
name=TRKVirtualPackages 
packages="$*" 
echo "virtual rpms: $packages" 

tmpdir=`mktemp /tmp/virtual.XXXXXX` 
rm -f $tmpdir 
mkdir -p $tmpdir/{RPMS/i386,SPECS} 

( 
# Header 
cat <<EOF 
Summary: virtual provided rpms 
Name: $name 
Version: $SCRIPT_VERSION 
Release: $RELEASE_VERSION 
Group: Trinity Rescue Kit
License: GPL 
BuildArch: i386 
EOF

( 
for i in $packages; do 
   if echo $i | grep -q /; then   ##filenames containing slashes 
          echo $i 
     else 
          if rpm -q $i --quiet ; then 
               rpm -q --provides `basename $i` 
          fi 
          echo $i 

     fi 
done 
) | sort | uniq | sed 's/^/Provides: /' 

# spec-Trailer 
cat <<EOF 

%description 
Virtual packages and provides for TRK rpm-database 

%files
EOF

) > $tmpdir/SPECS/$mySpecName 

cat $tmpdir/SPECS/$mySpecName 
rpm -bb --define "_topdir $tmpdir" $tmpdir/SPECS/$mySpecName 

echo "copying $tmpdir/RPMS/i386/*rpm" 
cp -p  $tmpdir/RPMS/i386/*rpm . 
rm -fr $tmpdir 
