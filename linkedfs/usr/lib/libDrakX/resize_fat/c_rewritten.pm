package resize_fat::c_rewritten; # $Id: c_rewritten.pm,v 1.4 2002/11/28 12:47:16 prigaux Exp $



require DynaLoader;

our @ISA = qw(DynaLoader Exporter);
our $VERSION = '0.01';
our @EXPORT_OK = qw(next set_next);

resize_fat::c_rewritten->bootstrap($VERSION);

1;

