package class_discard; # $Id: class_discard.pm,v 1.4 2003/10/10 11:08:42 gc Exp $

use log;

sub new { bless {}, "class_discard" }

sub AUTOLOAD {
    log::l("class_discard: $AUTOLOAD called at ", caller, ", params ", join(', ', @_));
}

1;
