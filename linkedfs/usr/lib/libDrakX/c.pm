package c; # $Id: c.pm,v 1.9 2004/12/13 15:55:33 tvignaud Exp $



use c::stuff;
use MDK::Common;

sub AUTOLOAD() {
    $AUTOLOAD =~ /::(.*)/ or return;
    my @l = eval { &{$c::stuff::{$1}} };
    if (my $err = $@) {
	$err =~ /Undefined subroutine &main::/ ?
	  die("can not find function $AUTOLOAD\n" . backtrace()) :
	  die($err);	
    }
    wantarray() ? @l : $l[0];
}

1;
