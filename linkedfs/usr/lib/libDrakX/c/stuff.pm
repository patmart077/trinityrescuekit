package c::stuff; # $Id: stuff.pm,v 1.13 2002/11/27 20:50:12 prigaux Exp $




require DynaLoader;

@ISA = qw(DynaLoader);
$VERSION = '0.01';
# perl_checker: EXPORT-ALL

c::stuff->bootstrap($VERSION);

sub from_utf8 { iconv($_[0], "utf-8", standard_charset()) }
sub to_utf8 { iconv($_[0], standard_charset(), "utf-8") }

1;
