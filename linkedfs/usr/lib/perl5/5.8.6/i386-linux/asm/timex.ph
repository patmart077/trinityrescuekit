require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASMi386_TIMEX_H)) {
    eval 'sub _ASMi386_TIMEX_H () {1;}' unless defined(&_ASMi386_TIMEX_H);
    require 'asm/processor.ph';
    if(defined(&CONFIG_X86_ELAN)) {
	eval 'sub CLOCK_TICK_RATE () {1189200;}' unless defined(&CLOCK_TICK_RATE);
    } else {
	eval 'sub CLOCK_TICK_RATE () {1193182;}' unless defined(&CLOCK_TICK_RATE);
    }
    if(defined(&__KERNEL__)) {
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub get_cycles {
	    local($void) = @_;
    	    eval q({ my $ret=0;  $ret; });
	}' unless defined(&get_cycles);
    }
}
1;
