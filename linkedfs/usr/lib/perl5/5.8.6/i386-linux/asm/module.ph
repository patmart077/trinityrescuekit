require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_I386_MODULE_H)) {
    eval 'sub _ASM_I386_MODULE_H () {1;}' unless defined(&_ASM_I386_MODULE_H);
    eval 'sub Elf_Shdr () { &Elf32_Shdr;}' unless defined(&Elf_Shdr);
    eval 'sub Elf_Sym () { &Elf32_Sym;}' unless defined(&Elf_Sym);
    eval 'sub Elf_Ehdr () { &Elf32_Ehdr;}' unless defined(&Elf_Ehdr);
    if(defined(&__KERNEL__)) {
	if(defined(&CONFIG_M386)) {
	    eval 'sub MODULE_PROC_FAMILY () {"386 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_M486) ? &CONFIG_M486 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"486 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_M586) ? &CONFIG_M586 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"586 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_M586TSC) ? &CONFIG_M586TSC : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"586TSC ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_M586MMX) ? &CONFIG_M586MMX : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"586MMX ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_M686) ? &CONFIG_M686 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"686 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MPENTIUMII) ? &CONFIG_MPENTIUMII : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"PENTIUMII ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MPENTIUMIII) ? &CONFIG_MPENTIUMIII : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"PENTIUMIII ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MPENTIUMM) ? &CONFIG_MPENTIUMM : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"PENTIUMM ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MPENTIUM4) ? &CONFIG_MPENTIUM4 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"PENTIUM4 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MK6) ? &CONFIG_MK6 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"K6 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MK7) ? &CONFIG_MK7 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"K7 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MK8) ? &CONFIG_MK8 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"K8 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_X86_ELAN) ? &CONFIG_X86_ELAN : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"ELAN ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MCRUSOE) ? &CONFIG_MCRUSOE : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"CRUSOE ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MEFFICEON) ? &CONFIG_MEFFICEON : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"EFFICEON ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MWINCHIPC6) ? &CONFIG_MWINCHIPC6 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"WINCHIPC6 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MWINCHIP2) ? &CONFIG_MWINCHIP2 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"WINCHIP2 ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MWINCHIP3D) ? &CONFIG_MWINCHIP3D : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"WINCHIP3D ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MCYRIXIII) ? &CONFIG_MCYRIXIII : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"CYRIXIII ";}' unless defined(&MODULE_PROC_FAMILY);
	}
 elsif(defined (defined(&CONFIG_MVIAC3_2) ? &CONFIG_MVIAC3_2 : 0)) {
	    eval 'sub MODULE_PROC_FAMILY () {"VIAC3-2 ";}' unless defined(&MODULE_PROC_FAMILY);
	} else {
	    die("unknown\ processor\ family");
	}
	if(defined(&CONFIG_REGPARM)) {
	    eval 'sub MODULE_REGPARM () {"REGPARM ";}' unless defined(&MODULE_REGPARM);
	} else {
	    eval 'sub MODULE_REGPARM () {"";}' unless defined(&MODULE_REGPARM);
	}
	if(defined(&CONFIG_4KSTACKS)) {
	    eval 'sub MODULE_STACKSIZE () {"4KSTACKS ";}' unless defined(&MODULE_STACKSIZE);
	} else {
	    eval 'sub MODULE_STACKSIZE () {"";}' unless defined(&MODULE_STACKSIZE);
	}
	eval 'sub MODULE_ARCH_VERMAGIC () { &MODULE_PROC_FAMILY  &MODULE_REGPARM  &MODULE_STACKSIZE;}' unless defined(&MODULE_ARCH_VERMAGIC);
    }
}
1;
