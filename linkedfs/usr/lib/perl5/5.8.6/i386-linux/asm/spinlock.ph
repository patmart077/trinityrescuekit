require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_SPINLOCK_H)) {
    eval 'sub __ASM_SPINLOCK_H () {1;}' unless defined(&__ASM_SPINLOCK_H);
    require 'asm/atomic.ph';
    require 'asm/rwlock.ph';
    require 'asm/page.ph';
    require 'linux/config.ph';
    require 'linux/compiler.ph';
    if(defined(&CONFIG_DEBUG_SPINLOCK)) {
    }
    eval 'sub SPINLOCK_MAGIC () {0xdead4ead;}' unless defined(&SPINLOCK_MAGIC);
    if(defined(&CONFIG_DEBUG_SPINLOCK)) {
	eval 'sub SPINLOCK_MAGIC_INIT () {,  &SPINLOCK_MAGIC;}' unless defined(&SPINLOCK_MAGIC_INIT);
    } else {
	eval 'sub SPINLOCK_MAGIC_INIT () {1;}' unless defined(&SPINLOCK_MAGIC_INIT);
    }
    eval 'sub SPIN_LOCK_UNLOCKED () {( &spinlock_t) { 1 &SPINLOCK_MAGIC_INIT };}' unless defined(&SPIN_LOCK_UNLOCKED);
    eval 'sub spin_lock_init {
        local($x) = @_;
	    eval q( &do { *($x) =  &SPIN_LOCK_UNLOCKED; }  &while(0));
    }' unless defined(&spin_lock_init);
    eval 'sub spin_is_locked {
        local($x) = @_;
	    eval q((*( &volatile \'signed char\' *)(($x)-> &lock) <= 0));
    }' unless defined(&spin_is_locked);
    eval 'sub spin_unlock_wait {
        local($x) = @_;
	    eval q( &do {  &barrier(); }  &while( &spin_is_locked($x)));
    }' unless defined(&spin_unlock_wait);
    eval 'sub spin_lock_string () {"\\n1:\\t" "lock ; decb %0\\n\\t" "jns 3f\\n" "2:\\t" "rep;nop\\n\\t" "cmpb $0,%0\\n\\t" "jle 2b\\n\\t" "jmp 1b\\n" "3:\\n\\t";}' unless defined(&spin_lock_string);
    eval 'sub spin_lock_string_flags () {"\\n1:\\t" "lock ; decb %0\\n\\t" "jns 4f\\n\\t" "2:\\t" "testl $0x200, %1\\n\\t" "jz 3f\\n\\t" "sti\\n\\t" "3:\\t" "rep;nop\\n\\t" "cmpb $0, %0\\n\\t" "jle 3b\\n\\t" "cli\\n\\t" "jmp 1b\\n" "4:\\n\\t";}' unless defined(&spin_lock_string_flags);
    if(!defined( &CONFIG_X86_OOSTORE)  && !defined( &CONFIG_X86_PPRO_FENCE)) {
	eval 'sub spin_unlock_string () {"movb $1,%0" :"=m" ( ($lock->{lock})) : : "memory";}' unless defined(&spin_unlock_string);
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub _raw_spin_unlock {
	    local($lock) = @_;
    	    eval q({  &__asm__  &__volatile__(  &spin_unlock_string ); });
	}' unless defined(&_raw_spin_unlock);
    } else {
	eval 'sub spin_unlock_string () {"xchgb %b0, %1" :"=q" ( &oldval), "=m" ( ($lock->{lock})) :"0" ( &oldval) : "memory";}' unless defined(&spin_unlock_string);
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub _raw_spin_unlock {
	    local($lock) = @_;
    	    eval q({ \'char\'  &oldval = 1;  &__asm__  &__volatile__(  &spin_unlock_string ); });
	}' unless defined(&_raw_spin_unlock);
    }
    eval 'sub _raw_spin_trylock {
        local($lock) = @_;
	    eval q({ \'char\'  &oldval;  &__asm__  &__volatile__( \\"xchgb %b0,%1\\" :\\"=q\\" ( &oldval), \\"=m\\" ( ($lock->{lock})) :\\"0\\" (0) : \\"memory\\");  &oldval > 0; });
    }' unless defined(&_raw_spin_trylock);
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub _raw_spin_lock {
        local($lock) = @_;
	    eval q({  &__asm__  &__volatile__(  &spin_lock_string :\\"=m\\" ( ($lock->{lock})) : : \\"memory\\"); });
    }' unless defined(&_raw_spin_lock);
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub _raw_spin_lock_flags {
        local($lock,$flags) = @_;
	    eval q({  &__asm__  &__volatile__(  &spin_lock_string_flags :\\"=m\\" ( ($lock->{lock})) : \\"r\\" ($flags) : \\"memory\\"); });
    }' unless defined(&_raw_spin_lock_flags);
    if(defined(&CONFIG_DEBUG_SPINLOCK)) {
    }
    eval 'sub RWLOCK_MAGIC () {0xdeaf1eed;}' unless defined(&RWLOCK_MAGIC);
    if(defined(&CONFIG_DEBUG_SPINLOCK)) {
	eval 'sub RWLOCK_MAGIC_INIT () {,  &RWLOCK_MAGIC;}' unless defined(&RWLOCK_MAGIC_INIT);
    } else {
	eval 'sub RWLOCK_MAGIC_INIT () {1;}' unless defined(&RWLOCK_MAGIC_INIT);
    }
    eval 'sub RW_LOCK_UNLOCKED () {( &rwlock_t) {  &RW_LOCK_BIAS  &RWLOCK_MAGIC_INIT };}' unless defined(&RW_LOCK_UNLOCKED);
    eval 'sub rwlock_init {
        local($x) = @_;
	    eval q( &do { *($x) =  &RW_LOCK_UNLOCKED; }  &while(0));
    }' unless defined(&rwlock_init);
    eval 'sub rwlock_is_locked {
        local($x) = @_;
	    eval q((($x)-> &lock !=  &RW_LOCK_BIAS));
    }' unless defined(&rwlock_is_locked);
    if(defined(&CONFIG_DEBUG_SPINLOCK)) {
    }
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub _raw_write_lock {
        local($rw) = @_;
	    eval q({  &__build_write_lock($rw, \\"__write_lock_failed\\"); });
    }' unless defined(&_raw_write_lock);
    eval 'sub _raw_read_unlock {
        local($rw) = @_;
	    eval q( &asm  &volatile(\\"lock ; incl %0\\" :\\"=m\\" (($rw)-> &lock) : : \\"memory\\"));
    }' unless defined(&_raw_read_unlock);
    eval 'sub _raw_write_unlock {
        local($rw) = @_;
	    eval q( &asm  &volatile(\\"lock ; addl $\\"  &RW_LOCK_BIAS_STR \\",%0\\":\\"=m\\" (($rw)-> &lock) : : \\"memory\\"));
    }' unless defined(&_raw_write_unlock);
    eval 'sub _raw_write_trylock {
        local($lock) = @_;
	    eval q({  &atomic_t * &count = ( &atomic_t *)$lock;  &if ( &atomic_sub_and_test( &RW_LOCK_BIAS,  &count)) 1;  &atomic_add( &RW_LOCK_BIAS,  &count); 0; });
    }' unless defined(&_raw_write_trylock);
}
1;
