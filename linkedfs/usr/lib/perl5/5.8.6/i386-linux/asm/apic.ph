require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_APIC_H)) {
    eval 'sub __ASM_APIC_H () {1;}' unless defined(&__ASM_APIC_H);
    require 'linux/config.ph';
    require 'linux/pm.ph';
    require 'asm/fixmap.ph';
    require 'asm/apicdef.ph';
    require 'asm/system.ph';
    eval 'sub Dprintk () {( &x...);}' unless defined(&Dprintk);
    eval 'sub APIC_QUIET () {0;}' unless defined(&APIC_QUIET);
    eval 'sub APIC_VERBOSE () {1;}' unless defined(&APIC_VERBOSE);
    eval 'sub APIC_DEBUG () {2;}' unless defined(&APIC_DEBUG);
    eval 'sub apic_printk () {( &v,  &s,  &a...)  &do {  &if (( &v) <=  &apic_verbosity)  &printk( &s,  &a); }  &while (0);}' unless defined(&apic_printk);
    if(defined(&CONFIG_X86_LOCAL_APIC)) {
	eval 'sub apic_write_atomic {
	    local($reg,$v) = @_;
    	    eval q({  &xchg(( &volatile \'unsigned long\' *)( &APIC_BASE+$reg), $v); });
	}' unless defined(&apic_write_atomic);
	eval 'sub apic_read {
	    local($reg) = @_;
    	    eval q({ *(( &volatile \'unsigned long\' *)( &APIC_BASE+$reg)); });
	}' unless defined(&apic_read);
	eval 'sub apic_wait_icr_idle {
	    local($void) = @_;
    	    eval q({  &while (  &apic_read(  &APIC_ICR ) &  &APIC_ICR_BUSY )  &cpu_relax(); });
	}' unless defined(&apic_wait_icr_idle);
	if(defined(&CONFIG_X86_GOOD_APIC)) {
	    eval 'sub FORCE_READ_AROUND_WRITE () {0;}' unless defined(&FORCE_READ_AROUND_WRITE);
	    eval 'sub apic_read_around {
	        local($x) = @_;
    		eval q();
	    }' unless defined(&apic_read_around);
	    eval 'sub apic_write_around {
	        local($x,$y) = @_;
    		eval q( &apic_write(($x),($y)));
	    }' unless defined(&apic_write_around);
	} else {
	    eval 'sub FORCE_READ_AROUND_WRITE () {1;}' unless defined(&FORCE_READ_AROUND_WRITE);
	    eval 'sub apic_read_around {
	        local($x) = @_;
    		eval q( &apic_read($x));
	    }' unless defined(&apic_read_around);
	    eval 'sub apic_write_around {
	        local($x,$y) = @_;
    		eval q( &apic_write_atomic(($x),($y)));
	    }' unless defined(&apic_write_around);
	}
	eval 'sub ack_APIC_irq {
	    local($void) = @_;
    	    eval q({  &apic_write_around( &APIC_EOI, 0); });
	}' unless defined(&ack_APIC_irq);
	eval 'sub NMI_NONE () {0;}' unless defined(&NMI_NONE);
	eval 'sub NMI_IO_APIC () {1;}' unless defined(&NMI_IO_APIC);
	eval 'sub NMI_LOCAL_APIC () {2;}' unless defined(&NMI_LOCAL_APIC);
	eval 'sub NMI_INVALID () {3;}' unless defined(&NMI_INVALID);
    } else {
    }
}
1;
