require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_STAT_H)) {
    eval 'sub _I386_STAT_H () {1;}' unless defined(&_I386_STAT_H);
    eval 'sub STAT64_HAS_BROKEN_ST_INO () {1;}' unless defined(&STAT64_HAS_BROKEN_ST_INO);
    eval 'sub STAT_HAVE_NSEC () {1;}' unless defined(&STAT_HAVE_NSEC);
}
1;
