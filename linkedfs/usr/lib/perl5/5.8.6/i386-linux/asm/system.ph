require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_SYSTEM_H)) {
    eval 'sub __ASM_SYSTEM_H () {1;}' unless defined(&__ASM_SYSTEM_H);
    require 'linux/kernel.ph';
    require 'asm/segment.ph';
    require 'asm/cpufeature.ph';
    require 'linux/bitops.ph';
    if(defined(&__KERNEL__)) {
	eval 'sub switch_to {
	    local($prev,$next,$last) = @_;
    	    eval q( &do { \'unsigned long esi\', &edi;  &asm  &volatile(\\"pushfl\\\\n\\\\t\\" \\"pushl %%ebp\\\\n\\\\t\\" \\"movl %%esp,%0\\\\n\\\\t\\" \\"movl %5,%%esp\\\\n\\\\t\\" \\"movl $1f,%1\\\\n\\\\t\\" \\"pushl %6\\\\n\\\\t\\" \\"jmp __switch_to\\\\n\\" \\"1:\\\\t\\" \\"popl %%ebp\\\\n\\\\t\\" \\"popfl\\" :\\"=m\\" ( ($prev->{thread}->{esp})),\\"=m\\" ( ($prev->{thread}->{eip})), \\"=a\\" ($last),\\"=S\\" ( &esi),\\"=D\\" ( &edi) :\\"m\\" ( ($next->{thread}->{esp})),\\"m\\" ( ($next->{thread}->{eip})), \\"2\\" ($prev), \\"d\\" ($next)); }  &while (0));
	}' unless defined(&switch_to);
	eval 'sub _set_base {
	    local($addr,$base) = @_;
    	    eval q( &do { \'unsigned long __pr\';  &__asm__  &__volatile__ (\\"movw %%dx,%1\\\\n\\\\t\\" \\"rorl $16,%%edx\\\\n\\\\t\\" \\"movb %%dl,%2\\\\n\\\\t\\" \\"movb %%dh,%3\\" :\\"=&d\\" ( &__pr) :\\"m\\" (*(($addr)+2)), \\"m\\" (*(($addr)+4)), \\"m\\" (*(($addr)+7)), \\"0\\" ($base) ); }  &while(0));
	}' unless defined(&_set_base);
	eval 'sub _set_limit {
	    local($addr,$limit) = @_;
    	    eval q( &do { \'unsigned long __lr\';  &__asm__  &__volatile__ (\\"movw %%dx,%1\\\\n\\\\t\\" \\"rorl $16,%%edx\\\\n\\\\t\\" \\"movb %2,%%dh\\\\n\\\\t\\" \\"andb $0xf0,%%dh\\\\n\\\\t\\" \\"orb %%dh,%%dl\\\\n\\\\t\\" \\"movb %%dl,%2\\" :\\"=&d\\" ( &__lr) :\\"m\\" (*($addr)), \\"m\\" (*(($addr)+6)), \\"0\\" ($limit) ); }  &while(0));
	}' unless defined(&_set_limit);
	eval 'sub set_base {
	    local($ldt,$base) = @_;
    	    eval q( &_set_base( (($ldt)) , ($base) ));
	}' unless defined(&set_base);
	eval 'sub set_limit {
	    local($ldt,$limit) = @_;
    	    eval q( &_set_limit( (($ldt)) , (($limit)-1)>>12));
	}' unless defined(&set_limit);
	eval 'sub _get_base {
	    local($addr) = @_;
    	    eval q({ my $__base;  &__asm__(\\"movb %3,%%dh\\\\n\\\\t\\" \\"movb %2,%%dl\\\\n\\\\t\\" \\"shll $16,%%edx\\\\n\\\\t\\" \\"movw %1,%%dx\\" :\\"=&d\\" ( $__base) :\\"m\\" (*(($addr)+2)), \\"m\\" (*(($addr)+4)), \\"m\\" (*(($addr)+7)));  $__base; });
	}' unless defined(&_get_base);
	eval 'sub get_base {
	    local($ldt) = @_;
    	    eval q( &_get_base( (($ldt)) ));
	}' unless defined(&get_base);
	eval 'sub loadsegment {
	    local($seg,$value) = @_;
    	    eval q( &asm  &volatile(\\"\\\\n\\" \\"1:\\\\t\\" \\"movl %0,%%\\" $seg \\"\\\\n\\" \\"2:\\\\n\\" \\".section .fixup,\\\\\\"ax\\\\\\"\\\\n\\" \\"3:\\\\t\\" \\"pushl $0\\\\n\\\\t\\" \\"popl %%\\" $seg \\"\\\\n\\\\t\\" \\"jmp 2b\\\\n\\" \\".previous\\\\n\\" \\".section __ex_table,\\\\\\"a\\\\\\"\\\\n\\\\t\\" \\".align 4\\\\n\\\\t\\" \\".long 1b,3b\\\\n\\" \\".previous\\" : :\\"m\\" (*($value))));
	}' unless defined(&loadsegment);
	eval 'sub savesegment {
	    local($seg, $value) = @_;
    	    eval q( &asm  &volatile(\\"movl %%\\" $seg \\",%0\\":\\"=m\\" (*($value))));
	}' unless defined(&savesegment);
	eval 'sub clts () {
	    eval q( &__asm__  &__volatile__ (\\"clts\\"));
	}' unless defined(&clts);
	eval 'sub read_cr0 () {
	    eval q(({ \'unsigned int __dummy\';  &__asm__( \\"movl %%cr0,%0\\\\n\\\\t\\" :\\"=r\\" ( &__dummy));  &__dummy; }));
	}' unless defined(&read_cr0);
	eval 'sub write_cr0 {
	    local($x) = @_;
    	    eval q( &__asm__(\\"movl %0,%%cr0\\": :\\"r\\" ($x)););
	}' unless defined(&write_cr0);
	eval 'sub read_cr4 () {
	    eval q(({ \'unsigned int __dummy\';  &__asm__( \\"movl %%cr4,%0\\\\n\\\\t\\" :\\"=r\\" ( &__dummy));  &__dummy; }));
	}' unless defined(&read_cr4);
	eval 'sub write_cr4 {
	    local($x) = @_;
    	    eval q( &__asm__(\\"movl %0,%%cr4\\": :\\"r\\" ($x)););
	}' unless defined(&write_cr4);
	eval 'sub stts () {
	    eval q( &write_cr0(8|  &read_cr0()));
	}' unless defined(&stts);
    }
    eval 'sub wbinvd () {
        eval q( &__asm__  &__volatile__ (\\"wbinvd\\": : :\\"memory\\"););
    }' unless defined(&wbinvd);
    eval 'sub get_limit {
        local($segment) = @_;
	    eval q({ my $__limit;  &__asm__(\\"lsll %1,%0\\" :\\"=r\\" ( $__limit):\\"r\\" ($segment));  $__limit+1; });
    }' unless defined(&get_limit);
    eval 'sub nop () {
        eval q( &__asm__  &__volatile__ (\\"nop\\"));
    }' unless defined(&nop);
    eval 'sub xchg {
        local($ptr,$v) = @_;
	    eval q((( &__typeof__(*($ptr))) &__xchg(($v),($ptr),$sizeof{($ptr)})));
    }' unless defined(&xchg);
    eval 'sub tas {
        local($ptr) = @_;
	    eval q(( &xchg(($ptr),1)));
    }' unless defined(&tas);
    eval 'sub __xg {
        local($x) = @_;
	    eval q((($x)));
    }' unless defined(&__xg);
    eval 'sub __set_64bit_constant {
        local($ptr,$value) = @_;
	    eval q({  &__set_64bit($ptr,($value), (($value)>>32)); });
    }' unless defined(&__set_64bit_constant);
    eval 'sub ll_low {
        local($x) = @_;
	    eval q(*((($x))+0));
    }' unless defined(&ll_low);
    eval 'sub ll_high {
        local($x) = @_;
	    eval q(*((($x))+1));
    }' unless defined(&ll_high);
    eval 'sub __set_64bit_var {
        local($ptr,$value) = @_;
	    eval q({  &__set_64bit($ptr, &ll_low($value),  &ll_high($value)); });
    }' unless defined(&__set_64bit_var);
    eval 'sub set_64bit {
        local($ptr,$value) = @_;
	    eval q(( &__builtin_constant_p($value) ?  &__set_64bit_constant($ptr, $value) :  &__set_64bit_var($ptr, $value) ));
    }' unless defined(&set_64bit);
    eval 'sub _set_64bit {
        local($ptr,$value) = @_;
	    eval q(( &__builtin_constant_p($value) ?  &__set_64bit($ptr, ($value), (($value)>>32) ) :  &__set_64bit($ptr,  &ll_low($value),  &ll_high($value)) ));
    }' unless defined(&_set_64bit);
    if(defined(&CONFIG_X86_CMPXCHG)) {
	eval 'sub __HAVE_ARCH_CMPXCHG () {1;}' unless defined(&__HAVE_ARCH_CMPXCHG);
    }
    eval 'sub __cmpxchg {
        local($ptr,$old,$new,$size) = @_;
	    eval q({ my $prev;  &switch ($size) {  &case 1:  &__asm__  &__volatile__( &LOCK_PREFIX \\"cmpxchgb %b1,%2\\" : \\"=a\\"( $prev) : \\"q\\"($new), \\"m\\"(* &__xg($ptr)), \\"0\\"($old) : \\"memory\\");  $prev;  &case 2:  &__asm__  &__volatile__( &LOCK_PREFIX \\"cmpxchgw %w1,%2\\" : \\"=a\\"( $prev) : \\"q\\"($new), \\"m\\"(* &__xg($ptr)), \\"0\\"($old) : \\"memory\\");  $prev;  &case 4:  &__asm__  &__volatile__( &LOCK_PREFIX \\"cmpxchgl %1,%2\\" : \\"=a\\"( $prev) : \\"q\\"($new), \\"m\\"(* &__xg($ptr)), \\"0\\"($old) : \\"memory\\");  $prev; } $old; });
    }' unless defined(&__cmpxchg);
    eval 'sub cmpxchg {
        local($ptr,$o,$n) = @_;
	    eval q((( &__typeof__(*($ptr))) &__cmpxchg(($ptr),($o), ($n),$sizeof{($ptr)})));
    }' unless defined(&cmpxchg);
    if(defined(&__KERNEL__)) {
    }
    eval 'sub alternative {
        local($oldinstr, $newinstr, $feature) = @_;
	    eval q( &asm  &volatile (\\"661:\\\\n\\\\t\\" $oldinstr \\"\\\\n662:\\\\n\\" \\".section .altinstructions,\\\\\\"a\\\\\\"\\\\n\\" \\"  .align 4\\\\n\\" \\"  .long 661b\\\\n\\" \\"  .long 663f\\\\n\\" \\"  .byte %c0\\\\n\\" \\"  .byte 662b-661b\\\\n\\" \\"  .byte 664f-663f\\\\n\\" \\".previous\\\\n\\" \\".section .altinstr_replacement,\\\\\\"ax\\\\\\"\\\\n\\" \\"663:\\\\n\\\\t\\" $newinstr \\"\\\\n664:\\\\n\\" \\".previous\\" :: \\"i\\" ($feature) : \\"memory\\"));
    }' unless defined(&alternative);
    eval 'sub alternative_input () {( &oldinstr,  &newinstr,  &feature,  &input...)  &asm  &volatile ("661:\\n\\t"  &oldinstr "\\n662:\\n" ".section .altinstructions,\\"a\\"\\n" "  .align 4\\n" "  .long 661b\\n" "  .long 663f\\n" "  .byte %c0\\n" "  .byte 662b-661b\\n" "  .byte 664f-663f\\n" ".previous\\n" ".section .altinstr_replacement,\\"ax\\"\\n" "663:\\n\\t"  &newinstr "\\n664:\\n" ".previous" :: "i" ( &feature),  &input);}' unless defined(&alternative_input);
    eval 'sub mb () {
        eval q( &alternative(\\"lock; addl $0,0(%%esp)\\", \\"mfence\\",  &X86_FEATURE_XMM2));
    }' unless defined(&mb);
    eval 'sub rmb () {
        eval q( &alternative(\\"lock; addl $0,0(%%esp)\\", \\"lfence\\",  &X86_FEATURE_XMM2));
    }' unless defined(&rmb);
    eval 'sub read_barrier_depends () {
        eval q( &do { }  &while(0));
    }' unless defined(&read_barrier_depends);
    if(defined(&CONFIG_X86_OOSTORE)) {
	eval 'sub wmb () {
	    eval q( &alternative(\\"lock; addl $0,0(%%esp)\\", \\"sfence\\",  &X86_FEATURE_XMM));
	}' unless defined(&wmb);
    } else {
	eval 'sub wmb () {
	    eval q( &__asm__  &__volatile__ (\\"\\": : :\\"memory\\"));
	}' unless defined(&wmb);
    }
    if(defined(&CONFIG_SMP)) {
	eval 'sub smp_mb () {
	    eval q( &mb());
	}' unless defined(&smp_mb);
	eval 'sub smp_rmb () {
	    eval q( &rmb());
	}' unless defined(&smp_rmb);
	eval 'sub smp_wmb () {
	    eval q( &wmb());
	}' unless defined(&smp_wmb);
	eval 'sub smp_read_barrier_depends () {
	    eval q( &read_barrier_depends());
	}' unless defined(&smp_read_barrier_depends);
	eval 'sub set_mb {
	    local($var, $value) = @_;
    	    eval q( &do {  &xchg($var, $value); }  &while (0));
	}' unless defined(&set_mb);
    } else {
	eval 'sub smp_mb () {
	    eval q( &barrier());
	}' unless defined(&smp_mb);
	eval 'sub smp_rmb () {
	    eval q( &barrier());
	}' unless defined(&smp_rmb);
	eval 'sub smp_wmb () {
	    eval q( &barrier());
	}' unless defined(&smp_wmb);
	eval 'sub smp_read_barrier_depends () {
	    eval q( &do { }  &while(0));
	}' unless defined(&smp_read_barrier_depends);
	eval 'sub set_mb {
	    local($var, $value) = @_;
    	    eval q( &do { $var = $value;  &barrier(); }  &while (0));
	}' unless defined(&set_mb);
    }
    eval 'sub set_wmb {
        local($var, $value) = @_;
	    eval q( &do { $var = $value;  &wmb(); }  &while (0));
    }' unless defined(&set_wmb);
    eval 'sub local_save_flags {
        local($x) = @_;
	    eval q( &do {  &typecheck(\'unsigned long\',$x);  &__asm__  &__volatile__(\\"pushfl ; popl %0\\":\\"=g\\" ($x): ); }  &while (0));
    }' unless defined(&local_save_flags);
    eval 'sub local_irq_restore {
        local($x) = @_;
	    eval q( &do {  &typecheck(\'unsigned long\',$x);  &__asm__  &__volatile__(\\"pushl %0 ; popfl\\": :\\"g\\" ($x):\\"memory\\", \\"cc\\"); }  &while (0));
    }' unless defined(&local_irq_restore);
    eval 'sub local_irq_disable () {
        eval q( &__asm__  &__volatile__(\\"cli\\": : :\\"memory\\"));
    }' unless defined(&local_irq_disable);
    eval 'sub local_irq_enable () {
        eval q( &__asm__  &__volatile__(\\"sti\\": : :\\"memory\\"));
    }' unless defined(&local_irq_enable);
    eval 'sub safe_halt () {
        eval q( &__asm__  &__volatile__(\\"sti; hlt\\": : :\\"memory\\"));
    }' unless defined(&safe_halt);
    eval 'sub irqs_disabled () {
        eval q(({ \'unsigned long flags\';  &local_save_flags( &flags); !( &flags & (1<<9)); }));
    }' unless defined(&irqs_disabled);
    eval 'sub local_irq_save {
        local($x) = @_;
	    eval q( &__asm__  &__volatile__(\\"pushfl ; popl %0 ; cli\\":\\"=g\\" ($x): :\\"memory\\"));
    }' unless defined(&local_irq_save);
    eval 'sub HAVE_DISABLE_HLT () {1;}' unless defined(&HAVE_DISABLE_HLT);
}
1;
