require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__I386_DIV64)) {
    eval 'sub __I386_DIV64 () {1;}' unless defined(&__I386_DIV64);
    eval 'sub do_div {
        local($n,$base) = @_;
	    eval q(({ \'unsigned long __upper\',  &__low,  &__high,  &__mod,  &__base;  &__base = ($base);  &asm(\\"\\":\\"=a\\" ( &__low), \\"=d\\" ( &__high):\\"A\\" ($n));  &__upper =  &__high;  &if ( &__high) {  &__upper =  &__high % ( &__base);  &__high =  &__high / ( &__base); }  &asm(\\"divl %2\\":\\"=a\\" ( &__low), \\"=d\\" ( &__mod):\\"rm\\" ( &__base), \\"0\\" ( &__low), \\"1\\" ( &__upper));  &asm(\\"\\":\\"=A\\" ($n):\\"a\\" ( &__low),\\"d\\" ( &__high));  &__mod; }));
    }' unless defined(&do_div);
    eval 'sub div_long_long_rem {
        local($a,$b,$c) = @_;
	    eval q( &div_ll_X_l_rem($a,$b,$c));
    }' unless defined(&div_long_long_rem);
1;
