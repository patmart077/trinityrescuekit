require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_IO_APIC_H)) {
    eval 'sub __ASM_IO_APIC_H () {1;}' unless defined(&__ASM_IO_APIC_H);
    require 'linux/config.ph';
    require 'asm/types.ph';
    require 'asm/mpspec.ph';
    if(defined(&CONFIG_X86_IO_APIC)) {
	if(defined(&CONFIG_PCI_MSI)) {
	    eval 'sub startup_level_ioapic () { &startup_level_ioapic_vector;}' unless defined(&startup_level_ioapic);
	    eval 'sub shutdown_level_ioapic () { &mask_IO_APIC_vector;}' unless defined(&shutdown_level_ioapic);
	    eval 'sub enable_level_ioapic () { &unmask_IO_APIC_vector;}' unless defined(&enable_level_ioapic);
	    eval 'sub disable_level_ioapic () { &mask_IO_APIC_vector;}' unless defined(&disable_level_ioapic);
	    eval 'sub mask_and_ack_level_ioapic () { &mask_and_ack_level_ioapic_vector;}' unless defined(&mask_and_ack_level_ioapic);
	    eval 'sub end_level_ioapic () { &end_level_ioapic_vector;}' unless defined(&end_level_ioapic);
	    eval 'sub set_ioapic_affinity () { &set_ioapic_affinity_vector;}' unless defined(&set_ioapic_affinity);
	    eval 'sub startup_edge_ioapic () { &startup_edge_ioapic_vector;}' unless defined(&startup_edge_ioapic);
	    eval 'sub shutdown_edge_ioapic () { &disable_edge_ioapic_vector;}' unless defined(&shutdown_edge_ioapic);
	    eval 'sub enable_edge_ioapic () { &unmask_IO_APIC_vector;}' unless defined(&enable_edge_ioapic);
	    eval 'sub disable_edge_ioapic () { &disable_edge_ioapic_vector;}' unless defined(&disable_edge_ioapic);
	    eval 'sub ack_edge_ioapic () { &ack_edge_ioapic_vector;}' unless defined(&ack_edge_ioapic);
	    eval 'sub end_edge_ioapic () { &end_edge_ioapic_vector;}' unless defined(&end_edge_ioapic);
	} else {
	    eval 'sub startup_level_ioapic () { &startup_level_ioapic_irq;}' unless defined(&startup_level_ioapic);
	    eval 'sub shutdown_level_ioapic () { &mask_IO_APIC_irq;}' unless defined(&shutdown_level_ioapic);
	    eval 'sub enable_level_ioapic () { &unmask_IO_APIC_irq;}' unless defined(&enable_level_ioapic);
	    eval 'sub disable_level_ioapic () { &mask_IO_APIC_irq;}' unless defined(&disable_level_ioapic);
	    eval 'sub mask_and_ack_level_ioapic () { &mask_and_ack_level_ioapic_irq;}' unless defined(&mask_and_ack_level_ioapic);
	    eval 'sub end_level_ioapic () { &end_level_ioapic_irq;}' unless defined(&end_level_ioapic);
	    eval 'sub set_ioapic_affinity () { &set_ioapic_affinity_irq;}' unless defined(&set_ioapic_affinity);
	    eval 'sub startup_edge_ioapic () { &startup_edge_ioapic_irq;}' unless defined(&startup_edge_ioapic);
	    eval 'sub shutdown_edge_ioapic () { &disable_edge_ioapic_irq;}' unless defined(&shutdown_edge_ioapic);
	    eval 'sub enable_edge_ioapic () { &unmask_IO_APIC_irq;}' unless defined(&enable_edge_ioapic);
	    eval 'sub disable_edge_ioapic () { &disable_edge_ioapic_irq;}' unless defined(&disable_edge_ioapic);
	    eval 'sub ack_edge_ioapic () { &ack_edge_ioapic_irq;}' unless defined(&ack_edge_ioapic);
	    eval 'sub end_edge_ioapic () { &end_edge_ioapic_irq;}' unless defined(&end_edge_ioapic);
	}
	eval 'sub IO_APIC_BASE {
	    local($idx) = @_;
    	    eval q((( &volatile \'int\' *)( &__fix_to_virt( &FIX_IO_APIC_BASE_0 + $idx) + ( ($mp_ioapics[$idx]->{mpc_apicaddr}) & ~ &PAGE_MASK))));
	}' unless defined(&IO_APIC_BASE);
	eval("sub dest_Fixed () { 0; }") unless defined(&dest_Fixed);
	eval("sub dest_LowestPrio () { 1; }") unless defined(&dest_LowestPrio);
	eval("sub dest_SMI () { 2; }") unless defined(&dest_SMI);
	eval("sub dest__reserved_1 () { 3; }") unless defined(&dest__reserved_1);
	eval("sub dest_NMI () { 4; }") unless defined(&dest_NMI);
	eval("sub dest_INIT () { 5; }") unless defined(&dest_INIT);
	eval("sub dest__reserved_2 () { 6; }") unless defined(&dest__reserved_2);
	eval("sub dest_ExtINT () { 7; }") unless defined(&dest_ExtINT);
	eval 'sub io_apic_read {
	    local($apic,$reg) = @_;
    	    eval q({ * &IO_APIC_BASE($apic) = $reg; *( &IO_APIC_BASE($apic)+4); });
	}' unless defined(&io_apic_read);
	eval 'sub io_apic_write {
	    local($apic,$reg,$value) = @_;
    	    eval q({ * &IO_APIC_BASE($apic) = $reg; *( &IO_APIC_BASE($apic)+4) = $value; });
	}' unless defined(&io_apic_write);
	eval 'sub io_apic_modify {
	    local($apic,$reg,$value) = @_;
    	    eval q({  &if ( &sis_apic_bug) * &IO_APIC_BASE($apic) = $reg; *( &IO_APIC_BASE($apic)+4) = $value; });
	}' unless defined(&io_apic_modify);
	eval 'sub io_apic_assign_pci_irqs () {( &mp_irq_entries  && ! &skip_ioapic_setup  &&  &io_apic_irqs);}' unless defined(&io_apic_assign_pci_irqs);
	if(defined(&CONFIG_ACPI_BOOT)) {
	}
    } else {
	eval 'sub io_apic_assign_pci_irqs () {0;}' unless defined(&io_apic_assign_pci_irqs);
    }
}
1;
