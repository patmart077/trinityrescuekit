require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ARCH_I386_PERCPU__)) {
    eval 'sub __ARCH_I386_PERCPU__ () {1;}' unless defined(&__ARCH_I386_PERCPU__);
    require 'asm-generic/percpu.ph';
}
1;
