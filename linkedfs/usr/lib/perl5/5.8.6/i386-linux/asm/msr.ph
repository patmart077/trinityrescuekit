require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_MSR_H)) {
    eval 'sub __ASM_MSR_H () {1;}' unless defined(&__ASM_MSR_H);
    eval 'sub rdmsr {
        local($msr,$val1,$val2) = @_;
	    eval q( &__asm__  &__volatile__(\\"rdmsr\\" : \\"=a\\" ($val1), \\"=d\\" ($val2) : \\"c\\" ($msr)));
    }' unless defined(&rdmsr);
    eval 'sub wrmsr {
        local($msr,$val1,$val2) = @_;
	    eval q( &__asm__  &__volatile__(\\"wrmsr\\" : : \\"c\\" ($msr), \\"a\\" ($val1), \\"d\\" ($val2)));
    }' unless defined(&wrmsr);
    eval 'sub rdmsrl {
        local($msr,$val) = @_;
	    eval q( &do { \'unsigned long l__\', &h__;  &rdmsr ($msr,  &l__,  &h__); $val =  &l__; $val |= (( &u64) &h__<<32); }  &while(0));
    }' unless defined(&rdmsrl);
    eval 'sub wrmsrl {
        local($msr,$val) = @_;
	    eval q({ my $lo,  &hi;  $lo =  $val;  &hi = $val >> 32;  &wrmsr ($msr,  $lo,  &hi); });
    }' unless defined(&wrmsrl);
    eval 'sub rdtsc {
        local($low,$high) = @_;
	    eval q( &__asm__  &__volatile__(\\"rdtsc\\" : \\"=a\\" ($low), \\"=d\\" ($high)));
    }' unless defined(&rdtsc);
    eval 'sub rdtscl {
        local($low) = @_;
	    eval q( &__asm__  &__volatile__(\\"rdtsc\\" : \\"=a\\" ($low) : : \\"edx\\"));
    }' unless defined(&rdtscl);
    eval 'sub rdtscll {
        local($val) = @_;
	    eval q( &__asm__  &__volatile__(\\"rdtsc\\" : \\"=A\\" ($val)));
    }' unless defined(&rdtscll);
    eval 'sub write_tsc {
        local($val1,$val2) = @_;
	    eval q( &wrmsr(0x10, $val1, $val2));
    }' unless defined(&write_tsc);
    eval 'sub rdpmc {
        local($counter,$low,$high) = @_;
	    eval q( &__asm__  &__volatile__(\\"rdpmc\\" : \\"=a\\" ($low), \\"=d\\" ($high) : \\"c\\" ($counter)));
    }' unless defined(&rdpmc);
    eval 'sub MSR_IA32_P5_MC_ADDR () {0;}' unless defined(&MSR_IA32_P5_MC_ADDR);
    eval 'sub MSR_IA32_P5_MC_TYPE () {1;}' unless defined(&MSR_IA32_P5_MC_TYPE);
    eval 'sub MSR_IA32_PLATFORM_ID () {0x17;}' unless defined(&MSR_IA32_PLATFORM_ID);
    eval 'sub MSR_IA32_EBL_CR_POWERON () {0x2a;}' unless defined(&MSR_IA32_EBL_CR_POWERON);
    eval 'sub MSR_IA32_APICBASE () {0x1b;}' unless defined(&MSR_IA32_APICBASE);
    eval 'sub MSR_IA32_APICBASE_BSP () {(1<<8);}' unless defined(&MSR_IA32_APICBASE_BSP);
    eval 'sub MSR_IA32_APICBASE_ENABLE () {(1<<11);}' unless defined(&MSR_IA32_APICBASE_ENABLE);
    eval 'sub MSR_IA32_APICBASE_BASE () {(0xfffff<<12);}' unless defined(&MSR_IA32_APICBASE_BASE);
    eval 'sub MSR_IA32_UCODE_WRITE () {0x79;}' unless defined(&MSR_IA32_UCODE_WRITE);
    eval 'sub MSR_IA32_UCODE_REV () {0x8b;}' unless defined(&MSR_IA32_UCODE_REV);
    eval 'sub MSR_P6_PERFCTR0 () {0xc1;}' unless defined(&MSR_P6_PERFCTR0);
    eval 'sub MSR_P6_PERFCTR1 () {0xc2;}' unless defined(&MSR_P6_PERFCTR1);
    eval 'sub MSR_IA32_BBL_CR_CTL () {0x119;}' unless defined(&MSR_IA32_BBL_CR_CTL);
    eval 'sub MSR_IA32_SYSENTER_CS () {0x174;}' unless defined(&MSR_IA32_SYSENTER_CS);
    eval 'sub MSR_IA32_SYSENTER_ESP () {0x175;}' unless defined(&MSR_IA32_SYSENTER_ESP);
    eval 'sub MSR_IA32_SYSENTER_EIP () {0x176;}' unless defined(&MSR_IA32_SYSENTER_EIP);
    eval 'sub MSR_IA32_MCG_CAP () {0x179;}' unless defined(&MSR_IA32_MCG_CAP);
    eval 'sub MSR_IA32_MCG_STATUS () {0x17a;}' unless defined(&MSR_IA32_MCG_STATUS);
    eval 'sub MSR_IA32_MCG_CTL () {0x17b;}' unless defined(&MSR_IA32_MCG_CTL);
    eval 'sub MSR_IA32_MCG_EAX () {0x180;}' unless defined(&MSR_IA32_MCG_EAX);
    eval 'sub MSR_IA32_MCG_EBX () {0x181;}' unless defined(&MSR_IA32_MCG_EBX);
    eval 'sub MSR_IA32_MCG_ECX () {0x182;}' unless defined(&MSR_IA32_MCG_ECX);
    eval 'sub MSR_IA32_MCG_EDX () {0x183;}' unless defined(&MSR_IA32_MCG_EDX);
    eval 'sub MSR_IA32_MCG_ESI () {0x184;}' unless defined(&MSR_IA32_MCG_ESI);
    eval 'sub MSR_IA32_MCG_EDI () {0x185;}' unless defined(&MSR_IA32_MCG_EDI);
    eval 'sub MSR_IA32_MCG_EBP () {0x186;}' unless defined(&MSR_IA32_MCG_EBP);
    eval 'sub MSR_IA32_MCG_ESP () {0x187;}' unless defined(&MSR_IA32_MCG_ESP);
    eval 'sub MSR_IA32_MCG_EFLAGS () {0x188;}' unless defined(&MSR_IA32_MCG_EFLAGS);
    eval 'sub MSR_IA32_MCG_EIP () {0x189;}' unless defined(&MSR_IA32_MCG_EIP);
    eval 'sub MSR_IA32_MCG_RESERVED () {0x18a;}' unless defined(&MSR_IA32_MCG_RESERVED);
    eval 'sub MSR_P6_EVNTSEL0 () {0x186;}' unless defined(&MSR_P6_EVNTSEL0);
    eval 'sub MSR_P6_EVNTSEL1 () {0x187;}' unless defined(&MSR_P6_EVNTSEL1);
    eval 'sub MSR_IA32_PERF_STATUS () {0x198;}' unless defined(&MSR_IA32_PERF_STATUS);
    eval 'sub MSR_IA32_PERF_CTL () {0x199;}' unless defined(&MSR_IA32_PERF_CTL);
    eval 'sub MSR_IA32_THERM_CONTROL () {0x19a;}' unless defined(&MSR_IA32_THERM_CONTROL);
    eval 'sub MSR_IA32_THERM_INTERRUPT () {0x19b;}' unless defined(&MSR_IA32_THERM_INTERRUPT);
    eval 'sub MSR_IA32_THERM_STATUS () {0x19c;}' unless defined(&MSR_IA32_THERM_STATUS);
    eval 'sub MSR_IA32_MISC_ENABLE () {0x1a0;}' unless defined(&MSR_IA32_MISC_ENABLE);
    eval 'sub MSR_IA32_DEBUGCTLMSR () {0x1d9;}' unless defined(&MSR_IA32_DEBUGCTLMSR);
    eval 'sub MSR_IA32_LASTBRANCHFROMIP () {0x1db;}' unless defined(&MSR_IA32_LASTBRANCHFROMIP);
    eval 'sub MSR_IA32_LASTBRANCHTOIP () {0x1dc;}' unless defined(&MSR_IA32_LASTBRANCHTOIP);
    eval 'sub MSR_IA32_LASTINTFROMIP () {0x1dd;}' unless defined(&MSR_IA32_LASTINTFROMIP);
    eval 'sub MSR_IA32_LASTINTTOIP () {0x1de;}' unless defined(&MSR_IA32_LASTINTTOIP);
    eval 'sub MSR_IA32_MC0_CTL () {0x400;}' unless defined(&MSR_IA32_MC0_CTL);
    eval 'sub MSR_IA32_MC0_STATUS () {0x401;}' unless defined(&MSR_IA32_MC0_STATUS);
    eval 'sub MSR_IA32_MC0_ADDR () {0x402;}' unless defined(&MSR_IA32_MC0_ADDR);
    eval 'sub MSR_IA32_MC0_MISC () {0x403;}' unless defined(&MSR_IA32_MC0_MISC);
    eval 'sub MSR_P4_BPU_PERFCTR0 () {0x300;}' unless defined(&MSR_P4_BPU_PERFCTR0);
    eval 'sub MSR_P4_BPU_PERFCTR1 () {0x301;}' unless defined(&MSR_P4_BPU_PERFCTR1);
    eval 'sub MSR_P4_BPU_PERFCTR2 () {0x302;}' unless defined(&MSR_P4_BPU_PERFCTR2);
    eval 'sub MSR_P4_BPU_PERFCTR3 () {0x303;}' unless defined(&MSR_P4_BPU_PERFCTR3);
    eval 'sub MSR_P4_MS_PERFCTR0 () {0x304;}' unless defined(&MSR_P4_MS_PERFCTR0);
    eval 'sub MSR_P4_MS_PERFCTR1 () {0x305;}' unless defined(&MSR_P4_MS_PERFCTR1);
    eval 'sub MSR_P4_MS_PERFCTR2 () {0x306;}' unless defined(&MSR_P4_MS_PERFCTR2);
    eval 'sub MSR_P4_MS_PERFCTR3 () {0x307;}' unless defined(&MSR_P4_MS_PERFCTR3);
    eval 'sub MSR_P4_FLAME_PERFCTR0 () {0x308;}' unless defined(&MSR_P4_FLAME_PERFCTR0);
    eval 'sub MSR_P4_FLAME_PERFCTR1 () {0x309;}' unless defined(&MSR_P4_FLAME_PERFCTR1);
    eval 'sub MSR_P4_FLAME_PERFCTR2 () {0x30a;}' unless defined(&MSR_P4_FLAME_PERFCTR2);
    eval 'sub MSR_P4_FLAME_PERFCTR3 () {0x30b;}' unless defined(&MSR_P4_FLAME_PERFCTR3);
    eval 'sub MSR_P4_IQ_PERFCTR0 () {0x30c;}' unless defined(&MSR_P4_IQ_PERFCTR0);
    eval 'sub MSR_P4_IQ_PERFCTR1 () {0x30d;}' unless defined(&MSR_P4_IQ_PERFCTR1);
    eval 'sub MSR_P4_IQ_PERFCTR2 () {0x30e;}' unless defined(&MSR_P4_IQ_PERFCTR2);
    eval 'sub MSR_P4_IQ_PERFCTR3 () {0x30f;}' unless defined(&MSR_P4_IQ_PERFCTR3);
    eval 'sub MSR_P4_IQ_PERFCTR4 () {0x310;}' unless defined(&MSR_P4_IQ_PERFCTR4);
    eval 'sub MSR_P4_IQ_PERFCTR5 () {0x311;}' unless defined(&MSR_P4_IQ_PERFCTR5);
    eval 'sub MSR_P4_BPU_CCCR0 () {0x360;}' unless defined(&MSR_P4_BPU_CCCR0);
    eval 'sub MSR_P4_BPU_CCCR1 () {0x361;}' unless defined(&MSR_P4_BPU_CCCR1);
    eval 'sub MSR_P4_BPU_CCCR2 () {0x362;}' unless defined(&MSR_P4_BPU_CCCR2);
    eval 'sub MSR_P4_BPU_CCCR3 () {0x363;}' unless defined(&MSR_P4_BPU_CCCR3);
    eval 'sub MSR_P4_MS_CCCR0 () {0x364;}' unless defined(&MSR_P4_MS_CCCR0);
    eval 'sub MSR_P4_MS_CCCR1 () {0x365;}' unless defined(&MSR_P4_MS_CCCR1);
    eval 'sub MSR_P4_MS_CCCR2 () {0x366;}' unless defined(&MSR_P4_MS_CCCR2);
    eval 'sub MSR_P4_MS_CCCR3 () {0x367;}' unless defined(&MSR_P4_MS_CCCR3);
    eval 'sub MSR_P4_FLAME_CCCR0 () {0x368;}' unless defined(&MSR_P4_FLAME_CCCR0);
    eval 'sub MSR_P4_FLAME_CCCR1 () {0x369;}' unless defined(&MSR_P4_FLAME_CCCR1);
    eval 'sub MSR_P4_FLAME_CCCR2 () {0x36a;}' unless defined(&MSR_P4_FLAME_CCCR2);
    eval 'sub MSR_P4_FLAME_CCCR3 () {0x36b;}' unless defined(&MSR_P4_FLAME_CCCR3);
    eval 'sub MSR_P4_IQ_CCCR0 () {0x36c;}' unless defined(&MSR_P4_IQ_CCCR0);
    eval 'sub MSR_P4_IQ_CCCR1 () {0x36d;}' unless defined(&MSR_P4_IQ_CCCR1);
    eval 'sub MSR_P4_IQ_CCCR2 () {0x36e;}' unless defined(&MSR_P4_IQ_CCCR2);
    eval 'sub MSR_P4_IQ_CCCR3 () {0x36f;}' unless defined(&MSR_P4_IQ_CCCR3);
    eval 'sub MSR_P4_IQ_CCCR4 () {0x370;}' unless defined(&MSR_P4_IQ_CCCR4);
    eval 'sub MSR_P4_IQ_CCCR5 () {0x371;}' unless defined(&MSR_P4_IQ_CCCR5);
    eval 'sub MSR_P4_ALF_ESCR0 () {0x3ca;}' unless defined(&MSR_P4_ALF_ESCR0);
    eval 'sub MSR_P4_ALF_ESCR1 () {0x3cb;}' unless defined(&MSR_P4_ALF_ESCR1);
    eval 'sub MSR_P4_BPU_ESCR0 () {0x3b2;}' unless defined(&MSR_P4_BPU_ESCR0);
    eval 'sub MSR_P4_BPU_ESCR1 () {0x3b3;}' unless defined(&MSR_P4_BPU_ESCR1);
    eval 'sub MSR_P4_BSU_ESCR0 () {0x3a0;}' unless defined(&MSR_P4_BSU_ESCR0);
    eval 'sub MSR_P4_BSU_ESCR1 () {0x3a1;}' unless defined(&MSR_P4_BSU_ESCR1);
    eval 'sub MSR_P4_CRU_ESCR0 () {0x3b8;}' unless defined(&MSR_P4_CRU_ESCR0);
    eval 'sub MSR_P4_CRU_ESCR1 () {0x3b9;}' unless defined(&MSR_P4_CRU_ESCR1);
    eval 'sub MSR_P4_CRU_ESCR2 () {0x3cc;}' unless defined(&MSR_P4_CRU_ESCR2);
    eval 'sub MSR_P4_CRU_ESCR3 () {0x3cd;}' unless defined(&MSR_P4_CRU_ESCR3);
    eval 'sub MSR_P4_CRU_ESCR4 () {0x3e0;}' unless defined(&MSR_P4_CRU_ESCR4);
    eval 'sub MSR_P4_CRU_ESCR5 () {0x3e1;}' unless defined(&MSR_P4_CRU_ESCR5);
    eval 'sub MSR_P4_DAC_ESCR0 () {0x3a8;}' unless defined(&MSR_P4_DAC_ESCR0);
    eval 'sub MSR_P4_DAC_ESCR1 () {0x3a9;}' unless defined(&MSR_P4_DAC_ESCR1);
    eval 'sub MSR_P4_FIRM_ESCR0 () {0x3a4;}' unless defined(&MSR_P4_FIRM_ESCR0);
    eval 'sub MSR_P4_FIRM_ESCR1 () {0x3a5;}' unless defined(&MSR_P4_FIRM_ESCR1);
    eval 'sub MSR_P4_FLAME_ESCR0 () {0x3a6;}' unless defined(&MSR_P4_FLAME_ESCR0);
    eval 'sub MSR_P4_FLAME_ESCR1 () {0x3a7;}' unless defined(&MSR_P4_FLAME_ESCR1);
    eval 'sub MSR_P4_FSB_ESCR0 () {0x3a2;}' unless defined(&MSR_P4_FSB_ESCR0);
    eval 'sub MSR_P4_FSB_ESCR1 () {0x3a3;}' unless defined(&MSR_P4_FSB_ESCR1);
    eval 'sub MSR_P4_IQ_ESCR0 () {0x3ba;}' unless defined(&MSR_P4_IQ_ESCR0);
    eval 'sub MSR_P4_IQ_ESCR1 () {0x3bb;}' unless defined(&MSR_P4_IQ_ESCR1);
    eval 'sub MSR_P4_IS_ESCR0 () {0x3b4;}' unless defined(&MSR_P4_IS_ESCR0);
    eval 'sub MSR_P4_IS_ESCR1 () {0x3b5;}' unless defined(&MSR_P4_IS_ESCR1);
    eval 'sub MSR_P4_ITLB_ESCR0 () {0x3b6;}' unless defined(&MSR_P4_ITLB_ESCR0);
    eval 'sub MSR_P4_ITLB_ESCR1 () {0x3b7;}' unless defined(&MSR_P4_ITLB_ESCR1);
    eval 'sub MSR_P4_IX_ESCR0 () {0x3c8;}' unless defined(&MSR_P4_IX_ESCR0);
    eval 'sub MSR_P4_IX_ESCR1 () {0x3c9;}' unless defined(&MSR_P4_IX_ESCR1);
    eval 'sub MSR_P4_MOB_ESCR0 () {0x3aa;}' unless defined(&MSR_P4_MOB_ESCR0);
    eval 'sub MSR_P4_MOB_ESCR1 () {0x3ab;}' unless defined(&MSR_P4_MOB_ESCR1);
    eval 'sub MSR_P4_MS_ESCR0 () {0x3c0;}' unless defined(&MSR_P4_MS_ESCR0);
    eval 'sub MSR_P4_MS_ESCR1 () {0x3c1;}' unless defined(&MSR_P4_MS_ESCR1);
    eval 'sub MSR_P4_PMH_ESCR0 () {0x3ac;}' unless defined(&MSR_P4_PMH_ESCR0);
    eval 'sub MSR_P4_PMH_ESCR1 () {0x3ad;}' unless defined(&MSR_P4_PMH_ESCR1);
    eval 'sub MSR_P4_RAT_ESCR0 () {0x3bc;}' unless defined(&MSR_P4_RAT_ESCR0);
    eval 'sub MSR_P4_RAT_ESCR1 () {0x3bd;}' unless defined(&MSR_P4_RAT_ESCR1);
    eval 'sub MSR_P4_SAAT_ESCR0 () {0x3ae;}' unless defined(&MSR_P4_SAAT_ESCR0);
    eval 'sub MSR_P4_SAAT_ESCR1 () {0x3af;}' unless defined(&MSR_P4_SAAT_ESCR1);
    eval 'sub MSR_P4_SSU_ESCR0 () {0x3be;}' unless defined(&MSR_P4_SSU_ESCR0);
    eval 'sub MSR_P4_SSU_ESCR1 () {0x3bf;}' unless defined(&MSR_P4_SSU_ESCR1);
    eval 'sub MSR_P4_TBPU_ESCR0 () {0x3c2;}' unless defined(&MSR_P4_TBPU_ESCR0);
    eval 'sub MSR_P4_TBPU_ESCR1 () {0x3c3;}' unless defined(&MSR_P4_TBPU_ESCR1);
    eval 'sub MSR_P4_TC_ESCR0 () {0x3c4;}' unless defined(&MSR_P4_TC_ESCR0);
    eval 'sub MSR_P4_TC_ESCR1 () {0x3c5;}' unless defined(&MSR_P4_TC_ESCR1);
    eval 'sub MSR_P4_U2L_ESCR0 () {0x3b0;}' unless defined(&MSR_P4_U2L_ESCR0);
    eval 'sub MSR_P4_U2L_ESCR1 () {0x3b1;}' unless defined(&MSR_P4_U2L_ESCR1);
    eval 'sub MSR_K6_EFER () {0xc0000080;}' unless defined(&MSR_K6_EFER);
    eval 'sub MSR_K6_STAR () {0xc0000081;}' unless defined(&MSR_K6_STAR);
    eval 'sub MSR_K6_WHCR () {0xc0000082;}' unless defined(&MSR_K6_WHCR);
    eval 'sub MSR_K6_UWCCR () {0xc0000085;}' unless defined(&MSR_K6_UWCCR);
    eval 'sub MSR_K6_EPMR () {0xc0000086;}' unless defined(&MSR_K6_EPMR);
    eval 'sub MSR_K6_PSOR () {0xc0000087;}' unless defined(&MSR_K6_PSOR);
    eval 'sub MSR_K6_PFIR () {0xc0000088;}' unless defined(&MSR_K6_PFIR);
    eval 'sub MSR_K7_EVNTSEL0 () {0xc0010000;}' unless defined(&MSR_K7_EVNTSEL0);
    eval 'sub MSR_K7_EVNTSEL1 () {0xc0010001;}' unless defined(&MSR_K7_EVNTSEL1);
    eval 'sub MSR_K7_EVNTSEL2 () {0xc0010002;}' unless defined(&MSR_K7_EVNTSEL2);
    eval 'sub MSR_K7_EVNTSEL3 () {0xc0010003;}' unless defined(&MSR_K7_EVNTSEL3);
    eval 'sub MSR_K7_PERFCTR0 () {0xc0010004;}' unless defined(&MSR_K7_PERFCTR0);
    eval 'sub MSR_K7_PERFCTR1 () {0xc0010005;}' unless defined(&MSR_K7_PERFCTR1);
    eval 'sub MSR_K7_PERFCTR2 () {0xc0010006;}' unless defined(&MSR_K7_PERFCTR2);
    eval 'sub MSR_K7_PERFCTR3 () {0xc0010007;}' unless defined(&MSR_K7_PERFCTR3);
    eval 'sub MSR_K7_HWCR () {0xc0010015;}' unless defined(&MSR_K7_HWCR);
    eval 'sub MSR_K7_CLK_CTL () {0xc001001b;}' unless defined(&MSR_K7_CLK_CTL);
    eval 'sub MSR_K7_FID_VID_CTL () {0xc0010041;}' unless defined(&MSR_K7_FID_VID_CTL);
    eval 'sub MSR_K7_FID_VID_STATUS () {0xc0010042;}' unless defined(&MSR_K7_FID_VID_STATUS);
    eval 'sub MSR_EFER () {0xc0000080;}' unless defined(&MSR_EFER);
    eval 'sub _EFER_NX () {11;}' unless defined(&_EFER_NX);
    eval 'sub EFER_NX () {(1<< &_EFER_NX);}' unless defined(&EFER_NX);
    eval 'sub MSR_IDT_FCR1 () {0x107;}' unless defined(&MSR_IDT_FCR1);
    eval 'sub MSR_IDT_FCR2 () {0x108;}' unless defined(&MSR_IDT_FCR2);
    eval 'sub MSR_IDT_FCR3 () {0x109;}' unless defined(&MSR_IDT_FCR3);
    eval 'sub MSR_IDT_FCR4 () {0x10a;}' unless defined(&MSR_IDT_FCR4);
    eval 'sub MSR_IDT_MCR0 () {0x110;}' unless defined(&MSR_IDT_MCR0);
    eval 'sub MSR_IDT_MCR1 () {0x111;}' unless defined(&MSR_IDT_MCR1);
    eval 'sub MSR_IDT_MCR2 () {0x112;}' unless defined(&MSR_IDT_MCR2);
    eval 'sub MSR_IDT_MCR3 () {0x113;}' unless defined(&MSR_IDT_MCR3);
    eval 'sub MSR_IDT_MCR4 () {0x114;}' unless defined(&MSR_IDT_MCR4);
    eval 'sub MSR_IDT_MCR5 () {0x115;}' unless defined(&MSR_IDT_MCR5);
    eval 'sub MSR_IDT_MCR6 () {0x116;}' unless defined(&MSR_IDT_MCR6);
    eval 'sub MSR_IDT_MCR7 () {0x117;}' unless defined(&MSR_IDT_MCR7);
    eval 'sub MSR_IDT_MCR_CTRL () {0x120;}' unless defined(&MSR_IDT_MCR_CTRL);
    eval 'sub MSR_VIA_FCR () {0x1107;}' unless defined(&MSR_VIA_FCR);
    eval 'sub MSR_VIA_LONGHAUL () {0x110a;}' unless defined(&MSR_VIA_LONGHAUL);
    eval 'sub MSR_VIA_RNG () {0x110b;}' unless defined(&MSR_VIA_RNG);
    eval 'sub MSR_VIA_BCR2 () {0x1147;}' unless defined(&MSR_VIA_BCR2);
    eval 'sub MSR_TMTA_LONGRUN_CTRL () {0x80868010;}' unless defined(&MSR_TMTA_LONGRUN_CTRL);
    eval 'sub MSR_TMTA_LONGRUN_FLAGS () {0x80868011;}' unless defined(&MSR_TMTA_LONGRUN_FLAGS);
    eval 'sub MSR_TMTA_LRTI_READOUT () {0x80868018;}' unless defined(&MSR_TMTA_LRTI_READOUT);
    eval 'sub MSR_TMTA_LRTI_VOLT_MHZ () {0x8086801a;}' unless defined(&MSR_TMTA_LRTI_VOLT_MHZ);
}
1;
