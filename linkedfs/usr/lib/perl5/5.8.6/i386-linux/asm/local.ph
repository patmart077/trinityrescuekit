require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ARCH_I386_LOCAL_H)) {
    eval 'sub _ARCH_I386_LOCAL_H () {1;}' unless defined(&_ARCH_I386_LOCAL_H);
    require 'linux/percpu.ph';
    eval 'sub LOCAL_INIT {
        local($i) = @_;
	    eval q({ ($i) });
    }' unless defined(&LOCAL_INIT);
    eval 'sub local_read {
        local($v) = @_;
	    eval q((($v)-> &counter));
    }' unless defined(&local_read);
    eval 'sub local_set {
        local($v,$i) = @_;
	    eval q(((($v)-> &counter) = ($i)));
    }' unless defined(&local_set);
    eval 'sub local_inc {
        local($v) = @_;
	    eval q({  &__asm__  &__volatile__( \\"incl %0\\" :\\"=m\\" ( ($v->{counter})) :\\"m\\" ( ($v->{counter}))); });
    }' unless defined(&local_inc);
    eval 'sub local_dec {
        local($v) = @_;
	    eval q({  &__asm__  &__volatile__( \\"decl %0\\" :\\"=m\\" ( ($v->{counter})) :\\"m\\" ( ($v->{counter}))); });
    }' unless defined(&local_dec);
    eval 'sub local_add {
        local($i,$v) = @_;
	    eval q({  &__asm__  &__volatile__( \\"addl %1,%0\\" :\\"=m\\" ( ($v->{counter})) :\\"ir\\" ($i), \\"m\\" ( ($v->{counter}))); });
    }' unless defined(&local_add);
    eval 'sub local_sub {
        local($i,$v) = @_;
	    eval q({  &__asm__  &__volatile__( \\"subl %1,%0\\" :\\"=m\\" ( ($v->{counter})) :\\"ir\\" ($i), \\"m\\" ( ($v->{counter}))); });
    }' unless defined(&local_sub);
    eval 'sub __local_inc {
        local($l) = @_;
	    eval q( &local_inc($l));
    }' unless defined(&__local_inc);
    eval 'sub __local_dec {
        local($l) = @_;
	    eval q( &local_dec($l));
    }' unless defined(&__local_dec);
    eval 'sub __local_add {
        local($i,$l) = @_;
	    eval q( &local_add(($i),($l)));
    }' unless defined(&__local_add);
    eval 'sub __local_sub {
        local($i,$l) = @_;
	    eval q( &local_sub(($i),($l)));
    }' unless defined(&__local_sub);
    eval 'sub cpu_local_read {
        local($v) = @_;
	    eval q( &local_read(& &__get_cpu_var($v)));
    }' unless defined(&cpu_local_read);
    eval 'sub cpu_local_set {
        local($v, $i) = @_;
	    eval q( &local_set(& &__get_cpu_var($v), ($i)));
    }' unless defined(&cpu_local_set);
    eval 'sub cpu_local_inc {
        local($v) = @_;
	    eval q( &local_inc(& &__get_cpu_var($v)));
    }' unless defined(&cpu_local_inc);
    eval 'sub cpu_local_dec {
        local($v) = @_;
	    eval q( &local_dec(& &__get_cpu_var($v)));
    }' unless defined(&cpu_local_dec);
    eval 'sub cpu_local_add {
        local($i, $v) = @_;
	    eval q( &local_add(($i), & &__get_cpu_var($v)));
    }' unless defined(&cpu_local_add);
    eval 'sub cpu_local_sub {
        local($i, $v) = @_;
	    eval q( &local_sub(($i), & &__get_cpu_var($v)));
    }' unless defined(&cpu_local_sub);
    eval 'sub __cpu_local_inc {
        local($v) = @_;
	    eval q( &cpu_local_inc($v));
    }' unless defined(&__cpu_local_inc);
    eval 'sub __cpu_local_dec {
        local($v) = @_;
	    eval q( &cpu_local_dec($v));
    }' unless defined(&__cpu_local_dec);
    eval 'sub __cpu_local_add {
        local($i, $v) = @_;
	    eval q( &cpu_local_add(($i), ($v)));
    }' unless defined(&__cpu_local_add);
    eval 'sub __cpu_local_sub {
        local($i, $v) = @_;
	    eval q( &cpu_local_sub(($i), ($v)));
    }' unless defined(&__cpu_local_sub);
}
1;
