require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_SEGMENT_H)) {
    eval 'sub _ASM_SEGMENT_H () {1;}' unless defined(&_ASM_SEGMENT_H);
    eval 'sub GDT_ENTRY_TLS_ENTRIES () {3;}' unless defined(&GDT_ENTRY_TLS_ENTRIES);
    eval 'sub GDT_ENTRY_TLS_MIN () {6;}' unless defined(&GDT_ENTRY_TLS_MIN);
    eval 'sub GDT_ENTRY_TLS_MAX () {( &GDT_ENTRY_TLS_MIN +  &GDT_ENTRY_TLS_ENTRIES - 1);}' unless defined(&GDT_ENTRY_TLS_MAX);
    eval 'sub TLS_SIZE () {( &GDT_ENTRY_TLS_ENTRIES * 8);}' unless defined(&TLS_SIZE);
    eval 'sub GDT_ENTRY_DEFAULT_USER_CS () {14;}' unless defined(&GDT_ENTRY_DEFAULT_USER_CS);
    eval 'sub __USER_CS () {( &GDT_ENTRY_DEFAULT_USER_CS * 8+ 3);}' unless defined(&__USER_CS);
    eval 'sub GDT_ENTRY_DEFAULT_USER_DS () {15;}' unless defined(&GDT_ENTRY_DEFAULT_USER_DS);
    eval 'sub __USER_DS () {( &GDT_ENTRY_DEFAULT_USER_DS * 8+ 3);}' unless defined(&__USER_DS);
    eval 'sub GDT_ENTRY_KERNEL_BASE () {12;}' unless defined(&GDT_ENTRY_KERNEL_BASE);
    eval 'sub GDT_ENTRY_KERNEL_CS () {( &GDT_ENTRY_KERNEL_BASE + 0);}' unless defined(&GDT_ENTRY_KERNEL_CS);
    eval 'sub __KERNEL_CS () {( &GDT_ENTRY_KERNEL_CS * 8);}' unless defined(&__KERNEL_CS);
    eval 'sub GDT_ENTRY_KERNEL_DS () {( &GDT_ENTRY_KERNEL_BASE + 1);}' unless defined(&GDT_ENTRY_KERNEL_DS);
    eval 'sub __KERNEL_DS () {( &GDT_ENTRY_KERNEL_DS * 8);}' unless defined(&__KERNEL_DS);
    eval 'sub GDT_ENTRY_TSS () {( &GDT_ENTRY_KERNEL_BASE + 4);}' unless defined(&GDT_ENTRY_TSS);
    eval 'sub GDT_ENTRY_LDT () {( &GDT_ENTRY_KERNEL_BASE + 5);}' unless defined(&GDT_ENTRY_LDT);
    eval 'sub GDT_ENTRY_PNPBIOS_BASE () {( &GDT_ENTRY_KERNEL_BASE + 6);}' unless defined(&GDT_ENTRY_PNPBIOS_BASE);
    eval 'sub GDT_ENTRY_APMBIOS_BASE () {( &GDT_ENTRY_KERNEL_BASE + 11);}' unless defined(&GDT_ENTRY_APMBIOS_BASE);
    eval 'sub GDT_ENTRY_DOUBLEFAULT_TSS () {31;}' unless defined(&GDT_ENTRY_DOUBLEFAULT_TSS);
    eval 'sub GDT_ENTRIES () {32;}' unless defined(&GDT_ENTRIES);
    eval 'sub GDT_SIZE () {( &GDT_ENTRIES * 8);}' unless defined(&GDT_SIZE);
    eval 'sub GDT_ENTRY_BOOT_CS () {2;}' unless defined(&GDT_ENTRY_BOOT_CS);
    eval 'sub __BOOT_CS () {( &GDT_ENTRY_BOOT_CS * 8);}' unless defined(&__BOOT_CS);
    eval 'sub GDT_ENTRY_BOOT_DS () {( &GDT_ENTRY_BOOT_CS + 1);}' unless defined(&GDT_ENTRY_BOOT_DS);
    eval 'sub __BOOT_DS () {( &GDT_ENTRY_BOOT_DS * 8);}' unless defined(&__BOOT_DS);
    eval 'sub IDT_ENTRIES () {256;}' unless defined(&IDT_ENTRIES);
}
1;
