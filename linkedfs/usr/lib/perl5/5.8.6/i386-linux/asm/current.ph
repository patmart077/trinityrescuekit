require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_CURRENT_H)) {
    eval 'sub _I386_CURRENT_H () {1;}' unless defined(&_I386_CURRENT_H);
    require 'linux/thread_info.ph';
    eval 'sub get_current {
        local($void) = @_;
	    eval q({  &current_thread_info()-> &task; });
    }' unless defined(&get_current);
    eval 'sub current () { &get_current();}' unless defined(&current);
}
1;
