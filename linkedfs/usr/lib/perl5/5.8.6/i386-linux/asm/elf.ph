require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASMi386_ELF_H)) {
    eval 'sub __ASMi386_ELF_H () {1;}' unless defined(&__ASMi386_ELF_H);
    require 'asm/ptrace.ph';
    require 'asm/user.ph';
    require 'asm/processor.ph';
    require 'asm/system.ph';
    require 'linux/utsname.ph';
    eval 'sub R_386_NONE () {0;}' unless defined(&R_386_NONE);
    eval 'sub R_386_32 () {1;}' unless defined(&R_386_32);
    eval 'sub R_386_PC32 () {2;}' unless defined(&R_386_PC32);
    eval 'sub R_386_GOT32 () {3;}' unless defined(&R_386_GOT32);
    eval 'sub R_386_PLT32 () {4;}' unless defined(&R_386_PLT32);
    eval 'sub R_386_COPY () {5;}' unless defined(&R_386_COPY);
    eval 'sub R_386_GLOB_DAT () {6;}' unless defined(&R_386_GLOB_DAT);
    eval 'sub R_386_JMP_SLOT () {7;}' unless defined(&R_386_JMP_SLOT);
    eval 'sub R_386_RELATIVE () {8;}' unless defined(&R_386_RELATIVE);
    eval 'sub R_386_GOTOFF () {9;}' unless defined(&R_386_GOTOFF);
    eval 'sub R_386_GOTPC () {10;}' unless defined(&R_386_GOTPC);
    eval 'sub R_386_NUM () {11;}' unless defined(&R_386_NUM);
    eval 'sub ELF_NGREG () {($sizeof{\'struct user_regs_struct\'} / $sizeof{ &elf_greg_t});}' unless defined(&ELF_NGREG);
    eval 'sub elf_check_arch {
        local($x) = @_;
	    eval q(((($x)-> &e_machine ==  &EM_386) || (($x)-> &e_machine ==  &EM_486)));
    }' unless defined(&elf_check_arch);
    eval 'sub ELF_CLASS () { &ELFCLASS32;}' unless defined(&ELF_CLASS);
    eval 'sub ELF_DATA () { &ELFDATA2LSB;}' unless defined(&ELF_DATA);
    eval 'sub ELF_ARCH () { &EM_386;}' unless defined(&ELF_ARCH);
    eval 'sub ELF_PLAT_INIT {
        local($_r, $load_addr) = @_;
	    eval q( &do {  ($_r->{ebx}) = 0;  ($_r->{ecx}) = 0;  ($_r->{edx}) = 0;  ($_r->{esi}) = 0;  ($_r->{edi}) = 0;  ($_r->{ebp}) = 0;  ($_r->{eax}) = 0; }  &while (0));
    }' unless defined(&ELF_PLAT_INIT);
    eval 'sub USE_ELF_CORE_DUMP () {1;}' unless defined(&USE_ELF_CORE_DUMP);
    eval 'sub ELF_EXEC_PAGESIZE () {4096;}' unless defined(&ELF_EXEC_PAGESIZE);
    eval 'sub ELF_ET_DYN_BASE () {( &TASK_SIZE / 3* 2);}' unless defined(&ELF_ET_DYN_BASE);
    eval 'sub ELF_CORE_COPY_REGS {
        local($pr_reg, $regs) = @_;
	    eval q($pr_reg->[0] =  ($regs->{ebx}); $pr_reg->[1] =  ($regs->{ecx}); $pr_reg->[2] =  ($regs->{edx}); $pr_reg->[3] =  ($regs->{esi}); $pr_reg->[4] =  ($regs->{edi}); $pr_reg->[5] =  ($regs->{ebp}); $pr_reg->[6] =  ($regs->{eax}); $pr_reg->[7] =  ($regs->{xds}); $pr_reg->[8] =  ($regs->{xes});  &savesegment( &fs,$pr_reg->[9]);  &savesegment( &gs,$pr_reg->[10]); $pr_reg->[11] =  ($regs->{orig_eax}); $pr_reg->[12] =  ($regs->{eip}); $pr_reg->[13] =  ($regs->{xcs}); $pr_reg->[14] =  ($regs->{eflags}); $pr_reg->[15] =  ($regs->{esp}); $pr_reg->[16] =  ($regs->{xss}););
    }' unless defined(&ELF_CORE_COPY_REGS);
    eval 'sub ELF_HWCAP () {( ($boot_cpu_data->{x86_capability[0]}));}' unless defined(&ELF_HWCAP);
    eval 'sub ELF_PLATFORM () {( ($system_utsname->{machine}));}' unless defined(&ELF_PLATFORM);
    eval 'sub AT_SYSINFO () {32;}' unless defined(&AT_SYSINFO);
    eval 'sub AT_SYSINFO_EHDR () {33;}' unless defined(&AT_SYSINFO_EHDR);
    if(defined(&__KERNEL__)) {
	eval 'sub SET_PERSONALITY {
	    local($ex, $ibcs2) = @_;
    	    eval q( &do { }  &while (0));
	}' unless defined(&SET_PERSONALITY);
	eval 'sub elf_read_implies_exec {
	    local($ex, $have_pt_gnu_stack) = @_;
    	    eval q((!($have_pt_gnu_stack)));
	}' unless defined(&elf_read_implies_exec);
	eval 'sub ELF_CORE_COPY_TASK_REGS {
	    local($tsk, $elf_regs) = @_;
    	    eval q( &dump_task_regs($tsk, $elf_regs));
	}' unless defined(&ELF_CORE_COPY_TASK_REGS);
	eval 'sub ELF_CORE_COPY_FPREGS {
	    local($tsk, $elf_fpregs) = @_;
    	    eval q( &dump_task_fpu($tsk, $elf_fpregs));
	}' unless defined(&ELF_CORE_COPY_FPREGS);
	eval 'sub ELF_CORE_COPY_XFPREGS {
	    local($tsk, $elf_xfpregs) = @_;
    	    eval q( &dump_task_extended_fpu($tsk, $elf_xfpregs));
	}' unless defined(&ELF_CORE_COPY_XFPREGS);
	eval 'sub VSYSCALL_BASE () {( &__fix_to_virt( &FIX_VSYSCALL));}' unless defined(&VSYSCALL_BASE);
	eval 'sub VSYSCALL_EHDR () {(( &const \'struct elfhdr\' *)  &VSYSCALL_BASE);}' unless defined(&VSYSCALL_EHDR);
	eval 'sub VSYSCALL_ENTRY () {( & &__kernel_vsyscall);}' unless defined(&VSYSCALL_ENTRY);
	eval 'sub ARCH_DLINFO () { &do {  &NEW_AUX_ENT( &AT_SYSINFO,  &VSYSCALL_ENTRY);  &NEW_AUX_ENT( &AT_SYSINFO_EHDR,  &VSYSCALL_BASE); }  &while (0);}' unless defined(&ARCH_DLINFO);
	eval 'sub ELF_CORE_EXTRA_PHDRS () {( ($VSYSCALL_EHDR->{e_phnum}));}' unless defined(&ELF_CORE_EXTRA_PHDRS);
	eval 'sub ELF_CORE_WRITE_EXTRA_PHDRS () { &do {  &const \'struct elf_phdr\' * &const  &vsyscall_phdrs = ( &const \'struct elf_phdr\' *) ( &VSYSCALL_BASE +  ($VSYSCALL_EHDR->{e_phoff})); \'int\'  &i;  &Elf32_Off  &ofs = 0;  &for ( &i = 0;  &i <  ($VSYSCALL_EHDR->{e_phnum}); ++ &i) { \'struct elf_phdr\'  &phdr =  $vsyscall_phdrs[ &i];  &if ( ($phdr->{p_type}) ==  &PT_LOAD) {  &BUG_ON( &ofs != 0);  &ofs =  ($phdr->{p_offset}) =  &offset;  ($phdr->{p_memsz}) =  &PAGE_ALIGN( ($phdr->{p_memsz}));  ($phdr->{p_filesz}) =  ($phdr->{p_memsz});  &offset +=  ($phdr->{p_filesz}); }  &else  ($phdr->{p_offset}) +=  &ofs;  ($phdr->{p_paddr}) = 0;  &DUMP_WRITE( &phdr, $sizeof{ &phdr}); } }  &while (0);}' unless defined(&ELF_CORE_WRITE_EXTRA_PHDRS);
	eval 'sub ELF_CORE_WRITE_EXTRA_DATA () { &do {  &const \'struct elf_phdr\' * &const  &vsyscall_phdrs = ( &const \'struct elf_phdr\' *) ( &VSYSCALL_BASE +  ($VSYSCALL_EHDR->{e_phoff})); \'int\'  &i;  &for ( &i = 0;  &i <  ($VSYSCALL_EHDR->{e_phnum}); ++ &i) {  &if ( ($vsyscall_phdrs[&i]->{p_type}) ==  &PT_LOAD)  &DUMP_WRITE(( &void *)  ($vsyscall_phdrs[&i]->{p_vaddr}),  &PAGE_ALIGN( ($vsyscall_phdrs[&i]->{p_memsz}))); } }  &while (0);}' unless defined(&ELF_CORE_WRITE_EXTRA_DATA);
    }
}
1;
