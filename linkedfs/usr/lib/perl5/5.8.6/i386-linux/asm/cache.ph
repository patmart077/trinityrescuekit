require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ARCH_I386_CACHE_H)) {
    eval 'sub __ARCH_I386_CACHE_H () {1;}' unless defined(&__ARCH_I386_CACHE_H);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	eval 'sub L1_CACHE_SHIFT () {( &CONFIG_X86_L1_CACHE_SHIFT);}' unless defined(&L1_CACHE_SHIFT);
	eval 'sub L1_CACHE_BYTES () {(1<<  &L1_CACHE_SHIFT);}' unless defined(&L1_CACHE_BYTES);
	eval 'sub L1_CACHE_SHIFT_MAX () {7;}' unless defined(&L1_CACHE_SHIFT_MAX);
    }
}
1;
