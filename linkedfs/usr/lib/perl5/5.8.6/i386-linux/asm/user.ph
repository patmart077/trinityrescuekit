require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_USER_H)) {
    eval 'sub _I386_USER_H () {1;}' unless defined(&_I386_USER_H);
    require 'asm/page.ph';
    eval 'sub NBPG () { &PAGE_SIZE;}' unless defined(&NBPG);
    eval 'sub UPAGES () {1;}' unless defined(&UPAGES);
    eval 'sub HOST_TEXT_START_ADDR () {( ($u->{start_code}));}' unless defined(&HOST_TEXT_START_ADDR);
    eval 'sub HOST_STACK_END_ADDR () {( ($u->{start_stack}) +  ($u->{u_ssize}) *  &NBPG);}' unless defined(&HOST_STACK_END_ADDR);
}
1;
