require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_I386_PROCESSOR_H)) {
    eval 'sub __ASM_I386_PROCESSOR_H () {1;}' unless defined(&__ASM_I386_PROCESSOR_H);
    if(defined(&__KERNEL__)) {
	require 'asm/vm86.ph';
	require 'asm/math_emu.ph';
	require 'asm/segment.ph';
	require 'asm/page.ph';
	require 'asm/types.ph';
	require 'asm/sigcontext.ph';
	require 'asm/cpufeature.ph';
	require 'asm/msr.ph';
	require 'asm/system.ph';
	require 'linux/cache.ph';
	require 'linux/threads.ph';
	require 'asm/percpu.ph';
	eval 'sub desc_empty {
	    local($desc) = @_;
    	    eval q((!(($desc)-> &a + ($desc)-> &b)));
	}' unless defined(&desc_empty);
	eval 'sub desc_equal {
	    local($desc1, $desc2) = @_;
    	    eval q(((($desc1)-> &a == ($desc2)-> &a)  && (($desc1)-> &b == ($desc2)-> &b)));
	}' unless defined(&desc_equal);
	eval 'sub current_text_addr () {
	    eval q(({  &void * &pc;  &__asm__(\\"movl $1f,%0\\\\n1:\\":\\"=g\\" ( &pc));  &pc; }));
	}' unless defined(&current_text_addr);
	eval 'sub X86_VENDOR_INTEL () {0;}' unless defined(&X86_VENDOR_INTEL);
	eval 'sub X86_VENDOR_CYRIX () {1;}' unless defined(&X86_VENDOR_CYRIX);
	eval 'sub X86_VENDOR_AMD () {2;}' unless defined(&X86_VENDOR_AMD);
	eval 'sub X86_VENDOR_UMC () {3;}' unless defined(&X86_VENDOR_UMC);
	eval 'sub X86_VENDOR_NEXGEN () {4;}' unless defined(&X86_VENDOR_NEXGEN);
	eval 'sub X86_VENDOR_CENTAUR () {5;}' unless defined(&X86_VENDOR_CENTAUR);
	eval 'sub X86_VENDOR_RISE () {6;}' unless defined(&X86_VENDOR_RISE);
	eval 'sub X86_VENDOR_TRANSMETA () {7;}' unless defined(&X86_VENDOR_TRANSMETA);
	eval 'sub X86_VENDOR_NSC () {8;}' unless defined(&X86_VENDOR_NSC);
	eval 'sub X86_VENDOR_NUM () {9;}' unless defined(&X86_VENDOR_NUM);
	eval 'sub X86_VENDOR_UNKNOWN () {0xff;}' unless defined(&X86_VENDOR_UNKNOWN);
	eval 'sub current_cpu_data () { $cpu_data[ &smp_processor_id()];}' unless defined(&current_cpu_data);
	eval 'sub X86_EFLAGS_CF () {0x1;}' unless defined(&X86_EFLAGS_CF);
	eval 'sub X86_EFLAGS_PF () {0x4;}' unless defined(&X86_EFLAGS_PF);
	eval 'sub X86_EFLAGS_AF () {0x10;}' unless defined(&X86_EFLAGS_AF);
	eval 'sub X86_EFLAGS_ZF () {0x40;}' unless defined(&X86_EFLAGS_ZF);
	eval 'sub X86_EFLAGS_SF () {0x80;}' unless defined(&X86_EFLAGS_SF);
	eval 'sub X86_EFLAGS_TF () {0x100;}' unless defined(&X86_EFLAGS_TF);
	eval 'sub X86_EFLAGS_IF () {0x200;}' unless defined(&X86_EFLAGS_IF);
	eval 'sub X86_EFLAGS_DF () {0x400;}' unless defined(&X86_EFLAGS_DF);
	eval 'sub X86_EFLAGS_OF () {0x800;}' unless defined(&X86_EFLAGS_OF);
	eval 'sub X86_EFLAGS_IOPL () {0x3000;}' unless defined(&X86_EFLAGS_IOPL);
	eval 'sub X86_EFLAGS_NT () {0x4000;}' unless defined(&X86_EFLAGS_NT);
	eval 'sub X86_EFLAGS_RF () {0x10000;}' unless defined(&X86_EFLAGS_RF);
	eval 'sub X86_EFLAGS_VM () {0x20000;}' unless defined(&X86_EFLAGS_VM);
	eval 'sub X86_EFLAGS_AC () {0x40000;}' unless defined(&X86_EFLAGS_AC);
	eval 'sub X86_EFLAGS_VIF () {0x80000;}' unless defined(&X86_EFLAGS_VIF);
	eval 'sub X86_EFLAGS_VIP () {0x100000;}' unless defined(&X86_EFLAGS_VIP);
	eval 'sub X86_EFLAGS_ID () {0x200000;}' unless defined(&X86_EFLAGS_ID);
	eval 'sub cpuid_ebx {
	    local($op) = @_;
    	    eval q({ my $eax,  &ebx;  &__asm__(\\"cpuid\\" : \\"=a\\" ( $eax), \\"=b\\" ( &ebx) : \\"0\\" ($op) : \\"cx\\", \\"dx\\" );  &ebx; });
	}' unless defined(&cpuid_ebx);
	eval 'sub cpuid_ecx {
	    local($op) = @_;
    	    eval q({ my $eax,  &ecx;  &__asm__(\\"cpuid\\" : \\"=a\\" ( $eax), \\"=c\\" ( &ecx) : \\"0\\" ($op) : \\"bx\\", \\"dx\\" );  &ecx; });
	}' unless defined(&cpuid_ecx);
	eval 'sub cpuid_edx {
	    local($op) = @_;
    	    eval q({ my $eax,  &edx;  &__asm__(\\"cpuid\\" : \\"=a\\" ( $eax), \\"=d\\" ( &edx) : \\"0\\" ($op) : \\"bx\\", \\"cx\\");  &edx; });
	}' unless defined(&cpuid_edx);
	eval 'sub load_cr3 {
	    local($pgdir) = @_;
    	    eval q( &asm  &volatile(\\"movl %0,%%cr3\\": :\\"r\\" ( &__pa($pgdir))));
	}' unless defined(&load_cr3);
	eval 'sub X86_CR4_VME () {0x1;}' unless defined(&X86_CR4_VME);
	eval 'sub X86_CR4_PVI () {0x2;}' unless defined(&X86_CR4_PVI);
	eval 'sub X86_CR4_TSD () {0x4;}' unless defined(&X86_CR4_TSD);
	eval 'sub X86_CR4_DE () {0x8;}' unless defined(&X86_CR4_DE);
	eval 'sub X86_CR4_PSE () {0x10;}' unless defined(&X86_CR4_PSE);
	eval 'sub X86_CR4_PAE () {0x20;}' unless defined(&X86_CR4_PAE);
	eval 'sub X86_CR4_MCE () {0x40;}' unless defined(&X86_CR4_MCE);
	eval 'sub X86_CR4_PGE () {0x80;}' unless defined(&X86_CR4_PGE);
	eval 'sub X86_CR4_PCE () {0x100;}' unless defined(&X86_CR4_PCE);
	eval 'sub X86_CR4_OSFXSR () {0x200;}' unless defined(&X86_CR4_OSFXSR);
	eval 'sub X86_CR4_OSXMMEXCPT () {0x400;}' unless defined(&X86_CR4_OSXMMEXCPT);
	eval 'sub set_in_cr4 {
	    local($mask) = @_;
    	    eval q({  &mmu_cr4_features |= $mask;  &__asm__(\\"movl %%cr4,%%eax\\\\n\\\\t\\" \\"orl %0,%%eax\\\\n\\\\t\\" \\"movl %%eax,%%cr4\\\\n\\" : : \\"irg\\" ($mask) :\\"ax\\"); });
	}' unless defined(&set_in_cr4);
	eval 'sub clear_in_cr4 {
	    local($mask) = @_;
    	    eval q({  &mmu_cr4_features &= ~$mask;  &__asm__(\\"movl %%cr4,%%eax\\\\n\\\\t\\" \\"andl %0,%%eax\\\\n\\\\t\\" \\"movl %%eax,%%cr4\\\\n\\" : : \\"irg\\" (~$mask) :\\"ax\\"); });
	}' unless defined(&clear_in_cr4);
	eval 'sub CX86_PCR0 () {0x20;}' unless defined(&CX86_PCR0);
	eval 'sub CX86_GCR () {0xb8;}' unless defined(&CX86_GCR);
	eval 'sub CX86_CCR0 () {0xc0;}' unless defined(&CX86_CCR0);
	eval 'sub CX86_CCR1 () {0xc1;}' unless defined(&CX86_CCR1);
	eval 'sub CX86_CCR2 () {0xc2;}' unless defined(&CX86_CCR2);
	eval 'sub CX86_CCR3 () {0xc3;}' unless defined(&CX86_CCR3);
	eval 'sub CX86_CCR4 () {0xe8;}' unless defined(&CX86_CCR4);
	eval 'sub CX86_CCR5 () {0xe9;}' unless defined(&CX86_CCR5);
	eval 'sub CX86_CCR6 () {0xea;}' unless defined(&CX86_CCR6);
	eval 'sub CX86_CCR7 () {0xeb;}' unless defined(&CX86_CCR7);
	eval 'sub CX86_PCR1 () {0xf0;}' unless defined(&CX86_PCR1);
	eval 'sub CX86_DIR0 () {0xfe;}' unless defined(&CX86_DIR0);
	eval 'sub CX86_DIR1 () {0xff;}' unless defined(&CX86_DIR1);
	eval 'sub CX86_ARR_BASE () {0xc4;}' unless defined(&CX86_ARR_BASE);
	eval 'sub CX86_RCR_BASE () {0xdc;}' unless defined(&CX86_RCR_BASE);
	eval 'sub getCx86 {
	    local($reg) = @_;
    	    eval q(({  &outb(($reg), 0x22);  &inb(0x23); }));
	}' unless defined(&getCx86);
	eval 'sub setCx86 {
	    local($reg, $data) = @_;
    	    eval q( &do {  &outb(($reg), 0x22);  &outb(($data), 0x23); }  &while (0));
	}' unless defined(&setCx86);
	eval 'sub __monitor {
	    local($eax,$ecx,$edx) = @_;
    	    eval q({  &asm  &volatile( \\".byte 0x0f,0x01,0xc8;\\" : :\\"a\\" ($eax), \\"c\\" ($ecx), \\"d\\"($edx)); });
	}' unless defined(&__monitor);
	eval 'sub __mwait {
	    local($eax,$ecx) = @_;
    	    eval q({  &asm  &volatile( \\".byte 0x0f,0x01,0xc9;\\" : :\\"a\\" ($eax), \\"c\\" ($ecx)); });
	}' unless defined(&__mwait);
	eval 'sub TASK_SIZE () {( &PAGE_OFFSET);}' unless defined(&TASK_SIZE);
	eval 'sub TASK_UNMAPPED_BASE () {( &PAGE_ALIGN( &TASK_SIZE / 3));}' unless defined(&TASK_UNMAPPED_BASE);
	eval 'sub HAVE_ARCH_PICK_MMAP_LAYOUT () {1;}' unless defined(&HAVE_ARCH_PICK_MMAP_LAYOUT);
	eval 'sub IO_BITMAP_BITS () {65536;}' unless defined(&IO_BITMAP_BITS);
	eval 'sub IO_BITMAP_BYTES () {( &IO_BITMAP_BITS/8);}' unless defined(&IO_BITMAP_BYTES);
	eval 'sub IO_BITMAP_LONGS () {( &IO_BITMAP_BYTES/$sizeof{\'long\'});}' unless defined(&IO_BITMAP_LONGS);
	eval 'sub IO_BITMAP_OFFSET () { &offsetof(\'struct tss_struct\', &io_bitmap);}' unless defined(&IO_BITMAP_OFFSET);
	eval 'sub INVALID_IO_BITMAP_OFFSET () {0x8000;}' unless defined(&INVALID_IO_BITMAP_OFFSET);
	eval 'sub INVALID_IO_BITMAP_OFFSET_LAZY () {0x9000;}' unless defined(&INVALID_IO_BITMAP_OFFSET_LAZY);
	eval 'sub ARCH_MIN_TASKALIGN () {16;}' unless defined(&ARCH_MIN_TASKALIGN);
	eval 'sub INIT_THREAD () {{ . &vm86_info =  &NULL, . &sysenter_cs =  &__KERNEL_CS, . &io_bitmap_ptr =  &NULL, };}' unless defined(&INIT_THREAD);
	eval 'sub INIT_TSS () {{ . &esp0 = $sizeof{ &init_stack} +  &init_stack, . &ss0 =  &__KERNEL_DS, . &ss1 =  &__KERNEL_CS, . &ldt =  &GDT_ENTRY_LDT, . &io_bitmap_base =  &INVALID_IO_BITMAP_OFFSET, . &io_bitmap = { [ 0...  &IO_BITMAP_LONGS] = ~0}, };}' unless defined(&INIT_TSS);
	eval 'sub load_esp0 {
	    local($tss,$thread) = @_;
    	    eval q({  ($tss->{esp0}) =  ($thread->{esp0});  &if ( &unlikely( ($tss->{ss1}) !=  ($thread->{sysenter_cs}))) {  ($tss->{ss1}) =  ($thread->{sysenter_cs});  &wrmsr( &MSR_IA32_SYSENTER_CS,  ($thread->{sysenter_cs}), 0); } });
	}' unless defined(&load_esp0);
	eval 'sub start_thread {
	    local($regs, $new_eip, $new_esp) = @_;
    	    eval q( &do {  &__asm__(\\"movl %0,%%fs ; movl %0,%%gs\\": :\\"r\\" (0));  &set_fs( &USER_DS);  ($regs->{xds}) =  &__USER_DS;  ($regs->{xes}) =  &__USER_DS;  ($regs->{xss}) =  &__USER_DS;  ($regs->{xcs}) =  &__USER_CS;  ($regs->{eip}) = $new_eip;  ($regs->{esp}) = $new_esp; }  &while (0));
	}' unless defined(&start_thread);
	eval 'sub THREAD_SIZE_LONGS () {( &THREAD_SIZE/$sizeof{\'unsigned long\'});}' unless defined(&THREAD_SIZE_LONGS);
	eval 'sub KSTK_TOP {
	    local($info) = @_;
    	    eval q(({ \'unsigned long\' * &__ptr = ($info); (& $__ptr[ &THREAD_SIZE_LONGS]); }));
	}' unless defined(&KSTK_TOP);
	eval 'sub task_pt_regs {
	    local($task) = @_;
    	    eval q(({ \'struct pt_regs\' * &__regs__;  &__regs__ =  &KSTK_TOP(($task)-> &thread_info);  &__regs__ - 1; }));
	}' unless defined(&task_pt_regs);
	eval 'sub KSTK_EIP {
	    local($task) = @_;
    	    eval q(( &task_pt_regs($task)-> &eip));
	}' unless defined(&KSTK_EIP);
	eval 'sub KSTK_ESP {
	    local($task) = @_;
    	    eval q(( &task_pt_regs($task)-> &esp));
	}' unless defined(&KSTK_ESP);
	eval 'sub MICROCODE_IOCFREE () { &_IO(ord(\'6\'),0);}' unless defined(&MICROCODE_IOCFREE);
	eval 'sub cpu_relax () {
	    eval q( &rep_nop());
	}' unless defined(&cpu_relax);
	eval 'sub GENERIC_NOP1 () {".byte 0x90\\n";}' unless defined(&GENERIC_NOP1);
	eval 'sub GENERIC_NOP2 () {".byte 0x89,0xf6\\n";}' unless defined(&GENERIC_NOP2);
	eval 'sub GENERIC_NOP3 () {".byte 0x8d,0x76,0x00\\n";}' unless defined(&GENERIC_NOP3);
	eval 'sub GENERIC_NOP4 () {".byte 0x8d,0x74,0x26,0x00\\n";}' unless defined(&GENERIC_NOP4);
	eval 'sub GENERIC_NOP5 () { &GENERIC_NOP1  &GENERIC_NOP4;}' unless defined(&GENERIC_NOP5);
	eval 'sub GENERIC_NOP6 () {".byte 0x8d,0xb6,0x00,0x00,0x00,0x00\\n";}' unless defined(&GENERIC_NOP6);
	eval 'sub GENERIC_NOP7 () {".byte 0x8d,0xb4,0x26,0x00,0x00,0x00,0x00\\n";}' unless defined(&GENERIC_NOP7);
	eval 'sub GENERIC_NOP8 () { &GENERIC_NOP1  &GENERIC_NOP7;}' unless defined(&GENERIC_NOP8);
	eval 'sub K8_NOP1 () { &GENERIC_NOP1;}' unless defined(&K8_NOP1);
	eval 'sub K8_NOP2 () {".byte 0x66,0x90\\n";}' unless defined(&K8_NOP2);
	eval 'sub K8_NOP3 () {".byte 0x66,0x66,0x90\\n";}' unless defined(&K8_NOP3);
	eval 'sub K8_NOP4 () {".byte 0x66,0x66,0x66,0x90\\n";}' unless defined(&K8_NOP4);
	eval 'sub K8_NOP5 () { &K8_NOP3  &K8_NOP2;}' unless defined(&K8_NOP5);
	eval 'sub K8_NOP6 () { &K8_NOP3  &K8_NOP3;}' unless defined(&K8_NOP6);
	eval 'sub K8_NOP7 () { &K8_NOP4  &K8_NOP3;}' unless defined(&K8_NOP7);
	eval 'sub K8_NOP8 () { &K8_NOP4  &K8_NOP4;}' unless defined(&K8_NOP8);
	eval 'sub K7_NOP1 () { &GENERIC_NOP1;}' unless defined(&K7_NOP1);
	eval 'sub K7_NOP2 () {".byte 0x8b,0xc0\\n";}' unless defined(&K7_NOP2);
	eval 'sub K7_NOP3 () {".byte 0x8d,0x04,0x20\\n";}' unless defined(&K7_NOP3);
	eval 'sub K7_NOP4 () {".byte 0x8d,0x44,0x20,0x00\\n";}' unless defined(&K7_NOP4);
	eval 'sub K7_NOP5 () { &K7_NOP4  &ASM_NOP1;}' unless defined(&K7_NOP5);
	eval 'sub K7_NOP6 () {".byte 0x8d,0x80,0,0,0,0\\n";}' unless defined(&K7_NOP6);
	eval 'sub K7_NOP7 () {".byte 0x8D,0x04,0x05,0,0,0,0\\n";}' unless defined(&K7_NOP7);
	eval 'sub K7_NOP8 () { &K7_NOP7  &ASM_NOP1;}' unless defined(&K7_NOP8);
	if(defined(&CONFIG_MK8)) {
	    eval 'sub ASM_NOP1 () { &K8_NOP1;}' unless defined(&ASM_NOP1);
	    eval 'sub ASM_NOP2 () { &K8_NOP2;}' unless defined(&ASM_NOP2);
	    eval 'sub ASM_NOP3 () { &K8_NOP3;}' unless defined(&ASM_NOP3);
	    eval 'sub ASM_NOP4 () { &K8_NOP4;}' unless defined(&ASM_NOP4);
	    eval 'sub ASM_NOP5 () { &K8_NOP5;}' unless defined(&ASM_NOP5);
	    eval 'sub ASM_NOP6 () { &K8_NOP6;}' unless defined(&ASM_NOP6);
	    eval 'sub ASM_NOP7 () { &K8_NOP7;}' unless defined(&ASM_NOP7);
	    eval 'sub ASM_NOP8 () { &K8_NOP8;}' unless defined(&ASM_NOP8);
	}
 elsif(defined( &CONFIG_MK7)) {
	    eval 'sub ASM_NOP1 () { &K7_NOP1;}' unless defined(&ASM_NOP1);
	    eval 'sub ASM_NOP2 () { &K7_NOP2;}' unless defined(&ASM_NOP2);
	    eval 'sub ASM_NOP3 () { &K7_NOP3;}' unless defined(&ASM_NOP3);
	    eval 'sub ASM_NOP4 () { &K7_NOP4;}' unless defined(&ASM_NOP4);
	    eval 'sub ASM_NOP5 () { &K7_NOP5;}' unless defined(&ASM_NOP5);
	    eval 'sub ASM_NOP6 () { &K7_NOP6;}' unless defined(&ASM_NOP6);
	    eval 'sub ASM_NOP7 () { &K7_NOP7;}' unless defined(&ASM_NOP7);
	    eval 'sub ASM_NOP8 () { &K7_NOP8;}' unless defined(&ASM_NOP8);
	} else {
	    eval 'sub ASM_NOP1 () { &GENERIC_NOP1;}' unless defined(&ASM_NOP1);
	    eval 'sub ASM_NOP2 () { &GENERIC_NOP2;}' unless defined(&ASM_NOP2);
	    eval 'sub ASM_NOP3 () { &GENERIC_NOP3;}' unless defined(&ASM_NOP3);
	    eval 'sub ASM_NOP4 () { &GENERIC_NOP4;}' unless defined(&ASM_NOP4);
	    eval 'sub ASM_NOP5 () { &GENERIC_NOP5;}' unless defined(&ASM_NOP5);
	    eval 'sub ASM_NOP6 () { &GENERIC_NOP6;}' unless defined(&ASM_NOP6);
	    eval 'sub ASM_NOP7 () { &GENERIC_NOP7;}' unless defined(&ASM_NOP7);
	    eval 'sub ASM_NOP8 () { &GENERIC_NOP8;}' unless defined(&ASM_NOP8);
	}
	eval 'sub ASM_NOP_MAX () {8;}' unless defined(&ASM_NOP_MAX);
	eval 'sub ARCH_HAS_PREFETCH () {1;}' unless defined(&ARCH_HAS_PREFETCH);
1;
