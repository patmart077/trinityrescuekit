require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_SYS_CDEFS_H)) {
    eval 'sub _SYS_CDEFS_H () {1;}' unless defined(&_SYS_CDEFS_H);
    unless(defined(&_FEATURES_H)) {
	require 'features.ph';
    }
    if(defined (defined(&__GNUC__) ? &__GNUC__ : 0)  && !defined (defined(&__STDC__) ? &__STDC__ : 0)) {
	die("You need a ISO C conforming compiler to use the glibc headers");
    }
    undef(&__P) if defined(&__P);
    undef(&__PMT) if defined(&__PMT);
    if(defined(&__GNUC__)) {
	if(!defined (defined(&__cplusplus) ? &__cplusplus : 0)  &&  &__GNUC_PREREQ (3, 3)) {
	    eval 'sub __THROW () { &__attribute__ (( &__nothrow__));}' unless defined(&__THROW);
	    eval 'sub __NTH {
	        local($fct) = @_;
    		eval q( &__attribute__ (( &__nothrow__)) $fct);
	    }' unless defined(&__NTH);
	} else {
	    if(defined (defined(&__cplusplus) ? &__cplusplus : 0)  &&  &__GNUC_PREREQ (2,8)) {
		eval 'sub __THROW () { &throw ();}' unless defined(&__THROW);
		eval 'sub __NTH {
		    local($fct) = @_;
    		    eval q($fct  &throw ());
		}' unless defined(&__NTH);
	    } else {
		eval 'sub __THROW () {1;}' unless defined(&__THROW);
		eval 'sub __NTH {
		    local($fct) = @_;
    		    eval q($fct);
		}' unless defined(&__NTH);
	    }
	}
    } else {
	eval 'sub __inline () {1;}' unless defined(&__inline);
	eval 'sub __THROW () {1;}' unless defined(&__THROW);
	eval 'sub __NTH {
	    local($fct) = @_;
    	    eval q($fct);
	}' unless defined(&__NTH);
	eval 'sub __const () { &const;}' unless defined(&__const);
	eval 'sub __signed () {\'signed\';}' unless defined(&__signed);
	eval 'sub __volatile () { &volatile;}' unless defined(&__volatile);
    }
    eval 'sub __P {
        local($args) = @_;
	    eval q($args);
    }' unless defined(&__P);
    eval 'sub __PMT {
        local($args) = @_;
	    eval q($args);
    }' unless defined(&__PMT);
    eval 'sub __CONCAT {
        local($x,$y) = @_;
	    eval q($x  $y);
    }' unless defined(&__CONCAT);
    eval 'sub __STRING {
        local($x) = @_;
	    eval q($x);
    }' unless defined(&__STRING);
    eval 'sub __ptr_t () { &void *;}' unless defined(&__ptr_t);
    eval 'sub __long_double_t () {\'long double\';}' unless defined(&__long_double_t);
    if(defined(&__cplusplus)) {
	eval 'sub __BEGIN_DECLS () { &extern "C" {;}' unless defined(&__BEGIN_DECLS);
	eval 'sub __END_DECLS () {};}' unless defined(&__END_DECLS);
    } else {
	eval 'sub __BEGIN_DECLS () {1;}' unless defined(&__BEGIN_DECLS);
	eval 'sub __END_DECLS () {1;}' unless defined(&__END_DECLS);
    }
    if(defined (defined(&__cplusplus) ? &__cplusplus : 0)  && defined (defined(&_GLIBCPP_USE_NAMESPACES) ? &_GLIBCPP_USE_NAMESPACES : 0)) {
	eval 'sub __BEGIN_NAMESPACE_STD () { &namespace  &std {;}' unless defined(&__BEGIN_NAMESPACE_STD);
	eval 'sub __END_NAMESPACE_STD () {};}' unless defined(&__END_NAMESPACE_STD);
	eval 'sub __USING_NAMESPACE_STD {
	    local($name) = @_;
    	    eval q( &using  &std::$name;);
	}' unless defined(&__USING_NAMESPACE_STD);
	eval 'sub __BEGIN_NAMESPACE_C99 () { &namespace  &__c99 {;}' unless defined(&__BEGIN_NAMESPACE_C99);
	eval 'sub __END_NAMESPACE_C99 () {};}' unless defined(&__END_NAMESPACE_C99);
	eval 'sub __USING_NAMESPACE_C99 {
	    local($name) = @_;
    	    eval q( &using  &__c99::$name;);
	}' unless defined(&__USING_NAMESPACE_C99);
    } else {
	eval 'sub __BEGIN_NAMESPACE_STD () {1;}' unless defined(&__BEGIN_NAMESPACE_STD);
	eval 'sub __END_NAMESPACE_STD () {1;}' unless defined(&__END_NAMESPACE_STD);
	eval 'sub __USING_NAMESPACE_STD {
	    local($name) = @_;
    	    eval q();
	}' unless defined(&__USING_NAMESPACE_STD);
	eval 'sub __BEGIN_NAMESPACE_C99 () {1;}' unless defined(&__BEGIN_NAMESPACE_C99);
	eval 'sub __END_NAMESPACE_C99 () {1;}' unless defined(&__END_NAMESPACE_C99);
	eval 'sub __USING_NAMESPACE_C99 {
	    local($name) = @_;
    	    eval q();
	}' unless defined(&__USING_NAMESPACE_C99);
    }
    unless(defined(&__BOUNDED_POINTERS__)) {
	eval 'sub __bounded () {1;}' unless defined(&__bounded);
	eval 'sub __unbounded () {1;}' unless defined(&__unbounded);
	eval 'sub __ptrvalue () {1;}' unless defined(&__ptrvalue);
    }
    eval 'sub __bos {
        local($ptr) = @_;
	    eval q( &__builtin_object_size ($ptr,  &__USE_FORTIFY_LEVEL > 1));
    }' unless defined(&__bos);
    eval 'sub __bos0 {
        local($ptr) = @_;
	    eval q( &__builtin_object_size ($ptr, 0));
    }' unless defined(&__bos0);
    if( &__GNUC_PREREQ (2,97)) {
	eval 'sub __flexarr () {[];}' unless defined(&__flexarr);
    } else {
	if(defined(&__GNUC__)) {
	    eval 'sub __flexarr () {[0];}' unless defined(&__flexarr);
	} else {
	    if(defined (defined(&__STDC_VERSION__) ? &__STDC_VERSION__ : 0)  && (defined(&__STDC_VERSION__) ? &__STDC_VERSION__ : 0) >= 199901) {
		eval 'sub __flexarr () {[];}' unless defined(&__flexarr);
	    } else {
		eval 'sub __flexarr () {[1];}' unless defined(&__flexarr);
	    }
	}
    }
    if(defined (defined(&__GNUC__) ? &__GNUC__ : 0)  && (defined(&__GNUC__) ? &__GNUC__ : 0) >= 2) {
	eval 'sub __REDIRECT {
	    local($name, $proto, $alias) = @_;
    	    eval q($name $proto  &__asm__ ( &__ASMNAME ($alias)));
	}' unless defined(&__REDIRECT);
	if(defined(&__cplusplus)) {
	    eval 'sub __REDIRECT_NTH {
	        local($name, $proto, $alias) = @_;
    		eval q($name $proto  &__THROW  &__asm__ ( &__ASMNAME ($alias)));
	    }' unless defined(&__REDIRECT_NTH);
	} else {
	    eval 'sub __REDIRECT_NTH {
	        local($name, $proto, $alias) = @_;
    		eval q($name $proto  &__asm__ ( &__ASMNAME ($alias))  &__THROW);
	    }' unless defined(&__REDIRECT_NTH);
	}
	eval 'sub __ASMNAME {
	    local($cname) = @_;
    	    eval q( &__ASMNAME2 ( &__USER_LABEL_PREFIX__, $cname));
	}' unless defined(&__ASMNAME);
	eval 'sub __ASMNAME2 {
	    local($prefix, $cname) = @_;
    	    eval q( &__STRING ($prefix) $cname);
	}' unless defined(&__ASMNAME2);
    }
    if(!defined (defined(&__GNUC__) ? &__GNUC__ : 0) || (defined(&__GNUC__) ? &__GNUC__ : 0) < 2) {
	eval 'sub __attribute__ {
	    local($xyz) = @_;
    	    eval q();
	}' unless defined(&__attribute__);
    }
    if( &__GNUC_PREREQ (2,96)) {
	eval 'sub __attribute_malloc__ () { &__attribute__ (( &__malloc__));}' unless defined(&__attribute_malloc__);
    } else {
	eval 'sub __attribute_malloc__ () {1;}' unless defined(&__attribute_malloc__);
    }
    if( &__GNUC_PREREQ (2,96)) {
	eval 'sub __attribute_pure__ () { &__attribute__ (( &__pure__));}' unless defined(&__attribute_pure__);
    } else {
	eval 'sub __attribute_pure__ () {1;}' unless defined(&__attribute_pure__);
    }
    if( &__GNUC_PREREQ (3,1)) {
	eval 'sub __attribute_used__ () { &__attribute__ (( &__used__));}' unless defined(&__attribute_used__);
	eval 'sub __attribute_noinline__ () { &__attribute__ (( &__noinline__));}' unless defined(&__attribute_noinline__);
    } else {
	eval 'sub __attribute_used__ () { &__attribute__ (( &__unused__));}' unless defined(&__attribute_used__);
	eval 'sub __attribute_noinline__ () {1;}' unless defined(&__attribute_noinline__);
    }
    if( &__GNUC_PREREQ (3,2)) {
	eval 'sub __attribute_deprecated__ () { &__attribute__ (( &__deprecated__));}' unless defined(&__attribute_deprecated__);
    } else {
	eval 'sub __attribute_deprecated__ () {1;}' unless defined(&__attribute_deprecated__);
    }
    if( &__GNUC_PREREQ (2,8)) {
	eval 'sub __attribute_format_arg__ {
	    local($x) = @_;
    	    eval q( &__attribute__ (( &__format_arg__ ($x))));
	}' unless defined(&__attribute_format_arg__);
    } else {
	eval 'sub __attribute_format_arg__ {
	    local($x) = @_;
    	    eval q();
	}' unless defined(&__attribute_format_arg__);
    }
    if( &__GNUC_PREREQ (2,97)) {
	eval 'sub __attribute_format_strfmon__ {
	    local($a,$b) = @_;
    	    eval q( &__attribute__ (( &__format__ ( &__strfmon__, $a, $b))));
	}' unless defined(&__attribute_format_strfmon__);
    } else {
	eval 'sub __attribute_format_strfmon__ {
	    local($a,$b) = @_;
    	    eval q();
	}' unless defined(&__attribute_format_strfmon__);
    }
    if( &__GNUC_PREREQ (3,3)) {
	eval 'sub __nonnull {
	    local($params) = @_;
    	    eval q( &__attribute__ (( &__nonnull__ $params)));
	}' unless defined(&__nonnull);
    } else {
	eval 'sub __nonnull {
	    local($params) = @_;
    	    eval q();
	}' unless defined(&__nonnull);
    }
    if(! &__GNUC_PREREQ (2,8)) {
	eval 'sub __extension__ () {1;}' unless defined(&__extension__);
    }
    if(! &__GNUC_PREREQ (2,92)) {
	eval 'sub __restrict () {1;}' unless defined(&__restrict);
    }
    if( &__GNUC_PREREQ (3,1)  && !defined (defined(&__GNUG__) ? &__GNUG__ : 0)) {
	eval 'sub __restrict_arr () { &__restrict;}' unless defined(&__restrict_arr);
    } else {
	if(defined(&__GNUC__)) {
	    eval 'sub __restrict_arr () {1;}' unless defined(&__restrict_arr);
	} else {
	    if(defined (defined(&__STDC_VERSION__) ? &__STDC_VERSION__ : 0)  && (defined(&__STDC_VERSION__) ? &__STDC_VERSION__ : 0) >= 199901) {
		eval 'sub __restrict_arr () { &restrict;}' unless defined(&__restrict_arr);
	    } else {
		eval 'sub __restrict_arr () {1;}' unless defined(&__restrict_arr);
	    }
	}
    }
}
1;
