require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_STAT_H)) {
    eval 'sub _LINUX_STAT_H () {1;}' unless defined(&_LINUX_STAT_H);
    if(defined(&__KERNEL__)) {
	require 'asm/stat.ph';
    }
    if(defined( &__KERNEL__) || !defined( &__GLIBC__) || ((defined(&__GLIBC__) ? &__GLIBC__ : 0) < 2)) {
	eval 'sub S_IFMT () {00170000;}' unless defined(&S_IFMT);
	eval 'sub S_IFSOCK () {0140000;}' unless defined(&S_IFSOCK);
	eval 'sub S_IFLNK () {0120000;}' unless defined(&S_IFLNK);
	eval 'sub S_IFREG () {0100000;}' unless defined(&S_IFREG);
	eval 'sub S_IFBLK () {0060000;}' unless defined(&S_IFBLK);
	eval 'sub S_IFDIR () {0040000;}' unless defined(&S_IFDIR);
	eval 'sub S_IFCHR () {0020000;}' unless defined(&S_IFCHR);
	eval 'sub S_IFIFO () {0010000;}' unless defined(&S_IFIFO);
	eval 'sub S_ISUID () {0004000;}' unless defined(&S_ISUID);
	eval 'sub S_ISGID () {0002000;}' unless defined(&S_ISGID);
	eval 'sub S_ISVTX () {0001000;}' unless defined(&S_ISVTX);
	eval 'sub S_ISLNK {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFLNK));
	}' unless defined(&S_ISLNK);
	eval 'sub S_ISREG {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFREG));
	}' unless defined(&S_ISREG);
	eval 'sub S_ISDIR {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFDIR));
	}' unless defined(&S_ISDIR);
	eval 'sub S_ISCHR {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFCHR));
	}' unless defined(&S_ISCHR);
	eval 'sub S_ISBLK {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFBLK));
	}' unless defined(&S_ISBLK);
	eval 'sub S_ISFIFO {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFIFO));
	}' unless defined(&S_ISFIFO);
	eval 'sub S_ISSOCK {
	    local($m) = @_;
    	    eval q(((($m) &  &S_IFMT) ==  &S_IFSOCK));
	}' unless defined(&S_ISSOCK);
	eval 'sub S_IRWXU () {00700;}' unless defined(&S_IRWXU);
	eval 'sub S_IRUSR () {00400;}' unless defined(&S_IRUSR);
	eval 'sub S_IWUSR () {00200;}' unless defined(&S_IWUSR);
	eval 'sub S_IXUSR () {00100;}' unless defined(&S_IXUSR);
	eval 'sub S_IRWXG () {00070;}' unless defined(&S_IRWXG);
	eval 'sub S_IRGRP () {00040;}' unless defined(&S_IRGRP);
	eval 'sub S_IWGRP () {00020;}' unless defined(&S_IWGRP);
	eval 'sub S_IXGRP () {00010;}' unless defined(&S_IXGRP);
	eval 'sub S_IRWXO () {00007;}' unless defined(&S_IRWXO);
	eval 'sub S_IROTH () {00004;}' unless defined(&S_IROTH);
	eval 'sub S_IWOTH () {00002;}' unless defined(&S_IWOTH);
	eval 'sub S_IXOTH () {00001;}' unless defined(&S_IXOTH);
    }
    if(defined(&__KERNEL__)) {
	eval 'sub S_IRWXUGO () {( &S_IRWXU| &S_IRWXG| &S_IRWXO);}' unless defined(&S_IRWXUGO);
	eval 'sub S_IALLUGO () {( &S_ISUID| &S_ISGID| &S_ISVTX| &S_IRWXUGO);}' unless defined(&S_IALLUGO);
	eval 'sub S_IRUGO () {( &S_IRUSR| &S_IRGRP| &S_IROTH);}' unless defined(&S_IRUGO);
	eval 'sub S_IWUGO () {( &S_IWUSR| &S_IWGRP| &S_IWOTH);}' unless defined(&S_IWUGO);
	eval 'sub S_IXUGO () {( &S_IXUSR| &S_IXGRP| &S_IXOTH);}' unless defined(&S_IXUGO);
	require 'linux/types.ph';
	require 'linux/time.ph';
    }
}
1;
