require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_CAPABILITY_H)) {
    eval 'sub _LINUX_CAPABILITY_H () {1;}' unless defined(&_LINUX_CAPABILITY_H);
    require 'linux/types.ph';
    require 'linux/compiler.ph';
    eval 'sub _LINUX_CAPABILITY_VERSION () {0x19980330;}' unless defined(&_LINUX_CAPABILITY_VERSION);
    if(defined(&__KERNEL__)) {
	require 'linux/spinlock.ph';
	if(defined(&STRICT_CAP_T_TYPECHECKS)) {
	} else {
	}
	eval 'sub _USER_CAP_HEADER_SIZE () {(2*$sizeof{ &__u32});}' unless defined(&_USER_CAP_HEADER_SIZE);
	eval 'sub _KERNEL_CAP_T_SIZE () {($sizeof{ &kernel_cap_t});}' unless defined(&_KERNEL_CAP_T_SIZE);
    }
    eval 'sub CAP_CHOWN () {0;}' unless defined(&CAP_CHOWN);
    eval 'sub CAP_DAC_OVERRIDE () {1;}' unless defined(&CAP_DAC_OVERRIDE);
    eval 'sub CAP_DAC_READ_SEARCH () {2;}' unless defined(&CAP_DAC_READ_SEARCH);
    eval 'sub CAP_FOWNER () {3;}' unless defined(&CAP_FOWNER);
    eval 'sub CAP_FSETID () {4;}' unless defined(&CAP_FSETID);
    eval 'sub CAP_FS_MASK () {0x1f;}' unless defined(&CAP_FS_MASK);
    eval 'sub CAP_KILL () {5;}' unless defined(&CAP_KILL);
    eval 'sub CAP_SETGID () {6;}' unless defined(&CAP_SETGID);
    eval 'sub CAP_SETUID () {7;}' unless defined(&CAP_SETUID);
    eval 'sub CAP_SETPCAP () {8;}' unless defined(&CAP_SETPCAP);
    eval 'sub CAP_LINUX_IMMUTABLE () {9;}' unless defined(&CAP_LINUX_IMMUTABLE);
    eval 'sub CAP_NET_BIND_SERVICE () {10;}' unless defined(&CAP_NET_BIND_SERVICE);
    eval 'sub CAP_NET_BROADCAST () {11;}' unless defined(&CAP_NET_BROADCAST);
    eval 'sub CAP_NET_ADMIN () {12;}' unless defined(&CAP_NET_ADMIN);
    eval 'sub CAP_NET_RAW () {13;}' unless defined(&CAP_NET_RAW);
    eval 'sub CAP_IPC_LOCK () {14;}' unless defined(&CAP_IPC_LOCK);
    eval 'sub CAP_IPC_OWNER () {15;}' unless defined(&CAP_IPC_OWNER);
    eval 'sub CAP_SYS_MODULE () {16;}' unless defined(&CAP_SYS_MODULE);
    eval 'sub CAP_SYS_RAWIO () {17;}' unless defined(&CAP_SYS_RAWIO);
    eval 'sub CAP_SYS_CHROOT () {18;}' unless defined(&CAP_SYS_CHROOT);
    eval 'sub CAP_SYS_PTRACE () {19;}' unless defined(&CAP_SYS_PTRACE);
    eval 'sub CAP_SYS_PACCT () {20;}' unless defined(&CAP_SYS_PACCT);
    eval 'sub CAP_SYS_ADMIN () {21;}' unless defined(&CAP_SYS_ADMIN);
    eval 'sub CAP_SYS_BOOT () {22;}' unless defined(&CAP_SYS_BOOT);
    eval 'sub CAP_SYS_NICE () {23;}' unless defined(&CAP_SYS_NICE);
    eval 'sub CAP_SYS_RESOURCE () {24;}' unless defined(&CAP_SYS_RESOURCE);
    eval 'sub CAP_SYS_TIME () {25;}' unless defined(&CAP_SYS_TIME);
    eval 'sub CAP_SYS_TTY_CONFIG () {26;}' unless defined(&CAP_SYS_TTY_CONFIG);
    eval 'sub CAP_MKNOD () {27;}' unless defined(&CAP_MKNOD);
    eval 'sub CAP_LEASE () {28;}' unless defined(&CAP_LEASE);
    if(defined(&__KERNEL__)) {
	if(defined(&STRICT_CAP_T_TYPECHECKS)) {
	    eval 'sub to_cap_t {
	        local($x) = @_;
    		eval q({ $x });
	    }' unless defined(&to_cap_t);
	    eval 'sub cap_t {
	        local($x) = @_;
    		eval q(($x). &cap);
	    }' unless defined(&cap_t);
	} else {
	    eval 'sub to_cap_t {
	        local($x) = @_;
    		eval q(($x));
	    }' unless defined(&to_cap_t);
	    eval 'sub cap_t {
	        local($x) = @_;
    		eval q(($x));
	    }' unless defined(&cap_t);
	}
	eval 'sub CAP_EMPTY_SET () { &to_cap_t(0);}' unless defined(&CAP_EMPTY_SET);
	eval 'sub CAP_FULL_SET () { &to_cap_t(~0);}' unless defined(&CAP_FULL_SET);
	eval 'sub CAP_INIT_EFF_SET () { &to_cap_t(~0& ~ &CAP_TO_MASK( &CAP_SETPCAP));}' unless defined(&CAP_INIT_EFF_SET);
	eval 'sub CAP_INIT_INH_SET () { &to_cap_t(0);}' unless defined(&CAP_INIT_INH_SET);
	eval 'sub CAP_TO_MASK {
	    local($x) = @_;
    	    eval q((1<< ($x)));
	}' unless defined(&CAP_TO_MASK);
	eval 'sub cap_raise {
	    local($c, $flag) = @_;
    	    eval q(( &cap_t($c) |=  &CAP_TO_MASK($flag)));
	}' unless defined(&cap_raise);
	eval 'sub cap_lower {
	    local($c, $flag) = @_;
    	    eval q(( &cap_t($c) &= ~ &CAP_TO_MASK($flag)));
	}' unless defined(&cap_lower);
	eval 'sub cap_raised {
	    local($c, $flag) = @_;
    	    eval q(( &cap_t($c) &  &CAP_TO_MASK($flag)));
	}' unless defined(&cap_raised);
	eval 'sub cap_combine {
	    local($a,$b) = @_;
    	    eval q({  &kernel_cap_t  &dest;  &cap_t( &dest) =  &cap_t($a) |  &cap_t($b);  &dest; });
	}' unless defined(&cap_combine);
	eval 'sub cap_intersect {
	    local($a,$b) = @_;
    	    eval q({  &kernel_cap_t  &dest;  &cap_t( &dest) =  &cap_t($a) &  &cap_t($b);  &dest; });
	}' unless defined(&cap_intersect);
	eval 'sub cap_drop {
	    local($a,$drop) = @_;
    	    eval q({  &kernel_cap_t  &dest;  &cap_t( &dest) =  &cap_t($a) & ~ &cap_t($drop);  &dest; });
	}' unless defined(&cap_drop);
	eval 'sub cap_invert {
	    local($c) = @_;
    	    eval q({  &kernel_cap_t  &dest;  &cap_t( &dest) = ~ &cap_t($c);  &dest; });
	}' unless defined(&cap_invert);
	eval 'sub cap_isclear {
	    local($c) = @_;
    	    eval q((! &cap_t($c)));
	}' unless defined(&cap_isclear);
	eval 'sub cap_issubset {
	    local($a,$set) = @_;
    	    eval q((!( &cap_t($a) & ~ &cap_t($set))));
	}' unless defined(&cap_issubset);
	eval 'sub cap_clear {
	    local($c) = @_;
    	    eval q( &do {  &cap_t($c) = 0; }  &while(0));
	}' unless defined(&cap_clear);
	eval 'sub cap_set_full {
	    local($c) = @_;
    	    eval q( &do {  &cap_t($c) = ~0; }  &while(0));
	}' unless defined(&cap_set_full);
	eval 'sub cap_mask {
	    local($c,$mask) = @_;
    	    eval q( &do {  &cap_t($c) &=  &cap_t($mask); }  &while(0));
	}' unless defined(&cap_mask);
	eval 'sub cap_is_fs_cap {
	    local($c) = @_;
    	    eval q(( &CAP_TO_MASK($c) &  &CAP_FS_MASK));
	}' unless defined(&cap_is_fs_cap);
    }
}
1;
