require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_SMP_H)) {
    eval 'sub __LINUX_SMP_H () {1;}' unless defined(&__LINUX_SMP_H);
    require 'linux/preempt.ph';
    require 'linux/kernel.ph';
    require 'linux/compiler.ph';
    require 'linux/thread_info.ph';
    require 'asm/smp.ph';
    require 'asm/bug.ph';
    eval 'sub MSG_ALL_BUT_SELF () {0x8000;}' unless defined(&MSG_ALL_BUT_SELF);
    eval 'sub MSG_ALL () {0x8001;}' unless defined(&MSG_ALL);
    eval 'sub MSG_INVALIDATE_TLB () {0x1;}' unless defined(&MSG_INVALIDATE_TLB);
    eval 'sub MSG_STOP_CPU () {0x2;}' unless defined(&MSG_STOP_CPU);
    eval 'sub MSG_RESCHEDULE () {0x3;}' unless defined(&MSG_RESCHEDULE);
    eval 'sub MSG_CALL_FUNCTION () {0x4;}' unless defined(&MSG_CALL_FUNCTION);
    eval 'sub get_cpu () {
        eval q(({  &preempt_disable();  &smp_processor_id(); }));
    }' unless defined(&get_cpu);
    eval 'sub put_cpu () {
        eval q( &preempt_enable());
    }' unless defined(&put_cpu);
    eval 'sub put_cpu_no_resched () {
        eval q( &preempt_enable_no_resched());
    }' unless defined(&put_cpu_no_resched);
}
1;
