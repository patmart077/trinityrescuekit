require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_KERNEL_H)) {
    eval 'sub _LINUX_KERNEL_H () {1;}' unless defined(&_LINUX_KERNEL_H);
    if(defined(&__KERNEL__)) {
	require 'stdarg.ph';
	require 'linux/linkage.ph';
	require 'linux/stddef.ph';
	require 'linux/types.ph';
	require 'linux/compiler.ph';
	require 'linux/bitops.ph';
	require 'asm/byteorder.ph';
	require 'asm/bug.ph';
	eval 'sub INT_MAX () {((~0>>1));}' unless defined(&INT_MAX);
	eval 'sub INT_MIN () {(- &INT_MAX - 1);}' unless defined(&INT_MIN);
	eval 'sub UINT_MAX () {(~0);}' unless defined(&UINT_MAX);
	eval 'sub LONG_MAX () {((~0>>1));}' unless defined(&LONG_MAX);
	eval 'sub LONG_MIN () {(- &LONG_MAX - 1);}' unless defined(&LONG_MIN);
	eval 'sub ULONG_MAX () {(~0);}' unless defined(&ULONG_MAX);
	eval 'sub STACK_MAGIC () {0xdeadbeef;}' unless defined(&STACK_MAGIC);
	eval 'sub ARRAY_SIZE {
	    local($x) = @_;
    	    eval q(($sizeof{$x} / $sizeof{($x)[0]}));
	}' unless defined(&ARRAY_SIZE);
	eval 'sub ALIGN {
	    local($x,$a) = @_;
    	    eval q(((($x)+($a)-1)&~(($a)-1)));
	}' unless defined(&ALIGN);
	eval 'sub KERN_EMERG () {"<0>";}' unless defined(&KERN_EMERG);
	eval 'sub KERN_ALERT () {"<1>";}' unless defined(&KERN_ALERT);
	eval 'sub KERN_CRIT () {"<2>";}' unless defined(&KERN_CRIT);
	eval 'sub KERN_ERR () {"<3>";}' unless defined(&KERN_ERR);
	eval 'sub KERN_WARNING () {"<4>";}' unless defined(&KERN_WARNING);
	eval 'sub KERN_NOTICE () {"<5>";}' unless defined(&KERN_NOTICE);
	eval 'sub KERN_INFO () {"<6>";}' unless defined(&KERN_INFO);
	eval 'sub KERN_DEBUG () {"<7>";}' unless defined(&KERN_DEBUG);
	eval 'sub console_loglevel () {( $console_printk[0]);}' unless defined(&console_loglevel);
	eval 'sub default_message_loglevel () {( $console_printk[1]);}' unless defined(&default_message_loglevel);
	eval 'sub minimum_console_loglevel () {( $console_printk[2]);}' unless defined(&minimum_console_loglevel);
	eval 'sub default_console_loglevel () {( $console_printk[3]);}' unless defined(&default_console_loglevel);
	if(defined(&CONFIG_DEBUG_SPINLOCK_SLEEP)) {
	    eval 'sub might_sleep () {
	        eval q( &__might_sleep( &__FILE__,  &__LINE__));
	    }' unless defined(&might_sleep);
	    eval 'sub might_sleep_if {
	        local($cond) = @_;
    		eval q( &do {  &if ( &unlikely($cond))  &might_sleep(); }  &while (0));
	    }' unless defined(&might_sleep_if);
	} else {
	    eval 'sub might_sleep () {
	        eval q( &do {}  &while(0));
	    }' unless defined(&might_sleep);
	    eval 'sub might_sleep_if {
	        local($cond) = @_;
    		eval q( &do {}  &while (0));
	    }' unless defined(&might_sleep_if);
	}
	eval 'sub abs {
	    local($x) = @_;
    	    eval q(({ \'int\'  &__x = ($x); ( &__x < 0) ? - &__x :  &__x; }));
	}' unless defined(&abs);
	eval 'sub labs {
	    local($x) = @_;
    	    eval q(({ \'long __x\' = ($x); ( &__x < 0) ? - &__x :  &__x; }));
	}' unless defined(&labs);
	eval 'sub long_log2 {
	    local($x) = @_;
    	    eval q({ \'int\'  &r = 0;  &for ($x >>= 1; $x > 0; $x >>= 1)  &r++;  &r; });
	}' unless defined(&long_log2);
	eval 'sub roundup_pow_of_two {
	    local($x) = @_;
    	    eval q({ (1 <<  &fls($x - 1)); });
	}' unless defined(&roundup_pow_of_two);
	eval 'sub console_silent {
	    local($void) = @_;
    	    eval q({  &console_loglevel = 0; });
	}' unless defined(&console_silent);
	eval 'sub console_verbose {
	    local($void) = @_;
    	    eval q({  &if ( &console_loglevel)  &console_loglevel = 15; });
	}' unless defined(&console_verbose);
	eval 'sub TAINT_PROPRIETARY_MODULE () {(1<<0);}' unless defined(&TAINT_PROPRIETARY_MODULE);
	eval 'sub TAINT_FORCED_MODULE () {(1<<1);}' unless defined(&TAINT_FORCED_MODULE);
	eval 'sub TAINT_UNSAFE_SMP () {(1<<2);}' unless defined(&TAINT_UNSAFE_SMP);
	eval 'sub TAINT_FORCED_RMMOD () {(1<<3);}' unless defined(&TAINT_FORCED_RMMOD);
	eval 'sub TAINT_MACHINE_CHECK () {(1<<4);}' unless defined(&TAINT_MACHINE_CHECK);
	eval 'sub TAINT_BAD_PAGE () {(1<<5);}' unless defined(&TAINT_BAD_PAGE);
	if(defined(&DEBUG)) {
	    eval 'sub pr_debug () {( &fmt, &arg...)  &printk( &KERN_DEBUG  &fmt, &arg);}' unless defined(&pr_debug);
	} else {
	    eval 'sub pr_debug () {( &fmt, &arg...)  &do { }  &while (0);}' unless defined(&pr_debug);
	}
	eval 'sub pr_info () {( &fmt, &arg...)  &printk( &KERN_INFO  &fmt, &arg);}' unless defined(&pr_info);
	eval 'sub NIPQUAD {
	    local($addr) = @_;
    	    eval q(($addr)[0], ($addr)[1], ($addr)[2], ($addr)[3]);
	}' unless defined(&NIPQUAD);
	eval 'sub NIP6 {
	    local($addr) = @_;
    	    eval q( &ntohs(($addr). $s6_addr16[0]),  &ntohs(($addr). $s6_addr16[1]),  &ntohs(($addr). $s6_addr16[2]),  &ntohs(($addr). $s6_addr16[3]),  &ntohs(($addr). $s6_addr16[4]),  &ntohs(($addr). $s6_addr16[5]),  &ntohs(($addr). $s6_addr16[6]),  &ntohs(($addr). $s6_addr16[7]));
	}' unless defined(&NIP6);
	if(defined( &__LITTLE_ENDIAN)) {
	    eval 'sub HIPQUAD {
	        local($addr) = @_;
    		eval q(($addr)[3], ($addr)[2], ($addr)[1], ($addr)[0]);
	    }' unless defined(&HIPQUAD);
	}
 elsif(defined( &__BIG_ENDIAN)) {
	    eval 'sub HIPQUAD () { &NIPQUAD;}' unless defined(&HIPQUAD);
	} else {
	    die("Please fix asm/byteorder.h");
	}
	eval 'sub min {
	    local($x,$y) = @_;
    	    eval q(({  &typeof($x)  &_x = ($x);  &typeof($y)  &_y = ($y); ( &void) (& &_x == & &_y);  &_x <  &_y ?  &_x :  &_y; }));
	}' unless defined(&min);
	eval 'sub max {
	    local($x,$y) = @_;
    	    eval q(({  &typeof($x)  &_x = ($x);  &typeof($y)  &_y = ($y); ( &void) (& &_x == & &_y);  &_x >  &_y ?  &_x :  &_y; }));
	}' unless defined(&max);
	eval 'sub min_t {
	    local($type,$x,$y) = @_;
    	    eval q(({ $type  &__x = ($x); $type  &__y = ($y);  &__x <  &__y ?  &__x:  &__y; }));
	}' unless defined(&min_t);
	eval 'sub max_t {
	    local($type,$x,$y) = @_;
    	    eval q(({ $type  &__x = ($x); $type  &__y = ($y);  &__x >  &__y ?  &__x:  &__y; }));
	}' unless defined(&max_t);
	eval 'sub container_of {
	    local($ptr, $type, $member) = @_;
    	    eval q(({  &const  &typeof( (($type *)0)->$member ) * &__mptr = ($ptr); ($type *)(  &__mptr -  &offsetof($type,$member) );}));
	}' unless defined(&container_of);
	eval 'sub typecheck {
	    local($type,$x) = @_;
    	    eval q(({ $type  &__dummy;  &typeof($x)  &__dummy2; ( &void)(& &__dummy == & &__dummy2); 1; }));
	}' unless defined(&typecheck);
    }
    eval 'sub SI_LOAD_SHIFT () {16;}' unless defined(&SI_LOAD_SHIFT);
    eval 'sub BUILD_BUG_ON {
        local($condition) = @_;
	    eval q( &do {  &if ($condition)  &BUILD_BUG(); }  &while(0));
    }' unless defined(&BUILD_BUG_ON);
    if((defined(&__GNUC__) ? &__GNUC__ : 0) > 2|| (defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 95) {
	eval 'sub __FUNCTION__ () {( &__func__);}' unless defined(&__FUNCTION__);
    }
}
1;
