require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_STDDEF_H)) {
    eval 'sub _LINUX_STDDEF_H () {1;}' unless defined(&_LINUX_STDDEF_H);
    require 'linux/compiler.ph';
    undef(&NULL) if defined(&NULL);
    if(defined( &__cplusplus)) {
	eval 'sub NULL () {0;}' unless defined(&NULL);
    } else {
	eval 'sub NULL () {(( &void *)0);}' unless defined(&NULL);
    }
    undef(&offsetof) if defined(&offsetof);
    if(defined(&__compiler_offsetof)) {
	eval 'sub offsetof {
	    local($TYPE,$MEMBER) = @_;
    	    eval q( &__compiler_offsetof($TYPE,$MEMBER));
	}' unless defined(&offsetof);
    } else {
	eval 'sub offsetof {
	    local($TYPE, $MEMBER) = @_;
    	    eval q(( (($TYPE *)0)->$MEMBER));
	}' unless defined(&offsetof);
    }
}
1;
