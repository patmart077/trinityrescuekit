require '_h2ph_pre.ph';

no warnings 'redefine';

require 'linux/compiler-gcc.ph';
unless(defined(&__KERNEL__)) {
    eval 'sub inline () { &inline  &__attribute__(( &always_inline));}' unless defined(&inline);
    eval 'sub __inline__ () { &__inline__  &__attribute__(( &always_inline));}' unless defined(&__inline__);
    eval 'sub __inline () { &__inline  &__attribute__(( &always_inline));}' unless defined(&__inline);
}
unless(defined(&__deprecated)) {
    sub __deprecated () {	 &__attribute__(( &deprecated));}
}
unless(defined(&__attribute_used__)) {
    sub __attribute_used__ () {	 &__attribute__(( &__used__));}
}
unless(defined(&__attribute_pure__)) {
    sub __attribute_pure__ () {	 &__attribute__(( &pure));}
}
unless(defined(&__attribute_const__)) {
    sub __attribute_const__ () {	 &__attribute__(( &__const__));}
}
unless(defined(&__must_check)) {
    sub __must_check () {	 &__attribute__(( &warn_unused_result));}
}
1;
