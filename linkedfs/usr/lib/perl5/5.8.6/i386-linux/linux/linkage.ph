require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_LINKAGE_H)) {
    eval 'sub _LINUX_LINKAGE_H () {1;}' unless defined(&_LINUX_LINKAGE_H);
    require 'linux/config.ph';
    require 'asm/linkage.ph';
    if(defined(&__cplusplus)) {
	eval 'sub CPP_ASMLINKAGE () { &extern "C";}' unless defined(&CPP_ASMLINKAGE);
    } else {
	eval 'sub CPP_ASMLINKAGE () {1;}' unless defined(&CPP_ASMLINKAGE);
    }
    unless(defined(&asmlinkage)) {
	eval 'sub asmlinkage () { &CPP_ASMLINKAGE;}' unless defined(&asmlinkage);
    }
    unless(defined(&prevent_tail_call)) {
	eval 'sub prevent_tail_call {
	    local($ret) = @_;
    	    eval q( &do { }  &while (0));
	}' unless defined(&prevent_tail_call);
    }
    unless(defined(&__ALIGN)) {
	eval 'sub __ALIGN () {. &align 4,0x90;}' unless defined(&__ALIGN);
	eval 'sub __ALIGN_STR () {".align 4,0x90";}' unless defined(&__ALIGN_STR);
    }
    if(defined(&__ASSEMBLY__)) {
	eval 'sub ALIGN () { &__ALIGN;}' unless defined(&ALIGN);
	eval 'sub ALIGN_STR () { &__ALIGN_STR;}' unless defined(&ALIGN_STR);
	eval 'sub ENTRY {
	    local($name) = @_;
    	    eval q(. &globl $name;  &ALIGN; $name:);
	}' unless defined(&ENTRY);
    }
    eval 'sub NORET_TYPE () {1;}' unless defined(&NORET_TYPE);
    eval 'sub ATTRIB_NORET () { &__attribute__(( &noreturn));}' unless defined(&ATTRIB_NORET);
    eval 'sub NORET_AND () { &noreturn,;}' unless defined(&NORET_AND);
    unless(defined(&FASTCALL)) {
	eval 'sub FASTCALL {
	    local($x) = @_;
    	    eval q($x);
	}' unless defined(&FASTCALL);
	eval 'sub fastcall () {1;}' unless defined(&fastcall);
    }
}
1;
