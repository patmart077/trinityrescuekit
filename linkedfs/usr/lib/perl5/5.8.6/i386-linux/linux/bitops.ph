require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_BITOPS_H)) {
    eval 'sub _LINUX_BITOPS_H () {1;}' unless defined(&_LINUX_BITOPS_H);
    require 'asm/types.ph';
    require 'asm/bitops.ph';
    eval 'sub get_bitmask_order {
        local($count) = @_;
	    eval q({ \'int\'  &order;  &order =  &fls($count);  &order; });
    }' unless defined(&get_bitmask_order);
    eval 'sub generic_hweight16 {
        local($w) = @_;
	    eval q({ my $res = ($w & 0x5555) + (($w >> 1) & 0x5555);  $res = ( $res & 0x3333) + (( $res >> 2) & 0x3333);  $res = ( $res & 0xf0f) + (( $res >> 4) & 0xf0f); ( $res & 0xff) + (( $res >> 8) & 0xff); });
    }' unless defined(&generic_hweight16);
    eval 'sub generic_hweight8 {
        local($w) = @_;
	    eval q({ my $res = ($w & 0x55) + (($w >> 1) & 0x55);  $res = ( $res & 0x33) + (( $res >> 2) & 0x33); ( $res & 0xf) + (( $res >> 4) & 0xf); });
    }' unless defined(&generic_hweight8);
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub generic_hweight64 {
        local($w) = @_;
	    eval q({ });
    }' unless defined(&generic_hweight64);
    eval 'sub hweight_long {
        local($w) = @_;
	    eval q({ $sizeof{$w} == 4?  &generic_hweight32($w) :  &generic_hweight64($w); });
    }' unless defined(&hweight_long);
}
1;
