require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX__AIO_H)) {
    eval 'sub __LINUX__AIO_H () {1;}' unless defined(&__LINUX__AIO_H);
    require 'linux/list.ph';
    require 'linux/workqueue.ph';
    require 'linux/aio_abi.ph';
    require 'asm/atomic.ph';
    eval 'sub AIO_MAXSEGS () {4;}' unless defined(&AIO_MAXSEGS);
    eval 'sub AIO_KIOGRP_NR_ATOMIC () {8;}' unless defined(&AIO_KIOGRP_NR_ATOMIC);
    eval 'sub KIOCB_C_CANCELLED () {0x1;}' unless defined(&KIOCB_C_CANCELLED);
    eval 'sub KIOCB_C_COMPLETE () {0x2;}' unless defined(&KIOCB_C_COMPLETE);
    eval 'sub KIOCB_SYNC_KEY () {(~0);}' unless defined(&KIOCB_SYNC_KEY);
    eval 'sub KIF_LOCKED () {0;}' unless defined(&KIF_LOCKED);
    eval 'sub KIF_KICKED () {1;}' unless defined(&KIF_KICKED);
    eval 'sub KIF_CANCELLED () {2;}' unless defined(&KIF_CANCELLED);
    eval 'sub kiocbTryLock {
        local($iocb) = @_;
	    eval q( &test_and_set_bit( &KIF_LOCKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbTryLock);
    eval 'sub kiocbTryKick {
        local($iocb) = @_;
	    eval q( &test_and_set_bit( &KIF_KICKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbTryKick);
    eval 'sub kiocbSetLocked {
        local($iocb) = @_;
	    eval q( &set_bit( &KIF_LOCKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbSetLocked);
    eval 'sub kiocbSetKicked {
        local($iocb) = @_;
	    eval q( &set_bit( &KIF_KICKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbSetKicked);
    eval 'sub kiocbSetCancelled {
        local($iocb) = @_;
	    eval q( &set_bit( &KIF_CANCELLED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbSetCancelled);
    eval 'sub kiocbClearLocked {
        local($iocb) = @_;
	    eval q( &clear_bit( &KIF_LOCKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbClearLocked);
    eval 'sub kiocbClearKicked {
        local($iocb) = @_;
	    eval q( &clear_bit( &KIF_KICKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbClearKicked);
    eval 'sub kiocbClearCancelled {
        local($iocb) = @_;
	    eval q( &clear_bit( &KIF_CANCELLED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbClearCancelled);
    eval 'sub kiocbIsLocked {
        local($iocb) = @_;
	    eval q( &test_bit( &KIF_LOCKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbIsLocked);
    eval 'sub kiocbIsKicked {
        local($iocb) = @_;
	    eval q( &test_bit( &KIF_KICKED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbIsKicked);
    eval 'sub kiocbIsCancelled {
        local($iocb) = @_;
	    eval q( &test_bit( &KIF_CANCELLED, ($iocb)-> &ki_flags));
    }' unless defined(&kiocbIsCancelled);
    eval 'sub is_sync_kiocb {
        local($iocb) = @_;
	    eval q((($iocb)-> &ki_key ==  &KIOCB_SYNC_KEY));
    }' unless defined(&is_sync_kiocb);
    eval 'sub init_sync_kiocb {
        local($x, $filp) = @_;
	    eval q( &do { \'struct task_struct\' * &tsk =  &current; ($x)-> &ki_flags = 0; ($x)-> &ki_users = 1; ($x)-> &ki_key =  &KIOCB_SYNC_KEY; ($x)-> &ki_filp = ($filp); ($x)-> &ki_ctx =  ($tsk->{active_mm}->{default_kioctx}); ($x)-> &ki_cancel =  &NULL; ($x)-> &ki_dtor =  &NULL; ($x)-> ($ki_obj->{tsk}) =  &tsk; ($x)-> &ki_user_data = 0;  &init_wait((($x)-> &ki_wait)); }  &while (0));
    }' unless defined(&init_sync_kiocb);
    eval 'sub AIO_RING_MAGIC () {0xa10a10a1;}' unless defined(&AIO_RING_MAGIC);
    eval 'sub AIO_RING_COMPAT_FEATURES () {1;}' unless defined(&AIO_RING_COMPAT_FEATURES);
    eval 'sub AIO_RING_INCOMPAT_FEATURES () {0;}' unless defined(&AIO_RING_INCOMPAT_FEATURES);
    eval 'sub aio_ring_avail {
        local($info, $ring) = @_;
	    eval q(((($ring)-> &head + ($info)-> &nr - 1- ($ring)-> &tail) % ($info)-> &nr));
    }' unless defined(&aio_ring_avail);
    eval 'sub AIO_RING_PAGES () {8;}' unless defined(&AIO_RING_PAGES);
    eval 'sub get_ioctx {
        local($kioctx) = @_;
	    eval q( &do {  &if ( &unlikely( &atomic_read(($kioctx)-> &users) <= 0))  &BUG();  &atomic_inc(($kioctx)-> &users); }  &while (0));
    }' unless defined(&get_ioctx);
    eval 'sub put_ioctx {
        local($kioctx) = @_;
	    eval q( &do {  &if ( &unlikely( &atomic_dec_and_test(($kioctx)-> &users)))  &__put_ioctx($kioctx);  &else  &if ( &unlikely( &atomic_read(($kioctx)-> &users) < 0))  &BUG(); }  &while (0));
    }' unless defined(&put_ioctx);
    eval 'sub in_aio () {
        eval q(! &is_sync_wait( ($current->{io_wait})));
    }' unless defined(&in_aio);
    eval 'sub warn_if_async () {
        eval q( &do {  &if ( &in_aio()) {  &printk( &KERN_ERR \\"%s(%s:%d) called in async context!\\\\n\\",  &__FUNCTION__,  &__FILE__,  &__LINE__);  &dump_stack(); } }  &while (0));
    }' unless defined(&warn_if_async);
    eval 'sub io_wait_to_kiocb {
        local($wait) = @_;
	    eval q( &container_of($wait, \'struct kiocb\',  &ki_wait));
    }' unless defined(&io_wait_to_kiocb);
    eval 'sub is_retried_kiocb {
        local($iocb) = @_;
	    eval q((($iocb)-> &ki_retried > 1));
    }' unless defined(&is_retried_kiocb);
    require 'linux/aio_abi.ph';
    eval 'sub kiocb {
        eval q(* &list_kiocb(\'struct list_head\' * &h) {  &list_entry( &h, \'struct kiocb\',  &ki_list); });
    }' unless defined(&kiocb);
}
1;
