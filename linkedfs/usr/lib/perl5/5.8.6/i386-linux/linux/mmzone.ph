require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_MMZONE_H)) {
    eval 'sub _LINUX_MMZONE_H () {1;}' unless defined(&_LINUX_MMZONE_H);
    if(defined(&__KERNEL__)) {
	unless(defined(&__ASSEMBLY__)) {
	    require 'linux/config.ph';
	    require 'linux/spinlock.ph';
	    require 'linux/list.ph';
	    require 'linux/wait.ph';
	    require 'linux/cache.ph';
	    require 'linux/threads.ph';
	    require 'linux/numa.ph';
	    require 'asm/atomic.ph';
	    unless(defined(&CONFIG_FORCE_MAX_ZONEORDER)) {
		eval 'sub MAX_ORDER () {11;}' unless defined(&MAX_ORDER);
	    } else {
		eval 'sub MAX_ORDER () { &CONFIG_FORCE_MAX_ZONEORDER;}' unless defined(&MAX_ORDER);
	    }
	    if(defined( &CONFIG_SMP)) {
		eval 'sub ZONE_PADDING {
		    local($name) = @_;
    		    eval q(\'struct zone_padding\' $name;);
		}' unless defined(&ZONE_PADDING);
	    } else {
		eval 'sub ZONE_PADDING {
		    local($name) = @_;
    		    eval q();
		}' unless defined(&ZONE_PADDING);
	    }
	    if(defined(&CONFIG_NUMA)) {
	    }
	    eval 'sub ZONE_DMA () {0;}' unless defined(&ZONE_DMA);
	    eval 'sub ZONE_NORMAL () {1;}' unless defined(&ZONE_NORMAL);
	    eval 'sub ZONE_HIGHMEM () {2;}' unless defined(&ZONE_HIGHMEM);
	    eval 'sub MAX_NR_ZONES () {3;}' unless defined(&MAX_NR_ZONES);
	    eval 'sub ZONES_SHIFT () {2;}' unless defined(&ZONES_SHIFT);
	    eval 'sub GFP_ZONEMASK () {0x3;}' unless defined(&GFP_ZONEMASK);
	    eval 'sub GFP_ZONETYPES () {(( &GFP_ZONEMASK + 1) / 2+ 1);}' unless defined(&GFP_ZONETYPES);
	    eval 'sub DEF_PRIORITY () {12;}' unless defined(&DEF_PRIORITY);
	    eval 'sub node_present_pages {
	        local($nid) = @_;
    		eval q(( &NODE_DATA($nid)-> &node_present_pages));
	    }' unless defined(&node_present_pages);
	    eval 'sub node_spanned_pages {
	        local($nid) = @_;
    		eval q(( &NODE_DATA($nid)-> &node_spanned_pages));
	    }' unless defined(&node_spanned_pages);
	    eval 'sub zone_idx {
	        local($zone) = @_;
    		eval q((($zone) - ($zone)-> ($zone_pgdat->{node_zones})));
	    }' unless defined(&zone_idx);
	    eval 'sub for_each_pgdat {
	        local($pgdat) = @_;
    		eval q( &for ($pgdat =  &pgdat_list; $pgdat; $pgdat =  ($pgdat->{pgdat_next})));
	    }' unless defined(&for_each_pgdat);
	    eval 'sub for_each_zone {
	        local($zone) = @_;
    		eval q( &for ($zone =  ($pgdat_list->{node_zones}); $zone; $zone =  &next_zone($zone)));
	    }' unless defined(&for_each_zone);
	    eval 'sub is_highmem_idx {
	        local($idx) = @_;
    		eval q({ ($idx ==  &ZONE_HIGHMEM); });
	    }' unless defined(&is_highmem_idx);
	    eval 'sub is_normal_idx {
	        local($idx) = @_;
    		eval q({ ($idx ==  &ZONE_NORMAL); });
	    }' unless defined(&is_normal_idx);
	    eval 'sub is_normal {
	        local($zone) = @_;
    		eval q({ ( &is_normal_idx($zone -  ($zone->{zone_pgdat}->{node_zones}))); });
	    }' unless defined(&is_normal);
	    require 'linux/topology.ph';
	    eval 'sub numa_node_id () {
	        eval q(( &cpu_to_node( &smp_processor_id())));
	    }' unless defined(&numa_node_id);
	    unless(defined(&CONFIG_DISCONTIGMEM)) {
		eval 'sub NODE_DATA {
		    local($nid) = @_;
    		    eval q(( &contig_page_data));
		}' unless defined(&NODE_DATA);
		eval 'sub NODE_MEM_MAP {
		    local($nid) = @_;
    		    eval q( &mem_map);
		}' unless defined(&NODE_MEM_MAP);
		eval 'sub MAX_NODES_SHIFT () {1;}' unless defined(&MAX_NODES_SHIFT);
		eval 'sub pfn_to_nid {
		    local($pfn) = @_;
    		    eval q((0));
		}' unless defined(&pfn_to_nid);
	    } else {
		require 'asm/mmzone.ph';
		if((defined(&BITS_PER_LONG) ? &BITS_PER_LONG : 0) == 32|| defined( &ARCH_HAS_ATOMIC_UNSIGNED)) {
		    eval 'sub MAX_NODES_SHIFT () {6;}' unless defined(&MAX_NODES_SHIFT);
		}
 elsif((defined(&BITS_PER_LONG) ? &BITS_PER_LONG : 0) == 64) {
		    eval 'sub MAX_NODES_SHIFT () {10;}' unless defined(&MAX_NODES_SHIFT);
		}
	    }
	    if((defined(&NODES_SHIFT) ? &NODES_SHIFT : 0) > (defined(&MAX_NODES_SHIFT) ? &MAX_NODES_SHIFT : 0)) {
		die("NODES_SHIFT\ \>\ MAX_NODES_SHIFT");
	    }
	    eval 'sub MAX_ZONES_SHIFT () {2;}' unless defined(&MAX_ZONES_SHIFT);
	    if((defined(&ZONES_SHIFT) ? &ZONES_SHIFT : 0) > (defined(&MAX_ZONES_SHIFT) ? &MAX_ZONES_SHIFT : 0)) {
		die("ZONES_SHIFT\ \>\ MAX_ZONES_SHIFT");
	    }
	}
    }
}
1;
