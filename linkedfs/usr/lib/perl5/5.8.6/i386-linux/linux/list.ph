require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_LIST_H)) {
    eval 'sub _LINUX_LIST_H () {1;}' unless defined(&_LINUX_LIST_H);
    if(defined(&__KERNEL__)) {
	require 'linux/stddef.ph';
	require 'linux/prefetch.ph';
	require 'asm/system.ph';
	eval 'sub LIST_POISON1 () {(( &void *) 0x100100);}' unless defined(&LIST_POISON1);
	eval 'sub LIST_POISON2 () {(( &void *) 0x200200);}' unless defined(&LIST_POISON2);
	eval 'sub LIST_HEAD_INIT {
	    local($name) = @_;
    	    eval q({ ($name), ($name) });
	}' unless defined(&LIST_HEAD_INIT);
	eval 'sub LIST_HEAD {
	    local($name) = @_;
    	    eval q(\'struct list_head\' $name =  &LIST_HEAD_INIT($name));
	}' unless defined(&LIST_HEAD);
	eval 'sub INIT_LIST_HEAD {
	    local($ptr) = @_;
    	    eval q( &do { ($ptr)-> &next = ($ptr); ($ptr)-> &prev = ($ptr); }  &while (0));
	}' unless defined(&INIT_LIST_HEAD);
	eval 'sub __list_splice {
	    local($list,$head) = @_;
    	    eval q({ \'struct list_head\' * &first =  ($list->{next}); \'struct list_head\' * &last =  ($list->{prev}); \'struct list_head\' * &at =  ($head->{next});  ($first->{prev}) = $head;  ($head->{next}) =  &first;  ($last->{next}) =  &at;  ($at->{prev}) =  &last; });
	}' unless defined(&__list_splice);
	eval 'sub list_entry {
	    local($ptr, $type, $member) = @_;
    	    eval q( &container_of($ptr, $type, $member));
	}' unless defined(&list_entry);
	eval 'sub list_for_each {
	    local($pos, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &next,  &prefetch( ($pos->{next})); $pos != ($head); $pos =  ($pos->{next}),  &prefetch( ($pos->{next}))));
	}' unless defined(&list_for_each);
	eval 'sub __list_for_each {
	    local($pos, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &next; $pos != ($head); $pos =  ($pos->{next})));
	}' unless defined(&__list_for_each);
	eval 'sub list_for_each_prev {
	    local($pos, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &prev,  &prefetch( ($pos->{prev})); $pos != ($head); $pos =  ($pos->{prev}),  &prefetch( ($pos->{prev}))));
	}' unless defined(&list_for_each_prev);
	eval 'sub list_for_each_safe {
	    local($pos, $n, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &next, $n =  ($pos->{next}); $pos != ($head); $pos = $n, $n =  ($pos->{next})));
	}' unless defined(&list_for_each_safe);
	eval 'sub list_for_each_entry {
	    local($pos, $head, $member) = @_;
    	    eval q( &for ($pos =  &list_entry(($head)-> &next,  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{next}));  ($pos->{member}) != ($head); $pos =  &list_entry( ($pos->{member}->{next}),  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{next}))));
	}' unless defined(&list_for_each_entry);
	eval 'sub list_for_each_entry_reverse {
	    local($pos, $head, $member) = @_;
    	    eval q( &for ($pos =  &list_entry(($head)-> &prev,  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{prev}));  ($pos->{member}) != ($head); $pos =  &list_entry( ($pos->{member}->{prev}),  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{prev}))));
	}' unless defined(&list_for_each_entry_reverse);
	eval 'sub list_prepare_entry {
	    local($pos, $head, $member) = @_;
    	    eval q((($pos) ? :  &list_entry($head,  &typeof(*$pos), $member)));
	}' unless defined(&list_prepare_entry);
	eval 'sub list_for_each_entry_continue {
	    local($pos, $head, $member) = @_;
    	    eval q( &for ($pos =  &list_entry( ($pos->{member}->{next}),  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{next}));  ($pos->{member}) != ($head); $pos =  &list_entry( ($pos->{member}->{next}),  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{next}))));
	}' unless defined(&list_for_each_entry_continue);
	eval 'sub list_for_each_entry_safe {
	    local($pos, $n, $head, $member) = @_;
    	    eval q( &for ($pos =  &list_entry(($head)-> &next,  &typeof(*$pos), $member), $n =  &list_entry( ($pos->{member}->{next}),  &typeof(*$pos), $member);  ($pos->{member}) != ($head); $pos = $n, $n =  &list_entry( ($n->{member}->{next}),  &typeof(*$n), $member)));
	}' unless defined(&list_for_each_entry_safe);
	eval 'sub list_for_each_rcu {
	    local($pos, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &next,  &prefetch( ($pos->{next})); $pos != ($head); $pos =  &rcu_dereference( ($pos->{next})),  &prefetch( ($pos->{next}))));
	}' unless defined(&list_for_each_rcu);
	eval 'sub __list_for_each_rcu {
	    local($pos, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &next; $pos != ($head); $pos =  &rcu_dereference( ($pos->{next}))));
	}' unless defined(&__list_for_each_rcu);
	eval 'sub list_for_each_safe_rcu {
	    local($pos, $n, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &next, $n =  ($pos->{next}); $pos != ($head); $pos =  &rcu_dereference($n), $n =  ($pos->{next})));
	}' unless defined(&list_for_each_safe_rcu);
	eval 'sub list_for_each_entry_rcu {
	    local($pos, $head, $member) = @_;
    	    eval q( &for ($pos =  &list_entry(($head)-> &next,  &typeof(*$pos), $member),  &prefetch( ($pos->{member}->{next}));  ($pos->{member}) != ($head); $pos =  &rcu_dereference( &list_entry( ($pos->{member}->{next}),  &typeof(*$pos), $member)),  &prefetch( ($pos->{member}->{next}))));
	}' unless defined(&list_for_each_entry_rcu);
	eval 'sub list_for_each_continue_rcu {
	    local($pos, $head) = @_;
    	    eval q( &for (($pos) = ($pos)-> &next,  &prefetch(($pos)-> &next); ($pos) != ($head); ($pos) =  &rcu_dereference(($pos)-> &next),  &prefetch(($pos)-> &next)));
	}' unless defined(&list_for_each_continue_rcu);
	eval 'sub HLIST_HEAD_INIT () {{ . &first =  &NULL };}' unless defined(&HLIST_HEAD_INIT);
	eval 'sub HLIST_HEAD {
	    local($name) = @_;
    	    eval q(\'struct hlist_head\' $name = { . &first =  &NULL });
	}' unless defined(&HLIST_HEAD);
	eval 'sub INIT_HLIST_HEAD {
	    local($ptr) = @_;
    	    eval q((($ptr)-> &first =  &NULL));
	}' unless defined(&INIT_HLIST_HEAD);
	eval 'sub INIT_HLIST_NODE {
	    local($ptr) = @_;
    	    eval q((($ptr)-> &next =  &NULL, ($ptr)-> &pprev =  &NULL));
	}' unless defined(&INIT_HLIST_NODE);
	eval 'sub hlist_unhashed {
	    local($h) = @_;
    	    eval q({ ! ($h->{pprev}); });
	}' unless defined(&hlist_unhashed);
	eval 'sub hlist_empty {
	    local($h) = @_;
    	    eval q({ ! ($h->{first}); });
	}' unless defined(&hlist_empty);
	eval 'sub __hlist_del {
	    local($n) = @_;
    	    eval q({ \'struct hlist_node\' * &next =  ($n->{next}); \'struct hlist_node\' ** &pprev =  ($n->{pprev}); * &pprev =  &next;  &if ( &next)  ($next->{pprev}) =  &pprev; });
	}' unless defined(&__hlist_del);
	eval 'sub hlist_del {
	    local($n) = @_;
    	    eval q({  &__hlist_del($n);  ($n->{next}) =  &LIST_POISON1;  ($n->{pprev}) =  &LIST_POISON2; });
	}' unless defined(&hlist_del);
	eval 'sub hlist_del_init {
	    local($n) = @_;
    	    eval q({  &if ( ($n->{pprev})) {  &__hlist_del($n);  &INIT_HLIST_NODE($n); } });
	}' unless defined(&hlist_del_init);
	eval 'sub hlist_add_head {
	    local($n,$h) = @_;
    	    eval q({ \'struct hlist_node\' * &first =  ($h->{first});  ($n->{next}) =  &first;  &if ( &first)  ($first->{pprev}) =  ($n->{next});  ($h->{first}) = $n;  ($n->{pprev}) =  ($h->{first}); });
	}' unless defined(&hlist_add_head);
	eval 'sub hlist_add_after {
	    local($n,$next) = @_;
    	    eval q({  ($next->{next}) =  ($n->{next});  ($n->{next}) = $next;  ($next->{pprev}) =  ($n->{next});  &if( ($next->{next}))  ($next->{next}->{pprev})  =  ($next->{next}); });
	}' unless defined(&hlist_add_after);
	eval 'sub hlist_entry {
	    local($ptr, $type, $member) = @_;
    	    eval q( &container_of($ptr,$type,$member));
	}' unless defined(&hlist_entry);
	eval 'sub hlist_for_each {
	    local($pos, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &first; $pos  && ({  &prefetch( ($pos->{next})); 1; }); $pos =  ($pos->{next})));
	}' unless defined(&hlist_for_each);
	eval 'sub hlist_for_each_safe {
	    local($pos, $n, $head) = @_;
    	    eval q( &for ($pos = ($head)-> &first; $pos  && ({ $n =  ($pos->{next}); 1; }); $pos = $n));
	}' unless defined(&hlist_for_each_safe);
	eval 'sub hlist_for_each_rcu {
	    local($pos, $head) = @_;
    	    eval q( &for (($pos) = ($head)-> &first; $pos  && ({  &prefetch(($pos)-> &next); 1; }); ($pos) =  &rcu_dereference(($pos)-> &next)));
	}' unless defined(&hlist_for_each_rcu);
	eval 'sub hlist_for_each_entry {
	    local($tpos, $pos, $head, $member) = @_;
    	    eval q( &for ($pos = ($head)-> &first; $pos  && ({  &prefetch( ($pos->{next})); 1;})  && ({ $tpos =  &hlist_entry($pos,  &typeof(*$tpos), $member); 1;}); $pos =  ($pos->{next})));
	}' unless defined(&hlist_for_each_entry);
	eval 'sub hlist_for_each_entry_continue {
	    local($tpos, $pos, $member) = @_;
    	    eval q( &for ($pos = ($pos)-> &next; $pos  && ({  &prefetch( ($pos->{next})); 1;})  && ({ $tpos =  &hlist_entry($pos,  &typeof(*$tpos), $member); 1;}); $pos =  ($pos->{next})));
	}' unless defined(&hlist_for_each_entry_continue);
	eval 'sub hlist_for_each_entry_from {
	    local($tpos, $pos, $member) = @_;
    	    eval q( &for (; $pos  && ({  &prefetch( ($pos->{next})); 1;})  && ({ $tpos =  &hlist_entry($pos,  &typeof(*$tpos), $member); 1;}); $pos =  ($pos->{next})));
	}' unless defined(&hlist_for_each_entry_from);
	eval 'sub hlist_for_each_entry_safe {
	    local($tpos, $pos, $n, $head, $member) = @_;
    	    eval q( &for ($pos = ($head)-> &first; $pos  && ({ $n =  ($pos->{next}); 1; })  && ({ $tpos =  &hlist_entry($pos,  &typeof(*$tpos), $member); 1;}); $pos = $n));
	}' unless defined(&hlist_for_each_entry_safe);
	eval 'sub hlist_for_each_entry_rcu {
	    local($tpos, $pos, $head, $member) = @_;
    	    eval q( &for ($pos = ($head)-> &first; $pos  && ({  &prefetch( ($pos->{next})); 1;})  && ({ $tpos =  &hlist_entry($pos,  &typeof(*$tpos), $member); 1;}); $pos =  &rcu_dereference( ($pos->{next}))));
	}' unless defined(&hlist_for_each_entry_rcu);
    } else {
	warn("\"don\'t\ include\ kernel\ headers\ in\ userspace\"");
    }
}
1;
