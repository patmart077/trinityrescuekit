require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_SEQLOCK_H)) {
    eval 'sub __LINUX_SEQLOCK_H () {1;}' unless defined(&__LINUX_SEQLOCK_H);
    require 'linux/config.ph';
    require 'linux/spinlock.ph';
    require 'linux/preempt.ph';
    eval 'sub SEQLOCK_UNLOCKED () {{ 0,  &SPIN_LOCK_UNLOCKED };}' unless defined(&SEQLOCK_UNLOCKED);
    eval 'sub seqlock_init {
        local($x) = @_;
	    eval q( &do { *($x) = ( &seqlock_t)  &SEQLOCK_UNLOCKED; }  &while (0));
    }' unless defined(&seqlock_init);
    eval 'sub write_sequnlock {
        local($sl) = @_;
	    eval q({  &smp_wmb();  ($sl->{sequence})++;  &spin_unlock( ($sl->{lock})); });
    }' unless defined(&write_sequnlock);
    eval 'sub write_tryseqlock {
        local($sl) = @_;
	    eval q({ \'int\'  &ret =  &spin_trylock( ($sl->{lock}));  &if ( &ret) { ++ ($sl->{sequence});  &smp_wmb(); }  &ret; });
    }' unless defined(&write_tryseqlock);
    eval 'sub SEQCNT_ZERO () {{ 0};}' unless defined(&SEQCNT_ZERO);
    eval 'sub seqcount_init {
        local($x) = @_;
	    eval q( &do { *($x) = ( &seqcount_t)  &SEQCNT_ZERO; }  &while (0));
    }' unless defined(&seqcount_init);
    eval 'sub write_seqcount_end {
        local($s) = @_;
	    eval q({  &smp_wmb();  ($s->{sequence})++; });
    }' unless defined(&write_seqcount_end);
    eval 'sub write_seqlock_irqsave {
        local($lock, $flags) = @_;
	    eval q( &do {  &local_irq_save($flags);  &write_seqlock($lock); }  &while (0));
    }' unless defined(&write_seqlock_irqsave);
    eval 'sub write_seqlock_irq {
        local($lock) = @_;
	    eval q( &do {  &local_irq_disable();  &write_seqlock($lock); }  &while (0));
    }' unless defined(&write_seqlock_irq);
    eval 'sub write_seqlock_bh {
        local($lock) = @_;
	    eval q( &do {  &local_bh_disable();  &write_seqlock($lock); }  &while (0));
    }' unless defined(&write_seqlock_bh);
    eval 'sub write_sequnlock_irqrestore {
        local($lock, $flags) = @_;
	    eval q( &do {  &write_sequnlock($lock);  &local_irq_restore($flags); }  &while(0));
    }' unless defined(&write_sequnlock_irqrestore);
    eval 'sub write_sequnlock_irq {
        local($lock) = @_;
	    eval q( &do {  &write_sequnlock($lock);  &local_irq_enable(); }  &while(0));
    }' unless defined(&write_sequnlock_irq);
    eval 'sub write_sequnlock_bh {
        local($lock) = @_;
	    eval q( &do {  &write_sequnlock($lock);  &local_bh_enable(); }  &while(0));
    }' unless defined(&write_sequnlock_bh);
    eval 'sub read_seqbegin_irqsave {
        local($lock, $flags) = @_;
	    eval q(({  &local_irq_save($flags);  &read_seqbegin($lock); }));
    }' unless defined(&read_seqbegin_irqsave);
    eval 'sub read_seqretry_irqrestore {
        local($lock, $iv, $flags) = @_;
	    eval q(({ \'int\'  &ret =  &read_seqretry($lock, $iv);  &local_irq_restore($flags);  &ret; }));
    }' unless defined(&read_seqretry_irqrestore);
}
1;
