require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_WORKQUEUE_H)) {
    eval 'sub _LINUX_WORKQUEUE_H () {1;}' unless defined(&_LINUX_WORKQUEUE_H);
    require 'linux/timer.ph';
    require 'linux/linkage.ph';
    require 'linux/bitops.ph';
    eval 'sub __WORK_INITIALIZER {
        local($n, $f, $d) = @_;
	    eval q({ . &entry = { ($n). &entry, ($n). &entry }, . &func = ($f), . &data = ($d), . &timer =  &TIMER_INITIALIZER( &NULL, 0, 0), });
    }' unless defined(&__WORK_INITIALIZER);
    eval 'sub DECLARE_WORK {
        local($n, $f, $d) = @_;
	    eval q(\'struct work_struct\' $n =  &__WORK_INITIALIZER($n, $f, $d));
    }' unless defined(&DECLARE_WORK);
    eval 'sub PREPARE_WORK {
        local($_work, $_func, $_data) = @_;
	    eval q( &do { ($_work)-> &func = $_func; ($_work)-> &data = $_data; }  &while (0));
    }' unless defined(&PREPARE_WORK);
    eval 'sub INIT_WORK {
        local($_work, $_func, $_data) = @_;
	    eval q( &do {  &INIT_LIST_HEAD(($_work)-> &entry); ($_work)-> &pending = 0;  &PREPARE_WORK(($_work), ($_func), ($_data));  &init_timer(($_work)-> &timer); }  &while (0));
    }' unless defined(&INIT_WORK);
    eval 'sub create_workqueue {
        local($name) = @_;
	    eval q( &__create_workqueue(($name), 0));
    }' unless defined(&create_workqueue);
    eval 'sub create_singlethread_workqueue {
        local($name) = @_;
	    eval q( &__create_workqueue(($name), 1));
    }' unless defined(&create_singlethread_workqueue);
}
1;
