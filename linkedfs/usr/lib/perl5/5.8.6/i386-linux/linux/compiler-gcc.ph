require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&barrier)) {
    sub barrier () {
	eval q( &__asm__  &__volatile__(\"\": : :\"memory\"));
    }
}
unless(defined(&RELOC_HIDE)) {
    sub RELOC_HIDE {
	local($ptr, $off) = @_;
	eval q(({ 'unsigned long __ptr';  &__asm__ (\"\" : \"=r\"( &__ptr) : \"0\"($ptr)); ( &typeof($ptr)) ( &__ptr + ($off)); }));
    }
}
1;
