require '_h2ph_pre.ph';

no warnings 'redefine';

require 'linux/compiler-gcc.ph';
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 1 && defined (defined(&__KERNEL__) ? &__KERNEL__ : 0)) {
    eval 'sub inline () { &inline  &__attribute__(( &always_inline));}' unless defined(&inline);
    eval 'sub __inline__ () { &__inline__  &__attribute__(( &always_inline));}' unless defined(&__inline__);
    eval 'sub __inline () { &__inline  &__attribute__(( &always_inline));}' unless defined(&__inline);
}
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) > 0) {
    eval 'sub __deprecated () { &__attribute__(( &deprecated));}' unless defined(&__deprecated);
}
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 3) {
    eval 'sub __attribute_used__ () { &__attribute__(( &__used__));}' unless defined(&__attribute_used__);
} else {
    eval 'sub __attribute_used__ () { &__attribute__(( &__unused__));}' unless defined(&__attribute_used__);
}
unless(defined(&__attribute_pure__)) {
    sub __attribute_pure__ () {	 &__attribute__(( &pure));}
}
unless(defined(&__attribute_const__)) {
    sub __attribute_const__ () {	 &__attribute__(( &__const__));}
}
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 1) {
    eval 'sub noinline () { &__attribute__(( &noinline));}' unless defined(&noinline);
}
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 4) {
    eval 'sub __must_check () { &__attribute__(( &warn_unused_result));}' unless defined(&__must_check);
}
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 5) {
    eval 'sub __compiler_offsetof {
        local($a,$b) = @_;
	    eval q( &__builtin_offsetof($a,$b));
    }' unless defined(&__compiler_offsetof);
}
1;
