require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_TIMER_H)) {
    eval 'sub _LINUX_TIMER_H () {1;}' unless defined(&_LINUX_TIMER_H);
    require 'linux/list.ph';
    require 'linux/spinlock.ph';
    require 'linux/stddef.ph';
    eval 'sub TIMER_MAGIC () {0x4b87ad6e;}' unless defined(&TIMER_MAGIC);
    eval 'sub TIMER_INITIALIZER {
        local($_function, $_expires, $_data) = @_;
	    eval q({ . &function = ($_function), . &expires = ($_expires), . &data = ($_data), . &base =  &NULL, . &magic =  &TIMER_MAGIC, . &lock =  &SPIN_LOCK_UNLOCKED, });
    }' unless defined(&TIMER_INITIALIZER);
    eval 'sub del_timer_sync {
        local($t) = @_;
	    eval q( &del_timer($t));
    }' unless defined(&del_timer_sync);
    eval 'sub del_singleshot_timer_sync {
        local($t) = @_;
	    eval q( &del_timer($t));
    }' unless defined(&del_singleshot_timer_sync);
}
1;
