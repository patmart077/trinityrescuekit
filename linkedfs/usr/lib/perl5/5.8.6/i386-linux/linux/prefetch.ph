require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_PREFETCH_H)) {
    eval 'sub _LINUX_PREFETCH_H () {1;}' unless defined(&_LINUX_PREFETCH_H);
    require 'linux/types.ph';
    require 'asm/processor.ph';
    require 'asm/cache.ph';
    unless(defined(&ARCH_HAS_PREFETCH)) {
    }
    unless(defined(&ARCH_HAS_SPINLOCK_PREFETCH)) {
	eval 'sub spin_lock_prefetch {
	    local($x) = @_;
    	    eval q( &prefetchw($x));
	}' unless defined(&spin_lock_prefetch);
    }
    unless(defined(&PREFETCH_STRIDE)) {
	eval 'sub PREFETCH_STRIDE () {(4* &L1_CACHE_BYTES);}' unless defined(&PREFETCH_STRIDE);
    }
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub prefetch_range {
        local($addr,$len) = @_;
	    eval q({ });
    }' unless defined(&prefetch_range);
}
1;
