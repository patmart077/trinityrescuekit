require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_PERCPU_H)) {
    eval 'sub __LINUX_PERCPU_H () {1;}' unless defined(&__LINUX_PERCPU_H);
    require 'linux/spinlock.ph';
    require 'linux/slab.ph';
    require 'linux/smp.ph';
    require 'linux/string.ph';
    require 'asm/percpu.ph';
    unless(defined(&PERCPU_ENOUGH_ROOM)) {
	eval 'sub PERCPU_ENOUGH_ROOM () {32768;}' unless defined(&PERCPU_ENOUGH_ROOM);
    }
    eval 'sub get_cpu_var {
        local($var) = @_;
	    eval q((*({  &preempt_disable(); & &__get_cpu_var($var); })));
    }' unless defined(&get_cpu_var);
    eval 'sub put_cpu_var {
        local($var) = @_;
	    eval q( &preempt_enable());
    }' unless defined(&put_cpu_var);
    if(defined(&CONFIG_SMP)) {
	eval 'sub per_cpu_ptr {
	    local($ptr, $cpu) = @_;
    	    eval q(({ \'struct percpu_data\' * &__p = ~($ptr); ( &__typeof__($ptr)) ($__p->{ptrs[&(cpu)]}); }));
	}' unless defined(&per_cpu_ptr);
    } else {
	eval 'sub per_cpu_ptr {
	    local($ptr, $cpu) = @_;
    	    eval q(($ptr));
	}' unless defined(&per_cpu_ptr);
	eval 'sub void {
	    eval q(* &__alloc_percpu(\'size_t\'  &size, \'size_t\'  &align) {  &void * &ret =  &kmalloc( &size,  &GFP_KERNEL);  &if ( &ret)  &memset( &ret, 0,  &size);  &ret; });
	}' unless defined(&void);
	eval 'sub free_percpu {
	    local($ptr) = @_;
    	    eval q({  &kfree($ptr); });
	}' unless defined(&free_percpu);
    }
    eval 'sub alloc_percpu {
        local($type) = @_;
	    eval q((($type *)( &__alloc_percpu($sizeof{$type},  &__alignof__($type)))));
    }' unless defined(&alloc_percpu);
}
1;
