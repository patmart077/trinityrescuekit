require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_PREEMPT_H)) {
    eval 'sub __LINUX_PREEMPT_H () {1;}' unless defined(&__LINUX_PREEMPT_H);
    require 'linux/config.ph';
    require 'linux/linkage.ph';
    eval 'sub preempt_count () {
        eval q(( &current_thread_info()-> &preempt_count));
    }' unless defined(&preempt_count);
    eval 'sub inc_preempt_count () {
        eval q( &do {  &preempt_count()++; }  &while (0));
    }' unless defined(&inc_preempt_count);
    eval 'sub dec_preempt_count () {
        eval q( &do {  &preempt_count()--; }  &while (0));
    }' unless defined(&dec_preempt_count);
    if(defined(&CONFIG_PREEMPT)) {
	eval 'sub preempt_disable () {
	    eval q( &do {  &inc_preempt_count();  &barrier(); }  &while (0));
	}' unless defined(&preempt_disable);
	eval 'sub preempt_enable_no_resched () {
	    eval q( &do {  &barrier();  &dec_preempt_count(); }  &while (0));
	}' unless defined(&preempt_enable_no_resched);
	eval 'sub preempt_check_resched () {
	    eval q( &do {  &if ( &unlikely( &test_thread_flag( &TIF_NEED_RESCHED)))  &preempt_schedule(); }  &while (0));
	}' unless defined(&preempt_check_resched);
	eval 'sub preempt_enable () {
	    eval q( &do {  &preempt_enable_no_resched();  &preempt_check_resched(); }  &while (0));
	}' unless defined(&preempt_enable);
    } else {
	eval 'sub preempt_disable () {
	    eval q( &do { }  &while (0));
	}' unless defined(&preempt_disable);
	eval 'sub preempt_enable_no_resched () {
	    eval q( &do { }  &while (0));
	}' unless defined(&preempt_enable_no_resched);
	eval 'sub preempt_enable () {
	    eval q( &do { }  &while (0));
	}' unless defined(&preempt_enable);
	eval 'sub preempt_check_resched () {
	    eval q( &do { }  &while (0));
	}' unless defined(&preempt_check_resched);
    }
}
1;
