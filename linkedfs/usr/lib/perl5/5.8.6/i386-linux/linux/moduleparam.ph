require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_MODULE_PARAMS_H)) {
    eval 'sub _LINUX_MODULE_PARAMS_H () {1;}' unless defined(&_LINUX_MODULE_PARAMS_H);
    require 'linux/init.ph';
    require 'linux/stringify.ph';
    require 'linux/kernel.ph';
    if(defined(&MODULE)) {
	eval 'sub MODULE_PARAM_PREFIX () {1;}' unless defined(&MODULE_PARAM_PREFIX);
    } else {
	eval 'sub MODULE_PARAM_PREFIX () { &__stringify( &KBUILD_MODNAME) ".";}' unless defined(&MODULE_PARAM_PREFIX);
    }
    eval 'sub __module_param_call {
        local($prefix, $name, $set, $get, $arg, $perm) = @_;
	    eval q( &static \'char\'  &__param_str_$name->[] = $prefix $name;  &static \'struct kernel_param\'  &const  &__param_$name  &__attribute_used__  &__attribute__ (( &unused, &__section__ (\\"__param\\"), &aligned($sizeof{ &void }))) = {  &__param_str_$name, $perm, $set, $get, $arg });
    }' unless defined(&__module_param_call);
    eval 'sub module_param_call {
        local($name, $set, $get, $arg, $perm) = @_;
	    eval q( &__module_param_call( &MODULE_PARAM_PREFIX, $name, $set, $get, $arg, $perm));
    }' unless defined(&module_param_call);
    eval 'sub module_param_named {
        local($name, $value, $type, $perm) = @_;
	    eval q( &param_check_$type($name, ($value));  &module_param_call($name,  &param_set_$type,  &param_get_$type, $value, $perm));
    }' unless defined(&module_param_named);
    eval 'sub module_param {
        local($name, $type, $perm) = @_;
	    eval q( &module_param_named($name, $name, $type, $perm));
    }' unless defined(&module_param);
    eval 'sub module_param_string {
        local($name, $string, $len, $perm) = @_;
	    eval q( &static \'struct kparam_string\'  &__param_string_$name = { $len, $string };  &module_param_call($name,  &param_set_copystring,  &param_get_string, & &__param_string_$name, $perm));
    }' unless defined(&module_param_string);
    eval 'sub __param_check {
        local($name, $p, $type) = @_;
	    eval q( &static  &inline $type * &__check_$name( &void) {  &return($p); });
    }' unless defined(&__param_check);
    eval 'sub param_check_byte {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'unsigned char\'));
    }' unless defined(&param_check_byte);
    eval 'sub param_check_short {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'short\'));
    }' unless defined(&param_check_short);
    eval 'sub param_check_ushort {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'unsigned short\'));
    }' unless defined(&param_check_ushort);
    eval 'sub param_check_int {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'int\'));
    }' unless defined(&param_check_int);
    eval 'sub param_check_uint {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'unsigned int\'));
    }' unless defined(&param_check_uint);
    eval 'sub param_check_long {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'long\'));
    }' unless defined(&param_check_long);
    eval 'sub param_check_ulong {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'unsigned long\'));
    }' unless defined(&param_check_ulong);
    eval 'sub param_check_charp {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'char\' *));
    }' unless defined(&param_check_charp);
    eval 'sub param_check_bool {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'int\'));
    }' unless defined(&param_check_bool);
    eval 'sub param_check_invbool {
        local($name, $p) = @_;
	    eval q( &__param_check($name, $p, \'int\'));
    }' unless defined(&param_check_invbool);
    eval 'sub module_param_array_named {
        local($name, $array, $type, $nump, $perm) = @_;
	    eval q( &static \'struct kparam_array\'  &__param_arr_$name = {  &ARRAY_SIZE($array), $nump,  &param_set_$type,  &param_get_$type, $sizeof{$array->[0]}, $array };  &module_param_call($name,  &param_array_set,  &param_array_get, & &__param_arr_$name, $perm));
    }' unless defined(&module_param_array_named);
    eval 'sub module_param_array {
        local($name, $type, $nump, $perm) = @_;
	    eval q( &module_param_array_named($name, $name, $type, $nump, $perm));
    }' unless defined(&module_param_array);
}
1;
