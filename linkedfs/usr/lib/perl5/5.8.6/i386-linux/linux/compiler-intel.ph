require '_h2ph_pre.ph';

no warnings 'redefine';

if(defined(&__ECC)) {
    require 'asm/intrinsics.ph';
    undef(&barrier) if defined(&barrier);
    undef(&RELOC_HIDE) if defined(&RELOC_HIDE);
    eval 'sub barrier () {
        eval q( &__memory_barrier());
    }' unless defined(&barrier);
    eval 'sub RELOC_HIDE {
        local($ptr, $off) = @_;
	    eval q(({ \'unsigned long __ptr\';  &__ptr =  ($ptr); ( &typeof($ptr)) ( &__ptr + ($off)); }));
    }' unless defined(&RELOC_HIDE);
}
1;
