require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_SPINLOCK_H)) {
    eval 'sub __LINUX_SPINLOCK_H () {1;}' unless defined(&__LINUX_SPINLOCK_H);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	require 'linux/preempt.ph';
	require 'linux/linkage.ph';
	require 'linux/compiler.ph';
	require 'linux/thread_info.ph';
	require 'linux/kernel.ph';
	require 'linux/stringify.ph';
	require 'asm/processor.ph';
	require 'asm/system.ph';
	if(defined(&CONFIG_KGDB)) {
	    require 'asm/current.ph';
	    eval 'sub SET_WHO {
	        local($x, $him) = @_;
    		eval q(($x)-> &who = $him;);
	    }' unless defined(&SET_WHO);
	} else {
	    eval 'sub SET_WHO {
	        local($x, $him) = @_;
    		eval q();
	    }' unless defined(&SET_WHO);
	}
	eval 'sub LOCK_SECTION_NAME () {".text.lock."  &__stringify( &KBUILD_BASENAME);}' unless defined(&LOCK_SECTION_NAME);
	eval 'sub LOCK_SECTION_START {
	    local($extra) = @_;
    	    eval q(\\".subsection 1\\\\n\\\\t\\" $extra \\".ifndef \\"  &LOCK_SECTION_NAME \\"\\\\n\\\\t\\"  &LOCK_SECTION_NAME \\":\\\\n\\\\t\\" \\".endif\\\\n\\");
	}' unless defined(&LOCK_SECTION_START);
	eval 'sub LOCK_SECTION_END () {".previous\\n\\t";}' unless defined(&LOCK_SECTION_END);
	eval 'sub __lockfunc () { &fastcall  &__attribute__(( &section(".spinlock.text")));}' unless defined(&__lockfunc);
	if(defined(&CONFIG_SMP)) {
	    require 'asm/spinlock.ph';
	} else {
	    eval 'sub in_lock_functions {
	        local($ADDR) = @_;
    		eval q(0);
	    }' unless defined(&in_lock_functions);
	    if(!defined( &CONFIG_PREEMPT)  && !defined( &CONFIG_DEBUG_SPINLOCK)) {
		eval 'sub _atomic_dec_and_lock {
		    local($atomic,$lock) = @_;
    		    eval q( &atomic_dec_and_test($atomic));
		}' unless defined(&_atomic_dec_and_lock);
		eval 'sub ATOMIC_DEC_AND_LOCK () {1;}' unless defined(&ATOMIC_DEC_AND_LOCK);
	    }
	    if(defined(&CONFIG_DEBUG_SPINLOCK)) {
		eval 'sub SPINLOCK_MAGIC () {0x1d244b3c;}' unless defined(&SPINLOCK_MAGIC);
		if(defined(&CONFIG_KGDB)) {
		}
		eval 'sub SPIN_LOCK_UNLOCKED () {( &spinlock_t) {  &SPINLOCK_MAGIC, 0, 10,  &__FILE__ ,  &NULL, 0};}' unless defined(&SPIN_LOCK_UNLOCKED);
		eval 'sub spin_lock_init {
		    local($x) = @_;
    		    eval q( &do { ($x)-> &magic =  &SPINLOCK_MAGIC; ($x)-> &lock = 0; ($x)-> &babble = 5; ($x)-> &module =  &__FILE__; ($x)-> &owner =  &NULL; ($x)-> &oline = 0;  &SET_WHO($x,  &NULL) }  &while (0));
		}' unless defined(&spin_lock_init);
		eval 'sub CHECK_LOCK {
		    local($x) = @_;
    		    eval q( &do {  &if (($x)-> &magic !=  &SPINLOCK_MAGIC) {  &printk( &KERN_ERR \\"%s:%d: spin_is_locked on uninitialized spinlock %p.\\\\n\\",  &__FILE__,  &__LINE__, ($x)); } }  &while(0));
		}' unless defined(&CHECK_LOCK);
		eval 'sub _raw_spin_lock {
		    local($x) = @_;
    		    eval q( &do {  &CHECK_LOCK($x);  &if (($x)-> &lock &&($x)-> &babble) { ($x)-> &babble--;  &printk(\\"%s:%d: spin_lock(%s:%p) already locked by %s/%d\\\\n\\",  &__FILE__, &__LINE__, ($x)-> &module, ($x), ($x)-> &owner, ($x)-> &oline); } ($x)-> &lock = 1; ($x)-> &owner =  &__FILE__; ($x)-> &oline =  &__LINE__;  &SET_WHO($x,  &current) }  &while (0));
		}' unless defined(&_raw_spin_lock);
		eval 'sub spin_is_locked {
		    local($x) = @_;
    		    eval q(({  &CHECK_LOCK($x);  &if (($x)-> &lock &&($x)-> &babble) { ($x)-> &babble--;  &printk(\\"%s:%d: spin_is_locked(%s:%p) already locked by %s/%d\\\\n\\",  &__FILE__, &__LINE__, ($x)-> &module, ($x), ($x)-> &owner, ($x)-> &oline); } 0; }));
		}' unless defined(&spin_is_locked);
		eval 'sub _raw_spin_trylock {
		    local($x) = @_;
    		    eval q(({  &CHECK_LOCK($x);  &if (($x)-> &lock &&($x)-> &babble) { ($x)-> &babble--;  &printk(\\"%s:%d: spin_trylock(%s:%p) already locked by %s/%d\\\\n\\",  &__FILE__, &__LINE__, ($x)-> &module, ($x), ($x)-> &owner, ($x)-> &oline); } ($x)-> &lock = 1; ($x)-> &owner =  &__FILE__; ($x)-> &oline =  &__LINE__;  &SET_WHO($x,  &current) 1; }));
		}' unless defined(&_raw_spin_trylock);
		eval 'sub spin_unlock_wait {
		    local($x) = @_;
    		    eval q( &do {  &CHECK_LOCK($x);  &if (($x)-> &lock &&($x)-> &babble) { ($x)-> &babble--;  &printk(\\"%s:%d: spin_unlock_wait(%s:%p) owned by %s/%d\\\\n\\",  &__FILE__, &__LINE__, ($x)-> &module, ($x), ($x)-> &owner, ($x)-> &oline); } }  &while (0));
		}' unless defined(&spin_unlock_wait);
		eval 'sub _raw_spin_unlock {
		    local($x) = @_;
    		    eval q( &do {  &CHECK_LOCK($x);  &if (!($x)-> &lock &&($x)-> &babble) { ($x)-> &babble--;  &printk(\\"%s:%d: spin_unlock(%s:%p) not locked\\\\n\\",  &__FILE__, &__LINE__, ($x)-> &module, ($x)); } ($x)-> &lock = 0; }  &while (0));
		}' unless defined(&_raw_spin_unlock);
	    } else {
		if(((defined(&__GNUC__) ? &__GNUC__ : 0) > 2)) {
		    eval 'sub SPIN_LOCK_UNLOCKED () {( &spinlock_t) { };}' unless defined(&SPIN_LOCK_UNLOCKED);
		} else {
		    eval 'sub SPIN_LOCK_UNLOCKED () {( &spinlock_t) { 0};}' unless defined(&SPIN_LOCK_UNLOCKED);
		}
		eval 'sub spin_lock_init {
		    local($lock) = @_;
    		    eval q( &do { ( &void)($lock); }  &while(0));
		}' unless defined(&spin_lock_init);
		eval 'sub _raw_spin_lock {
		    local($lock) = @_;
    		    eval q( &do { ( &void)($lock); }  &while(0));
		}' unless defined(&_raw_spin_lock);
		eval 'sub spin_is_locked {
		    local($lock) = @_;
    		    eval q((( &void)($lock), 0));
		}' unless defined(&spin_is_locked);
		eval 'sub _raw_spin_trylock {
		    local($lock) = @_;
    		    eval q(((( &void)($lock), 1)));
		}' unless defined(&_raw_spin_trylock);
		eval 'sub spin_unlock_wait {
		    local($lock) = @_;
    		    eval q(( &void)($lock););
		}' unless defined(&spin_unlock_wait);
		eval 'sub _raw_spin_unlock {
		    local($lock) = @_;
    		    eval q( &do { ( &void)($lock); }  &while(0));
		}' unless defined(&_raw_spin_unlock);
	    }
	    if(((defined(&__GNUC__) ? &__GNUC__ : 0) > 2)) {
		eval 'sub RW_LOCK_UNLOCKED () {( &rwlock_t) { };}' unless defined(&RW_LOCK_UNLOCKED);
	    } else {
		eval 'sub RW_LOCK_UNLOCKED () {( &rwlock_t) { 0};}' unless defined(&RW_LOCK_UNLOCKED);
	    }
	    eval 'sub rwlock_init {
	        local($lock) = @_;
    		eval q( &do { ( &void)($lock); }  &while(0));
	    }' unless defined(&rwlock_init);
	    eval 'sub _raw_read_lock {
	        local($lock) = @_;
    		eval q( &do { ( &void)($lock); }  &while(0));
	    }' unless defined(&_raw_read_lock);
	    eval 'sub _raw_read_unlock {
	        local($lock) = @_;
    		eval q( &do { ( &void)($lock); }  &while(0));
	    }' unless defined(&_raw_read_unlock);
	    eval 'sub _raw_write_lock {
	        local($lock) = @_;
    		eval q( &do { ( &void)($lock); }  &while(0));
	    }' unless defined(&_raw_write_lock);
	    eval 'sub _raw_write_unlock {
	        local($lock) = @_;
    		eval q( &do { ( &void)($lock); }  &while(0));
	    }' unless defined(&_raw_write_unlock);
	    eval 'sub _raw_write_trylock {
	        local($lock) = @_;
    		eval q(({ ( &void)($lock); (1); }));
	    }' unless defined(&_raw_write_trylock);
	    eval 'sub _spin_trylock {
	        local($lock) = @_;
    		eval q(({ &preempt_disable();  &_raw_spin_trylock($lock) ? 1: ({ &preempt_enable(); 0;});}));
	    }' unless defined(&_spin_trylock);
	    eval 'sub _write_trylock {
	        local($lock) = @_;
    		eval q(({ &preempt_disable();  &_raw_write_trylock($lock) ? 1: ({ &preempt_enable(); 0;});}));
	    }' unless defined(&_write_trylock);
	    eval 'sub _spin_trylock_bh {
	        local($lock) = @_;
    		eval q(({ &preempt_disable();  &local_bh_disable();  &_raw_spin_trylock($lock) ? 1: ({ &preempt_enable();  &local_bh_enable(); 0;});}));
	    }' unless defined(&_spin_trylock_bh);
	    eval 'sub _spin_lock {
	        local($lock) = @_;
    		eval q( &do {  &preempt_disable();  &_raw_spin_lock($lock);  &__acquire($lock); }  &while(0));
	    }' unless defined(&_spin_lock);
	    eval 'sub _write_lock {
	        local($lock) = @_;
    		eval q( &do {  &preempt_disable();  &_raw_write_lock($lock);  &__acquire($lock); }  &while(0));
	    }' unless defined(&_write_lock);
	    eval 'sub _read_lock {
	        local($lock) = @_;
    		eval q( &do {  &preempt_disable();  &_raw_read_lock($lock);  &__acquire($lock); }  &while(0));
	    }' unless defined(&_read_lock);
	    eval 'sub _spin_unlock {
	        local($lock) = @_;
    		eval q( &do {  &_raw_spin_unlock($lock);  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_spin_unlock);
	    eval 'sub _write_unlock {
	        local($lock) = @_;
    		eval q( &do {  &_raw_write_unlock($lock);  &preempt_enable();  &__release($lock); }  &while(0));
	    }' unless defined(&_write_unlock);
	    eval 'sub _read_unlock {
	        local($lock) = @_;
    		eval q( &do {  &_raw_read_unlock($lock);  &preempt_enable();  &__release($lock); }  &while(0));
	    }' unless defined(&_read_unlock);
	    eval 'sub _spin_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q( &do {  &local_irq_save($flags);  &preempt_disable();  &_raw_spin_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_spin_lock_irqsave);
	    eval 'sub _spin_lock_irq {
	        local($lock) = @_;
    		eval q( &do {  &local_irq_disable();  &preempt_disable();  &_raw_spin_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_spin_lock_irq);
	    eval 'sub _spin_lock_bh {
	        local($lock) = @_;
    		eval q( &do {  &local_bh_disable();  &preempt_disable();  &_raw_spin_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_spin_lock_bh);
	    eval 'sub _read_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q( &do {  &local_irq_save($flags);  &preempt_disable();  &_raw_read_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_read_lock_irqsave);
	    eval 'sub _read_lock_irq {
	        local($lock) = @_;
    		eval q( &do {  &local_irq_disable();  &preempt_disable();  &_raw_read_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_read_lock_irq);
	    eval 'sub _read_lock_bh {
	        local($lock) = @_;
    		eval q( &do {  &local_bh_disable();  &preempt_disable();  &_raw_read_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_read_lock_bh);
	    eval 'sub _write_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q( &do {  &local_irq_save($flags);  &preempt_disable();  &_raw_write_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_write_lock_irqsave);
	    eval 'sub _write_lock_irq {
	        local($lock) = @_;
    		eval q( &do {  &local_irq_disable();  &preempt_disable();  &_raw_write_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_write_lock_irq);
	    eval 'sub _write_lock_bh {
	        local($lock) = @_;
    		eval q( &do {  &local_bh_disable();  &preempt_disable();  &_raw_write_lock($lock);  &__acquire($lock); }  &while (0));
	    }' unless defined(&_write_lock_bh);
	    eval 'sub _spin_unlock_irqrestore {
	        local($lock, $flags) = @_;
    		eval q( &do {  &_raw_spin_unlock($lock);  &local_irq_restore($flags);  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_spin_unlock_irqrestore);
	    eval 'sub _spin_unlock_irq {
	        local($lock) = @_;
    		eval q( &do {  &_raw_spin_unlock($lock);  &local_irq_enable();  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_spin_unlock_irq);
	    eval 'sub _spin_unlock_bh {
	        local($lock) = @_;
    		eval q( &do {  &_raw_spin_unlock($lock);  &preempt_enable();  &local_bh_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_spin_unlock_bh);
	    eval 'sub _write_unlock_bh {
	        local($lock) = @_;
    		eval q( &do {  &_raw_write_unlock($lock);  &preempt_enable();  &local_bh_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_write_unlock_bh);
	    eval 'sub _read_unlock_irqrestore {
	        local($lock, $flags) = @_;
    		eval q( &do {  &_raw_read_unlock($lock);  &local_irq_restore($flags);  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_read_unlock_irqrestore);
	    eval 'sub _write_unlock_irqrestore {
	        local($lock, $flags) = @_;
    		eval q( &do {  &_raw_write_unlock($lock);  &local_irq_restore($flags);  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_write_unlock_irqrestore);
	    eval 'sub _read_unlock_irq {
	        local($lock) = @_;
    		eval q( &do {  &_raw_read_unlock($lock);  &local_irq_enable();  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_read_unlock_irq);
	    eval 'sub _read_unlock_bh {
	        local($lock) = @_;
    		eval q( &do {  &_raw_read_unlock($lock);  &local_bh_enable();  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_read_unlock_bh);
	    eval 'sub _write_unlock_irq {
	        local($lock) = @_;
    		eval q( &do {  &_raw_write_unlock($lock);  &local_irq_enable();  &preempt_enable();  &__release($lock); }  &while (0));
	    }' unless defined(&_write_unlock_irq);
	}
	eval 'sub spin_trylock {
	    local($lock) = @_;
    	    eval q( &__cond_lock( &_spin_trylock($lock)));
	}' unless defined(&spin_trylock);
	eval 'sub write_trylock {
	    local($lock) = @_;
    	    eval q( &__cond_lock( &_write_trylock($lock)));
	}' unless defined(&write_trylock);
	eval 'sub spin_lock {
	    local($lock) = @_;
    	    eval q( &_spin_lock($lock));
	}' unless defined(&spin_lock);
	eval 'sub write_lock {
	    local($lock) = @_;
    	    eval q( &_write_lock($lock));
	}' unless defined(&write_lock);
	eval 'sub read_lock {
	    local($lock) = @_;
    	    eval q( &_read_lock($lock));
	}' unless defined(&read_lock);
	eval 'sub spin_unlock {
	    local($lock) = @_;
    	    eval q( &_spin_unlock($lock));
	}' unless defined(&spin_unlock);
	eval 'sub write_unlock {
	    local($lock) = @_;
    	    eval q( &_write_unlock($lock));
	}' unless defined(&write_unlock);
	eval 'sub read_unlock {
	    local($lock) = @_;
    	    eval q( &_read_unlock($lock));
	}' unless defined(&read_unlock);
	if(defined(&CONFIG_SMP)) {
	    eval 'sub spin_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q($flags =  &_spin_lock_irqsave($lock));
	    }' unless defined(&spin_lock_irqsave);
	    eval 'sub read_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q($flags =  &_read_lock_irqsave($lock));
	    }' unless defined(&read_lock_irqsave);
	    eval 'sub write_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q($flags =  &_write_lock_irqsave($lock));
	    }' unless defined(&write_lock_irqsave);
	} else {
	    eval 'sub spin_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q( &_spin_lock_irqsave($lock, $flags));
	    }' unless defined(&spin_lock_irqsave);
	    eval 'sub read_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q( &_read_lock_irqsave($lock, $flags));
	    }' unless defined(&read_lock_irqsave);
	    eval 'sub write_lock_irqsave {
	        local($lock, $flags) = @_;
    		eval q( &_write_lock_irqsave($lock, $flags));
	    }' unless defined(&write_lock_irqsave);
	}
	eval 'sub spin_lock_irq {
	    local($lock) = @_;
    	    eval q( &_spin_lock_irq($lock));
	}' unless defined(&spin_lock_irq);
	eval 'sub spin_lock_bh {
	    local($lock) = @_;
    	    eval q( &_spin_lock_bh($lock));
	}' unless defined(&spin_lock_bh);
	eval 'sub read_lock_irq {
	    local($lock) = @_;
    	    eval q( &_read_lock_irq($lock));
	}' unless defined(&read_lock_irq);
	eval 'sub read_lock_bh {
	    local($lock) = @_;
    	    eval q( &_read_lock_bh($lock));
	}' unless defined(&read_lock_bh);
	eval 'sub write_lock_irq {
	    local($lock) = @_;
    	    eval q( &_write_lock_irq($lock));
	}' unless defined(&write_lock_irq);
	eval 'sub write_lock_bh {
	    local($lock) = @_;
    	    eval q( &_write_lock_bh($lock));
	}' unless defined(&write_lock_bh);
	eval 'sub spin_unlock_irqrestore {
	    local($lock, $flags) = @_;
    	    eval q( &_spin_unlock_irqrestore($lock, $flags));
	}' unless defined(&spin_unlock_irqrestore);
	eval 'sub spin_unlock_irq {
	    local($lock) = @_;
    	    eval q( &_spin_unlock_irq($lock));
	}' unless defined(&spin_unlock_irq);
	eval 'sub spin_unlock_bh {
	    local($lock) = @_;
    	    eval q( &_spin_unlock_bh($lock));
	}' unless defined(&spin_unlock_bh);
	eval 'sub read_unlock_irqrestore {
	    local($lock, $flags) = @_;
    	    eval q( &_read_unlock_irqrestore($lock, $flags));
	}' unless defined(&read_unlock_irqrestore);
	eval 'sub read_unlock_irq {
	    local($lock) = @_;
    	    eval q( &_read_unlock_irq($lock));
	}' unless defined(&read_unlock_irq);
	eval 'sub read_unlock_bh {
	    local($lock) = @_;
    	    eval q( &_read_unlock_bh($lock));
	}' unless defined(&read_unlock_bh);
	eval 'sub write_unlock_irqrestore {
	    local($lock, $flags) = @_;
    	    eval q( &_write_unlock_irqrestore($lock, $flags));
	}' unless defined(&write_unlock_irqrestore);
	eval 'sub write_unlock_irq {
	    local($lock) = @_;
    	    eval q( &_write_unlock_irq($lock));
	}' unless defined(&write_unlock_irq);
	eval 'sub write_unlock_bh {
	    local($lock) = @_;
    	    eval q( &_write_unlock_bh($lock));
	}' unless defined(&write_unlock_bh);
	eval 'sub spin_trylock_bh {
	    local($lock) = @_;
    	    eval q( &__cond_lock( &_spin_trylock_bh($lock)));
	}' unless defined(&spin_trylock_bh);
	if(defined(&CONFIG_LOCKMETER)) {
	}
	unless(defined(&ATOMIC_DEC_AND_LOCK)) {
	    require 'asm/atomic.ph';
	}
	eval 'sub atomic_dec_and_lock {
	    local($atomic,$lock) = @_;
    	    eval q( &__cond_lock( &_atomic_dec_and_lock($atomic,$lock)));
	}' unless defined(&atomic_dec_and_lock);
	if(defined( &CONFIG_SMP) || defined( &CONFIG_DEBUG_SPINLOCK)) {
	}
	if(defined( &CONFIG_SMP) || defined( &CONFIG_DEBUG_SPINLOCK)) {
	}
	if(defined( &CONFIG_SMP) || defined( &CONFIG_DEBUG_SPINLOCK)) {
	}
	if(defined( &CONFIG_SMP) || defined( &CONFIG_DEBUG_SPINLOCK)) {
	}
 elsif(defined (defined(&CONFIG_PREEMPT) ? &CONFIG_PREEMPT : 0)) {
	} else {
	}
    } else {
    }
}
1;
