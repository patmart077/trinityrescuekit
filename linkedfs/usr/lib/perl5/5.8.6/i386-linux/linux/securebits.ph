require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_SECUREBITS_H)) {
    eval 'sub _LINUX_SECUREBITS_H () {1;}' unless defined(&_LINUX_SECUREBITS_H);
    eval 'sub SECUREBITS_DEFAULT () {0x;}' unless defined(&SECUREBITS_DEFAULT);
    eval 'sub SECURE_NOROOT () {0;}' unless defined(&SECURE_NOROOT);
    eval 'sub SECURE_NO_SETUID_FIXUP () {2;}' unless defined(&SECURE_NO_SETUID_FIXUP);
    eval 'sub issecure {
        local($X) = @_;
	    eval q(( (1<< ($X+1)) &  &SECUREBITS_DEFAULT ? (1<< ($X)) &  &SECUREBITS_DEFAULT : (1<< ($X)) &  &securebits ));
    }' unless defined(&issecure);
}
1;
