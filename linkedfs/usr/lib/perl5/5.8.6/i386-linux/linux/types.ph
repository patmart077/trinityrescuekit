require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_TYPES_H)) {
    eval 'sub _LINUX_TYPES_H () {1;}' unless defined(&_LINUX_TYPES_H);
    require 'linux/compiler.ph';
    require 'linux/posix_types.ph';
    require 'asm/types.ph';
    unless(defined(&__KERNEL_STRICT_NAMES)) {
	if(defined( &__GNUC__)  && !defined( &__STRICT_ANSI__)) {
	}
	unless(defined(&_SIZE_T)) {
	    eval 'sub _SIZE_T () {1;}' unless defined(&_SIZE_T);
	}
	unless(defined(&_SSIZE_T)) {
	    eval 'sub _SSIZE_T () {1;}' unless defined(&_SSIZE_T);
	}
	unless(defined(&_PTRDIFF_T)) {
	    eval 'sub _PTRDIFF_T () {1;}' unless defined(&_PTRDIFF_T);
	}
	unless(defined(&_TIME_T)) {
	    eval 'sub _TIME_T () {1;}' unless defined(&_TIME_T);
	}
	unless(defined(&_CLOCK_T)) {
	    eval 'sub _CLOCK_T () {1;}' unless defined(&_CLOCK_T);
	}
	unless(defined(&_CADDR_T)) {
	    eval 'sub _CADDR_T () {1;}' unless defined(&_CADDR_T);
	}
	unless(defined(&__BIT_TYPES_DEFINED__)) {
	    eval 'sub __BIT_TYPES_DEFINED__ () {1;}' unless defined(&__BIT_TYPES_DEFINED__);
	}
	if(defined( &__GNUC__)) {
	}
	unless(defined(&HAVE_SECTOR_T)) {
	}
    }
    unless(defined(&pgoff_t)) {
	eval 'sub pgoff_t () {\'unsigned long\';}' unless defined(&pgoff_t);
    }
    if(defined(&__CHECKER__)) {
	eval 'sub __bitwise () { &__attribute__(( &bitwise));}' unless defined(&__bitwise);
    } else {
	eval 'sub __bitwise () {1;}' unless defined(&__bitwise);
    }
    if(defined( &__GNUC__)) {
    }
}
1;
