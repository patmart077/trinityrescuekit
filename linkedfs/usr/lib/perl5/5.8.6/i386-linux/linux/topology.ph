require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_TOPOLOGY_H)) {
    eval 'sub _LINUX_TOPOLOGY_H () {1;}' unless defined(&_LINUX_TOPOLOGY_H);
    require 'linux/cpumask.ph';
    require 'linux/bitops.ph';
    require 'linux/mmzone.ph';
    require 'linux/smp.ph';
    require 'asm/topology.ph';
    unless(defined(&nr_cpus_node)) {
	eval 'sub nr_cpus_node {
	    local($node) = @_;
    	    eval q(({  &cpumask_t  &__tmp__;  &__tmp__ =  &node_to_cpumask($node);  &cpus_weight( &__tmp__); }));
	}' unless defined(&nr_cpus_node);
    }
    eval 'sub __next_node_with_cpus {
        local($node) = @_;
	    eval q({  &do ++$node;  &while ($node <  &numnodes  && ! &nr_cpus_node($node)); $node; });
    }' unless defined(&__next_node_with_cpus);
    eval 'sub for_each_node_with_cpus {
        local($node) = @_;
	    eval q( &for ($node = 0; $node <  &numnodes; $node =  &__next_node_with_cpus($node)));
    }' unless defined(&for_each_node_with_cpus);
    unless(defined(&node_distance)) {
	eval 'sub node_distance {
	    local($from,$to) = @_;
    	    eval q((($from) != ($to)));
	}' unless defined(&node_distance);
    }
    unless(defined(&PENALTY_FOR_NODE_WITH_CPUS)) {
	eval 'sub PENALTY_FOR_NODE_WITH_CPUS () {(1);}' unless defined(&PENALTY_FOR_NODE_WITH_CPUS);
    }
    if(defined(&CONFIG_SCHED_SMT)) {
	eval 'sub ARCH_HAS_SCHED_WAKE_IDLE () {1;}' unless defined(&ARCH_HAS_SCHED_WAKE_IDLE);
	unless(defined(&SD_SIBLING_INIT)) {
	    eval 'sub SD_SIBLING_INIT () { { . &span =  &CPU_MASK_NONE, . &parent =  &NULL, . &groups =  &NULL, . &min_interval = 1, . &max_interval = 2, . &busy_factor = 8, . &imbalance_pct = 110, . &cache_hot_time = 0, . &cache_nice_tries = 0, . &per_cpu_gain = 25, . &flags =  &SD_LOAD_BALANCE |  &SD_BALANCE_NEWIDLE |  &SD_BALANCE_EXEC |  &SD_WAKE_AFFINE |  &SD_WAKE_IDLE |  &SD_SHARE_CPUPOWER, . &last_balance =  &jiffies, . &balance_interval = 1, . &nr_balance_failed = 0, };}' unless defined(&SD_SIBLING_INIT);
	}
    }
    unless(defined(&SD_CPU_INIT)) {
	eval 'sub SD_CPU_INIT () { { . &span =  &CPU_MASK_NONE, . &parent =  &NULL, . &groups =  &NULL, . &min_interval = 1, . &max_interval = 4, . &busy_factor = 64, . &imbalance_pct = 125, . &cache_hot_time = (5*1000/2), . &cache_nice_tries = 1, . &per_cpu_gain = 100, . &flags =  &SD_LOAD_BALANCE |  &SD_BALANCE_NEWIDLE |  &SD_BALANCE_EXEC |  &SD_WAKE_AFFINE |  &SD_WAKE_BALANCE, . &last_balance =  &jiffies, . &balance_interval = 1, . &nr_balance_failed = 0, };}' unless defined(&SD_CPU_INIT);
    }
    if(defined(&CONFIG_NUMA)) {
	unless(defined(&SD_NODE_INIT)) {
	    die("Please\ define\ an\ appropriate\ SD_NODE_INIT\ in\ include\/asm\/topology\.h\!\!\!");
	}
    }
}
1;
