require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_KOBJECT_H_)) {
    eval 'sub _KOBJECT_H_ () {1;}' unless defined(&_KOBJECT_H_);
    if(defined(&__KERNEL__)) {
	require 'linux/types.ph';
	require 'linux/list.ph';
	require 'linux/sysfs.ph';
	require 'linux/rwsem.ph';
	require 'linux/kref.ph';
	require 'linux/kobject_uevent.ph';
	require 'asm/atomic.ph';
	eval 'sub KOBJ_NAME_LEN () {20;}' unless defined(&KOBJ_NAME_LEN);
	eval 'sub kobject_name {
	    local($kobj) = @_;
    	    eval q({  ($kobj->{k_name}); });
	}' unless defined(&kobject_name);
	eval 'sub to_kset {
	    local($kobj) = @_;
    	    eval q({ $kobj ?  &container_of($kobj,\'struct kset\',$kobj) :  &NULL; });
	}' unless defined(&to_kset);
	eval 'sub kset_get {
	    local($k) = @_;
    	    eval q({ $k ?  &to_kset( &kobject_get( ($k->{kobj}))) :  &NULL; });
	}' unless defined(&kset_get);
	eval 'sub kset_put {
	    local($k) = @_;
    	    eval q({  &kobject_put( ($k->{kobj})); });
	}' unless defined(&kset_put);
	eval 'sub get_ktype {
	    local($k) = @_;
    	    eval q({  &if ( ($k->{kset})  &&  ($k->{kset}->{ktype}))  ($k->{kset}->{ktype});  &else  ($k->{ktype}); });
	}' unless defined(&get_ktype);
	eval 'sub set_kset_name {
	    local($str) = @_;
    	    eval q(. &kset = { . &kobj = { . &name = $str } });
	}' unless defined(&set_kset_name);
	eval 'sub decl_subsys {
	    local($_name,$_type,$_hotplug_ops) = @_;
    	    eval q(\'struct subsystem\' $_name &_subsys = { . &kset = { . &kobj = { . &name =  &__stringify($_name) }, . &ktype = $_type, . &hotplug_ops =$_hotplug_ops, } });
	}' unless defined(&decl_subsys);
	eval 'sub decl_subsys_name {
	    local($_varname,$_name,$_type,$_hotplug_ops) = @_;
    	    eval q(\'struct subsystem\' $_varname &_subsys = { . &kset = { . &kobj = { . &name =  &__stringify($_name) }, . &ktype = $_type, . &hotplug_ops =$_hotplug_ops, } });
	}' unless defined(&decl_subsys_name);
	eval 'sub kobj_set_kset_s {
	    local($obj,$subsys) = @_;
    	    eval q(($obj)-> ($k$obj->{kset}) = ($subsys). &kset);
	}' unless defined(&kobj_set_kset_s);
	eval 'sub kset_set_kset_s {
	    local($obj,$subsys) = @_;
    	    eval q(($obj)-> ($kset->{k$obj}->{kset}) = ($subsys). &kset);
	}' unless defined(&kset_set_kset_s);
	eval 'sub subsys_set_kset {
	    local($obj,$_subsys) = @_;
    	    eval q(($obj)-> ($subsys->{kset}->{k$obj}->{kset}) = ($_subsys). &kset);
	}' unless defined(&subsys_set_kset);
	eval 'sub subsys_get {
	    local($s) = @_;
    	    eval q({ $s ?  &container_of( &kset_get( ($s->{k$set})),\'struct subsystem\', &kset) :  &NULL; });
	}' unless defined(&subsys_get);
	eval 'sub subsys_put {
	    local($s) = @_;
    	    eval q({  &kset_put( ($s->{k$set})); });
	}' unless defined(&subsys_put);
	if(defined(&CONFIG_HOTPLUG)) {
	} else {
	    eval 'sub add_hotplug_env_var {
	        local($envp,$num_envp,$cur_index,$buffer,$buffer_size,$cur_len,$format) = @_;
    		eval q({ 0; });
	    }' unless defined(&add_hotplug_env_var);
	}
    }
}
1;
