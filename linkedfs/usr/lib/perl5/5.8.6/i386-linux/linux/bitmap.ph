require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_BITMAP_H)) {
    eval 'sub __LINUX_BITMAP_H () {1;}' unless defined(&__LINUX_BITMAP_H);
    if(defined(&__KERNEL__)) {
	unless(defined(&__ASSEMBLY__)) {
	    require 'linux/types.ph';
	    require 'linux/bitops.ph';
	    require 'linux/string.ph';
	    eval 'sub BITMAP_LAST_WORD_MASK {
	        local($nbits) = @_;
    		eval q(( (($nbits) %  &BITS_PER_LONG) ? (1<<(($nbits) %  &BITS_PER_LONG))-1: ~0 ));
	    }' unless defined(&BITMAP_LAST_WORD_MASK);
	    eval 'sub bitmap_zero {
	        local($dst,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = 0;  &else { \'int\'  &len =  &BITS_TO_LONGS($nbits) * $sizeof{\'unsigned long\'};  &memset($dst, 0,  &len); } });
	    }' unless defined(&bitmap_zero);
	    eval 'sub bitmap_fill {
	        local($dst,$nbits) = @_;
    		eval q({ \'size_t\' \'nlongs\' =  &BITS_TO_LONGS($nbits);  &if (\'nlongs\' > 1) { \'int\'  &len = (\'nlongs\' - 1) * $sizeof{\'unsigned long\'};  &memset($dst, 0xff,  &len); } $dst->[\'nlongs\' - 1] =  &BITMAP_LAST_WORD_MASK($nbits); });
	    }' unless defined(&bitmap_fill);
	    eval 'sub bitmap_copy {
	        local($dst,$src,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = *$src;  &else { \'int\'  &len =  &BITS_TO_LONGS($nbits) * $sizeof{\'unsigned long\'};  &memcpy($dst, $src,  &len); } });
	    }' unless defined(&bitmap_copy);
	    eval 'sub bitmap_and {
	        local($dst,$src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = *$src1 & *$src2;  &else  &__bitmap_and($dst, $src1, $src2, $nbits); });
	    }' unless defined(&bitmap_and);
	    eval 'sub bitmap_or {
	        local($dst,$src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = *$src1 | *$src2;  &else  &__bitmap_or($dst, $src1, $src2, $nbits); });
	    }' unless defined(&bitmap_or);
	    eval 'sub bitmap_xor {
	        local($dst,$src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = *$src1 ^ *$src2;  &else  &__bitmap_xor($dst, $src1, $src2, $nbits); });
	    }' unless defined(&bitmap_xor);
	    eval 'sub bitmap_andnot {
	        local($dst,$src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = *$src1 & ~(*$src2);  &else  &__bitmap_andnot($dst, $src1, $src2, $nbits); });
	    }' unless defined(&bitmap_andnot);
	    eval 'sub bitmap_complement {
	        local($dst,$src,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = ~(*$src) &  &BITMAP_LAST_WORD_MASK($nbits);  &else  &__bitmap_complement($dst, $src, $nbits); });
	    }' unless defined(&bitmap_complement);
	    eval 'sub bitmap_equal {
	        local($src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) ! ((*$src1 ^ *$src2) &  &BITMAP_LAST_WORD_MASK($nbits));  &else  &__bitmap_equal($src1, $src2, $nbits); });
	    }' unless defined(&bitmap_equal);
	    eval 'sub bitmap_intersects {
	        local($src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) ((*$src1 & *$src2) &  &BITMAP_LAST_WORD_MASK($nbits)) != 0;  &else  &__bitmap_intersects($src1, $src2, $nbits); });
	    }' unless defined(&bitmap_intersects);
	    eval 'sub bitmap_subset {
	        local($src1,$src2,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) ! ((*$src1 & ~(*$src2)) &  &BITMAP_LAST_WORD_MASK($nbits));  &else  &__bitmap_subset($src1, $src2, $nbits); });
	    }' unless defined(&bitmap_subset);
	    eval 'sub bitmap_empty {
	        local($src,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) ! (*$src &  &BITMAP_LAST_WORD_MASK($nbits));  &else  &__bitmap_empty($src, $nbits); });
	    }' unless defined(&bitmap_empty);
	    eval 'sub bitmap_full {
	        local($src,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) ! (~(*$src) &  &BITMAP_LAST_WORD_MASK($nbits));  &else  &__bitmap_full($src, $nbits); });
	    }' unless defined(&bitmap_full);
	    eval 'sub bitmap_weight {
	        local($src,$nbits) = @_;
    		eval q({  &__bitmap_weight($src, $nbits); });
	    }' unless defined(&bitmap_weight);
	    eval 'sub bitmap_shift_right {
	        local($dst,$src,$n,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = *$src >> $n;  &else  &__bitmap_shift_right($dst, $src, $n, $nbits); });
	    }' unless defined(&bitmap_shift_right);
	    eval 'sub bitmap_shift_left {
	        local($dst,$src,$n,$nbits) = @_;
    		eval q({  &if ($nbits <=  &BITS_PER_LONG) *$dst = (*$src << $n) &  &BITMAP_LAST_WORD_MASK($nbits);  &else  &__bitmap_shift_left($dst, $src, $n, $nbits); });
	    }' unless defined(&bitmap_shift_left);
	}
    }
}
1;
