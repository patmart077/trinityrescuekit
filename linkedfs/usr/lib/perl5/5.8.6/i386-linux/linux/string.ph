require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_STRING_H_)) {
    eval 'sub _LINUX_STRING_H_ () {1;}' unless defined(&_LINUX_STRING_H_);
    if(defined(&__KERNEL__)) {
	require 'linux/compiler.ph';
	require 'linux/types.ph';
	require 'linux/stddef.ph';
	if(defined(&__cplusplus)) {
	}
	require 'asm/string.ph';
	unless(defined(&__HAVE_ARCH_STRCPY)) {
	}
	unless(defined(&__HAVE_ARCH_STRNCPY)) {
	}
	unless(defined(&__HAVE_ARCH_STRLCPY)) {
	}
	unless(defined(&__HAVE_ARCH_STRCAT)) {
	}
	unless(defined(&__HAVE_ARCH_STRNCAT)) {
	}
	unless(defined(&__HAVE_ARCH_STRLCAT)) {
	}
	unless(defined(&__HAVE_ARCH_STRCMP)) {
	}
	unless(defined(&__HAVE_ARCH_STRNCMP)) {
	}
	unless(defined(&__HAVE_ARCH_STRNICMP)) {
	}
	unless(defined(&__HAVE_ARCH_STRCHR)) {
	}
	unless(defined(&__HAVE_ARCH_STRNCHR)) {
	}
	unless(defined(&__HAVE_ARCH_STRRCHR)) {
	}
	unless(defined(&__HAVE_ARCH_STRSTR)) {
	}
	unless(defined(&__HAVE_ARCH_STRLEN)) {
	}
	unless(defined(&__HAVE_ARCH_STRNLEN)) {
	}
	unless(defined(&__HAVE_ARCH_MEMSET)) {
	}
	unless(defined(&__HAVE_ARCH_MEMCPY)) {
	}
	unless(defined(&__HAVE_ARCH_MEMMOVE)) {
	}
	unless(defined(&__HAVE_ARCH_MEMSCAN)) {
	}
	unless(defined(&__HAVE_ARCH_MEMCMP)) {
	}
	unless(defined(&__HAVE_ARCH_MEMCHR)) {
	}
	if(defined(&__cplusplus)) {
	}
    }
}
1;
