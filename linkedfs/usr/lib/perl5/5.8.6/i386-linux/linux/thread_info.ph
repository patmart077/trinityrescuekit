require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_THREAD_INFO_H)) {
    eval 'sub _LINUX_THREAD_INFO_H () {1;}' unless defined(&_LINUX_THREAD_INFO_H);
    require 'linux/bitops.ph';
    require 'asm/thread_info.ph';
    if(defined(&__KERNEL__)) {
	eval 'sub clear_thread_flag {
	    local($flag) = @_;
    	    eval q({  &clear_bit($flag, &current_thread_info()-> &flags); });
	}' unless defined(&clear_thread_flag);
	eval 'sub test_and_set_thread_flag {
	    local($flag) = @_;
    	    eval q({  &test_and_set_bit($flag, &current_thread_info()-> &flags); });
	}' unless defined(&test_and_set_thread_flag);
	eval 'sub test_and_clear_thread_flag {
	    local($flag) = @_;
    	    eval q({  &test_and_clear_bit($flag, &current_thread_info()-> &flags); });
	}' unless defined(&test_and_clear_thread_flag);
	eval 'sub test_thread_flag {
	    local($flag) = @_;
    	    eval q({  &test_bit($flag, &current_thread_info()-> &flags); });
	}' unless defined(&test_thread_flag);
	eval 'sub set_ti_thread_flag {
	    local($ti,$flag) = @_;
    	    eval q({  &set_bit($flag, ($ti->{flags})); });
	}' unless defined(&set_ti_thread_flag);
	eval 'sub clear_ti_thread_flag {
	    local($ti,$flag) = @_;
    	    eval q({  &clear_bit($flag, ($ti->{flags})); });
	}' unless defined(&clear_ti_thread_flag);
	eval 'sub test_and_set_ti_thread_flag {
	    local($ti,$flag) = @_;
    	    eval q({  &test_and_set_bit($flag, ($ti->{flags})); });
	}' unless defined(&test_and_set_ti_thread_flag);
	eval 'sub test_and_clear_ti_thread_flag {
	    local($ti,$flag) = @_;
    	    eval q({  &test_and_clear_bit($flag, ($ti->{flags})); });
	}' unless defined(&test_and_clear_ti_thread_flag);
	eval 'sub test_ti_thread_flag {
	    local($ti,$flag) = @_;
    	    eval q({  &test_bit($flag, ($ti->{flags})); });
	}' unless defined(&test_ti_thread_flag);
	eval 'sub set_need_resched {
	    local($void) = @_;
    	    eval q({  &set_thread_flag( &TIF_NEED_RESCHED); });
	}' unless defined(&set_need_resched);
	eval 'sub clear_need_resched {
	    local($void) = @_;
    	    eval q({  &clear_thread_flag( &TIF_NEED_RESCHED); });
	}' unless defined(&clear_need_resched);
    }
}
1;
