require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_GENERIC_BUG_H)) {
    eval 'sub _ASM_GENERIC_BUG_H () {1;}' unless defined(&_ASM_GENERIC_BUG_H);
    require 'linux/compiler.ph';
    require 'linux/config.ph';
    unless(defined(&HAVE_ARCH_BUG)) {
	eval 'sub BUG () {
	    eval q( &do {  &printk(\\"kernel BUG at %s:%d!\\\\n\\",  &__FILE__,  &__LINE__);  &panic(\\"BUG!\\"); }  &while (0));
	}' unless defined(&BUG);
    }
    unless(defined(&HAVE_ARCH_PAGE_BUG)) {
	eval 'sub PAGE_BUG {
	    local($page) = @_;
    	    eval q( &do {  &printk(\\"page BUG for page at %p\\\\n\\", $page);  &BUG(); }  &while (0));
	}' unless defined(&PAGE_BUG);
    }
    unless(defined(&HAVE_ARCH_BUG_ON)) {
	eval 'sub BUG_ON {
	    local($condition) = @_;
    	    eval q( &do {  &if ( &unlikely(($condition)!=0))  &BUG(); }  &while(0));
	}' unless defined(&BUG_ON);
    }
    unless(defined(&HAVE_ARCH_WARN_ON)) {
	eval 'sub WARN_ON {
	    local($condition) = @_;
    	    eval q( &do {  &if ( &unlikely(($condition)!=0)) {  &printk(\\"Badness in %s at %s:%d\\\\n\\",  &__FUNCTION__,  &__FILE__,  &__LINE__);  &dump_stack(); } }  &while (0));
	}' unless defined(&WARN_ON);
    }
}
1;
