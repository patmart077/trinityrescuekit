require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_GENERIC_ERRNO_BASE_H)) {
    eval 'sub _ASM_GENERIC_ERRNO_BASE_H () {1;}' unless defined(&_ASM_GENERIC_ERRNO_BASE_H);
    eval 'sub EPERM () {1;}' unless defined(&EPERM);
    eval 'sub ENOENT () {2;}' unless defined(&ENOENT);
    eval 'sub ESRCH () {3;}' unless defined(&ESRCH);
    eval 'sub EINTR () {4;}' unless defined(&EINTR);
    eval 'sub EIO () {5;}' unless defined(&EIO);
    eval 'sub ENXIO () {6;}' unless defined(&ENXIO);
    eval 'sub E2BIG () {7;}' unless defined(&E2BIG);
    eval 'sub ENOEXEC () {8;}' unless defined(&ENOEXEC);
    eval 'sub EBADF () {9;}' unless defined(&EBADF);
    eval 'sub ECHILD () {10;}' unless defined(&ECHILD);
    eval 'sub EAGAIN () {11;}' unless defined(&EAGAIN);
    eval 'sub ENOMEM () {12;}' unless defined(&ENOMEM);
    eval 'sub EACCES () {13;}' unless defined(&EACCES);
    eval 'sub EFAULT () {14;}' unless defined(&EFAULT);
    eval 'sub ENOTBLK () {15;}' unless defined(&ENOTBLK);
    eval 'sub EBUSY () {16;}' unless defined(&EBUSY);
    eval 'sub EEXIST () {17;}' unless defined(&EEXIST);
    eval 'sub EXDEV () {18;}' unless defined(&EXDEV);
    eval 'sub ENODEV () {19;}' unless defined(&ENODEV);
    eval 'sub ENOTDIR () {20;}' unless defined(&ENOTDIR);
    eval 'sub EISDIR () {21;}' unless defined(&EISDIR);
    eval 'sub EINVAL () {22;}' unless defined(&EINVAL);
    eval 'sub ENFILE () {23;}' unless defined(&ENFILE);
    eval 'sub EMFILE () {24;}' unless defined(&EMFILE);
    eval 'sub ENOTTY () {25;}' unless defined(&ENOTTY);
    eval 'sub ETXTBSY () {26;}' unless defined(&ETXTBSY);
    eval 'sub EFBIG () {27;}' unless defined(&EFBIG);
    eval 'sub ENOSPC () {28;}' unless defined(&ENOSPC);
    eval 'sub ESPIPE () {29;}' unless defined(&ESPIPE);
    eval 'sub EROFS () {30;}' unless defined(&EROFS);
    eval 'sub EMLINK () {31;}' unless defined(&EMLINK);
    eval 'sub EPIPE () {32;}' unless defined(&EPIPE);
    eval 'sub EDOM () {33;}' unless defined(&EDOM);
    eval 'sub ERANGE () {34;}' unless defined(&ERANGE);
}
1;
