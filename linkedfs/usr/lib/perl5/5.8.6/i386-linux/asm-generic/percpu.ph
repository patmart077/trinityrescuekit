require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_GENERIC_PERCPU_H_)) {
    eval 'sub _ASM_GENERIC_PERCPU_H_ () {1;}' unless defined(&_ASM_GENERIC_PERCPU_H_);
    require 'linux/compiler.ph';
    eval 'sub __GENERIC_PER_CPU () {1;}' unless defined(&__GENERIC_PER_CPU);
    if(defined(&CONFIG_SMP)) {
	eval 'sub DEFINE_PER_CPU {
	    local($type, $name) = @_;
    	    eval q( &__attribute__(( &__section__(\\".data.percpu\\")))  &__typeof__($type)  &per_cpu__$name);
	}' unless defined(&DEFINE_PER_CPU);
	eval 'sub per_cpu {
	    local($var, $cpu) = @_;
    	    eval q((* &RELOC_HIDE( &per_cpu__$var,  $__per_cpu_offset[$cpu])));
	}' unless defined(&per_cpu);
	eval 'sub __get_cpu_var {
	    local($var) = @_;
    	    eval q( &per_cpu($var,  &smp_processor_id()));
	}' unless defined(&__get_cpu_var);
	eval 'sub percpu_modcopy {
	    local($pcpudst, $src, $size) = @_;
    	    eval q( &do { \'unsigned int __i\';  &for ( &__i = 0;  &__i <  &NR_CPUS;  &__i++)  &if ( &cpu_possible( &__i))  &memcpy(($pcpudst)+ $__per_cpu_offset[ &__i], ($src), ($size)); }  &while (0));
	}' unless defined(&percpu_modcopy);
    } else {
	eval 'sub DEFINE_PER_CPU {
	    local($type, $name) = @_;
    	    eval q( &__typeof__($type)  &per_cpu__$name);
	}' unless defined(&DEFINE_PER_CPU);
	eval 'sub per_cpu {
	    local($var, $cpu) = @_;
    	    eval q((*(( &void)$cpu,  &per_cpu__$var)));
	}' unless defined(&per_cpu);
	eval 'sub __get_cpu_var {
	    local($var) = @_;
    	    eval q( &per_cpu__$var);
	}' unless defined(&__get_cpu_var);
    }
    eval 'sub DECLARE_PER_CPU {
        local($type, $name) = @_;
	    eval q( &extern  &__typeof__($type)  &per_cpu__$name);
    }' unless defined(&DECLARE_PER_CPU);
    eval 'sub EXPORT_PER_CPU_SYMBOL {
        local($var) = @_;
	    eval q( &EXPORT_SYMBOL( &per_cpu__$var));
    }' unless defined(&EXPORT_PER_CPU_SYMBOL);
    eval 'sub EXPORT_PER_CPU_SYMBOL_GPL {
        local($var) = @_;
	    eval q( &EXPORT_SYMBOL_GPL( &per_cpu__$var));
    }' unless defined(&EXPORT_PER_CPU_SYMBOL_GPL);
}
1;
