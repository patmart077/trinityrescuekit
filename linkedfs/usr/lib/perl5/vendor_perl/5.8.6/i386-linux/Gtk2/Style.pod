=head1 NAME

Gtk2::Style

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Style

=for object Gtk2::Style
=cut




=head1 METHODS

=head2 style = Gtk2::Style-E<gt>B<new> 

=over

=back

=head2 $style-E<gt>B<apply_default_background> ($window, $set_bg, $state_type, $area, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=item * $set_bg (boolean) 

=item * $state_type (Gtk2::StateType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 style = $style-E<gt>B<attach> ($window)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=back

=back

=head2 boolean = $style-E<gt>B<attached> 

=over

=back

=head2 $style-E<gt>B<set_background> ($window, $state_type)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=item * $state_type (Gtk2::StateType) 

=back

=back

=head2 color = $style-E<gt>B<base> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<base_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 color = $style-E<gt>B<bg> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<bg_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 pixmap = $style-E<gt>B<bg_pixmap> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 scalar = $style-E<gt>B<black> 

=over

=back

=head2 scalar = $style-E<gt>B<black_gc> 

=over

=back

=head2 style = $style-E<gt>B<copy> 

=over

=back

=head2 color = $style-E<gt>B<dark> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<dark_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 $style-E<gt>B<detach> 

=over

=back

=head2 Gtk2-E<gt>B<draw_insertion_cursor> ($widget, $drawable, $area, $location, $is_primary, $direction, $draw_arrow)

=over

=over

=item * $widget (Gtk2::Widget) 

=item * $drawable (Gtk2::Gdk::Drawable) 

=item * $area (Gtk2::Gdk::Rectangle) 

=item * $location (Gtk2::Gdk::Rectangle) 

=item * $is_primary (boolean) 

=item * $direction (Gtk2::TextDirection) 

=item * $draw_arrow (boolean) 

=back

=back

=head2 color = $style-E<gt>B<fg> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<fg_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 scalar = $style-E<gt>B<font_desc> 

=over

=back

=head2 color = $style-E<gt>B<light> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<light_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 iconset = $style-E<gt>B<lookup_icon_set> ($stock_id)

=over

=over

=item * $stock_id (string) 

=back

=back

=head2 color = $style-E<gt>B<mid> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<mid_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 $style-E<gt>B<paint_arrow> ($window, $state_type, $shadow_type, $area, $widget, $detail, $arrow_type, $fill, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $arrow_type (Gtk2::ArrowType) 

=item * $fill (boolean) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_box> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_box_gap> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height, $gap_side, $gap_x, $gap_width)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=item * $gap_side (Gtk2::PositionType) 

=item * $gap_x (integer) 

=item * $gap_width (integer) 

=back

=back

=head2 $style-E<gt>B<paint_check> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_diamond> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_expander> ($window, $state_type, $area, $widget, $detail, $x, $y, $expander_style)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $expander_style (Gtk2::ExpanderStyle) 

=back

=back

=head2 $style-E<gt>B<paint_extension> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height, $gap_side)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=item * $gap_side (Gtk2::PositionType) 

=back

=back

=head2 $style-E<gt>B<paint_flat_box> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_focus> ($window, $state_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_handle> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height, $orientation)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=item * $orientation (Gtk2::Orientation) 

=back

=back

=head2 $style-E<gt>B<paint_hline> ($window, $state_type, $area, $widget, $detail, $x1, $x2, $y)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x1 (integer) 

=item * $x2 (integer) 

=item * $y (integer) 

=back

=back

=head2 $style-E<gt>B<paint_layout> ($window, $state_type, $use_text, $area, $widget, $detail, $x, $y, $layout)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $use_text (boolean) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $layout (Gtk2::Pango::Layout) 

=back

=back

=head2 $style-E<gt>B<paint_option> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_polygon> ($window, $state_type, $shadow_type, $area, $widget, $detail, $fill, $x1, $y1, ...)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $fill (boolean) 

=item * $x1 (integer) x coordinate of the first vertex

=item * $y1 (integer) y coordinate of the first vertex

=item * ... (list) pairs of x and y coordinates

=back



=back

=head2 $style-E<gt>B<paint_resize_grip> ($window, $state_type, $area, $widget, $detail, $edge, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $edge (Gtk2::Gdk::WindowEdge) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_shadow> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_shadow_gap> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height, $gap_side, $gap_x, $gap_width)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=item * $gap_side (Gtk2::PositionType) 

=item * $gap_x (integer) 

=item * $gap_width (integer) 

=back

=back

=head2 $style-E<gt>B<paint_slider> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height, $orientation)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=item * $orientation (Gtk2::Orientation) 

=back

=back

=head2 $style-E<gt>B<paint_tab> ($window, $state_type, $shadow_type, $area, $widget, $detail, $x, $y, $width, $height)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $shadow_type (Gtk2::ShadowType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget) 

=item * $detail (string or undef) 

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $style-E<gt>B<paint_vline> ($window, $state_type, $area, $widget, $detail, $y1_, $y2_, $x)

=over

=over

=item * $window (Gtk2::Gdk::Drawable) 

=item * $state_type (Gtk2::StateType) 

=item * $area (Gtk2::Gdk::Rectangle or undef) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=item * $y1_ (integer) 

=item * $y2_ (integer) 

=item * $x (integer) 

=back

=back

=head2 pixbuf = $style-E<gt>B<render_icon> ($source, $direction, $state, $size, $widget, $detail=undef)

=over

=over

=item * $source (Gtk2::IconSource) 

=item * $direction (Gtk2::TextDirection) 

=item * $state (Gtk2::StateType) 

=item * $size (Gtk2::IconSize) 

=item * $widget (Gtk2::Widget or undef) 

=item * $detail (string or undef) 

=back

=back

=head2 color = $style-E<gt>B<text> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 color = $style-E<gt>B<text_aa> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<text_aa_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 gc = $style-E<gt>B<text_gc> ($state)

=over

=over

=item * $state (Gtk2::StateType) 

=back

=back

=head2 scalar = $style-E<gt>B<white> 

=over

=back

=head2 scalar = $style-E<gt>B<white_gc> 

=over

=back

=head2 scalar = $style-E<gt>B<xthickness> 

=over

=back

=head2 scalar = $style-E<gt>B<ythickness> 

=over

=back


=head1 SIGNALS

=over

=item B<realize> (Gtk2::Style)

=item B<unrealize> (Gtk2::Style)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

