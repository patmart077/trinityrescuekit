=head1 NAME

Gtk2::SizeGroup

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::SizeGroup


=head1 METHODS

=head2 sizegroup = Gtk2::SizeGroup-E<gt>B<new> ($mode)

=over

=over

=item * $mode (Gtk2::SizeGroupMode) 

=back

=back

=head2 $size_group-E<gt>B<add_widget> ($widget)

=over

=over

=item * $widget (Gtk2::Widget) 

=back

=back

=head2 sizegroupmode = $size_group-E<gt>B<get_mode> 

=over

=back

=head2 $size_group-E<gt>B<set_mode> ($mode)

=over

=over

=item * $mode (Gtk2::SizeGroupMode) 

=back

=back

=head2 $size_group-E<gt>B<remove_widget> ($widget)

=over

=over

=item * $widget (Gtk2::Widget) 

=back

=back


=head1 PROPERTIES

=over

=item 'mode' (Gtk2::SizeGroupMode : readable / writable)

The directions in which the size group effects the requested sizes of its component widgets

=back


=head1 ENUMS AND FLAGS

=head2 enum Gtk2::SizeGroupMode



=over

=item * 'none' / 'GTK_SIZE_GROUP_NONE'

=item * 'horizontal' / 'GTK_SIZE_GROUP_HORIZONTAL'

=item * 'vertical' / 'GTK_SIZE_GROUP_VERTICAL'

=item * 'both' / 'GTK_SIZE_GROUP_BOTH'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

