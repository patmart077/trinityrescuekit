=head1 NAME

Gtk2::Gdk


=head1 METHODS

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_CLIPBOARD> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_PRIMARY> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_SECONDARY> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_ATOM> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_BITMAP> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_COLORMAP> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_DRAWABLE> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_INTEGER> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_PIXMAP> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_STRING> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<SELECTION_TYPE_WINDOW> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<TARGET_BITMAP> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<TARGET_COLORMAP> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<TARGET_DRAWABLE> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<TARGET_PIXMAP> 

=over

=back

=head2 atom = Gtk2::Gdk-E<gt>B<TARGET_STRING> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<beep> 

=over

=back

=head2 window = Gtk2::Gdk-E<gt>B<get_default_root_window> 

=over

=back

=head2 list = Gtk2::Gdk-E<gt>B<devices_list> 

=over


Returns a list of I<GdkDevice>s.


=back

=head2 string = Gtk2::Gdk-E<gt>B<get_display_arg_name> 

=over

=back

=head2 string = Gtk2::Gdk-E<gt>B<get_display> 

=over

=back

=head2 integer = Gtk2::Gdk-E<gt>B<error_trap_pop> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<error_trap_push> 

=over

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<events_pending> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<flush> 

=over

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<init> 

=over

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<init_check> 

=over

=back

=head2 grabstatus = Gtk2::Gdk-E<gt>B<keyboard_grab> ($window, $owner_events, $time_)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=item * $owner_events (boolean) 

=item * $time_ (unsigned) 

=back

=back

=head2 Gtk2::Gdk-E<gt>B<keyboard_ungrab> ($time_)

=over

=over

=item * $time_ (unsigned) 

=back

=back

=head2 (lower, upper) = Gtk2::Gdk::Keyval->B<convert_case> ($symbol)

=over

=over

=item * $symbol (integer) 

=back



=back

=head2 integer = Gtk2::Gdk-E<gt>B<keyval_from_name> ($keyval_name)

=over

=over

=item * $keyval_name (string) 

=back

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<keyval_is_lower> ($keyval)

=over

=over

=item * $keyval (integer) 

=back

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<keyval_is_upper> ($keyval)

=over

=over

=item * $keyval (integer) 

=back

=back

=head2 string = Gtk2::Gdk-E<gt>B<keyval_name> ($keyval)

=over

=over

=item * $keyval (integer) 

=back

=back

=head2 integer = Gtk2::Gdk-E<gt>B<keyval_to_lower> ($keyval)

=over

=over

=item * $keyval (integer) 

=back

=back

=head2 unsigned = Gtk2::Gdk-E<gt>B<keyval_to_unicode> ($keyval)

=over

=over

=item * $keyval (integer) 

=back

=back

=head2 integer = Gtk2::Gdk-E<gt>B<keyval_to_upper> ($keyval)

=over

=over

=item * $keyval (integer) 

=back

=back

=head2 list = Gtk2::Gdk-E<gt>B<list_visuals> 

=over

=back

=head2 string = Gtk2::Gdk-E<gt>B<set_locale> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<notify_startup_complete> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<parse_args> 

=over

=back

=head2 grabstatus = Gtk2::Gdk-E<gt>B<pointer_grab> ($window, $owner_events, $event_mask, $confine_to, $cursor, $time_)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=item * $owner_events (boolean) 

=item * $event_mask (Gtk2::Gdk::EventMask) 

=item * $confine_to (Gtk2::Gdk::Window or undef) 

=item * $cursor (Gtk2::Gdk::Cursor or undef) 

=item * $time_ (unsigned) 

=back

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<pointer_is_grabbed> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<pointer_ungrab> ($time_)

=over

=over

=item * $time_ (unsigned) 

=back

=back

=head2 string = Gtk2::Gdk-E<gt>B<get_program_class> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<set_program_class> ($program_class)

=over

=over

=item * $program_class (string) 

=back

=back

=head2 list = Gtk2::Gdk-E<gt>B<query_depths> 

=over

Returns a list of depths.

=back

=head2 list = Gtk2::Gdk-E<gt>B<query_visual_types> 

=over

=back

=head2 integer = Gtk2::Gdk-E<gt>B<screen_height> 

=over

=back

=head2 integer = Gtk2::Gdk-E<gt>B<screen_height_mm> 

=over

=back

=head2 integer = Gtk2::Gdk-E<gt>B<screen_width> 

=over

=back

=head2 integer = Gtk2::Gdk-E<gt>B<screen_width_mm> 

=over

=back

=head2 scalar = Gtk2::Gdk-E<gt>B<setting_get> ($name)

=over

=over

=item * $name (string) 

=back

=back

=head2 boolean = Gtk2::Gdk-E<gt>B<get_show_events> 

=over

=back

=head2 Gtk2::Gdk-E<gt>B<set_show_events> ($show_events)

=over

=over

=item * $show_events (boolean) 

=back

=back

=head2 Gtk2::Gdk-E<gt>B<set_sm_client_id> ($sm_client_id=undef)

=over

=over

=item * $sm_client_id (string or undef) 

=back

=back

=head2 list = Gtk2::Gdk-E<gt>B<string_to_compound_text> ($str)

=over

=over

=item * $str (string) 

=back

Returns a list of strings.

=back

=head2 list = Gtk2::Gdk-E<gt>B<string_to_compound_text_for_display> ($display, $str)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $str (string) 

=back

Returns a list of strings.

=back

=head2 list = Gtk2::Gdk-E<gt>B<text_property_to_text_list> ($encoding, $format, $text)

=over

=over

=item * $encoding (Gtk2::Gdk::Atom) 

=item * $format (integer) 

=item * $text (scalar) 

=back

Returns a list of strings.

=back

=head2 list = Gtk2::Gdk-E<gt>B<text_property_to_text_list_for_display> ($display, $encoding, $format, $text)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $encoding (Gtk2::Gdk::Atom) 

=item * $format (integer) 

=item * $text (scalar) 

=back

Returns a list of strings.

=back

=head2 list = Gtk2::Gdk-E<gt>B<text_property_to_utf8_list> ($encoding, $format, $text)

=over

=over

=item * $encoding (Gtk2::Gdk::Atom) 

=item * $format (integer) 

=item * $text (scalar) 

=back

Returns a list of strings.

=back

=head2 list = Gtk2::Gdk-E<gt>B<text_property_to_utf8_list_for_display> ($display, $encoding, $format, $text)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $encoding (Gtk2::Gdk::Atom) 

=item * $format (integer) 

=item * $text (scalar) 

=back

Returns a list of strings.

=back

=head2 integer = Gtk2::Gdk-E<gt>B<unicode_to_keyval> ($wc)

=over

=over

=item * $wc (unsigned) 

=back

=back

=head2 list = Gtk2::Gdk-E<gt>B<utf8_to_compound_text> ($str)

=over

=over

=item * $str (string) 

=back

Returns a list of strings.

=back

=head2 list = Gtk2::Gdk-E<gt>B<utf8_to_compound_text_for_display> ($display, $str)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $str (string) 

=back

Returns a list of strings.

=back

=head2 string or undef = Gtk2::Gdk-E<gt>B<utf8_to_string_target> ($str)

=over

=over

=item * $str (string) 

=back

Returns a list of strings.

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

