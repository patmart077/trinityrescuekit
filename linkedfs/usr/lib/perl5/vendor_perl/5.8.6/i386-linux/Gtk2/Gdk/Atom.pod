=head1 NAME

Gtk2::Gdk::Atom


=head1 METHODS

=head2 atom = Gtk2::Gdk::Atom-E<gt>B<new> ($atom_name, $only_if_exists=FALSE)

=over

=over

=item * $atom_name (string) 

=item * $only_if_exists (boolean) 

=back

=back

=head2 atom = Gtk2::Gdk::Atom-E<gt>B<intern> ($atom_name, $only_if_exists=FALSE)

=over

=over

=item * $atom_name (string) 

=item * $only_if_exists (boolean) 

=back

=back

=head2 string = $atom-E<gt>B<name> 

=over

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

