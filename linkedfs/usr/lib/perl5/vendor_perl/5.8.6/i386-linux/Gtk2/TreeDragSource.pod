=head1 NAME

Gtk2::TreeDragSource


=head1 METHODS

=head2 boolean = $drag_source-E<gt>B<drag_data_delete> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back

=head2 selectiondata = $drag_source-E<gt>B<drag_data_get> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back

=head2 boolean = $drag_source-E<gt>B<row_draggable> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

