=head1 NAME

Gtk2::CellRendererToggle

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::CellRenderer
              +----Gtk2::CellRendererToggle


=head1 METHODS

=head2 cellrenderer = Gtk2::CellRendererToggle-E<gt>B<new> 

=over

=back

=head2 boolean = $toggle-E<gt>B<get_active> 

=over

=back

=head2 $toggle-E<gt>B<set_active> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 boolean = $toggle-E<gt>B<get_radio> 

=over

=back

=head2 $toggle-E<gt>B<set_radio> ($radio)

=over

=over

=item * $radio (boolean) 

=back

=back


=head1 PROPERTIES

=over

=item 'activatable' (boolean : readable / writable)

The toggle button can be activated

=item 'active' (boolean : readable / writable)

The toggle state of the button

=item 'inconsistent' (boolean : readable / writable)

The inconsistent state of the button

=item 'radio' (boolean : readable / writable)

Draw the toggle button as a radio button

=back


=head1 SIGNALS

=over

=item B<toggled> (Gtk2::CellRendererToggle, string)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::CellRenderer>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

