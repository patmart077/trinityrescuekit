=head1 NAME

Gtk2::Window

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Bin
                          +----Gtk2::Window

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::Window-E<gt>B<new> ($type=GTK_WINDOW_TOPLEVEL)

=over

=over

=item * $type (Gtk2::WindowType) 

=back

=back

=head2 boolean = $window-E<gt>B<get_accept_focus> 

=over

=back

=head2 $window-E<gt>B<set_accept_focus> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 boolean = $window-E<gt>B<activate_default> 

=over

=back

=head2 boolean = $window-E<gt>B<activate_focus> 

=over

=back

=head2 boolean = $window-E<gt>B<activate_key> ($event)

=over

=over

=item * $event (Gtk2::Gdk::Event::Key) 

=back



=back

=head2 $window-E<gt>B<add_accel_group> ($accel_group)

=over

=over

=item * $accel_group (Gtk2::AccelGroup) 

=back

=back

=head2 $window-E<gt>B<add_embedded_xid> ($xid)

=over

=over

=item * $xid (integer) 

=back

=back

=head2 $window-E<gt>B<add_mnemonic> ($keyval, $target)

=over

=over

=item * $keyval (integer) 

=item * $target (Gtk2::Widget) 

=back

=back

=head2 Gtk2::Window-E<gt>B<set_auto_startup_notification> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 $window-E<gt>B<begin_move_drag> ($button, $root_x, $root_y, $timestamp)

=over

=over

=item * $button (integer) 

=item * $root_x (integer) 

=item * $root_y (integer) 

=item * $timestamp (unsigned) 

=back

=back

=head2 $window-E<gt>B<begin_resize_drag> ($edge, $button, $root_x, $root_y, $timestamp)

=over

=over

=item * $edge (Gtk2::Gdk::WindowEdge) 

=item * $button (integer) 

=item * $root_x (integer) 

=item * $root_y (integer) 

=item * $timestamp (unsigned) 

=back

=back

=head2 boolean = $window-E<gt>B<get_decorated> 

=over

=back

=head2 $window-E<gt>B<set_decorated> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 Gtk2::Window->B<set_default_icon_from_file> ($filename)

=head2 $window->B<set_default_icon_from_file> ($filename)

=over

=over

=item * $filename (localized file name) 

=back



May croak with a L<Glib::Error> in $@ on failure.

=back

=head2 list = Gtk2::Window-E<gt>B<get_default_icon_list> 

=over

Gets the value set by L<$window-E<gt>set_default_icon_list>.

=back

=head2 $window->B<set_default_icon_list> ($pixbuf1, ...)

=over

=over

=item * ... (list) 

=item * $pixbuf1 (Gtk2::Gdk::Pixbuf) 

=back



=back

=head2 Gtk2::Window-E<gt>B<set_default_icon_name> ($name)

=over

=over

=item * $name (string) 

=back

=back

=head2 Gtk2::Window-E<gt>B<set_default_icon> ($icon)

=over

=over

=item * $icon (Gtk2::Gdk::Pixbuf) 

=back

=back

=head2 $window-E<gt>B<set_default> ($default_widget)

=over

=over

=item * $default_widget (Gtk2::Widget or undef) 

=back

=back

=head2 (width, height) = $window-E<gt>B<get_default_size> 

=over

=back

=head2 $window-E<gt>B<set_default_size> ($width, $height)

=over

=over

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $window-E<gt>B<deiconify> 

=over

=back

=head2 boolean = $window-E<gt>B<get_destroy_with_parent> 

=over

=back

=head2 $window-E<gt>B<set_destroy_with_parent> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 widget or undef = $window-E<gt>B<get_focus> 

=over

=back

=head2 boolean = $window-E<gt>B<get_focus_on_map> 

=over

=back

=head2 $window-E<gt>B<set_focus_on_map> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 $window-E<gt>B<set_focus> ($focus=undef)

=over

=over

=item * $focus (Gtk2::Widget or undef) 

=back

=back

=head2 (left, top, right, bottom) = $window-E<gt>B<get_frame_dimensions> 

=over

=back

=head2 $window-E<gt>B<set_frame_dimensions> ($left, $top, $right, $bottom)

=over

=over

=item * $left (integer) 

=item * $top (integer) 

=item * $right (integer) 

=item * $bottom (integer) 

=back

=back

=head2 $window-E<gt>B<fullscreen> 

=over

=back

=head2 $window->B<set_geometry_hints> ($geometry_widget, $geometry)

=head2 $window->B<set_geometry_hints> ($geometry_widget, $geometry, $geom_mask)

=over

=over

=item * $geometry_widget (Gtk2::Widget) 

=item * $geometry_ref (scalar) 

=item * $geom_mask_sv (scalar) 

=item * $geom_mask (Gtk2::Gdk::WindowHints) optional, usually inferred from I<$geometry>

=item * $geometry (Gtk2::Gdk::Geometry) 

=back




The geom_mask argument, describing which fields in the geometry are valid, is
optional.  If omitted it will be inferred from the geometry itself.


=back

=head2 gravity = $window-E<gt>B<get_gravity> 

=over

=back

=head2 $window-E<gt>B<set_gravity> ($gravity)

=over

=over

=item * $gravity (Gtk2::Gdk::Gravity) 

=back

=back

=head2 boolean = $window-E<gt>B<get_has_frame> 

=over

=back

=head2 $window-E<gt>B<set_has_frame> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 boolean = $window-E<gt>B<has_toplevel_focus> 

=over

=back

=head2 $window-E<gt>B<set_icon_from_file> ($filename)

=over

=over

=item * $filename (localized file name) 

=back



May croak with a L<Glib::Error> in $@ on failure.

=back

=head2 pixbuf or undef = $window-E<gt>B<get_icon> 

=over

=back

=head2 list = $window-E<gt>B<get_icon_list> 

=over

Retrieves the list of icons set by L<set_icon_list ()|$window-E<gt>set_icon_list>.

=back

=head2 $window-E<gt>B<set_icon_list> (...)

=over

=over

=item * ... (list) of Gtk2::Gdk::Pixbuf's

=back

Sets up the icon representing a Gtk2::Window. The icon is used when the window
is minimized (also known as iconified). Some window managers or desktop
environments may also place it in the window frame, or display it in other
contexts.

L<set_icon_list ()|$window-E<gt>set_icon_list> allows you to pass in the same icon in several
hand-drawn sizes. The list should contain the natural sizes your icon is
available in; that is, don't scale the image before passing it to GTK+.
Scaling is postponed until the last minute, when the desired final size is
known, to allow best quality.

By passing several sizes, you may improve the final image quality of the icon,
by reducing or eliminating automatic image scaling.

Recommended sizes to provide: 16x16, 32x32, 48x48 at minimum, and larger
images (64x64, 128x128) if you have them. 

=back

=head2 string or undef = $window-E<gt>B<get_icon_name> 

=over

=back

=head2 $window-E<gt>B<set_icon_name> ($name)

=over

=over

=item * $name (string) 

=back

=back

=head2 $window-E<gt>B<set_icon> ($icon)

=over

=over

=item * $icon (Gtk2::Gdk::Pixbuf or undef) 

=back

=back

=head2 $window-E<gt>B<iconify> 

=over

=back

=head2 boolean = $window-E<gt>B<is_active> 

=over

=back

=head2 $window-E<gt>B<set_keep_above> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 $window-E<gt>B<set_keep_below> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 list = Gtk2::Window-E<gt>B<list_toplevels> 

=over

Returns a list of all existing toplevel windows. 

=back

=head2 $window-E<gt>B<maximize> 

=over

=back

=head2 boolean = $window-E<gt>B<mnemonic_activate> ($keyval, $modifier)

=over

=over

=item * $keyval (integer) 

=item * $modifier (Gtk2::Gdk::ModifierType) 

=back

=back

=head2 modifiertype = $window-E<gt>B<get_mnemonic_modifier> 

=over

=back

=head2 $window-E<gt>B<set_mnemonic_modifier> ($modifier)

=over

=over

=item * $modifier (Gtk2::Gdk::ModifierType) 

=back

=back

=head2 boolean = $window-E<gt>B<get_modal> 

=over

=back

=head2 $window-E<gt>B<set_modal> ($modal)

=over

=over

=item * $modal (boolean) 

=back

=back

=head2 $window-E<gt>B<move> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 boolean = $window-E<gt>B<parse_geometry> ($geometry)

=over

=over

=item * $geometry (string) 

=back

=back

=head2 (root_x, root_y) = $window-E<gt>B<get_position> 

=over

=back

=head2 $window-E<gt>B<set_position> ($position)

=over

=over

=item * $position (Gtk2::WindowPosition) 

=back

=back

=head2 $window-E<gt>B<present> 

=over

=back

=head2 boolean = $window-E<gt>B<propagate_key_event> ($event)

=over

=over

=item * $event (Gtk2::Gdk::Event::Key) 

=back



=back

=head2 $window-E<gt>B<remove_accel_group> ($accel_group)

=over

=over

=item * $accel_group (Gtk2::AccelGroup) 

=back

=back

=head2 $window-E<gt>B<remove_embedded_xid> ($xid)

=over

=over

=item * $xid (integer) 

=back

=back

=head2 $window-E<gt>B<remove_mnemonic> ($keyval, $target)

=over

=over

=item * $keyval (integer) 

=item * $target (Gtk2::Widget) 

=back

=back

=head2 $window-E<gt>B<reshow_with_initial_size> 

=over

=back

=head2 boolean = $window-E<gt>B<get_resizable> 

=over

=back

=head2 $window-E<gt>B<set_resizable> ($resizable)

=over

=over

=item * $resizable (boolean) 

=back

=back

=head2 $window-E<gt>B<resize> ($width, $height)

=over

=over

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 string = $window-E<gt>B<get_role> 

=over

=back

=head2 $window-E<gt>B<set_role> ($role)

=over

=over

=item * $role (string) 

=back

=back

=head2 screen = $window-E<gt>B<get_screen> 

=over

=back

=head2 $window-E<gt>B<set_screen> ($screen)

=over

=over

=item * $screen (Gtk2::Gdk::Screen) 

=back

=back

=head2 (width, height) = $window-E<gt>B<get_size> 

=over

=back

=head2 boolean = $window-E<gt>B<get_skip_pager_hint> 

=over

=back

=head2 $window-E<gt>B<set_skip_pager_hint> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 boolean = $window-E<gt>B<get_skip_taskbar_hint> 

=over

=back

=head2 $window-E<gt>B<set_skip_taskbar_hint> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 $window-E<gt>B<stick> 

=over

=back

=head2 string = $window-E<gt>B<get_title> 

=over

=back

=head2 $window-E<gt>B<set_title> ($title=undef)

=over

=over

=item * $title (string or undef) 

=back

=back

=head2 window or undef = $window-E<gt>B<get_transient_for> 

=over

=back

=head2 $window-E<gt>B<set_transient_for> ($parent)

=over

=over

=item * $parent (Gtk2::Window or undef) 

=back

=back

=head2 windowtypehint = $window-E<gt>B<get_type_hint> 

=over

=back

=head2 $window-E<gt>B<set_type_hint> ($hint)

=over

=over

=item * $hint (Gtk2::Gdk::WindowTypeHint) 

=back

=back

=head2 $window-E<gt>B<unfullscreen> 

=over

=back

=head2 $window-E<gt>B<unmaximize> 

=over

=back

=head2 $window-E<gt>B<unstick> 

=over

=back

=head2 $window-E<gt>B<set_wmclass> ($wmclass_name, $wmclass_class)

=over

=over

=item * $wmclass_name (string) 

=item * $wmclass_class (string) 

=back

=back


=head1 PROPERTIES

=over

=item 'accept-focus' (boolean : readable / writable)

TRUE if the window should receive the input focus.

=item 'allow-grow' (boolean : readable / writable)

If TRUE, users can expand the window beyond its minimum size

=item 'allow-shrink' (boolean : readable / writable)

If TRUE, the window has no mimimum size. Setting this to TRUE is 99% of the time a bad idea

=item 'decorated' (boolean : readable / writable)

Whether the window should be decorated by the window manager

=item 'default-height' (integer : readable / writable)

The default height of the window, used when initially showing the window

=item 'default-width' (integer : readable / writable)

The default width of the window, used when initially showing the window

=item 'destroy-with-parent' (boolean : readable / writable)

If this window should be destroyed when the parent is destroyed

=item 'focus-on-map' (boolean : readable / writable)

TRUE if the window should receive the input focus when mapped.

=item 'gravity' (Gtk2::Gdk::Gravity : readable / writable)

The window gravity of the window

=item 'has-toplevel-focus' (boolean : readable)

Whether the input focus is within this GtkWindow

=item 'icon' (Gtk2::Gdk::Pixbuf : readable / writable)

Icon for this window

=item 'icon-name' (string : readable / writable)

Name of the themed icon for this window

=item 'is-active' (boolean : readable)

Whether the toplevel is the current active window

=item 'modal' (boolean : readable / writable)

If TRUE, the window is modal (other windows are not usable while this one is up)

=item 'resizable' (boolean : readable / writable)

If TRUE, users can resize the window

=item 'role' (string : readable / writable)

Unique identifier for the window to be used when restoring a session

=item 'screen' (Gtk2::Gdk::Screen : readable / writable)

The screen where this window will be displayed

=item 'skip-pager-hint' (boolean : readable / writable)

TRUE if the window should not be in the pager.

=item 'skip-taskbar-hint' (boolean : readable / writable)

TRUE if the window should not be in the task bar.

=item 'title' (string : readable / writable)

The title of the window

=item 'type' (Gtk2::WindowType : readable / writable / construct-only)

The type of the window

=item 'type-hint' (Gtk2::Gdk::WindowTypeHint : readable / writable)

Hint to help the desktop environment understand what kind of window this is and how to treat it.

=item 'window-position' (Gtk2::WindowPosition : readable / writable)

The initial position of the window

=back


=head1 SIGNALS

=over

=item B<set-focus> (Gtk2::Window, Gtk2::Widget)

=item boolean = B<frame-event> (Gtk2::Window, Gtk2::Gdk::Event)

=item B<activate-focus> (Gtk2::Window)

=item B<activate-default> (Gtk2::Window)

=item B<move-focus> (Gtk2::Window, Gtk2::DirectionType)

=item B<keys-changed> (Gtk2::Window)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back


=head2 flags Gtk2::Gdk::ModifierType

=over

=item * 'shift-mask' / 'GDK_SHIFT_MASK'

=item * 'lock-mask' / 'GDK_LOCK_MASK'

=item * 'control-mask' / 'GDK_CONTROL_MASK'

=item * 'mod1-mask' / 'GDK_MOD1_MASK'

=item * 'mod2-mask' / 'GDK_MOD2_MASK'

=item * 'mod3-mask' / 'GDK_MOD3_MASK'

=item * 'mod4-mask' / 'GDK_MOD4_MASK'

=item * 'mod5-mask' / 'GDK_MOD5_MASK'

=item * 'button1-mask' / 'GDK_BUTTON1_MASK'

=item * 'button2-mask' / 'GDK_BUTTON2_MASK'

=item * 'button3-mask' / 'GDK_BUTTON3_MASK'

=item * 'button4-mask' / 'GDK_BUTTON4_MASK'

=item * 'button5-mask' / 'GDK_BUTTON5_MASK'

=item * 'release-mask' / 'GDK_RELEASE_MASK'

=item * 'modifier-mask' / 'GDK_MODIFIER_MASK'

=back


=head2 flags Gtk2::Gdk::WindowHints

=over

=item * 'pos' / 'GDK_HINT_POS'

=item * 'min-size' / 'GDK_HINT_MIN_SIZE'

=item * 'max-size' / 'GDK_HINT_MAX_SIZE'

=item * 'base-size' / 'GDK_HINT_BASE_SIZE'

=item * 'aspect' / 'GDK_HINT_ASPECT'

=item * 'resize-inc' / 'GDK_HINT_RESIZE_INC'

=item * 'win-gravity' / 'GDK_HINT_WIN_GRAVITY'

=item * 'user-pos' / 'GDK_HINT_USER_POS'

=item * 'user-size' / 'GDK_HINT_USER_SIZE'

=back


=head2 enum Gtk2::WindowPosition



=over

=item * 'none' / 'GTK_WIN_POS_NONE'

=item * 'center' / 'GTK_WIN_POS_CENTER'

=item * 'mouse' / 'GTK_WIN_POS_MOUSE'

=item * 'center-always' / 'GTK_WIN_POS_CENTER_ALWAYS'

=item * 'center-on-parent' / 'GTK_WIN_POS_CENTER_ON_PARENT'

=back


=head2 enum Gtk2::WindowType



=over

=item * 'toplevel' / 'GTK_WINDOW_TOPLEVEL'

=item * 'popup' / 'GTK_WINDOW_POPUP'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Bin>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

