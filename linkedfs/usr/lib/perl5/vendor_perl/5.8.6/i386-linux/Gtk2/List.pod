=head1 NAME

Gtk2::List

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::List

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::List-E<gt>B<new> 

=over

=back

=head2 $list-E<gt>B<append_items> (...)

=over

=over

=item * ... (list) of Gtk2::ListItem's to be appended

=back



=back

=head2 integer = $list-E<gt>B<child_position> ($child)

=over

=over

=item * $child (Gtk2::Widget) 

=back

=back

=head2 $list-E<gt>B<clear_items> ($start, $end)

=over

=over

=item * $start (integer) 

=item * $end (integer) 

=back

=back

=head2 $list-E<gt>B<end_drag_selection> 

=over

=back

=head2 $list-E<gt>B<end_selection> 

=over

=back

=head2 $list-E<gt>B<extend_selection> ($scroll_type, $position, $auto_start_selection)

=over

=over

=item * $scroll_type (Gtk2::ScrollType) 

=item * $position (double) 

=item * $auto_start_selection (boolean) 

=back

=back

=head2 $list-E<gt>B<insert_items> ($position, ...)

=over

=over

=item * $position (integer) 

=item * ... (list) of Gtk2::ListItem's to be inserted

=back



=back

=head2 $list-E<gt>B<prepend_items> (..., $list_item)

=over

=over

=item * ... (list) of Gtk2::ListItem's to be prepended

=item * $list_item (Gtk2::ListItem) 

=back



=back

=head2 $list-E<gt>B<remove_items> (..., $list_item)

=over

=over

=item * ... (list) of Gtk2::ListItem's to be removed

=item * $list_item (Gtk2::ListItem) 

=back



=back

=head2 $list-E<gt>B<scroll_horizontal> ($scroll_type, $position)

=over

=over

=item * $scroll_type (Gtk2::ScrollType) 

=item * $position (double) 

=back

=back

=head2 $list-E<gt>B<scroll_vertical> ($scroll_type, $position)

=over

=over

=item * $scroll_type (Gtk2::ScrollType) 

=item * $position (double) 

=back

=back

=head2 $list-E<gt>B<select_all> 

=over

=back

=head2 $list-E<gt>B<select_child> ($child)

=over

=over

=item * $child (Gtk2::Widget) 

=back

=back

=head2 $list-E<gt>B<select_item> ($item)

=over

=over

=item * $item (integer) 

=back

=back

=head2 $list-E<gt>B<set_selection_mode> ($mode)

=over

=over

=item * $mode (Gtk2::SelectionMode) 

=back

=back

=head2 $list-E<gt>B<start_selection> 

=over

=back

=head2 $list-E<gt>B<toggle_add_mode> 

=over

=back

=head2 $list-E<gt>B<toggle_focus_row> 

=over

=back

=head2 $list-E<gt>B<toggle_row> ($item)

=over

=over

=item * $item (Gtk2::Widget) 

=back

=back

=head2 $list-E<gt>B<undo_selection> 

=over

=back

=head2 $list-E<gt>B<unselect_all> 

=over

=back

=head2 $list-E<gt>B<unselect_child> ($child)

=over

=over

=item * $child (Gtk2::Widget) 

=back

=back

=head2 $list-E<gt>B<unselect_item> ($item)

=over

=over

=item * $item (integer) 

=back

=back


=head1 PROPERTIES

=over

=item 'selection-mode' (Gtk2::SelectionMode : readable / writable)

=back


=head1 SIGNALS

=over

=item B<selection-changed> (Gtk2::List)

=item B<select-child> (Gtk2::List, Gtk2::Widget)

=item B<unselect-child> (Gtk2::List, Gtk2::Widget)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

