=head1 NAME

Gtk2::Gdk::Event::Key

=for position post_hierarchy

=head1 HIERARCHY

  Gtk2::Gdk::Event
  +----Gtk2::Gdk::Event::Key

=cut




=head1 METHODS

=head2 unsigned = $eventkey-E<gt>B<group> ($newvalue=0)

=over

=over

=item * $newvalue (unsigned) 

=back

=back

=head2 unsigned = $eventkey-E<gt>B<hardware_keycode> ($newvalue=0)

=over

=over

=item * $newvalue (unsigned) 

=back

=back

=head2 integer = $eventkey-E<gt>B<keyval> ($newvalue=0)

=over

=over

=item * $newvalue (integer) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

