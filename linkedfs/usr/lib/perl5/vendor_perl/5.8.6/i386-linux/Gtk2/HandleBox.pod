=head1 NAME

Gtk2::HandleBox

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Bin
                          +----Gtk2::HandleBox

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::HandleBox-E<gt>B<new> 

=over

=back

=head2 boolean = $handle_box-E<gt>B<get_child_detached> 

=over

=back

=head2 positiontype = $handle_box-E<gt>B<get_handle_position> 

=over

=back

=head2 $handle_box-E<gt>B<set_handle_position> ($position)

=over

=over

=item * $position (Gtk2::PositionType) 

=back

=back

=head2 shadowtype = $handle_box-E<gt>B<get_shadow_type> 

=over

=back

=head2 $handle_box-E<gt>B<set_shadow_type> ($type)

=over

=over

=item * $type (Gtk2::ShadowType) 

=back

=back

=head2 positiontype = $handle_box-E<gt>B<get_snap_edge> 

=over

=back

=head2 $handle_box-E<gt>B<set_snap_edge> ($edge)

=over

=over

=item * $edge (Gtk2::PositionType) 

=back

=back


=head1 PROPERTIES

=over

=item 'handle-position' (Gtk2::PositionType : readable / writable)

Position of the handle relative to the child widget

=item 'shadow' (Gtk2::ShadowType : readable / writable)

Deprecated property, use shadow_type instead

=item 'shadow-type' (Gtk2::ShadowType : readable / writable)

Appearance of the shadow that surrounds the container

=item 'snap-edge' (Gtk2::PositionType : readable / writable)

Side of the handlebox that's lined up with the docking point to dock the handlebox

=item 'snap-edge-set' (boolean : readable / writable)

Whether to use the value from the snap_edge property or a value derived from handle_position

=back


=head1 SIGNALS

=over

=item B<child-attached> (Gtk2::HandleBox, Gtk2::Widget)

=item B<child-detached> (Gtk2::HandleBox, Gtk2::Widget)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back


=head2 enum Gtk2::PositionType



=over

=item * 'left' / 'GTK_POS_LEFT'

=item * 'right' / 'GTK_POS_RIGHT'

=item * 'top' / 'GTK_POS_TOP'

=item * 'bottom' / 'GTK_POS_BOTTOM'

=back


=head2 enum Gtk2::ShadowType



=over

=item * 'none' / 'GTK_SHADOW_NONE'

=item * 'in' / 'GTK_SHADOW_IN'

=item * 'out' / 'GTK_SHADOW_OUT'

=item * 'etched-in' / 'GTK_SHADOW_ETCHED_IN'

=item * 'etched-out' / 'GTK_SHADOW_ETCHED_OUT'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Bin>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

