=head1 NAME

Gtk2::Gdk::Event::Motion

=for position post_hierarchy

=head1 HIERARCHY

  Gtk2::Gdk::Event
  +----Gtk2::Gdk::Event::Motion

=cut




=head1 METHODS

=head2 device or undef = $eventmotion-E<gt>B<device> ($newvalue=undef)

=over

=over

=item * $newvalue (Gtk2::Gdk::Device or undef) 

=back

=back

=head2 integer = $eventmotion-E<gt>B<is_hint> ($newvalue=0)

=over

=over

=item * $newvalue (integer) 

=back

=back

=head2 double = $event-E<gt>B<x> ($newvalue=0.0)

=over

=over

=item * $newvalue (double) 

=back

=back

=head2 double = $event-E<gt>B<y> ($newvalue=0.0)

=over

=over

=item * $newvalue (double) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

