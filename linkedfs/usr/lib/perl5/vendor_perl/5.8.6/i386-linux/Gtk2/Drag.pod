=head1 NAME

Gtk2::Drag


=head1 METHODS

=head2 dragcontext = Gtk2::Drag-E<gt>B<begin> ($widget, $targets, $actions, $button, $event)

=over

=over

=item * $widget (Gtk2::Widget) 

=item * $targets (Gtk2::TargetList) 

=item * $actions (Gtk2::Gdk::DragAction) 

=item * $button (integer) 

=item * $event (Gtk2::Gdk::Event) 

=back

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::DragAction

=over

=item * 'default' / 'GDK_ACTION_DEFAULT'

=item * 'copy' / 'GDK_ACTION_COPY'

=item * 'move' / 'GDK_ACTION_MOVE'

=item * 'link' / 'GDK_ACTION_LINK'

=item * 'private' / 'GDK_ACTION_PRIVATE'

=item * 'ask' / 'GDK_ACTION_ASK'

=back



=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

