=head1 NAME

Gtk2::FileChooserButton

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Box
                          +----Gtk2::HBox
                                +----Gtk2::FileChooserButton

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface
  Gtk2::FileChooser


=head1 METHODS

=head2 widget = Gtk2::FileChooserButton-E<gt>B<new> ($title, $action)

=over

=over

=item * $title (string) 

=item * $action (Gtk2::FileChooserAction) 

=back

=back

=head2 widget = Gtk2::FileChooserButton-E<gt>B<new_with_backend> ($title, $action, $backend)

=over

=over

=item * $title (string) 

=item * $action (Gtk2::FileChooserAction) 

=item * $backend (string) 

=back

=back

=head2 widget = Gtk2::FileChooserButton-E<gt>B<new_with_dialog> ($dialog)

=over

=over

=item * $dialog (Gtk2::Widget) 

=back

=back

=head2 string = $button-E<gt>B<get_title> 

=over

=back

=head2 $button-E<gt>B<set_title> ($title)

=over

=over

=item * $title (string) 

=back

=back

=head2 integer = $button-E<gt>B<get_width_chars> 

=over

=back

=head2 $button-E<gt>B<set_width_chars> ($n_chars)

=over

=over

=item * $n_chars (integer) 

=back

=back


=head1 PROPERTIES

=over

=item 'dialog' (Gtk2::FileChooserDialog : writable / construct-only)

The file chooser dialog to use.

=item 'title' (string : readable / writable)

The title of the file chooser dialog.

=item 'width-chars' (integer : readable / writable)

The desired width of the button widget, in characters.

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Box>, L<Gtk2::HBox>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

