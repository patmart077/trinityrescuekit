=head1 NAME

Gtk2::Gdk::Pixmap

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Gdk::Drawable
        +----Gtk2::Gdk::Pixmap


=head1 METHODS

=head2 pixmap = Gtk2::Gdk::Pixmap-E<gt>B<new> ($drawable, $width, $height, $depth)

=over

=over

=item * $drawable (Gtk2::Gdk::Drawable or undef) 

=item * $width (integer) 

=item * $height (integer) 

=item * $depth (integer) 

=back

=back

=head2 (pixmap, mask) = Gtk2::Gdk::Pixmap->B<colormap_create_from_xpm> ($drawable, $colormap, $transparent_color, $filename)

=over

=over

=item * $drawable (Gtk2::Gdk::Drawable or undef) may be undef if I<$colormap> is given

=item * $colormap (Gtk2::Gdk::Colormap or undef) GdkColormap to use for the new image; may be undef if I<$drawable> is given.

=item * $transparent_color (Gtk2::Gdk::Color or undef) color of pixels that are transparent in the input file.  if undef, a default is used.

=item * $filename (localized file name) 

=back



=back

=head2 ($pixmap, $mask) = Gtk2::Gdk::Pixmap->B<colormap_create_from_xpm_d> ($drawable, $colormap, $transparent_color, @xpm_data)

=over

=over

=item * $drawable (Gtk2::Gdk::Drawable or undef) may be undef if I<$colormap> is given

=item * $colormap (Gtk2::Gdk::Colormap or undef) GdkColormap to use for the new image; may be undef if I<$drawable> is given.

=item * $transparent_color (Gtk2::Gdk::Color or undef) color of pixels that are transparent in the input file.  if undef, a default is used.

=item * ... (list) of strings, xpm data

=back


Create a pixmap from the provided xpm data, using a specific colormap.
See C<create_from_xpm_d>.

=back

=head2 pixmap = Gtk2::Gdk::Pixmap-E<gt>B<create_from_data> ($drawable, $data, $width, $height, $depth, $fg, $bg)

=over

=over

=item * $drawable (Gtk2::Gdk::Drawable) 

=item * $data (string) 

=item * $width (integer) 

=item * $height (integer) 

=item * $depth (integer) 

=item * $fg (Gtk2::Gdk::Color) 

=item * $bg (Gtk2::Gdk::Color) 

=back

=back

=head2 (pixmap, mask) = Gtk2::Gdk::Pixmap->B<create_from_xpm> ($drawable, $transparent_color, $filename)

=over

=over

=item * $drawable (Gtk2::Gdk::Drawable) 

=item * $transparent_color (Gtk2::Gdk::Color or undef) 

=item * $filename (localized file name) 

=back



=back

=head2 ($pixmap, $mask) = Gtk2::Gdk::Pixmap->B<create_from_xpm_d> ($drawable, $transparent_color, @xpm_data)

=over

=over

=item * $drawable (Gtk2::Gdk::Drawable) used to determine the colormap and visual of the image.

=item * $transparent_color (Gtk2::Gdk::Color or undef) color of pixels that are transparent in the input file.  if undef, a default is used.

=item * ... (list) of strings, xpm data

=back


Create a pixmap from the provided xpm data, usually included in the program as
an inline image.  See C<new_from_xpm_data> in L<Gtk2::Gdk::Pixbuf> for a 
description of the format of this data.

=back

=head2 pixmap = Gtk2::Gdk::Pixmap-E<gt>B<foreign_new> ($anid)

=over

=over

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 pixmap = Gtk2::Gdk::Pixmap-E<gt>B<foreign_new_for_display> ($display, $anid)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 pixmap = Gtk2::Gdk::Pixmap-E<gt>B<lookup> ($anid)

=over

=over

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 pixmap = Gtk2::Gdk::Pixmap-E<gt>B<lookup_for_display> ($display, $anid)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Gdk::Drawable>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

