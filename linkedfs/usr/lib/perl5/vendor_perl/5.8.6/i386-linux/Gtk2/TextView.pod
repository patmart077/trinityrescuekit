=head1 NAME

Gtk2::TextView

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::TextView

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::TextView-E<gt>B<new> 

=over

=back

=head2 widget = Gtk2::TextView-E<gt>B<new_with_buffer> ($buffer)

=over

=over

=item * $buffer (Gtk2::TextBuffer) 

=back

=back

=head2 boolean = $text_view-E<gt>B<get_accepts_tab> 

=over

=back

=head2 $text_view-E<gt>B<set_accepts_tab> ($accepts_tab)

=over

=over

=item * $accepts_tab (boolean) 

=back

=back

=head2 $text_view-E<gt>B<add_child_at_anchor> ($child, $anchor)

=over

=over

=item * $child (Gtk2::Widget) 

=item * $anchor (Gtk2::TextChildAnchor) 

=back

=back

=head2 $text_view-E<gt>B<add_child_in_window> ($child, $which_window, $xpos, $ypos)

=over

=over

=item * $child (Gtk2::Widget) 

=item * $which_window (Gtk2::TextWindowType) 

=item * $xpos (integer) 

=item * $ypos (integer) 

=back

=back

=head2 boolean = $text_view-E<gt>B<backward_display_line> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 boolean = $text_view-E<gt>B<backward_display_line_start> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 integer = $text_view-E<gt>B<get_border_window_size> ($type)

=over

=over

=item * $type (Gtk2::TextWindowType) 

=back

=back

=head2 $text_view-E<gt>B<set_border_window_size> ($type, $size)

=over

=over

=item * $type (Gtk2::TextWindowType) 

=item * $size (integer) 

=back

=back

=head2 textbuffer = $text_view-E<gt>B<get_buffer> 

=over

=back

=head2 $text_view-E<gt>B<set_buffer> ($buffer)

=over

=over

=item * $buffer (Gtk2::TextBuffer) 

=back

=back

=head2 (window_x, window_y) = $text_view-E<gt>B<buffer_to_window_coords> ($win, $buffer_x, $buffer_y)

=over

=over

=item * $win (Gtk2::TextWindowType) 

=item * $buffer_x (integer) 

=item * $buffer_y (integer) 

=back

=back

=head2 boolean = $text_view-E<gt>B<get_cursor_visible> 

=over

=back

=head2 $text_view-E<gt>B<set_cursor_visible> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 textattributes = $text_view-E<gt>B<get_default_attributes> 

=over

=back

=head2 boolean = $text_view-E<gt>B<get_editable> 

=over

=back

=head2 $text_view-E<gt>B<set_editable> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 boolean = $text_view-E<gt>B<forward_display_line> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 boolean = $text_view-E<gt>B<forward_display_line_end> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 integer = $text_view-E<gt>B<get_indent> 

=over

=back

=head2 $text_view-E<gt>B<set_indent> ($indent)

=over

=over

=item * $indent (integer) 

=back

=back

=head2 textiter = $text_view-E<gt>B<get_iter_at_location> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 ($iter, $trailing) = $text_view->B<get_iter_at_position> ($x, $y)

=head2 $iter = $text_view->B<get_iter_at_position> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

Retrieves the iterator pointing to the character at buffer coordinates x and y.
Buffer coordinates are coordinates for the entire buffer, not just the
currently-displayed portion.  If you have coordinates from an event, you
have to convert those to buffer coordinates with
C<< $text_view->window_to_buffer_coords() >>.

Note that this is different from C<< $text_view->get_iter_at_location() >>,
which returns cursor locations, i.e. positions between characters.

=back

=head2 rectangle = $text_view-E<gt>B<get_iter_location> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 justification = $text_view-E<gt>B<get_justification> 

=over

=back

=head2 $text_view-E<gt>B<set_justification> ($justification)

=over

=over

=item * $justification (Gtk2::Justification) 

=back

=back

=head2 integer = $text_view-E<gt>B<get_left_margin> 

=over

=back

=head2 $text_view-E<gt>B<set_left_margin> ($left_margin)

=over

=over

=item * $left_margin (integer) 

=back

=back

=head2 (target_iter, line_top) = $text_view->B<get_line_at_y> ($y)

=over

=over

=item * $y (integer) 

=back



=back

=head2 (y, height) = $text_view-E<gt>B<get_line_yrange> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 $text_view-E<gt>B<move_child> ($child, $xpos, $ypos)

=over

=over

=item * $child (Gtk2::Widget) 

=item * $xpos (integer) 

=item * $ypos (integer) 

=back

=back

=head2 boolean = $text_view-E<gt>B<move_mark_onscreen> ($mark)

=over

=over

=item * $mark (Gtk2::TextMark) 

=back

=back

=head2 boolean = $text_view-E<gt>B<move_visually> ($iter, $count)

=over

=over

=item * $iter (Gtk2::TextIter) 

=item * $count (integer) 

=back

=back

=head2 boolean = $text_view-E<gt>B<get_overwrite> 

=over

=back

=head2 $text_view-E<gt>B<set_overwrite> ($overwrite)

=over

=over

=item * $overwrite (boolean) 

=back

=back

=head2 integer = $text_view-E<gt>B<get_pixels_above_lines> 

=over

=back

=head2 $text_view-E<gt>B<set_pixels_above_lines> ($pixels_above_lines)

=over

=over

=item * $pixels_above_lines (integer) 

=back

=back

=head2 integer = $text_view-E<gt>B<get_pixels_below_lines> 

=over

=back

=head2 $text_view-E<gt>B<set_pixels_below_lines> ($pixels_below_lines)

=over

=over

=item * $pixels_below_lines (integer) 

=back

=back

=head2 integer = $text_view-E<gt>B<get_pixels_inside_wrap> 

=over

=back

=head2 $text_view-E<gt>B<set_pixels_inside_wrap> ($pixels_inside_wrap)

=over

=over

=item * $pixels_inside_wrap (integer) 

=back

=back

=head2 boolean = $text_view-E<gt>B<place_cursor_onscreen> 

=over

=back

=head2 integer = $text_view-E<gt>B<get_right_margin> 

=over

=back

=head2 $text_view-E<gt>B<set_right_margin> ($right_margin)

=over

=over

=item * $right_margin (integer) 

=back

=back

=head2 $text_view-E<gt>B<scroll_mark_onscreen> ($mark)

=over

=over

=item * $mark (Gtk2::TextMark) 

=back

=back

=head2 boolean = $text_view-E<gt>B<scroll_to_iter> ($iter, $within_margin, $use_align, $xalign, $yalign)

=over

=over

=item * $iter (Gtk2::TextIter) 

=item * $within_margin (double) 

=item * $use_align (boolean) 

=item * $xalign (double) 

=item * $yalign (double) 

=back

=back

=head2 $text_view-E<gt>B<scroll_to_mark> ($mark, $within_margin, $use_align, $xalign, $yalign)

=over

=over

=item * $mark (Gtk2::TextMark) 

=item * $within_margin (double) 

=item * $use_align (boolean) 

=item * $xalign (double) 

=item * $yalign (double) 

=back

=back

=head2 boolean = $text_view-E<gt>B<starts_display_line> ($iter)

=over

=over

=item * $iter (Gtk2::TextIter) 

=back

=back

=head2 tabarray = $text_view-E<gt>B<get_tabs> 

=over

=back

=head2 $text_view-E<gt>B<set_tabs> ($tabs)

=over

=over

=item * $tabs (Gtk2::Pango::TabArray) 

=back

=back

=head2 rectangle = $text_view-E<gt>B<get_visible_rect> 

=over

=back

=head2 window = $text_view-E<gt>B<get_window> ($win)

=over

=over

=item * $win (Gtk2::TextWindowType) 

=back

=back

=head2 (buffer_x, buffer_y) = $text_view-E<gt>B<window_to_buffer_coords> ($win, $window_x, $window_y)

=over

=over

=item * $win (Gtk2::TextWindowType) 

=item * $window_x (integer) 

=item * $window_y (integer) 

=back

=back

=head2 textwindowtype = $text_view-E<gt>B<get_window_type> ($window)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=back

=back

=head2 wrapmode = $text_view-E<gt>B<get_wrap_mode> 

=over

=back

=head2 $text_view-E<gt>B<set_wrap_mode> ($wrap_mode)

=over

=over

=item * $wrap_mode (Gtk2::WrapMode) 

=back

=back


=head1 PROPERTIES

=over

=item 'accepts-tab' (boolean : readable / writable)

Whether Tab will result in a tab character being entered

=item 'buffer' (Gtk2::TextBuffer : readable / writable)

The buffer which is displayed

=item 'cursor-visible' (boolean : readable / writable)

If the insertion cursor is shown

=item 'editable' (boolean : readable / writable)

Whether the text can be modified by the user

=item 'indent' (integer : readable / writable)

Amount to indent the paragraph, in pixels

=item 'justification' (Gtk2::Justification : readable / writable)

Left, right, or center justification

=item 'left-margin' (integer : readable / writable)

Width of the left margin in pixels

=item 'overwrite' (boolean : readable / writable)

Whether entered text overwrites existing contents

=item 'pixels-above-lines' (integer : readable / writable)

Pixels of blank space above paragraphs

=item 'pixels-below-lines' (integer : readable / writable)

Pixels of blank space below paragraphs

=item 'pixels-inside-wrap' (integer : readable / writable)

Pixels of blank space between wrapped lines in a paragraph

=item 'right-margin' (integer : readable / writable)

Width of the right margin in pixels

=item 'tabs' (Gtk2::Pango::TabArray : readable / writable)

Custom tabs for this text

=item 'wrap-mode' (Gtk2::WrapMode : readable / writable)

Whether to wrap lines never, at word boundaries, or at character boundaries

=back


=head1 SIGNALS

=over

=item B<set-scroll-adjustments> (Gtk2::TextView, Gtk2::Adjustment, Gtk2::Adjustment)

=item B<move-cursor> (Gtk2::TextView, Gtk2::MovementStep, integer, boolean)

=item B<select-all> (Gtk2::TextView, boolean)

=item B<move-focus> (Gtk2::TextView, Gtk2::DirectionType)

=item B<copy-clipboard> (Gtk2::TextView)

=item B<populate-popup> (Gtk2::TextView, Gtk2::Menu)

=item B<insert-at-cursor> (Gtk2::TextView, string)

=item B<delete-from-cursor> (Gtk2::TextView, Gtk2::DeleteType, integer)

=item B<backspace> (Gtk2::TextView)

=item B<cut-clipboard> (Gtk2::TextView)

=item B<paste-clipboard> (Gtk2::TextView)

=item B<toggle-overwrite> (Gtk2::TextView)

=item B<page-horizontally> (Gtk2::TextView, integer, boolean)

=item B<move-viewport> (Gtk2::TextView, GtkScrollStep, integer)

=item B<set-anchor> (Gtk2::TextView)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

