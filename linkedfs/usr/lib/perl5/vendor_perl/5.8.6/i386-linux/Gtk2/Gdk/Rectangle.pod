=head1 NAME

Gtk2::Gdk::Rectangle

=head1 HIERARCHY

  Glib::Boxed
  +----Gtk2::Gdk::Rectangle


=head1 METHODS

=head2 rectangle = Gtk2::Gdk::Rectangle-E<gt>B<new> ($x, $y, $width, $height)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 integer = $rectangle->B<height>

=head2 oldvalue = $rectangle->B<height> ($newvalue)

=over

=over

=item * $newvalue (integer) 

=back



=back

=head2 rectangle = $src1-E<gt>B<intersect> ($src2)

=over

=over

=item * $src2 (Gtk2::Gdk::Rectangle) 

=back

=back

=head2 rectangle = $src1-E<gt>B<union> ($src2)

=over

=over

=item * $src2 (Gtk2::Gdk::Rectangle) 

=back

=back

=head2 (x, y, width, height) = $rectangle->B<values>

=over



=back

=head2 integer = $rectangle->B<width>

=head2 oldvalue = $rectangle->B<width> ($newvalue)

=over

=over

=item * $newvalue (integer) 

=back



=back

=head2 integer = $rectangle->B<x>

=head2 oldvalue = $rectangle->B<x> ($newvalue)

=over

=over

=item * $newvalue (integer) 

=back



=back

=head2 integer = $rectangle->B<y>

=head2 oldvalue = $rectangle->B<y> ($newvalue)

=over

=over

=item * $newvalue (integer) 

=back



=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Boxed>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

