=head1 NAME

Gtk2::FontSelection

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Box
                          +----Gtk2::VBox
                                +----Gtk2::FontSelection

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::FontSelection-E<gt>B<new> 

=over

=back

=head2 font = $fontsel-E<gt>B<get_font> 

=over

=back

=head2 string = $fontsel-E<gt>B<get_font_name> 

=over

=back

=head2 boolean = $fontsel-E<gt>B<set_font_name> ($fontname)

=over

=over

=item * $fontname (string) 

=back

=back

=head2 string = $fontsel-E<gt>B<get_preview_text> 

=over

=back

=head2 $fontsel-E<gt>B<set_preview_text> ($text)

=over

=over

=item * $text (string) 

=back

=back


=head1 PROPERTIES

=over

=item 'font' (Gtk2::Gdk::Font : readable)

The GdkFont that is currently selected

=item 'font-name' (string : readable / writable)

The X string that represents this font

=item 'preview-text' (string : readable / writable)

The text to display in order to demonstrate the selected font

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Box>, L<Gtk2::VBox>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

