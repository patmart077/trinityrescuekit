=head1 NAME

Gtk2::ComboBox

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Bin
                          +----Gtk2::ComboBox

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface
  Gtk2::CellEditable
  Gtk2::CellLayout


=head1 METHODS

=head2 widget = Gtk2::ComboBox-E<gt>B<new> ($model=undef)

=over

=over

=item * $model (Gtk2::TreeModel) 

=back

=back

=head2 widget = Gtk2::ComboBox-E<gt>B<new_text> 

=over

=back

=head2 widget = Gtk2::ComboBox-E<gt>B<new_with_model> ($model=undef)

=over

=over

=item * $model (Gtk2::TreeModel) 

=back

=back

=head2 integer = $combo_box-E<gt>B<get_active> 

=over

=back

=head2 treeiter = $combo_box-E<gt>B<get_active_iter> 

=over

=back

=head2 $combo_box-E<gt>B<set_active_iter> ($iter)

=over

=over

=item * $iter (Gtk2::TreeIter) 

=back

=back

=head2 $combo_box-E<gt>B<set_active> ($index)

=over

=over

=item * $index (integer) 

=back

=back

=head2 string = $combo_box-E<gt>B<get_active_text> 

=over

=back

=head2 boolean = $combo_box-E<gt>B<get_add_tearoffs> 

=over

=back

=head2 $combo_box-E<gt>B<set_add_tearoffs> ($add_tearoffs)

=over

=over

=item * $add_tearoffs (boolean) 

=back

=back

=head2 $combo_box-E<gt>B<append_text> ($text)

=over

=over

=item * $text (string) 

=back

=back

=head2 integer = $combo_box-E<gt>B<get_column_span_column> 

=over

=back

=head2 $combo_box-E<gt>B<set_column_span_column> ($column_span)

=over

=over

=item * $column_span (integer) 

=back

=back

=head2 boolean = $combo_box-E<gt>B<get_focus_on_click> 

=over

=back

=head2 $combo_box-E<gt>B<set_focus_on_click> ($focus_on_click)

=over

=over

=item * $focus_on_click (boolean) 

=back

=back

=head2 $combo_box-E<gt>B<insert_text> ($position, $text)

=over

=over

=item * $position (integer) 

=item * $text (string) 

=back

=back

=head2 treemodel = $combo_box-E<gt>B<get_model> 

=over

=back

=head2 $combo_box-E<gt>B<set_model> ($model)

=over

=over

=item * $model (Gtk2::TreeModel) 

=back

=back

=head2 $combo_box-E<gt>B<popdown> 

=over

=back

=head2 $combo_box-E<gt>B<popup> 

=over

=back

=head2 $combo_box-E<gt>B<prepend_text> ($text)

=over

=over

=item * $text (string) 

=back

=back

=head2 $combo_box-E<gt>B<remove_text> ($position)

=over

=over

=item * $position (integer) 

=back

=back

=head2 $combo_box-E<gt>B<set_row_separator_func> ($func, $data=undef)

=over

=over

=item * $func (scalar) 

=item * $data (scalar) 

=back

=back

=head2 integer = $combo_box-E<gt>B<get_row_span_column> 

=over

=back

=head2 $combo_box-E<gt>B<set_row_span_column> ($row_span)

=over

=over

=item * $row_span (integer) 

=back

=back

=head2 integer = $combo_box-E<gt>B<get_wrap_width> 

=over

=back

=head2 $combo_box-E<gt>B<set_wrap_width> ($width)

=over

=over

=item * $width (integer) 

=back

=back


=head1 PROPERTIES

=over

=item 'active' (integer : readable / writable)

The item which is currently active

=item 'add-tearoffs' (boolean : readable / writable)

Whether dropdowns should have a tearoff menu item

=item 'column-span-column' (integer : readable / writable)

TreeModel column containing the column span values

=item 'focus-on-click' (boolean : readable / writable)

Whether the combo box grabs focus when it is clicked with the mouse

=item 'has-frame' (boolean : readable / writable)

Whether the combo box draws a frame around the child

=item 'model' (Gtk2::TreeModel : readable / writable)

The model for the combo box

=item 'row-span-column' (integer : readable / writable)

TreeModel column containing the row span values

=item 'wrap-width' (integer : readable / writable)

Wrap width for layouting the items in a grid

=back


=head1 SIGNALS

=over

=item B<changed> (Gtk2::ComboBox)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Bin>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

