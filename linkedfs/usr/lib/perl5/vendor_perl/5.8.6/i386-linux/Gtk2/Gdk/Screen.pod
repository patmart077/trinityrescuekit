=head1 NAME

Gtk2::Gdk::Screen

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Gdk::Screen


=head1 METHODS

=head2 $screen-E<gt>B<broadcast_client_message> ($event)

=over

=over

=item * $event (Gtk2::Gdk::Event) 

=back

=back

=head2 colormap = $screen-E<gt>B<get_default_colormap> 

=over

=back

=head2 $screen-E<gt>B<set_default_colormap> ($colormap)

=over

=over

=item * $colormap (Gtk2::Gdk::Colormap) 

=back

=back

=head2 screen or undef = Gtk2::Gdk::Screen-E<gt>B<get_default> 

=over

=back

=head2 display = $screen-E<gt>B<get_display> 

=over

=back

=head2 integer = $screen-E<gt>B<get_height> 

=over

=back

=head2 integer = $screen-E<gt>B<get_height_mm> 

=over

=back

=head2 list = $screen-E<gt>B<list_visuals> 

=over

Returns a list of Gtk2::Gdk::Visual's.

=back

=head2 string = $screen-E<gt>B<make_display_name> 

=over

=back

=head2 integer = $screen-E<gt>B<get_monitor_at_point> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 integer = $screen-E<gt>B<get_monitor_at_window> ($window)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=back

=back

=head2 rectangle = $screen-E<gt>B<get_monitor_geometry> ($monitor_num)

=over

=over

=item * $monitor_num (integer) 

=back

=back

=head2 integer = $screen-E<gt>B<get_n_monitors> 

=over

=back

=head2 integer = $screen-E<gt>B<get_number> 

=over

=back

=head2 colormap = $screen-E<gt>B<get_rgb_colormap> 

=over

=back

=head2 visual = $screen-E<gt>B<get_rgb_visual> 

=over

=back

=head2 window = $screen-E<gt>B<get_root_window> 

=over

=back

=head2 scalar = $screen-E<gt>B<get_setting> ($name)

=over

=over

=item * $name (string) 

=back

=back

=head2 colormap = $screen-E<gt>B<get_system_colormap> 

=over

=back

=head2 visual = $screen-E<gt>B<get_system_visual> 

=over

=back

=head2 list = $screen-E<gt>B<get_toplevel_windows> 

=over

Returns a list of Gtk2::Gdk::Window's.

=back

=head2 integer = $screen-E<gt>B<get_width> 

=over

=back

=head2 integer = $screen-E<gt>B<get_width_mm> 

=over

=back


=head1 SIGNALS

=over

=item B<size-changed> (Gtk2::Gdk::Screen)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

