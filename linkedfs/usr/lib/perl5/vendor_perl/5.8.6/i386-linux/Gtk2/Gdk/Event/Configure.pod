=head1 NAME

Gtk2::Gdk::Event::Configure

=for position post_hierarchy

=head1 HIERARCHY

  Gtk2::Gdk::Event
  +----Gtk2::Gdk::Event::Configure

=cut




=head1 METHODS

=head2 integer = $eventconfigure-E<gt>B<height> ($newvalue)

=over

=over

=item * $newvalue (integer) 

=back

=back

=head2 integer = $eventconfigure-E<gt>B<width> ($newvalue=0)

=over

=over

=item * $newvalue (integer) 

=back

=back

=head2 integer = $event-E<gt>B<x> ($newvalue=0)

=over

=over

=item * $newvalue (integer) 

=back

=back

=head2 integer = $event-E<gt>B<y> ($newvalue=0)

=over

=over

=item * $newvalue (integer) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

