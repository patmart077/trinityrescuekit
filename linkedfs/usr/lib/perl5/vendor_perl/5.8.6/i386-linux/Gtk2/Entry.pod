=head1 NAME

Gtk2::Entry

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Entry

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface
  Gtk2::CellEditable
  Gtk2::Editable


=head1 METHODS

=head2 widget = Gtk2::Entry-E<gt>B<new> 

=over

=back

=head2 widget = Gtk2::Entry-E<gt>B<new_with_max_length> ($max)

=over

=over

=item * $max (integer) 

=back

=back

=head2 boolean = $entry-E<gt>B<get_activates_default> 

=over

=back

=head2 $entry-E<gt>B<set_activates_default> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 double = $entry-E<gt>B<get_alignment> 

=over

=back

=head2 $entry-E<gt>B<set_alignment> ($xalign)

=over

=over

=item * $xalign (double) 

=back

=back

=head2 $entry-E<gt>B<append_text> ($text)

=over

=over

=item * $text (string) 

=back

=back

=head2 entrycompletion = $entry-E<gt>B<get_completion> 

=over

=back

=head2 $entry-E<gt>B<set_completion> ($completion)

=over

=over

=item * $completion (Gtk2::EntryCompletion) 

=back

=back

=head2 $entry-E<gt>B<set_editable> ($editable)

=over

=over

=item * $editable (boolean) 

=back

=back

=head2 boolean = $entry-E<gt>B<get_has_frame> 

=over

=back

=head2 $entry-E<gt>B<set_has_frame> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 character = $entry-E<gt>B<get_invisible_char> 

=over

=back

=head2 $entry-E<gt>B<set_invisible_char> ($ch)

=over

=over

=item * $ch (character) 

=back

=back

=head2 layout = $entry-E<gt>B<get_layout> 

=over

=back

=head2 integer = $entry-E<gt>B<layout_index_to_text_index> ($layout_index)

=over

=over

=item * $layout_index (integer) 

=back

=back

=head2 (x, y) = $entry-E<gt>B<get_layout_offsets> 

=over

=back

=head2 integer = $entry-E<gt>B<get_max_length> 

=over

=back

=head2 $entry-E<gt>B<set_max_length> ($max)

=over

=over

=item * $max (integer) 

=back

=back

=head2 $entry-E<gt>B<set_position> ($position)

=over

=over

=item * $position (integer) 

=back

=back

=head2 $entry-E<gt>B<prepend_text> ($text)

=over

=over

=item * $text (string) 

=back

=back

=head2 $entry-E<gt>B<select_region> ($start, $end)

=over

=over

=item * $start (integer) 

=item * $end (integer) 

=back

=back

=head2 string = $entry-E<gt>B<get_text> 

=over

=back

=head2 integer = $entry-E<gt>B<text_index_to_layout_index> ($text_index)

=over

=over

=item * $text_index (integer) 

=back

=back

=head2 $entry-E<gt>B<set_text> ($text)

=over

=over

=item * $text (string) 

=back

=back

=head2 boolean = $entry-E<gt>B<get_visibility> 

=over

=back

=head2 $entry-E<gt>B<set_visibility> ($visible)

=over

=over

=item * $visible (boolean) 

=back

=back

=head2 integer = $entry-E<gt>B<get_width_chars> 

=over

=back

=head2 $entry-E<gt>B<set_width_chars> ($n_chars)

=over

=over

=item * $n_chars (integer) 

=back

=back


=head1 PROPERTIES

=over

=item 'activates-default' (boolean : readable / writable)

Whether to activate the default widget (such as the default button in a dialog) when Enter is pressed

=item 'cursor-position' (integer : readable)

The current position of the insertion cursor in chars

=item 'editable' (boolean : readable / writable)

Whether the entry contents can be edited

=item 'has-frame' (boolean : readable / writable)

FALSE removes outside bevel from entry

=item 'invisible-char' (Glib::UInt : readable / writable)

The character to use when masking entry contents (in "password mode")

=item 'max-length' (integer : readable / writable)

Maximum number of characters for this entry. Zero if no maximum

=item 'scroll-offset' (integer : readable)

Number of pixels of the entry scrolled off the screen to the left

=item 'selection-bound' (integer : readable)

The position of the opposite end of the selection from the cursor in chars

=item 'text' (string : readable / writable)

The contents of the entry

=item 'visibility' (boolean : readable / writable)

FALSE displays the "invisible char" instead of the actual text (password mode)

=item 'width-chars' (integer : readable / writable)

Number of characters to leave space for in the entry

=item 'xalign' (Glib::Float : readable / writable)

The horizontal alignment, from 0 (left) to 1 (right). Reversed for RTL layouts.

=back


=head1 SIGNALS

=over

=item B<move-cursor> (Gtk2::Entry, Gtk2::MovementStep, integer, boolean)

=item B<activate> (Gtk2::Entry)

=item B<copy-clipboard> (Gtk2::Entry)

=item B<populate-popup> (Gtk2::Entry, Gtk2::Menu)

=item B<insert-at-cursor> (Gtk2::Entry, string)

=item B<delete-from-cursor> (Gtk2::Entry, Gtk2::DeleteType, integer)

=item B<backspace> (Gtk2::Entry)

=item B<cut-clipboard> (Gtk2::Entry)

=item B<paste-clipboard> (Gtk2::Entry)

=item B<toggle-overwrite> (Gtk2::Entry)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

