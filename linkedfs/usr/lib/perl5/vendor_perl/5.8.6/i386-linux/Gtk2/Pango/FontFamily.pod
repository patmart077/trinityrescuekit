=head1 NAME

Gtk2::Pango::FontFamily

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Pango::FontFamily


=head1 METHODS

=head2 boolean = $family-E<gt>B<is_monospace> 

=over

=back

=head2 list = $family-E<gt>B<list_faces> 

=over

=for apidoc @faces = $family->list_faces
Lists the different font faces that make up family. The faces in a family
share a common design, but differ in slant, weight, width and other aspects.

=back

=head2 string = $family-E<gt>B<get_name> 

=over

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

