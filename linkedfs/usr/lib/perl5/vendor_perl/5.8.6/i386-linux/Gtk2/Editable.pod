=head1 NAME

Gtk2::Editable


=head1 METHODS

=head2 string = $editable-E<gt>B<get_chars> ($start_pos, $end_pos)

=over

=over

=item * $start_pos (integer) 

=item * $end_pos (integer) 

=back

=back

=head2 $editable-E<gt>B<copy_clipboard> 

=over

=back

=head2 $editable-E<gt>B<cut_clipboard> 

=over

=back

=head2 $editable-E<gt>B<delete_selection> 

=over

=back

=head2 $editable-E<gt>B<delete_text> ($start_pos, $end_pos)

=over

=over

=item * $start_pos (integer) 

=item * $end_pos (integer) 

=back

=back

=head2 boolean = $editable-E<gt>B<get_editable> 

=over

=back

=head2 $editable-E<gt>B<set_editable> ($is_editable)

=over

=over

=item * $is_editable (boolean) 

=back

=back

=head2 new_position = $editable->B<insert_text> (new_text, position)

=over

=over

=item * $new_text (string) 

=item * ... (list) 

=back



=back

=head2 $editable-E<gt>B<paste_clipboard> 

=over

=back

=head2 integer = $editable-E<gt>B<get_position> 

=over

=back

=head2 $editable-E<gt>B<set_position> ($position)

=over

=over

=item * $position (integer) 

=back

=back

=head2 $editable-E<gt>B<select_region> ($start, $end)

=over

=over

=item * $start (integer) 

=item * $end (integer) 

=back

=back

=head2 (start, end) = $editable->B<get_selection_bounds>

=over

Returns integers, start and end.

=back


=head1 SIGNALS

=over

=item B<changed> (Gtk2::Editable)

=item B<insert-text> (Gtk2::Editable, string, integer, gpointer)

=item B<delete-text> (Gtk2::Editable, integer, integer)

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

