=head1 NAME

Gtk2::Rc


=head1 METHODS

=head2 Gtk2::Rc-E<gt>B<add_default_file> ($filename)

=over

=over

=item * $filename (localized file name) 

=back

=back

=head2 list = Gtk2::Rc-E<gt>B<get_default_files> 

=over

Returns the list of files that GTK+ will read at the end of Gtk2->init.

=back

=head2 Gtk2::Rc->B<set_default_files> (file1, ...)

=over

=over

=item * ... (list) of strings, the rc files to be parsed

=item * $file1 (localized file name) 

=back

Sets the list of files that GTK+ will read at the end of Gtk2->init.

=back

=head2 string = Gtk2::Rc-E<gt>B<get_im_module_file> 

=over

=back

=head2 string = Gtk2::Rc-E<gt>B<get_im_module_path> 

=over

=back

=head2 string = Gtk2::Rc-E<gt>B<get_module_dir> 

=over

=back

=head2 Gtk2::Rc-E<gt>B<parse> ($filename)

=over

=over

=item * $filename (localized file name) 

=back

=back

=head2 Gtk2::Rc-E<gt>B<parse_string> ($rc_string)

=over

=over

=item * $rc_string (string) 

=back

=back

=head2 boolean = Gtk2::Rc-E<gt>B<reparse_all> 

=over

=back

=head2 boolean = Gtk2::Rc-E<gt>B<reparse_all_for_settings> ($settings, $force_load)

=over

=over

=item * $settings (Gtk2::Settings) 

=item * $force_load (boolean) 

=back

=back

=head2 Gtk2::Rc-E<gt>B<reset_styles> ($settings)

=over

=over

=item * $settings (Gtk2::Settings) 

=back

=back

=head2 style = Gtk2::Rc-E<gt>B<get_style_by_paths> ($settings, $widget_path, $class_path, $package)

=over

=over

=item * $settings (Gtk2::Settings) 

=item * $widget_path (string) 

=item * $class_path (string) 

=item * $package (string) 

=back

=back

=head2 style = Gtk2::Rc-E<gt>B<get_style> ($widget)

=over

=over

=item * $widget (Gtk2::Widget) 

=back

=back

=head2 string = Gtk2::Rc-E<gt>B<get_theme_dir> 

=over

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

