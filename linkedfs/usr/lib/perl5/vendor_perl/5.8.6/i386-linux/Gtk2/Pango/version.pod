=head1 NAME

Gtk2::Pango::version

=for object Gtk2::Pango::version
=cut




=head1 METHODS

=head2 boolean = Gtk2::Pango-E<gt>B<CHECK_VERSION> ($major, $minor, $micro)

=over

=over

=item * $major (integer) 

=item * $minor (integer) 

=item * $micro (integer) 

=back

=back

=head2 (MAJOR, MINOR, MICRO) = Gtk2::Pango->B<GET_VERSION_INFO>

=over

Fetch as a list the version of pango with which Gtk2 was built.

=back


=head1 SEE ALSO

L<Gtk2>, L<Gtk2::version>, L<Glib::version>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

