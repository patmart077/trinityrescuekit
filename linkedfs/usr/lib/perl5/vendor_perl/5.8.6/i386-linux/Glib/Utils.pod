=head1 NAME

Glib::Utils -  Miscellaneous utility functions

=for position SYNOPSIS

=head1 SYNOPSIS

  use Glib;
  Glib::set_application_name (Glib::get_real_name."'s Cool Program");

  print "app name is ".Glib::get_application_name()."\n";

=for position DESCRIPTION

=head1 DESCRIPTION

Here are some useful miscellaneous utilities.
GLib is a portability library, providing portable utility functions for
C programs.  As such, most of these functions seem to violate the Glib
binding principle of not duplicating functionality that Perl already
provides, but there's a distinction for each one, i swear.  The functions
for dealing with user information are provided on all GLib-supported
platforms, not just where POSIX (which provides similar information) is
available, and even work on platforms where %ENV may not include the
expected information.  Also, the "application name" referred to by
(set|get)_application_name is a human readable name, distinct from the
actual program name provided by Perl's own $0.

=cut



=for object Glib::Utils Miscellaneous utility functions
=cut




=head1 METHODS

=head2 string = Glib::get_application_name 

=over

Get the human-readable application name set by C<set_application_name>.

=back

=head2 Glib::set_application_name ($application_name)

=over

=over

=item * $application_name (string) 

=back

Set the human-readable application name.

=back

=head2 string = Glib::get_home_dir 

=over

Find the current user's home directory, by system-dependent/appropriate
means.

=back

=head2 list = Glib::get_language_names 

=over

Computes a list of applicable locale names, which can be used to e.g. construct
locale-dependent filenames or search paths. The returned list is sorted from
most desirable to least desirable and always contains the default locale "C".

=back

=head2 string = Glib::get_real_name 

=over

Get the current user's real name.

=back

=head2 list = Glib::get_system_config_dirs 

=over

Returns an ordered list of base directories in which to access system-wide
configuration information.

=back

=head2 list = Glib::get_system_data_dirs 

=over

Returns an ordered list of base directories in which to access system-wide
application data.

=back

=head2 string = Glib::get_tmp_dir 

=over

Get the temp dir as appropriate for the current system.  See the GLib docs
for info on how it works.

=back

=head2 string = Glib::get_user_cache_dir 

=over

Gets the base directory in which to store non-essential, cached data specific
to particular user.

=back

=head2 string = Glib::get_user_config_dir 

=over

Gets the base directory in which to store user-specific application
configuration information such as user preferences and settings.

=back

=head2 string = Glib::get_user_data_dir 

=over

Get the base directory for application data such as icons that is customized
for a particular user.

=back

=head2 string = Glib::get_user_name 

=over

Get the current user's name by whatever system-dependent means necessary.

=back


=head1 SEE ALSO

L<Glib>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Glib> for a full notice.


=cut

