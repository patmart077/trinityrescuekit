package Net::DBus::RemoteObject;

use 5.006;
use strict;
use warnings;
use Carp;

our $VERSION = '0.0.1';
our $AUTOLOAD;

use Net::DBus::Binding::Message::MethodCall;

sub new {
    my $class = shift;
    my $self = {};

    $self->{service} = shift;
    $self->{object_path}  = shift;
    $self->{interface}    = shift;

    bless $self, $class;

    return $self;
}

sub connect_to_signal {
    my $self = shift;
    my $signal_name = shift;
    my $code = shift;
    my $lazy_binding = shift;
    
    $self->{service}->
	get_bus()->
	add_signal_receiver($code,
			    $signal_name,
			    $self->{interface},
			    $lazy_binding ? undef : $self->{service}->get_service_name(),
			    $self->{object_path});
}

sub DESTROY {
    # No op merely to stop AutoLoader trying to
    # call DESTROY on remote object
}

sub AUTOLOAD {
    my $self = shift;
    my $sub = $AUTOLOAD;
    
    (my $method = $AUTOLOAD) =~ s/.*:://;
    my $call = Net::DBus::Binding::Message::MethodCall->
	new(service_name => $self->{service}->get_service_name(),
	    object_path => $self->{object_path},
	    method_name => $method,
	    interface => $self->{interface});

    my $iter = $call->iterator;
    foreach my $arg (@_) {
	$iter->append($arg);
    }
    
    my $reply = $self->{service}->
	get_bus()->
	get_connection()->
	send_with_reply_and_block($call, 5000);
    
    my @reply = $reply->get_args_list;
    return wantarray ? @reply : $reply[0];
}


1;

