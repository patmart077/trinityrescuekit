package Net::DBus::Service;


sub new {
    my $class = shift;
    my $self = {};

    $self->{service_name} = shift;
    $self->{bus} = shift;
    
    bless $self, $class;

    $self->{bus}->get_connection()->acquire_service($self->{service_name});
    
    return $self;
}

sub get_bus {
    my $self = shift;
    return $self->{bus};
}

sub service_name {
    my $self = shift;
    return $self->{service_name};
}

1;

