
# Author : damien KROTKINE (damien@tuxfamily.org)
#
#
# Copyright (C) 2002 damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : shell
#
# This templates is used to parse shell style configuration files with values.
# There is no section handling.
#

# exportedshell package.
# this is derivated from the shell template
# it understands
# setenv key value

package Libconf;

$templates{exportedcshell} = {
                              #BUG we handle ``setenv foo bar'' but not ``setenv foo''
			  rules => [ q( if ($in =~ s/^\s*setenv\s+(\w+)\s*("([^"]*)"|'([^']*)'|[^'"\s]*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = defined $3 ? $3 : $2;
                                           $matched = 1;
                                        }
                                      ),
                                   ],
			  comments => [ ['#'] ],
		          output => {
				     KEY_VALUE => q(
                                            my ($key, $value) = ($atom->{key}, $atom->{value});
                                            $value =~ /"/ and $output_text = qq(setenv $key '$value'), return;
                                            $value =~ /'/ and $output_text = qq(setenv $key "$value"), return;
                                            $value =~ / / and $output_text = qq(setenv $key "$value"), return;
                                            $value ne '' and $output_text = qq(setenv $key $value), return;
					    $out->matchInStructComments("setenv $key") or $output_text = qq(# setenv $key);
                                     ),
				    },
			  edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	   $_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	   $i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args);
                                           return $index;
                                         ),
			  find_atom_pos => q(
                                             my $i = 0;
                                             my @res;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 my $flag = 1;
                                                 foreach (keys(%args)) {
                                                     $atom->{$_} eq $args{$_} or $flag = 0;
                                                 }
                                                 $flag and push(@res, $i);
                                                 $i++;
                                             }
#@res = map_index { my $a = $_; map { if_($a->{$_} eq $args{$_}, $::i) } keys %args } @{$out->{atoms}}
#@res = map_index { my $a = $_; if_(fold_left({ $::a && $args{$::b} eq $a->{$::b} }, 1, (keys(%args))), $::i) } @{$out->{atoms}};
                                             wantarray ? @res : $res[0];
                                           ),
                    };

1
