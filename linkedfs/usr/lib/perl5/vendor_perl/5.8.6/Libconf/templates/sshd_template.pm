
# Author : Charles LONGEAU (chl@tuxfamily.org)
#
#
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : sshd
#
# This templates is used to parse ssh deamon style configuration files.
# There is no section handling.
#

package Libconf;

$templates{sshd} = {
			   rules => [ 
				     q(
                                       if ($in =~ s/^\s*(\S+)\s*(.*)?\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $matched = 1;
                                       }
                                     ),
				    ],
			   comments => [ ['#'] ],
		           output => {
				      KEY_VALUE => q(
                                            my ($key, $value) = ($$atom{key}, $$atom{value});
                                            $output_text = qq($key $value\n);
                                      ),
				     },
			   edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	$_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	$i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args)
                                         ),
			  };

1
