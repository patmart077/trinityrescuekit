
# Author : Charles LONGEAU (chl@tuxfamily.org)
#
#
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : passwd
#
# This templates is used to parse passwd style configuration files.
# There is no section handling.
#

package Libconf;

$templates{group} = {
			   rules => [ 
				     q(
                                       if (my @infos = $in =~ /^\s*(\S+?):(\S*?):(\S*?):(.*?)\s*$/) {
                                           $in = '';
                                           $atom->{type} = 'VALUES';
                                           my $i = 0;
                                           $atom->{values}{$_} = $infos[$i++] foreach qw(group_name passwd GID user_list);
                                           $matched = 1;
                                       }
                                     ),
				    ],
			   comments => [ ['#'] ],
		           output => {
				      VALUES => q(
                                            $output_text = join(':', map { $atom->{values}{$_} } qw(group_name passwd GID user_list) );
                                      ),
				     },
			   edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	$_->{values}{group_name} eq $args{values}{group_name} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	$i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args)
                                         ),
			  };

1
