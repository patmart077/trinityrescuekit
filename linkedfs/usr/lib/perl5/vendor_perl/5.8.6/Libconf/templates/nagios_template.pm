# Author : Arnaud Desmons (adesmons@mandrakesoft.com)
#
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#                    Arnaud Desmons (adesmons@mandrakesoft.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : nagios
#
# This templates is used to parse nagios style configuration files.
#

package Libconf;

$templates{nagios} = {
			   rules => [
 				     q(
                                       if ($in =~ s/^\s*}\s*$//) {
                                           $atom->{type} = 'ENDSECTION';
                                           $matched = 1;
                                       }
                                      ),
				     q(
                                       if ($in =~ s/^\s*define\s+([\w]+)(.*)$/$2/) {
                                           $atom->{type} = 'SECTION';
                                           $atom->{section_name} = $1;
                                           $atom->{sections} = [ ];
                                           $out->{current_sections} = [ { name => $1 } ];
                                           $keep_atom = 1;
                                           $out->{line_continued} = 1;
                                       }
                                     ),
 				     q(
                                        if ($out->{line_continued} && $in =~ s/^\s*\{\s*$//) {
                                           $out->{line_continued} = 0;
                                           $matched = 1;
                                           $keep_atom = 0;
                                        }
                                      ),
				     q(
                                       if ($in =~ s/^\s*(\S+)\s+(.*)\s*$//) {
                                           $atom->{type} = 'KEY_LIST';
                                           $atom->{key} = $1;
                                           $atom->{list} = [split(/\s*,\s*/, $2)];
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     )
				    ],
			   comments => [ ['#'] ],
		           output => {
				      KEY_LIST => q(
                                          my ($key, $value) = ($atom->{key}, join(', ', @{$atom->{list}}));
                                          $output_indentation = $out->{current_indentation} ||= 1;
                                          $output_text = qq($key $value);
                                      ),
				      SECTION => q(
                                          $output_indentation = 0;
                                          $output_text = qq(define $atom->{section_name} \{);
                                          $out->{current_indentation} = 1;
                                      ),
				      ENDSECTION => q(
                                          $output_indentation = 0;
                                          $output_text = qq(\});
                                          $out->{current_indentation} = 1;
                                      ),
				     },
			   edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	$_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	$i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args)
                                         ),
			  find_atom_pos => q(
                                             $first_atom ||= 0;
                                             $last_atom ||= @{$out->{atoms}}-1;
                                             my @res;
                                             foreach my $pos ($first_atom..$last_atom) {
                                                 my $atom = $out->{atoms}->[$pos];
                                                 my $flag = 1;
                                                 foreach (keys(%args)) {
                                                     if ($_ eq 'sections') {
                                                         $atom->{sections}[0]{name} eq $args{sections}->[0]{name} or $flag = 0;
                                                     } elsif ($_ eq 'list') {
                                                         foreach my $i (0..@{$args{$_}}-1) {
                                                             $atom->{$_}->[$i] eq $args{$_}->[$i] or $flag = 0;
                                                         }
                                                     } else {
                                                         $atom->{$_} eq $args{$_} or $flag = 0;
                                                     }
                                                 }
                                                 $flag and push(@res, $pos);
                                             }
                                             wantarray ? @res : $res[-1];
                                           ),
			  };

1
