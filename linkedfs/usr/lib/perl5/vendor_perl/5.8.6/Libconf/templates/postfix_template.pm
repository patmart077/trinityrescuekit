
# Author : damien KROTKINE (damien@tuxfamily.org)
#
#
# Copyright (C) 2002 damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.




# Libconf Template : postfix
#
# This templates is used to postfix like configuration files.
# There is no section handling.


# the keyvalue template goal is to parse config files type whith atoms of the form :
# key = value
# but not being restrictive like the shell template. That mean that
# - all this lines are valid :
#   key=value
#   key = value
#   key= value
# - there is no quoting meaning, so this lines are valid :
#   variable = some words with blanks, "quotes", or 'any character actually...
# - the variable may not contain any \W character (that means it must be a word, with no blanks).
# - a line can be continued, if the next line begins with a \s (blank, tab...), like this :
# debugger_command =
#	 PATH=/usr/bin:/usr/X11R6/bin
#	 xxgdb $daemon_directory/$process_name $process_id & sleep 5
#

package Libconf;

$templates{postfix} = {
			  rules => [ q( if ($in =~ s/^(\S+)\s*=\s*(.*?)\s*$//) {
                                            $atom->{type} = 'KEY_VALUE';
                                            $atom->{key} = $1;
                                            $atom->{value} = $2;
                                            $matched = 1;
                                        }
                                      ),
                                     q( if ($in =~ /^(\s+\w.*?)\s*$/) {
                                            $switch_to_prev_atom = 1;
                                        }
                                      ),
                                     q( if ($in =~ s/^\s+(\w.*?)\s*$//) {
                                                $atom->{value} .= "\n\t$1";
                                                $matched = 1;
                                        }
                                      ),
                                   ],
			  comments => [ ['#'] ],
		          output => {
				     KEY_VALUE => q(
                                            my ($key, $value) = ($atom->{key}, $atom->{value});
                                            $output_text = qq($key = $value);
                                     ),
				    },
			  edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	   $_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	   $i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args);
                                           return $index;
                                         ),
			  find_atom_pos => q(
                                             my $i = 0;
                                             my @res;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 my $flag = 1;
                                                 foreach (keys(%args)) {
                                                     $atom->{$_} eq $args{$_} or $flag = 0;
                                                 }
                                                 $flag and push(@res, $i);
                                                 $i++;
                                             }
                                             wantarray ? @res : $res[0];
                                           ),
                    };

1
