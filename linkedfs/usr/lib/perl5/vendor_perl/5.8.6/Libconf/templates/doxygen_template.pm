
# Author : damien KROTKINE (damien@tuxfamily.org)
#
#
# Copyright (C) 2002 damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : shell
#
# This templates is used to parse shell style configuration files with values.
# There is no section handling.
#

# doxygen package

# This file describes the settings to be used by the documentation system
# doxygen (www.doxygen.org) for a project
#
# All text after a hash (#) is considered a comment and will be ignored
# The format is:
#       TAG = value [value, ...]
# For lists items can also be appended using:
#       TAG += value [value, ...]
# Values that contain spaces should be placed between quotes (" ")

package Libconf;

$templates{doxygen} = {
			  rules => [ q( if ($in =~ s/^\s*(\w+)\s*=\s*(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $matched = 1;
                                        }
                                      ) ],
			  comments => [ ['#'] ],
		          output => {
				     KEY_VALUE => q(
                                            my ($key, $value) = ($atom->{key}, $atom->{value});
                                            $output_text = qq($key\t= $value);
                                     ),
				    },
			  edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	   $_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	   $i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args);
                                           return $index;
                                         ),
			  find_atom_pos => q(
                                             my $i = 0;
                                             my @res;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 my $flag = 1;
                                                 foreach (keys(%args)) {
                                                     $atom->{$_} eq $args{$_} or $flag = 0;
                                                 }
                                                 $flag and push(@res, $i);
                                                 $i++;
                                             }
                                             wantarray ? @res : $res[0];
                                           ),
                    };

1
