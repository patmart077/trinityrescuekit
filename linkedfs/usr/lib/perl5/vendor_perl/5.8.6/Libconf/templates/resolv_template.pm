# Author : Damien KROTKINE (dams@tuxfamily.org)
#
#
# Copyright (C) 2002 Charles LONGEAU (dams@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : resolv.conf
#
# This templates is used to parse resolv.conf style configuration files.
# There is no section handling.
#

package Libconf;

$templates{resolv} = {
			   rules => [
				     q(
                                       if ($in =~ s/^\s*nameserver\s+(\S+)\s*$//) {
                                           $atom->{type} = 'VALUE';
                                           $atom->{type2} = 'RESOLV_NAMESERVER';
                                           $atom->{value} = $1;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
				     q(
                                       if ($in =~ s/^\s*domain\s+(\S+)\s*$//) {
                                           $atom->{type} = 'VALUE';
                                           $atom->{type2} = 'RESOLV_DOMAIN';
                                           $atom->{value} = $1;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
			             q(
                                       if ($in =~ s/^\s*search\s+(.+\S)\s*$//) {
                                           $atom->{type} = 'LIST';
                                           $atom->{type2} = 'RESOLV_SEARCH';
                                           $atom->{list} = [ split(' ', $1) ];
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
			             q(
                                       if ($in =~ s/^\s*sortlist\s+(.+\S)\s*$//) {
                                           $atom->{type} = 'LIST';
                                           $atom->{type2} = 'RESOLV_SORTLIST';
                                           $atom->{list} = [ split(' ', $1) ];
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
			             q(
                                       if ($in =~ s/^\s*options\s+(.+\S)\s*$//) {
                                           $atom->{type} = 'LIST';
                                           $atom->{type2} = 'RESOLV_OPTIONS';
                                           $atom->{list} = [ map { /([^:]+):([^:]+)/ ? { $1 => $2 } : $_ } split(' ', $1) ];
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
				    ],
			   comments => [ ['#'] ],
		           output => {
				      VALUE => q(
                                            my $temp = lc($atom->{type2});
                                            $temp =~ s/resolv_//;
                                            $output_text = "$temp " . $atom->{value};
                                      ),
				      LIST => q(
                                            if ($atom->{type2} eq 'RESOLV_SEARCH') {
                                                $output_text = join(' ', 'search', @{$atom->{list}});
                                            } elsif ($atom->{type2} eq 'RESOLV_SORTLIST') {
                                                $output_text = join(' ', 'sortlist', map { @$_ } @{$atom->{list}});
                                            } elsif ($atom->{type2} eq 'RESOLV_OPTIONS') {
                                                $output_text = join(' ', 'sortlist', map { ref($_) ? $_->[0] . ':' . $_->[1] : $_ } @{$atom->{list}});
                                            }
                                      ),
				     },
			   edit_atom => q(
                                         if ($index == -1) {
                                             my %args_search = %args;
                                             delete @args_search{qw(value list)};
                                             $index = (findAtomPos($out, \%args_search))[-1];
                                             $index or return -2;
                                         }
                                         exists $args{type} and @{$out->{atoms}}[$index]->{type} = $args{type};
                                         exists $args{type2} and @{$out->{atoms}}[$index]->{type2} = $args{type2};
                                         exists $args{value} and @{$out->{atoms}}[$index]->{value} = $args{value};
                                         exists $args{list} and @{$out->{atoms}}[$index]->{list} = [ @{$args{list}} ];
                                         @{$out->{atoms}}[$index]->{sections} ||= [ ];
                                         return $index;
                                         ),
			   find_atom_pos => q(
                                             my $pos = 0;
                                             my @res;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 my $flag = 1;
                                                 if (exists $args{type}) { $atom->{type} eq $args{type} or next }
                                                 if (exists $args{type2}) { $atom->{type2} eq $args{type2} or next }
                                                 if (exists $args{value}) { $atom->{value} eq $args{value} or next }
                                                 if (exists $args{list}) {
                                                     exists $atom->{list} or next;
                                                     @{$args{list}} == @{$atom->{list}} or next;
                                                     foreach my $i (0..@{$args{list}}-1) {
                                                         if (ref @{$args{list}}[$i] eq 'HASH') {
                                                             ref @{$atom->{list}}[$i] eq 'HASH' or $flag = 0, last;
                                                             foreach my $key (keys %{@{$args{list}}[$i]}) {
                                                                 @{$args{list}}[$i]->{$key} eq @{$atom->{list}}[$i]->{$key} or $flag=0, last;
                                                             }
                                                             $flag or last;
                                                         } elsif (ref @{$args{list}}[$i] eq '') {
                                                             @{$args{list}}[$i] eq @{$atom->{list}}[$i] or $flag=0, last;
                                                         }
                                                     }
                                                 }
                                                 $flag and push(@res, $pos);
                                             } continue {
                                                 $pos++;
                                             }
                                             wantarray ? @res : $res[0];
                                           ),
                     };

1
