
# Author : Charles LONGEAU (chl@tuxfamily.org)
#
# Contributors : 
# Damien KROTKINE
#
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : XF86Config
#
# This templates is used to parse XF86Config configuration files.
# Sections are fully supported.
#

package Libconf;

$templates{XF86Config} = {
                          rules => [ q(
                                       if ($in =~ s/^\s*Section\s+\"([^\"]+)\"\s*$//) {
                                           $atom->{type} = 'SECTION';
                                           $atom->{section_name} = $1;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           push @{$out->{current_sections}}, { name => $1 };
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*Sub[s|S]ection\s+\"([^\"]+)\"\s*$//) {
                                           $atom->{type} = 'SECTION';
                                           $atom->{section_name} = $1;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           push @{$out->{current_sections}}, { name => $1 };
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*EndSub[s|S]ection\s*$//) {
                                           $atom->{type} = 'ENDSECTION';
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           pop @{$out->{current_sections}};
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*EndSection\s*$//) {
                                           $atom->{type} = 'ENDSECTION';
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           pop @{$out->{current_sections}};
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*InputDevice\s+"([^"]+)"\s+(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{type2} = 'XF86_INPUTDEVICE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*Screen\s+([^\s"])*\s*"([^"]+)"\s*(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{type2} = 'XF86_SCREEN';
                                           $atom->{key} = $2; #BUG : the screen-id is not kept for now
                                           $atom->{value} = $3;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*Option\s+(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{type2} = 'XF86_OPTION';
                                           my $tmp = $1;
                                           $tmp =~ s/^\s*"([^"]*)"// and $atom->{key} = $1;
                                           $tmp =~ s/^\s*"([^"]*)"// and $atom->{value} = $1;
                                           $in = $tmp; #to allow the engine to check if some stuff were not interpreted
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*Load\s*(.*)\s*$//) {
                                           $atom->{type} = 'VALUE';
                                           $atom->{type2} = 'XF86_LOAD';
                                           my $tmp = $1;
                                           $tmp =~ s/^\s*"([^"]*)"// and $atom->{value} = $1;
                                           $atom->{value} = $1;
                                           $in = $tmp; #to allow the engine to check if some stuff were not interpreted
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*(\S+)\s+(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                     q(
                                       if ($in =~ s/^\s*(\S+)\s*$//) {
                                           $atom->{type} = 'VALUE';
                                           $atom->{value} = $1;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                   ],
                          comments => [ ['#'] ],
                          output => {
                                     VALUE => q(
                                          my ($value) = $atom->{value};
                                          $output_indentation = $out->{current_indentation};
                                          $atom->{type2} eq 'XF86_LOAD' and $output_text = qq(Load "$value"), return;
                                          $output_text = $value;
                                      ),
                                     KEY_VALUE => q(
                                          my ($key, $value) = ($atom->{key}, $atom->{value});
                                          #my $sections = join ('_', map { $_->{name} } @{$atom->{sections}} );
                                          $output_indentation = $out->{current_indentation};
                                          $atom->{type2} eq 'XF86_OPTION' and $output_text = qq(Option "$key" "$value"), return;
                                          $atom->{type2} eq 'XF86_INPUTDEVICE' and $output_text = qq(InputDevice "$key" $value), return;
                                          $atom->{type2} eq 'XF86_SCREEN' and $output_text = qq(Screen "$key" $value), return;
                                          $output_text = qq($key $value);
                                      ),
                                     SECTION => q(
                                          $output_indentation = $out->{current_indentation}++;
                                          $output_text = ($out->{current_indentation} == 1 ? qq(Section "$atom->{section_name}") : qq(Subsection "$atom->{section_name}"));
                                      ),
                                     ENDSECTION => q(
                                          $output_indentation = --$out->{current_indentation};
                                          $output_text = ($out->{current_indentation} == 0 ? qq(EndSection) : qq(EndSubsection));
                                      ),
                                    },
                          edit_atom => q(
                                         if ($index == -1) {
                                             my %args_search = %args;
                                             delete @args_search{qw(value list)};
                                             $index = findAtomPos($out, \%args_search);
                                             $index or return -2;
                                         }
                                         exists $args{section_name} and @{$out->{atoms}}[$index]->{section_names} = $args{section_name};
                                         exists $args{type} and @{$out->{atoms}}[$index]->{type} = $args{type};
                                         exists $args{key} and @{$out->{atoms}}[$index]->{key} = $args{key};
                                         exists $args{value} and @{$out->{atoms}}[$index]->{value} = $args{value};
                                         exists $args{list} and @{$out->{atoms}}[$index]->{list} = [ @{$args{list}} ];
                                         exists $args{sections} and @{$out->{atoms}}[$index]->{sections} [ @{$args{sections}} ];
                                         @{$out->{atoms}}[$index]->{sections} ||= [ ];
                                         return $index;
                                       ),
			  find_atom_pos => q(
                                             $first_atom ||= 0;
                                             $last_atom ||= @{$out->{atoms}}-1;
                                             my @res;
                                             foreach my $pos ($first_atom..$last_atom) {
                                                 my $atom = $out->{atoms}->[$pos];
                                                 my $flag = 1;
                                                 foreach (qw(section_name type type2 key value)) {
                                                     exists $args{$_} or next;
                                                     $atom->{$_} eq $args{$_} or $flag = 0;
                                                 }
                                                 $flag or next;
                                                 if (exists $args{sections}) {
                                                     exists $atom->{sections} or next;
                                                     $flag = compare_depth($args{sections}, $atom->{sections});
                                                 }
                                                 $flag or next;
                                                 if (exists $args{list}) {
                                                     exists $atom->{list} or next;
                                                     @{$args{list}} == @{$atom->{list}} or next;
                                                     foreach my $i (0..@{$args{list}}-1) {
                                                         @{$atom->{list}}[$i] eq @{$args{list}}[$i] or $flag = 0;
                                                     }
                                                 }
                                                 $flag and push(@res, $pos);
                                             }
                                             wantarray ? @res : $res[-1];
                                           )
                         };
1
