
# Author : damien KROTKINE (damien@tuxfamily.org)
#
#
# Copyright (C) 2002-2003 damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : ups
#
# This templates is used to parse configuration files with values and one level of section.
# There cannot be any nested sections. The beginning of a section ends the former section.
# This is a special /etc/ups/ups.conf template

package Libconf;

$templates{ups} = {
			   rules => [ q(
                                       if ($in =~ s/^\s*(.*?)\s*=\s*(.*\S)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
				      q(
                                       if ($in =~ s/^\s*\[([^\]]+)\]\s*$//) {
                                           $atom->{type} = 'SECTION';
                                           $atom->{section_name} = $1;
                                           $atom->{sections} = [ ];
                                           $out->{current_sections} = [ { name => $1 } ];
                                           $matched = 1;
                                       }
                                     ),
				    ],
			   comments => [ ['#']],
# 			   comment_output => q(
#                                               my $indent = $INDENT_SPACES x $out->{current_indentation};
#                                               /^(\s*)$/ ? "$_\n" : "$indent;$_\n"
#                                               ),
                           output => {
				      KEY_VALUE => q(
                                          my ($key, $value) = ($atom->{key}, $atom->{value});
                                          $output_indentation = $out->{current_indentation} ||= 1;
                                          $output_text = qq($key = $value);
                                      ),
				      SECTION => q(
                                          $output_indentation = 0;  #sections are not indented
                                          $output_text = qq([$atom->{section_name}]);  #this is the output of the indentation
                                          $out->{current_indentation} = 1;  #indentation for following atoms
                                      ),
				     },
			   edit_atom => q(
                                         if ($index == -1) {
                                             my $i = 0;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 #$atom->{$_} ne $args->{$_} and next foreach (keys(%args));
                                                 #we don't exit the loop, to have the last atom if multiples ones match
                                                 $atom->{key} eq $args{key} && 
                                                     (exists($args{sections}) ? $atom->{sections}[0]{name} eq $args{sections}[0]{name} : 1)
                                                         and $index = $i;
                                                 $i++;
                                             }
                                             $index == -1 and return -2;
                                         }
                                         @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args);
                                         return $index;
                                       ),
			  find_atom_pos => q(
                                             $first_atom ||= 0;
                                             $last_atom ||= @{$out->{atoms}}-1;
                                             my @res;
                                             foreach my $pos ($first_atom..$last_atom) {
                                                 my $atom = $out->{atoms}->[$pos];
                                                 my $flag = 1;
                                                 foreach (keys(%args)) {
                                                     if ($_ eq 'sections') {
                                                         $atom->{sections}[0]{name} eq $args{sections}->[0]{name} or $flag = 0;
                                                     } else {
                                                         $atom->{$_} eq $args{$_} or $flag = 0;
                                                     }
                                                 }
                                                 $flag and push(@res, $pos);
                                             }
                                             wantarray ? @res : $res[-1];
                                           ),
                          };

1
