# Author : Charles LONGEAU (chl@tuxfamily.org)
#
#
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : passwd
#
# This templates is used to parse passwd style configuration files.
# There is no section handling.
#

package Libconf;

$templates{passwd} = {
			   rules => [ 
				     q(
                                       if (my @infos = $in =~ /^\s*(\S+?):(\S*?):(\S*?):(\S*?):(.*?):(.*?):(.*?)\s*$/) {
                                           $in = '';
                                           $atom->{type} = 'KEY_VALUES';
                                           $atom->{key} = shift @infos;
                                           @{$atom->{values}}{qw(passwd UID GID GECOS directory shell)} = @infos;
                                           $matched = 1;
                                       }
                                     ),
				    ],
			   comments => [ ['#'] ],
		           output => {
				      KEY_VALUES => q(
                                         $output_text = join(':', $atom->{key}, @{$atom->{values}}{qw(passwd UID GID GECOS directory shell)});
                                      ),
				     },
			   edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	   $_->{key} eq $args{key} and $index = $i;
                                           	   $i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args);
                                           return $index;
                                         ),
			   find_atom_pos => q(
                                             my $i = 0;
                                             my @res;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 my $flag = 1;
                                                 foreach my $key (keys(%args)) {
                                                     if ($key eq 'values') {
                                                         foreach (keys(%{$args{values}})) {
                                                             $atom->{values}{$_} eq $args{values}->{$_} or $flag = 0, last;
                                                         }
                                                     } else {
                                                         $atom->{$key} eq $args{$key} or $flag = 0, last;
                                                     }
                                                 }
                                                 $flag and push(@res, $i);
                                                 $i++;
                                             }
                                             wantarray ? @res : $res[0];
                                           ),
			  };

1
