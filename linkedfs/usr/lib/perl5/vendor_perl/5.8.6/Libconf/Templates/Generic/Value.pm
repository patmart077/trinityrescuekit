
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Generic::Value;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::Generic::Value - Libconf generic low level template for semantic (VALUE) styles config files

=head1 DESCRIPTION

Libconf::Templates::Generic::Value is a generic template that handles config files that contain semantic informations of type : (VALUE).

=head1 SYNOPSIS

 $template = new Libconf::Templates::Generic::Value({
                                               filename => 'some_filename',
                                               regexp => '^\s*(\S+)\s*$',
                                               comments_struct => [['#']],
                                               output_function => sub { my ($atom) = @_; 'val : ' . $atom->{value} }
                                              });
 $template->read_conf();
 $template->edit_atom(-1, { value => 'foo' }, { value => 'foo' });
 $template->edit_atom(5, { value => 'some value' });
 ...
 (see L<Libconf::Templates> for transformation methods on $template) ...
 ...
 $template->write_conf("output_file");

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Generic::Value({
                                                      filename => 'some_file',
                                                      regexp => '^\s*(\S+)\s*$',
                                                      comments_struct => [['#']],
                                                      output_function => sub { my ($atom) = @_; 'val : ' . $atom->{value} }
                                                     })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

comments_struct [B<type> : ARRAY_REF of ARRAY_REF of STRING,STRING,SCALAR,
B<default> : [['#']] ] : defines the type and the characters of comments. See
L<Libconf::Templates::Keyvalue> for additional details on it

regexp [B<type> : STRING, B<default> : ''] : the regexp that is applied to each
line. It should catch something, that will be stored as the value

output_function [B<type> : FUNCTION_REF, B<default> : sub {} ] : the code to be
applied to an atom, to generate the line to output. It takes in arguments the
atom

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific method

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $default_args = {
                        filename => '',
                        comments_struct => [['#']],
                        regexp => '',
                        template_name => 'Generic::Value',
                        output_function => sub {  }
                       };
    bless $self->init_args($args, $default_args), $class;
}

sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom);
    $command eq 'failed' or return ($out, $command);

    my $regexp = $self->{regexp};
    if ($in =~ /$regexp/) {
        $atom->{type} = 'VALUE';
        $atom->{value} = $1;
        $in =~ s/$regexp//;
        return ($in, 'stop');
    }
    ($in, 'failed')
}

sub _output_atom {
    my ($self, $atom, $parser_env) = @_;
    $atom->{type} eq 'VALUE' and return ($self->{output_function}->($atom), 0);
    $self->SUPER::_output_atom($atom);
}

1
