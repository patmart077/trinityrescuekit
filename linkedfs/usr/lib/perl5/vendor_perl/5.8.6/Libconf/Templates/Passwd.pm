
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Passwd;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
use Libconf::Templates::Generic::KeyValues;
@ISA = qw(Libconf::Templates::Generic::KeyValues);

=head1 NAME

Libconf::Templates::Passwd - Libconf low level template for password file styles config files

=head1 DESCRIPTION

Libconf::Templates::Passwd is a template that handles files that contain informations like the ones in /etc/passwd, with lines like :

 passwd:UID:GID:GECOS:directory:shell

=head1 SYNOPSIS

 $template = new Libconf::Templates::Passwd({
                                            filename => '/etc/passwd',
                                           });
 $template->read_conf();
 $template->append_atom( { key => 'account_test',
                           values => { passwd => 'x',
                                       UID => '000123',
                                       GID => '000456',
                                       GECOS => 'This a test stuff',
                                       directory => '/home/test',
                                       shell => '/bin/bash',
                                     },
                           type => 'KEY_VALUES',
                         });
 ...
 (see L<Libconf::Templates> for transformation methods on $template)
 ...
 $template->write_conf('/etc/passwd');

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Passwd({
                                              filename => 'some_file',
                                            })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new({
                                   filename => $args->{filename},
                                   regexp => '^\s*(\S+?):(\S*?):(\S*?):(\S*?):(.*?):(.*?):(.*?)\s*$',
                                   keys_list => [qw(username passwd UID GID GECOS directory shell)],
                                   identifier_key => 'username',
                                   output_function => sub { my $atom = shift; join(':', map { $atom->{values}{$_} } @_ ) },
                                  });
    $self->{template_name} = 'Passwd';
    bless $self, $class;
}

1
