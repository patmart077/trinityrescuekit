
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Generic::KeyValue;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::Generic::KeyValue - Libconf generic low level template for semantic association (KEY - VALUE) styles config files

=head1 DESCRIPTION

Libconf::Templates::Generic::KeyValue is a generic template that handles config files that contain semantic informations of type : (KEY - VALUE).

=head1 SYNOPSIS

 $template = new Libconf::Templates::Generic::KeyValue({
                                               filename => 'autologin',
                                               separator_char => '=',
                                               allow_space => 1,
                                               comments_struct => [['#']],
                                               handle_quote => 1,
                                               handle_multiples_lines => 0,
                                               accept_empty_value => 1,
                                              });
 $template->read_conf();
 $template->edit_atom(-1, {key => 'USER' }, { value => 'plop'});
 $template->edit_atom(-1, {key => 'AUTOLOGIN' }, { value => 'no'});
 $template->edit_atom(-1, {key => 'EXEC' }, { value => 'startx'});
 ...
 (see L<Libconf::Templates> for transformation methods on $template) ...
 ...
 $template->write_conf("output_file");

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Generic::KeyValue({
                                                          filename => 'some_file',
                                                          separator_char => '=',
                                                          output_separator_char => "\t",
                                                          allow_space => 1,
                                                          comments_struct => [['#']],
                                                          handle_quote => 1,
                                                          simplify_quote => 0,
					                  handle_multiples_lines => 0,
                                                          accept_empty_value => 0,
                                                        })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

separator_char [B<type> : STRING, B<default> : '='] : the separator between key and value

output_separator_char [B<type> : STRING, B<default> : separator_char ] : the
separator between key and value used to write the file back

allow_spaces [B<type> : SCALAR, B<default> : 1] : if true, space around
B<separator_char> are allowed and ignored if false, they are not allowed.

comments_struct [B<type> : ARRAY_REF of ARRAY_REF of STRING,STRING,SCALAR,
B<default> : [['#']] ] : defines the type and the characters of comments. The
structure is :
  [ [ comment1_open_char, comment1_close_char, comment1_nested 1], [ comment2_open_char, comment2_close_char, comment2_nested ], ... ]

=over

open_char [B<type> : STRING, B<default> : '#'] : the character(s) that delimits the start of a comment

close_char [OPTIONAL, B<type> : STRING] : the character that delimits the end of the comment. If ommited, the comment stops at the end of the line

nested [OPTIONAL, B<type> : SCALAR] : if true, comments of comments are nested.

Example 1, one simple line comment with character # :
 [['#']]

Example 2, one simple line comment with character #, another simple line comment with ';' character, and the common C /* */ comment :
 [['#'], [';'], ['/*', '*/']

Example 3, the common C /* */ comment, the C++ // comment, and the nested ocaml (* *) :
 [['/*', '*/'], ['//'], ['(*', '*)', 1]

=back

handle_quote [B<type> : SCALAR, B<default> : true] : if true, quotes are
interpreted and removed from the value. When writing to the config file back,
quotes are added if needed. if false, quotes are not interpreted, and present
in the value.

simplify_quote [B<type> : SCALAR, B<default> : false] : if true, unneeded
quotes are removed from the value. If not, they are kept as they were in the
original file.

handle_multiples_lines [B<type> : BOOLEAN, B<default> : false] : if true, line
that begins with a space character and that doesn't contain the separator_char,
are taken as the continuation of the upper line

accept_empty_value [B<type> : BOOLEAN, B<default> : true] : if true, lines
where the value is not specified (like ``key='') are accepted, and the value is
set to empty string. If set to false, such a line will trigger an error.

_input_function [B<type> : FUNCTION_REF, B<default> : undefined ] : if defined,
it's used to parse. This function should behave as _parse_chunk. Don't
use it unless you know it. See the source for more info about _parse_chunk

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific method

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $default_args = {
                        filename => '',
                        separator_char => '=',
                        allow_space => 1,
                        comments_struct => [['#']],
                        handle_quote => 1,
                        simplify_quote => 0,
                        handle_multiples_lines => 0,
                        accept_empty_value => 1,
                        template_name => 'Generic::KeyValue'
                       };
    bless $self->init_args($args, $default_args), $class;
}

# _parse_chunk(STRING, HASH_REF, HASH_REF)

# This is the first core function of any template. It applies the templates rules on
# the string passed as argument. the _parse_chunk function is called by
# Templates::readConf() on each line(chunk) of the config file. It passes the
# same chunk until _parse_chunk returns 'stop' in the command. _parse_chunk is
# called by Templates::readConf() like this:

# ($chunk, $command) = $template->_parse_chunk($chunk, $atom, $parser_env)
# ...
# $template->_parse_eof();                 # signal end of document

# It calls _parse_eof so that the template terminates its work, and forget about sticky informations.
# _parse_chunk takes as argument:
#
# - the chunk (string) to work on
# - the reference on the atom. the atom is the structure representing the information found in the chunk.
#   I need to document this shortly. It is a HASH_REF.
# - parser_env, the environnement variables of the current parsing.
# 
# 
# _parse_chunk() has to return :
#
# - the modified chunk the chunk is returned, but the information that has been
# parsed is removed. So if the entire chunk has been interpreted, the string to
# return is ''
#
# - the command
# the command is a STRING. It can be one of :
#     - 'stop'
#      tells the caller that the work on the chunk is finished
#     - 'switch_to_prev_atom'
#      ask the caller to switch to the previous atom, then relaunch parse() on the returned chunk
#     - 'switch_to_next_atom'
#      ask the caller to switch to the next atom, then relaunch parse() on the returned chunk
#     - 'next_chunk'
#      ask the caller to keep the current atom, but relaunch parse() on the next chunk
#     - 'failed'
#      informs the caller that the parsing failed, no rule could be applied on the chunk

sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom);
    $command eq 'failed' or return ($out, $command);

    # rule n�1
    my $rule1;
    $rule1 = '^' . 
      ($self->{handle_multiples_lines} ? '(\S.+?)' : '\s*(.+?)') .
      ($self->{allow_space} ? '\s*' : '') .
      $self->{separator_char} .
      ($self->{allow_space} ? '\s*' : '') .
      ($self->{handle_quote} ? q/("([^"]*)"|'([^']*)'|([^'"\s]/ . ($self->{accept_empty_value} ? '*))' : '+))') :
      ($self->{accept_empty_value} ? '(.*?)' : '(.+?)') ) .
      '\s*$';
    if (!$parser_env->{multiline_atom} && $in =~ s/$rule1//) {
        $atom->{type} = 'KEY_VALUE';
        $atom->{key} = $1;
        $atom->{value} = defined $3 ? $3 : $2;
        $self->{handle_quote} && !$self->{simplify_quote} && $2 =~ /^("|').*\1$/ and $atom->{quote} = $1;
        return ($in, 'stop');
    }

    if ($self->{handle_multiples_lines}) {

        # rule n�2
	my $rule2;
	$rule2 = '^(\s+\S.*?)\s*$';
        if (!$parser_env->{multiline_atom} && $in =~ /$rule2/) {
            $parser_env->{multiline_atom} = 1;
            return ($in, 'switch_to_prev_atom');
        }

        # rule n�3
	if ($parser_env->{multiline_atom} && $in =~ s/$rule2//) {
            $atom->{value} .= "\n$1";
            $parser_env->{multiline_atom} = 0;
            return ($in, 'stop');
        }
    }
    ($in, 'failed');
}

# _parse_eof()

# executed when the parsing is finished. _parse_eof() is called by
# Templates::readConf() at the end of the config file. It also takes parser_env
# as arguemtn, which is the environnement variables of the current parsing.
#
#  $template->_parse_eof(HASH_REF);
#

sub _parse_eof {
    my ($self, $parser_env) = @_;
    $self->SUPER::_parse_eof($parser_env);
    delete $parser_env->{multiline_atom};
}

# This is the second set of core functions of any template. The three next
# functions are used to write back the informations to the config file. If not
# implemented here, a dummy one will be used. Usually you only need output_atom for simple templates.

# output_header()

# Takes no argument, and returns a string to be written at the beginning of the
# config file, and the indentation level of the string. Althought it takes no
# argument, it can use $self->{atoms} to work on the informations.
#
#  my ($header, $indentation) = $template->output_header()
#
# $indentation is an INTEGER. 0 means no identation, 1 means 1 level of
# indentation, etc. There is the option $Libconf::Templates::Indentspaces, to
# set the indentation characters and length (see Libconf::Templates)

# output_atom(OBJ_REF, HASH_REF)

# It takes an atom, and returns its text representation, and the indentation
# level of the string. It also takes parser_env as arguemtn, which is the
# environnement variables of the current parsing. output_atom() is called by
# Templates::write_conf() for each atoms of the template. It's used to write
# down the atom in the config file
#
#  my ($line, $indentation) = $template->output_atom($atom)
#
# $indentation is an INTEGER. 0 means no identation, 1 means 1 level of
# indentation, etc. There is an Option in Libconf : $Libconf::Indentspaces, to
# set the indentation characters and length (see Libconf)

sub _output_atom {
    my ($self, $atom, $parser_env) = @_;
    my $indent = @{$atom->{sections}};
    if ($atom->{type} eq 'KEY_VALUE') {
        my $sepchar = exists $self->{output_separator_char} ? $self->{output_separator_char} : $self->{separator_char};
        my $sepstr = $self->{allow_space} ? ' ' . $sepchar . ' ' : $sepchar;
        my ($key, $value) = ($atom->{key}, $atom->{value});
        if ($self->{handle_quote}) {
            $value =~ /"/ and return (qq($key$sepstr'$value'), $indent);
            $value =~ /'/ and return (qq($key$sepstr"$value"), $indent);
            $value =~ / / and return (qq($key$sepstr"$value"), $indent);
        }
        $atom->{quote} ne '' && !$self->{simplify_quote} and $value = $atom->{quote} . $value . $atom->{quote};
        return (qq($key$sepstr$value), $indent);
    }
    $self->SUPER::_output_atom($atom);
}

# output_footer()

# Takes no argument, and returns a string to be written at the end of the
# config file, and the indentation level of the string. Althought it takes no
# argument, it can use $self->{atoms} to work on the informations.
#
#  my ($footer, $indentation) = $template->output_footer()
#
# $indentation is an INTEGER. 0 means no identation, 1 means 1 level of
# indentation, etc. There is an Option in Libconf : $Libconf::Indentspaces, to
# set the indentation characters and length (see Libconf)
#
# Here KeyValue.pm doesn't need output_footer


# _atom_edition

# this function is called when editing an atom, It returns the atom index that
# has been modified (usefull when -1 is passed), or -2 if $index was equal to
# -1 but no atom where found.
#
# $template->_atom_edition($index, $struct)
# $template->_atom_edition(-1, { key => 'AUTOLOGIN', value => 'no' });
#
# $index specifies the atom to edit (first atom is 0). If $index = -1. the
# function will try to find the atom that is the most similar to $struct, then
# edit it.
# $struct contains the new properties the atom should be set to.
#

# sub _atom_edition {
#     my ($self, $index, $struct) = @_;
#     if ($index == -1) {
#         my $i = 0;
#         foreach (@{$self->{atoms}}) {
#             $_->{key} eq $struct->{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
#             $i++;
#         }
#         $index == -1 and return -2;
#     }
#     @{@{$self->{atoms}}[$index]}{keys(%$struct)} = values(%$struct);
#     return $index;
# }

# public method
# sub find_atom_pos {
#     my ($self, $struct, $first_atom, $last_atom) = @_;
#     if (exists $struct->{type}) {
#         $struct->{type} eq 'KEY_VALUE' or return ();
#     }
#     defined $first_atom or $first_atom = 0;
#     defined $last_atom or $last_atom = @{$self->{atoms}};
#     my @res = ();
#     foreach my $i ($first_atom..$last_atom) {
#         my $atom = $self->{atoms}->[$i];
#         my $flag = 1;
#         foreach (keys(%$struct)) {
#             $atom->{$_} eq $struct->{$_} or $flag = 0;
#         }
#         $flag and push(@res, $i);
#     }
#     wantarray ? @res : $res[-1];
#}

1
