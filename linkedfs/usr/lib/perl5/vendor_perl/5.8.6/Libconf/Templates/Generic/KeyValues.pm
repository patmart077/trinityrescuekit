
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Generic::KeyValues;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
use Libconf qw(:helpers);
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::Generic::KeyValues - Libconf generic low level template for semantic association (KEY - (KEY0 - VALUE0, ... KEYn - VALUEn)) styles config files

=head1 DESCRIPTION

Libconf::Templates::Generic::KeyValues is a generic template that handles config files that contain semantic informations of type : (KEY - (KEY0 - VALUE0, ... KEYn - VALUEn)).

=head1 SYNOPSIS

 my $template = new Libconf::Templates::Generic::KeyValues({
                                                           filename => 'some_file',
                                                           comments_struct => [['#']],
                                                           regexp => '^\s*(\S+?):(\S*?):(\S*?):(.*?)\s*$',
                                                           keys_list => [qw(group_name passwd GID user_list)],
                                                           identifier_key => 'group_name',
                                                           output_function => sub { join(':', map { $atom->{values}{$_} } @_ ) }
                                                         })
 $template->read_conf();
 ...
 (see L<Libconf::Templates> for transformation methods on $template)
 ...
 $template_write_conf();

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Generic::KeyValues({
                                                           filename => 'some_file',
                                                           comments_struct => [['#']],
                                                           regexp => '^\s*(\S+?):(\S*?):(\S*?):(.*?)\s*$',
                                                           keys_list => [qw(group_name passwd GID user_list)],
                                                           identifier_key => 'group_name',
                                                           output_function => sub { join(':', map { $atom->{values}{$_} } @_ ) }
                                                         })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

regexp [B<type> : STRING, B<default> : ''] : the regexp that is applied to each line

keys_list [B<type> : ARRAY_REF, B<default> : []] : the list of names of keys
which will receive the regexp matchs

identifier_key [B<type> : STRING, B<default> : $key_list->[0] ] : the key that identifies line

comments_struct [B<type> : ARRAY_REF of ARRAY_REF of STRING,STRING,SCALAR,
B<default> : [['#']] ] : defines the type and the characters of comments. See
L<Libconf::Templates::Keyvalue> for additional details on it

keys_list [B<type> : ARRAY_REF, B<default> : []] : the list of names of keys
which will receive the regexp matchs

output_function [B<type> : FUNCTION_REF, B<default> : sub {} ] : the code to be
applied to an atom, to generate the line to output. It takes in arguments the
atom, and the list of keys keys_list : ($atom, @key_list)

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $default_args = {
                        filename => '',
                        regexp => '',
                        keys_list => [],
                        identifier_key => '',
		        comments_struct => [[]],
                        output_function => {},
                       };
    $self->init_args($args, $default_args);
    @{$self->{keys_list}} == 0 and die 'the keys_list option cannot be empty';
    $self->{identifier_key} eq '' and $self->{identifier_key} = $self->{keys_list}->[0];
    member($self->{identifier_key}, @{$self->{keys_list}}) or
      die q|the identifier_key (``| . $self->{identifier_key} . q|'') has to be one of ``| . join(q|'', ``|, @{$self->{keys_list}}) . "''.";
    bless $self, $class;
}

# private function, called by Libconf::Template::read_conf
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom);
    $command eq 'failed' or return ($out, $command);

    # rule
    my $regexp = $self->{regexp};
    if (my @infos = $in =~ /$regexp/) {
        $atom->{type} = 'KEY_VALUES';
        $atom->{values}{$_} = shift @infos foreach (@{$self->{keys_list}});
        $atom->{key} = $atom->{values}{$self->{identifier_key}};
        delete $atom->{values}{$self->{identifier_key}};
        $in =~ s/$regexp//;
        return ($in, 'stop');
    }
    ($in, 'failed');
}

# private function, called by Libconf::Template::write_conf
sub _output_atom {
    my ($self, $atom, $parser_env) = @_;
    if ($atom->{type} eq 'KEY_VALUES') {
        $atom->{values}{$self->{identifier_key}} = $atom->{key};
        my @res = ($self->{output_function}->($atom, @{$self->{keys_list}}), 0);
        delete $atom->{values}{$self->{identifier_key}};
        return @res;
    }
    $self->SUPER::_output_atom($atom);
}

## private function, called by Libconf::Template::edit_atom
#sub _atom_edition {
#    my ($self, $index, $struct) = @_;
#    if ($index == -1) {
#        my $i = 0;
#        foreach (@{$self->{atoms}}) {
#            $_->{key} eq $struct->{key} and $index = $i;
#            #we don't exit the loop, to have the last atom if multiples ones match
#            $i++;
#        }
#        $index == -1 and return -2;
#    }
#    @{@{$self->{atoms}}[$index]}{keys(%$struct)} = values(%$struct);
#    return $index;
#}
#
## public method
#sub find_atom_pos {
#    my ($self, $struct, $first_atom, $last_atom) = @_;
#    if (exists $struct->{type}) {
#        $struct->{type} eq 'KEY_VALUES' or return ();
#    }
#    defined $first_atom or $first_atom = 0;
#    defined $last_atom or $last_atom = @{$self->{atoms}};
#    my @res;
#    foreach my $i ($first_atom..$last_atom) {
#        my $atom = $self->{atoms}->[$i];
#        my $flag = 1;
#        $atom->{key} eq $struct->{key} or return wantarray ? () : undef;
#        if (exists $struct->{values}) {
#            foreach (keys(%{$struct->{values}})) {
#                $atom->{values}->{$_} eq $struct->{values}->{$_} or $flag = 0;
#            }
#        }
#        $flag and push(@res, $i);
#    }
#    wantarray ? @res : $res[-1];
#}

1
