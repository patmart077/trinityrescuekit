#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors :
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Samba::Smb_conf;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
use Libconf::Templates::Samba;
@ISA = qw(Libconf::Glueconf);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $args, $data_synopsis_version) = @_;
    my ($data_description, $data_mapping);
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    my $libconf = new Libconf::Templates::Samba($args);
    $libconf->read_conf();
    $libconf->set_uniq(qw(key sections));

    tie my %wrapper, 'Libconf::Glueconf::Samba::Smb_conf::Wrapper', $libconf, $data_description, $data_mapping ;
    bless \%wrapper, $class;
}

package Libconf::Glueconf::Samba::Smb_conf::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    bless {
           libconf => $libconf,
           _data_description => $data_description,
           _data_mapping => $data_mapping,
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

#sub DESTROY {
#}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $key });
    foreach (@pos) {
        $obj->{libconf}->delete_section($_, 1);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->get_atom(0);
    $atom->{type} eq 'SECTION' or die "atom n� 0 is not a section\n";
    $atom->{section_name};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [] });
    my $i = 0;
    my $index;
    foreach (@pos) {
        $obj->{libconf}->get_atom($_)->{section_name} eq $lastkey and $index = $i;
        $i++;
    }
    defined $index or return undef;
    $index == $#pos and return undef;
    $obj->{libconf}->get_atom($pos[$index+1])->{section_name};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq 'HASH' or die 'trying to store something else than hash ref';
    my $index;
    my $pos = $obj->{libconf}->find_atom_pos({ type => 'SECTION', section_name => $key });
    defined $pos or $pos = $obj->{libconf}->append_atom({ section_name => $key, type => 'SECTION' });
    my %hash;
    tie %hash, 'Libconf::Glueconf::Samba::Smb_conf::SectionWrapper', $obj->{libconf}, $pos;
    %hash = %$value;
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my $section_position = $obj->{libconf}->find_atom_pos( { type => 'SECTION', section_name => $key, sections => [ ] });
    defined $section_position or $section_position = $obj->{libconf}->append_atom({ section_name => $key, type => 'SECTION' });
    my %ret;
    tie %ret, 'Libconf::Glueconf::Samba::Smb_conf::SectionWrapper', $obj->{libconf}, $section_position;
    \%ret;
}

package Libconf::Glueconf::Samba::Smb_conf::SectionWrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $beginning) = @_;
    debug("beginning : $beginning");
    my $atom = $libconf->get_atom($beginning);
    my @depth = @{$atom->{sections}};
    scalar(keys %{$depth[0]}) == 0 and shift @depth;
    push @depth, { name => $atom->{section_name} };

    my $key = $beginning+1;
    foreach ($beginning+1..$libconf->get_size()-1) {
        my $atom = $libconf->get_atom($_);
        @{$atom->{sections}} > @depth and next;
        Libconf::compare_depth($atom->{sections}, \@depth) or last;
    } continue {
        $key++;
    }
    my %hash = (
                libconf => $libconf,
                firstatom => $beginning,
                sections => \@depth,
                lastatom => $key,
               );
    debug("firstatom : $hash{firstatom} - lastatom : $hash{lastatom}");
    return bless \%hash, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{lastatom} -= $obj->{libconf}->clear_section($obj->{firstatom}, 1);
    $obj->{lastatom} == $obj->{firstatom} or die "removed atoms number is wrong";
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key : $key");
    my @pos = $obj->{libconf}->find_atom_pos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    foreach (@pos) {
        $obj->{libconf}->delete_atom($_);
        $obj->{lastatom}--;
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    $obj->{firstatom} + 1 == $obj->{lastatom} and return undef;
    my $atom = $obj->{libconf}->get_atom($obj->{firstatom}+1);
    $atom->{type} eq 'KEY_VALUE' and return $atom->{key};
    die "houston, we have a problem";
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->find_atom_pos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    defined $pos ? 1 : 0;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("SectionWrapper - NEXTKEY - lastkey : $lastkey");
    my $pos = $obj->{libconf}->find_atom_pos( {type => 'KEY_VALUE', key => $lastkey },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    !defined $pos || $pos+1 == $obj->{lastatom} and return undef;
    return $obj->{libconf}->get_atom($pos+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    #ref $value eq 'HASH' or die 'trying to store anything else than hash ref';
    ref $value eq '' or die "try to store a '" . ref($value) ."' in a kery_value atom";
    my $pos = $obj->{libconf}->find_atom_pos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    if (!defined $pos) {
        $obj->{libconf}->insert_atom($obj->{lastatom}, { type => 'KEY_VALUE', key => $key,
                                                                sections => [ @{$obj->{sections}} ],
                                                              }
                                           );
        $pos = $obj->{lastatom};
        $obj->{lastatom}++;
    }
    $obj->{libconf}->edit_atom($pos, { value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key\n");
    my $pos = $obj->{libconf}->find_atom_pos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    defined $pos or return undef;
    return $obj->{libconf}->get_atom($pos)->{value};
}

package Libconf::Glueconf::Samba;
$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {

    # mangling stuff (global - service ?)
    # 'mangle case' => { type => 'BOOLEAN', default => 0 },
    # 'case sensitive' => { type => 'BOOLEAN', default => 0 },
    # 'default case' => { type => 'BOOLEAN', default => 'lower', values => [qw (upper lower)] },
    # 'preserve case' => { type => 'BOOLEAN', default => 1 },
    # 'short preserve case' => { type => 'BOOLEAN', default => 1 },

    global => {
               # global stuff
               'acl compatibility (G)' => { type => 'STRING' },
               'add printer command (G)' => { type => 'COMMAND' },
               'add share command (G)' => { type => 'COMMAND' },
               'add user script (G)' => { type => 'COMMAND' },
               'allow trusted domains' => { type => 'BOOLEAN', default => 1 },
               'announce as' => { type => 'STRING', values => [ "NT Server", "NT", "NT Workstation", "Win95", "WfW"], default => 'NT' },
               'announce version' => { type => 'VERSION', default => '4.9' },
               'auto services' => { type => 'LIST', type2 => 'STRING', type3 => ' '},
               'bind interfaces only' => { type => 'LIST', type2 => 'NET_INTERFACE', type3 => ' ' },
               'browse list' => { type => 'BOOLEAN', default => 1 },
               'change notify timeout' => { type => 'DURATION', type2 => 'SECONDS', default => 60 },
               'change share command' => { type => 'COMMAND' },
               'character set' => { type => 'ENCODING' },
               'client code page' => { type => 'INTEGER', default => 850 },
               'code page directory' => { type => 'DIRECTORY' },
               'coding system' => { type => 'STRING' },
               'config file' => { type => 'FILENAME' },
               'deadtime' => { type => 'DURATION', type2 => 'MINUTES', default => 0 },
               'debug hires timestamp' => { type => 'BOOLEAN', default => 0 },
               'debug pid' => { type => 'PID' },
               'debug timestamp' => { type => 'BOOLEAN', default => 1 },
               'debug uid' => { type => 'BOOLEAN', default => 0 },
               'debuglevel' => { type => 'INTEGER', default => 0 },
               'default' => { type => 'LIST', type2 => 'STRING' },
               'default service' => { type => 'LIST', type2 => 'STRING' },
               'delete printer command' => { type => 'COMMAND' },
               'delete share command' => { type => 'COMMAND' },
               'delete user script' => { type => 'COMMAND' },
               'dfree command' => { type => 'COMMAND' },
               'disable spoolss' => { type => 'BOOLEAN', default => 0},
               'dns proxy' => { type => 'BOOLEAN', default => 1 },
               'domain admin group' => { type => 'LIST', type2 => 'SMB_UID|SMB_GID', type3 => ' ' },
               'domain guest group' => { type => 'LIST', type2 => 'SMB_UID|SMB_GID', type3 => ' ' },
               'domain logons' => { type => 'BOOLEAN', default => 0 },
               'domain master' => { type => 'STRING', default => 'auto' },
               'encrypt passwords' => { type => 'BOOLEAN', default => 0 },
               'enhanced browsing' => { type => 'BOOLEAN', default => 1},
               'enumports command' => { type => 'COMMAND' },
               'getwd cache' => { type => 'BOOLEAN', default => 1},
               'hide local users' => { type => 'BOOLEAN', default => 0},
               'hide unreadable' => { type => 'BOOLEAN', default => 0},
               'homedir map' => { type => 'STRING' },
               'host msdfs' => { type => 'BOOLEAN', default => 0},
               'hosts equiv' => { type => 'FILENAME' },
               'interfaces' => { type => 'LIST', type2 => 'STRING|NET_INTERFACE|IP/MASK', type3 => ' '},
               'keepalive' => { type => 'DURATION', type2 => 'SECONDS', default => 300 },
               'kernel oplocks' => { type => 'BOOLEAN', default => 1},
               'lanman auth' => { type => 'BOOLEAN', default => 1 },
               'large readwrite' => { type => 'BOOLEAN', default => 1 },
               'ldap admin dn' => { type => '', },
               'ldap filter' => { type => 'STRING', default => '(&(uid=%u)(objectclass=sambaAccount))' },
               'ldap port' => { type => '', },
               'ldap server' => { type => 'STRING', default => 'localhost' },
               'ldap ssl' => { type => 'STRING', values => [ qw(on off start_tls) ], default => 'on'},
               'ldap suffix' => { type => '', },
               'lm announce' => { type => 'STRING', values => [ qw(yes no  or auto) ], default => 'auto' },
               'lm interval' => { type => 'DURATION', type2 => 'SECONDS', default => 120 },
               'load printers' => { type => 'BOOLEAN', default => 1},
               'local master' => { type => 'BOOLEAN', default => 1 },
               'lock dir' => { type => 'FILENAME', default => '${prefix}/var/locks' },
               'lock directory' => { type => 'FILENAME', default => '${prefix}/var/locks' },
               'lock spin count' => { type => 'INTEGER', default => 2 },
               'lock spin time' => { type => 'DURATION', type2 => 'MICROSECONDS', default => 10 },
               'log file' => { type => 'FILENAME' },
               'log level' => { type => 'INTEGER', default => 0 },
               'logon drive' => { type => 'STRING', default => 'z:' },
               'logon home' => { type => 'STRING', default => '"\\%N\%U"'},
               'logon path' => { type => 'STRING', default => '\\%N\%U\profile'},
               'logon script' => { type => 'STRING', },
               'lpq cache time' => { type => 'DURATION', type2 => 'SECONDS', default => 10 },
               'machine password timeout' => { type => 'DURATION', type2 => 'SECONDS', default => 604800 },
               'mangled stack' => { type => 'INTEGER', default => 50 },
               'mangling method' => { type => 'STRING', values => [ qw(hash hash2) ], default => 'hash' },
               'map to guest' => { type => 'STRING', values => [ 'Never', 'Bad User', 'Bad Password'], default => 'Never' },
               'max disk size' => { type => 'SIZE', type2 => 'MB', default => 0},
               'max log size' => { type => 'SIZE', type2 => 'KB', default => 5000},
               'max mux' => { type => 'INTEGER', default => 50 },
               'max open files' => { type => 'INTEGER', default => 10000 },
               'max protocol' => { type => 'STRING', values => [ qw(CORE COREPLUS LANMAN1 LANMAN2 NT1) ], default => 'NT1'},
               'max smbd processes' => { type => 'INTEGER', default => 0 },
               'max ttl' => { type => 'DURATION', type2 => 'SECONDS', default => 259200 },
               'max wins ttl' => { type => 'DURATION', type2 => 'SECONDS', default => 518400 },
               'max xmit' => { type => 'INTEGER', default => 8192  },
               'message command' => { type => 'STRING', },
               'min passwd length' => { type => 'INTEGER', default => 5},
               'min password length' => { type => 'INTEGER', default => 5},
               'min protocol' => { type => 'STRING', values => [ qw(CORE COREPLUS LANMAN1 LANMAN2 NT1) ], default => 'CORE'},
               'min wins ttl' => { type => 'DURATION', type2 => 'SECONDS', default => 21600},
               'name resolve order' => { type => 'LIST', type2 => 'STRING', type3 => ' ', values => [ qw(lmhosts host wins bcast) ], default => 'lmhosts host wins bcast' },
               'netbios aliases' => { type => 'LIST', type2 => 'STRING', type3 => ' '},
               'netbios name' => { type => 'STRING' },
               'netbios scope' => { type => '', },
               'nis homedir' => { type => 'STRING', default => 'no' },
               'nt pipe support' => { type => 'BOOLEAN', default => 1},
               'nt smb support' => { type => 'BOOLEAN', default => 1},
               'nt status support' => { type => 'BOOLEAN', default => 1},
               'null passwords' => { type => 'BOOLEAN', default => 0},
               'obey pam restrictions' => { type => 'BOOLEAN', default => 0},
               'oplock break wait time' => { type => 'DURATION', type2 => 'MILLISECONDS', default => 0 },
               'os level' => { type => 'INTEGER', default => 20},
               'os2 driver map' => { type => 'FILENAME', default => ''},
               'pam password change' => { type => 'BOOLEAN', default => 0},
               'panic action' => { type => 'COMMAND', },
               'passwd chat' => { type => 'STRING', default => '*new*password* %n\n  *new*password*  %n\n *changed*'},
               'passwd chat debug' => { type => 'BOOLEAN', default => 'no'},
               'passwd program' => { type => 'COMMAND', default => '/bin/passwd'},
               'password level' => { type => 'INTEGER', default => 0},
               'password server' => { type => 'STRING', },
               'pid directory' => { type => 'DIRECTORY', default => '${prefix}/var/locks'},
               'prefered master' => { type => 'STRING', values => [ qw(yes no auto) ], default => 'auto' },
               'preferred master' => { type => 'STRING', values => [ qw(yes no auto) ], default => 'auto' },
               'preload' => { type => 'LIST', type2 => 'STRING', type3 => ' '},
               'printcap' => { type => 'STRING', default => '/etc/printcap'},
               'printcap name' => { type => 'STRING', default => '/etc/printcap'},
               'printer driver file' => { type => 'FILENAME', },
               'protocol' => { type => 'STRING', values => [ qw(CORE COREPLUS LANMAN1 LANMAN2 NT1) ], default => 'NT1'},
               'read bmpx' => { type => 'BOOLEAN', default => 0 },
               'read raw' => { type => 'BOOLEAN', default => 1 },
               'read size' => { type => 'INTEGER', default => 16384 },
               'remote announce' => { type => 'LIST', type2 => 'IP/MASK', type3 => ' '},
               'remote browse sync' => { type => 'LIST', type2 => 'IP', type3 => ' '},
               'restrict anonymous' => { type => 'BOOLEAN', default => 0},
               'root' => { type => 'DIRECTORY', default => '/'},
               'root dir' => { type => 'DIRECTORY', default => '/'},
               'root directory' => { type => 'DIRECTORY', default => '/'},
               'security' => { type => 'STRING', values => [ qw(user share server domain) ], default => 'user'},
               'server string' => { type => 'STRING', default => 'Samba %v'},
               'show add printer wizard' => { type => 'BOOLEAN', default => 1},
               'smb passwd file' => { type => 'DIRECTORY', default => '${prefix}/private/smbpasswd'},
               'socket address' => { type => 'IP', },
               'socket options' => { type => 'STRING', type2 => default => 'TCP_NODELAY'},
               'source environment' => { type => 'STRING', },
               'ssl' => { type => 'BOOLEAN', default => 0},
               'ssl CA certDir' => { type => 'DIRECTORY', default => '/usr/local/ssl/certs'},
               'ssl CA certFile' => { type => 'FILENAME', default => '/usr/local/ssl/certs/trustedCAs.pem'},
               'ssl ciphers' => { type => '', },
               'ssl client cert' => { type => 'FILENAME', default => '/usr/local/ssl/certs/smbclient.pem'},
               'ssl client key' => { type => 'FILENAME', default => '/usr/local/ssl/private/smbclient.pem'},
               'ssl compatibility' => { type => 'BOOLEAN', default => 0},
               'ssl egd socket' => { type => 'DIRECTORY', },
               'ssl entropy bytes' => { type => 'SIZE', type2 => 'BYTES', default => 255},
               'ssl entropy file' => { type => 'FILENAME', },
               'ssl hosts' => { type => 'IP', },
               'ssl hosts resign' => { type => 'IP', },
               'ssl require clientcert' => { type => 'BOOLEAN', default => 0},
               'ssl require servercert' => { type => 'BOOLEAN', default => 0},
               'ssl server cert' => { type => 'FILENAME', },
               'ssl server key' => { type => 'FILENAME', },
               'ssl version' => { type => 'STRING', values => [ qw(ssl2or3 ssl2 ssl3 tls1) ], default => '"ssl2or3"' },
               'stat cache' => { type => 'BOOLEAN', default => 1},
               'stat cache size' => { type => 'INTEGER', default => 50},
               'status' => { type => 'BOOLEAN', default => 1},
               'strip dot' => { type => 'BOOLEAN', default => 0},
               'syslog' => { type => 'INTEGER', default => 1},
               'syslog only' => { type => 'BOOLEAN', default => 0},
               'template homedir' => { type => 'DIRECTORY', default => '/home/%D/%U'},
               'template shell' => { type => 'COMMAND', default => '/bin/false'},
               'time offset' => { type => 'DURATION', type2 => 'MINUTES', default => 0},
               'time server' => { type => 'BOOLEAN', default => 0},
               'timestamp logs' => { type => 'BOOLEAN', default => 1 },
               'total print jobs' => { type => 'INTEGER', default => 0},
               'unix extensions' => { type => 'BOOLEAN', default => 0},
               'unix password sync' => { type => 'BOOLEAN', default => 0},
               'update encrypted' => { type => 'BOOLEAN', default => 0},
               'use mmap' => { type => 'BOOLEAN', default => 1},
               'use rhosts' => { type => 'BOOLEAN', default => 0},
               'username level' => { type => 'INTEGER', default => 0},
               'username map' => { type => 'FILENAME', },
               'utmp' => { type => 'BOOLEAN', default => 0},
               'utmp directory' => { type => 'DIRECTORY', },
               'valid chars' => { type => 'LIST', type2 => 'STRING', type3 => ' '},
               'winbind cache time' => { type => 'DURATION', type2 => 'SECONDS', default => 15},
               'winbind enum users' => { type => 'BOOLEAN', default => 1},
               'winbind enum groups' => { type => 'BOOLEAN', default => 1},
               'winbind gid' => { type => 'RANGE', type2 => 'INTEGER', type3 => ''},
               'winbind separator' => { type => 'STRING', default => '\\' },
               'winbind uid' => { type => 'RANGE', type2 => 'INTEGER'},
               'winbind use default domain' => { type => 'BOOLEAN', default => 1},
               'wins hook' => { type => 'COMMAND', },
               'wins proxy' => { type => 'BOOLEAN', default => 0},
               'wins server' => { type => 'IP', },
               'wins support' => { type => 'BOOLEAN', default => 1},
               'workgroup' => { type => 'STRING', default => 'WORKGROUP' },
               'write raw' => { type => 'BOOLEAN', default => 1},
              },
    service => {
                #Here is a list of all service parameters. See the section on each parameter for details. Note that some are synonyms.
                'admin users' => { type => '', },
                'allow hosts' => { type => '', },
                'available' => { type => '', },
                'blocking locks' => { type => '', },
                'block size' => { type => '', },
                'browsable' => { type => '', },
                'browseable' => { type => '', },
                'case sensitive' => { type => '', },
                'casesignames' => { type => '', },
                'comment' => { type => '', },
                'copy' => { type => '', },
                'create mask' => { type => '', },
                'create mode' => { type => '', },
                'csc policy' => { type => '', },
                'default case' => { type => '', },
                'default devmode' => { type => '', },
                'delete readonly' => { type => '', },
                'delete veto files' => { type => '', },
                'deny hosts' => { type => '', },
                'directory' => { type => '', },
                'directory mask' => { type => '', },
                'directory mode' => { type => '', },
                'directory security mask' => { type => '', },
                'dont descend' => { type => '', },
                'dos filemode' => { type => '', },
                'dos filetime resolution' => { type => '', },
                'dos filetimes' => { type => '', },
                'exec' => { type => '', },
                'fake directory create times' => { type => '', },
                'fake oplocks' => { type => '', },
                'follow symlinks' => { type => '', },
                'force create mode' => { type => '', },
                'force directory mode' => { type => '', },
                'force directory security mode' => { type => '', },
                'force group' => { type => '', },
                'force security mode' => { type => '', },
                'force unknown acl user' => { type => '', },
                'force user' => { type => '', },
                'fstype' => { type => '', },
                'group' => { type => '', },
                'guest account' => { type => '', },
                'guest ok' => { type => '', },
                'guest only' => { type => '', },
                'hide dot files' => { type => '', },
                'hide files' => { type => '', },
                'hosts allow' => { type => '', },
                'hosts deny' => { type => '', },
                'include' => { type => '', },
                'inherit acls' => { type => '', },
                'inherit permissions' => { type => '', },
                'invalid users' => { type => '', },
                'level2 oplocks' => { type => '', },
                'locking' => { type => '', },
                'lppause command' => { type => '', },
                'lpq command' => { type => '', },
                'lpresume command' => { type => '', },
                'lprm command' => { type => '', },
                'magic output' => { type => '', },
                'magic script' => { type => '', },
                'mangle case' => { type => '', },
                'mangled map' => { type => '', },
                'mangled names' => { type => '', },
                'mangling char' => { type => '', },
                'map archive' => { type => '', },
                'map hidden' => { type => '', },
                'map system' => { type => '', },
                'max connections' => { type => '', },
                'max print jobs' => { type => '', },
                'min print space' => { type => '', },
                'msdfs root' => { type => '', },
                'nt acl support' => { type => '', },
                'only guest' => { type => '', },
                'only user' => { type => '', },
                'oplock contention limit' => { type => '', },
                'oplocks' => { type => '', },
                'path' => { type => '', },
                'posix locking' => { type => '', },
                'postexec' => { type => '', },
                'postscript' => { type => '', },
                'preexec' => { type => '', },
                'preexec close' => { type => '', },
                'preserve case' => { type => '', },
                'print command' => { type => '', },
                'print ok' => { type => '', },
                'printable' => { type => '', },
                'printer' => { type => '', },
                'printer admin' => { type => '', },
                'printer driver' => { type => '', },
                'printer driver location' => { type => '', },
                'printer name' => { type => '', },
                'printing' => { type => '', },
                'profile acls' => { type => '', },
                'public' => { type => '', },
                'queuepause command' => { type => '', },
                'queueresume command' => { type => '', },
                'read list' => { type => '', },
                'read only' => { type => '', },
                'root postexec' => { type => '', },
                'root preexec' => { type => '', },
                'root preexec close' => { type => '', },
                'security mask' => { type => '', },
                'set directory' => { type => '', },
                'share modes' => { type => '', },
                'short preserve case' => { type => '', },
                'status' => { type => '', },
                'strict allocate' => { type => '', },
                'strict locking' => { type => '', },
                'strict sync' => { type => '', },
                'sync always' => { type => '', },
                'use client driver' => { type => '', },
                'use sendfile' => { type => '', },
                'user' => { type => '', },
                'username' => { type => '', },
                'users' => { type => '', },
                'valid users' => { type => '', },
                'veto files' => { type => '', },
                'veto oplock files' => { type => '', },
                'vfs object' => { type => '', },
                'vfs options' => { type => '', },
                'volume' => { type => '', },
                'wide links' => { type => '', },
                'writable' => { type => '', },
                'write cache size' => { type => '', },
                'write list' => { type => '', },
                'write ok' => { type => '', },
                'writeable' => { type => '', },
               }
   },
   mapping => {
               BOOLEAN => sub {
                   my ($value) = @_;
                   $value eq 'true' and return 1;
                   $value eq 'false' and return 0;
                   $value eq 'yes' and return 1;
                   $value eq 'no' and return 0;
                   $value ? 'true' : 'false';
               },
   },
  };

1;

