#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
# Charles LONGEAU (chl@tuxfamily.org)
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::System::Passwd;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
use Libconf::Templates::Passwd;
@ISA = qw(Libconf::Glueconf);

sub new {
    my ($class, $args) = @_;
    my $libconf = new Libconf::Templates::Passwd($args);
    $libconf->read_conf();
    tie my %wrapper, 'Libconf::Glueconf::System::Passwd::Wrapper', $libconf;
    bless \%wrapper, $class;
}

package Libconf::Glueconf::System::Passwd::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUES', key => $key });
    foreach (@pos) {
        $obj->{libconf}->delete_atom($_);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->get_atom(0);
#    $atom->{key};
    $atom->{key};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->find_atom_pos( { type => 'KEY_VALUES', key => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'KEY_VALUES', key => $lastkey }); #FIXME : double entries should be removed elsewhere
    $pos[-1]+1 >= $obj->{libconf}->get_size and return undef;
    $obj->{libconf}->get_atom($pos[-1]+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq 'HASH' or die 'trying to store anything else than a hash';
    my $index;
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUES', key => $key });
    if (@pos == 0) {
        $index = $obj->{libconf}->append_atom({ type => 'KEY_VALUES', key => $key });
    } else {
        $index = $pos[-1];
    }
    $obj->{libconf}->edit_atom($index, { type => 'KEY_VALUES', key => $key, values => { %$value } });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUES', key => $key });
    @pos == 0 and return undef;
    $obj->{libconf}->get_atom($pos[-1])->{values};
}

1;

