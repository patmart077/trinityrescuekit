#!/usr/bin/perl

# Author : Brian J. Murrell (brian@interlinx.bc.ca)
# Based heavily on Shell.pm (in fact I ripped it off completely!)
#
# Contributors : 
#
# Copyright (C) 2003 Brian J. Murrell (brian@interlinx.bc.ca)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Doxygen;
use strict;
use vars qw(@ISA);
use Libconf;
#use Libconf::Glueconf;
@ISA = qw(Libconf);

sub new {
    my ($pkg, $filename) = @_;
    my $libconf = Libconf::new('Libconf', $filename, 'doxygen', '');
    tie my %wrapper, 'Libconf::Glueconf::Doxygen::Doxygen::Wrapper', $libconf;
    bless \%wrapper, $pkg;
}

sub readConf {
    my ($obj) = @_;
    $obj->{libconf}->readConf;
}

sub writeConf {
    my ($obj, @args) = @_;
    $obj->{libconf}->writeConf(@args);
}

package Libconf::Glueconf::Doxygen::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUE', key => $key });
    foreach (@pos) {
        $obj->{libconf}->deleteAtom($_);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->getAtom(0);
    $atom->{key};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUE', key => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUE', key => $lastkey });
    $pos[-1]+1 >= $obj->{libconf}->size and return undef;
    $obj->{libconf}->getAtom($pos[-1]+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq '' or die 'trying to store anything else than a value';
    my $index;
    my @pos = $obj->{libconf}->findAtomPos({ type => 'KEY_VALUE', key => $key });
    if (@pos == 0) {
        $index = $obj->{libconf}->appendAtom({ type => 'KEY_VALUE', key => $key });
    } else {
        $index = $pos[-1];
    }
    $obj->{libconf}->editAtom($index, { type => 'KEY_VALUE', key => $key, value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    my @pos = $obj->{libconf}->findAtomPos({ type => 'KEY_VALUE', key => $key });
    $obj->{libconf}->getAtom($pos[-1])->{value};
}

1;

