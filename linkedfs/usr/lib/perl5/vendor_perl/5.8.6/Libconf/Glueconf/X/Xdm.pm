#!/usr/bin/perl

# Author : Damien KROTKINE (damien@libconf.net)
#
# Contributors : 
#
# Copyright (C) 2004 Damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::X::Xdm;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf::Generic::KeyValue;

our @ISA = qw(Libconf::Glueconf::Generic::KeyValue);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $filename, $data_synopsis_version, $data_description, $data_mapping) = @_;
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    my $template = 'xdm';
    my $libconf = Libconf::new('Libconf', $filename, $template, '');
    tie my %wrapper, 'Libconf::Glueconf::Generic::KeyValue::Wrapper', $libconf, $data_description, $data_mapping;
    bless \%wrapper, $class;
}

1;




