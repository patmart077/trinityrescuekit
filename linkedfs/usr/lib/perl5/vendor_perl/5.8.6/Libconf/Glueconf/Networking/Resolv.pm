#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
# Charles LONGEAU (chl@tuxfamily.org)
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Networking::Resolv;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
use Libconf::Templates::Resolv;
@ISA = qw(Libconf::Glueconf);

sub new {
    my ($class, $args) = @_;
    my $libconf = new Libconf::Templates::Resolv($args);
    $libconf->read_conf();
    tie my %wrapper, 'Libconf::Glueconf::Networking::Resolv::Wrapper', $libconf;
    bless \%wrapper, $class;
}

package Libconf::Glueconf::Networking::Resolv::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf) = @_;
    debug();
    my %hash;
    tie my @nameserver_wrapper, 'Libconf::Glueconf::Networking::Resolv::Nameserver::Wrapper', $libconf;
    $hash{nameserver} = \@nameserver_wrapper;
    tie my $domain_wrapper, 'Libconf::Glueconf::Networking::Resolv::Domain::Wrapper', $libconf;
    $hash{domain} = \$domain_wrapper;
    tie my @search_wrapper, 'Libconf::Glueconf::Networking::Resolv::Search::Wrapper', $libconf;
    $hash{search} = \@search_wrapper;
    tie my @sortlist_wrapper, 'Libconf::Glueconf::Networking::Resolv::Sortlist::Wrapper', $libconf;
    $hash{sortlist} = \@sortlist_wrapper;
    tie my @options_wrapper, 'Libconf::Glueconf::Networking::Resolv::Options::Wrapper', $libconf;
    $hash{options} = \@options_wrapper;
    bless {
           hash => \%hash,
           libconf => $libconf
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{hash} = {};
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    delete $obj->{hash};
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    scalar each %{$obj->{hash}};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    exists $obj->{hash};
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    scalar each %{$obj->{hash}};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    $obj->{hash}{$key} = $value;
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' ? $obj->{libconf} : $obj->{hash}{$key};
}


package Libconf::Glueconf::Networking::Resolv::Nameserver::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEARRAY {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub FETCH {
    my ($obj, $idx) = @_;
    debug("idx : $idx");
    $idx eq 'libconf' and return $obj->{libconf};
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    $obj->{libconf}->get_atom($pos[$idx])->{value};
}

sub STORE {
    my ($obj, $idx, $value) = @_;
    debug("idx : $idx - value : $value");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    if ($idx > $#pos) {
        $obj->{libconf}->insert_atom($pos[-1]+1, {type => 'VALUE', type2 => 'RESOLV_NAMESERVER', value => $value });
    } else {
        $obj->{libconf}->edit_atom($pos[$idx], {type => 'VALUE', type2 => 'RESOLV_NAMESERVER', value => $value });
    }
}

sub FETCHSIZE {
    my ($obj) = @_;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    scalar(@pos);
}

sub STORESIZE {
    my ($obj, $count) = @_;
    debug("count : $count");
    my $size = $obj->FETCHSIZE;
    if ($count > $size) {
        foreach ($count-$size..$count) {
            $obj->STORE($_, '');
        }
    } elsif ($count < $size) {
        foreach (0..$size-$count-2) {
            $obj->POP;
        }
    }
}

sub EXTEND {}

sub EXISTS {
    my ($obj, $index) = @_;
    debug("index : $index");
    defined $obj->FETCH($index);
}

sub CLEAR {
    my $obj = shift;
    debug();
    $obj->{libconf}->delete_atom($_) foreach $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
}

sub PUSH {
    my $obj = shift;
    my @list = @_;
    debug("list : [" . join('|', @list) . "]");
    my $last = $obj->FETCHSIZE;
    $obj->STORE($last + $_, $list[$_]) foreach (0..$#list);
    $obj->FETCHSIZE;
}

sub POP {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    my $value = $obj->FETCH($pos[-1]);
    $obj->{libconf}->delete_atom($pos[-1]);
    $value;
}

sub SHIFT {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    my $value = $obj->FETCH($pos[0]);
    $obj->{libconf}->delete_atom($pos[0]);
    $value;
}

sub UNSHIFT {
    my ($obj, @list) = @_;
    debug("list : [" . join('|', @list));
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    $obj->{libconf}->insert_atom($pos[0], {type => 'VALUE', type2 => 'RESOLV_NAMESERVER', value => $_ }) foreach reverse @list;
}

sub SPLICE {
    my $obj = shift;
    my $offset = shift || 0;
    my $length = shift || $obj->FETCHSIZE - $offset;
    my @list = @_;
    debug("offset : $offset - length : $length - list : [" . join('|', @list) . "]");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_NAMESERVER' });
    my @ret;
    foreach (0..$length-1) {
        push @ret, $obj->FETCH($offset+$_);
        exists $list[$_] ? $obj->STORE($offset+$_, $list[$_]) : $obj->{libconf}->delete_atom($pos[$offset+$_]);
    }
    wantarray() ? @ret : $ret[-1];
}

package Libconf::Glueconf::Networking::Resolv::Domain::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIESCALAR {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub FETCH {
    my ($obj) = @_;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_DOMAIN' });
    @pos > 0 or return undef;
    $obj->{libconf}->get_atom($pos[-1])->{value};
}

sub STORE {
    my ($obj, $value) = @_;
    debug("value : $value");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'VALUE', type2 => 'RESOLV_DOMAIN' });
    if (@pos > 0) {
        $obj->{libconf}->edit_atom($pos[-1], {type => 'VALUE', type2 => 'RESOLV_DOMAIN', value => $value });
    } else {
        $obj->{libconf}->append_atom({type => 'VALUE', type2 => 'RESOLV_DOMAIN', value => $value });
    }
}

package Libconf::Glueconf::Networking::Resolv::Search::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEARRAY {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub FETCH {
    my ($obj, $idx) = @_;
    debug("idx : $idx");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    $obj->{libconf}->get_atom($pos[-1])->{list}->[$idx];
}

sub STORE {
    my ($obj, $idx, $value) = @_;
    debug("idx : $idx - value : $value");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    $tab[$idx] = $value;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
}

sub FETCHSIZE {
    my ($obj) = @_;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    @pos > 0 or return 0;
#    require Data::Dumper;
#    print Data::Dumper->Dump([@pos], ['@pos']) . "\n";
#    my $pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
#    print Data::Dumper->Dump([$pos], ['$pos']) . "\n";
#    debug("SEARCH Wrapper - FETCHSIZE : " . scalar(@{$obj->{libconf}->get_atom($pos[-1])->{list}}) . "\n");
    scalar @{$obj->{libconf}->get_atom($pos[-1])->{list}};
}

sub STORESIZE {
    my ($obj, $count) = @_;
    debug("count : $count");
    my $size = $obj->FETCHSIZE;
    if ($count > $size) {
        foreach ($count-$size..$count) {
            $obj->STORE($_, '');
        }
    } elsif ($count < $size) {
        foreach (0..$size-$count-2) {
            $obj->POP;
        }
    }
}

sub EXTEND {}

sub EXISTS {
    my ($obj, $index) = @_;
    debug("index : $index");
    defined $obj->FETCH($index);
}

sub CLEAR {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    $obj->{libconf}->edit_atom($pos[-1], { list => [] });
}

sub PUSH {
    my $obj = shift;
    my @list = @_;
    debug("list : [" . join('|', @list) . "]");
    my $last = $obj->FETCHSIZE;
    $obj->STORE($last + $_, $list[$_]) foreach (0..$#list);
    $obj->FETCHSIZE;
}

sub POP {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my $value = pop @tab;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    $value;
}

sub SHIFT {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my $value = shift @tab;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    $value;
}

sub UNSHIFT {
    my ($obj, @list) = @_;
    debug("list : [" . join('|', @list));
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    unshift @tab, @list;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
}

sub SPLICE {
    my $obj = shift;
    my $offset = shift || 0;
    my $length = shift || $obj->FETCHSIZE - $offset;
    my @list = @_;
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SEARCH' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my @ret = splice @tab, $offset, $length, @list;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    wantarray() ? @ret : $ret[-1];
}

package Libconf::Glueconf::Networking::Resolv::Sortlist::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEARRAY {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub FETCH {
    my ($obj, $idx) = @_;
    debug("idx : $idx");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    $obj->{libconf}->get_atom($pos[-1])->{list}->[$idx];
}

sub STORE {
    my ($obj, $idx, $value) = @_;
    debug("idx : $idx - value : $value");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    $tab[$idx] = $value;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
}

sub FETCHSIZE {
    my ($obj) = @_;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    @pos > 0 or return 0;
    scalar @{$obj->{libconf}->get_atom($pos[-1])->{list}};
}

sub STORESIZE {
    my ($obj, $count) = @_;
    debug("count : $count");
    my $size = $obj->FETCHSIZE;
    if ($count > $size) {
        foreach ($count-$size..$count) {
            $obj->STORE($_, '');
        }
    } elsif ($count < $size) {
        foreach (0..$size-$count-2) {
            $obj->POP;
        }
    }
}

sub EXTEND {}

sub EXISTS {
    my ($obj, $index) = @_;
    debug("index : $index");
    defined $obj->FETCH($index);
}

sub CLEAR {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    $obj->{libconf}->edit_atom($pos[-1], { list => [] });
}

sub PUSH {
    my $obj = shift;
    my @list = @_;
    debug("list : [" . join('|', @list) . "]");
    my $last = $obj->FETCHSIZE;
    $obj->STORE($last + $_, $list[$_]) foreach (0..$#list);
    $obj->FETCHSIZE;
}

sub POP {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my $value = pop @tab;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    $value;
}

sub SHIFT {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my $value = shift @tab;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    $value;
}

sub UNSHIFT {
    my ($obj, @list) = @_;
    debug("list : [" . join('|', @list));
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    unshift @tab, @list;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
}

sub SPLICE {
    my $obj = shift;
    my $offset = shift || 0;
    my $length = shift || $obj->FETCHSIZE - $offset;
    my @list = @_;
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_SORTLIST' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my @ret = splice @tab, $offset, $length, @list;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    wantarray() ? @ret : $ret[-1];
}


package Libconf::Glueconf::Networking::Resolv::Options::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEARRAY {
    my ($pkg, $libconf) = @_;
    debug();
    bless { libconf => $libconf }, $pkg;
}

sub FETCH {
    my ($obj, $idx) = @_;
    debug("idx : $idx");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    $obj->{libconf}->get_atom($pos[-1])->{list}->[$idx];
}

sub STORE {
    my ($obj, $idx, $value) = @_;
    debug("idx : $idx - value : $value");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    $tab[$idx] = $value;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
}

sub FETCHSIZE {
    my ($obj) = @_;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    @pos > 0 or return 0;
    scalar @{$obj->{libconf}->get_atom($pos[-1])->{list}};
}

sub STORESIZE {
    my ($obj, $count) = @_;
    debug("count : $count");
    my $size = $obj->FETCHSIZE;
    if ($count > $size) {
        foreach ($count-$size..$count) {
            $obj->STORE($_, '');
        }
    } elsif ($count < $size) {
        foreach (0..$size-$count-2) {
            $obj->POP;
        }
    }
}

sub EXTEND {}

sub EXISTS {
    my ($obj, $index) = @_;
    debug("index : $index");
    defined $obj->FETCH($index);
}

sub CLEAR {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    $obj->{libconf}->edit_atom($pos[-1], { list => [] });
}

sub PUSH {
    my $obj = shift;
    my @list = @_;
    debug("list : [" . join('|', @list) . "]");
    my $last = $obj->FETCHSIZE;
    $obj->STORE($last + $_, $list[$_]) foreach (0..$#list);
    $obj->FETCHSIZE;
}

sub POP {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my $value = pop @tab;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    $value;
}

sub SHIFT {
    my $obj = shift;
    debug();
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my $value = shift @tab;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    $value;
}

sub UNSHIFT {
    my ($obj, @list) = @_;
    debug("list : [" . join('|', @list));
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    unshift @tab, @list;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
}

sub SPLICE {
    my $obj = shift;
    my $offset = shift || 0;
    my $length = shift || $obj->FETCHSIZE - $offset;
    my @list = @_;
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'LIST', type2 => 'RESOLV_OPTIONS' });
    my @tab = @{$obj->{libconf}->get_atom($pos[-1])->{list}};
    my @ret = splice @tab, $offset, $length, @list;
    $obj->{libconf}->edit_atom($pos[-1], { list => \@tab });
    wantarray() ? @ret : $ret[-1];
}

1;

