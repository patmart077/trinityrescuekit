#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Generic::ExportedShell;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
@ISA = qw(Libconf::Glueconf);

sub new {
    my ($class, $filename, $data_description, $data_mapping) = @_;
    my $libconf = Libconf::new('Libconf', $filename, 'exportedshell', '');
    tie my %wrapper, 'Libconf::Glueconf::Generic::ExportedShell::Wrapper', $libconf, $data_description, $data_mapping;
    bless \%wrapper, $class;
}

package Libconf::Glueconf::Generic::ExportedShell::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    bless {
           libconf => $libconf,
           _data_description => $data_description,
           _data_mapping => $data_mapping,
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUE', key => $key });
    foreach (@pos) {
        $obj->{libconf}->deleteAtom($_);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->getAtom(0);
    $atom->{key};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUE', key => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUE', key => $lastkey }); #FIXME : double entries should be removed elsewhere
    $pos[-1]+1 >= $obj->{libconf}->size and return undef;
    $obj->{libconf}->getAtom($pos[-1]+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq '' or die 'trying to store anything else than a value';
    my $index;
    my @pos = $obj->{libconf}->findAtomPos({ type => 'KEY_VALUE', key => $key });
    if (@pos == 0) {
        $index = $obj->{libconf}->appendAtom({ type => 'KEY_VALUE', key => $key, value => $value });
    } else {
        $index = $pos[-1];
    }
    $obj->{libconf}->editAtom($index, { type => 'KEY_VALUE', key => $key, value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my @pos = $obj->{libconf}->findAtomPos({ type => 'KEY_VALUE', key => $key });
    @pos == 0 and return undef;
    $obj->{libconf}->getAtom($pos[-1])->{value};
}

1;

